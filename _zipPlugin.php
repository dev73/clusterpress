<?php
/**
 * Tool to create the Plugin's Zip folder
 */

$package_info = file_get_contents( 'package.json' );
$package_info = json_decode( $package_info );

$plugin_slug = 'plugin';
if ( ! empty( $package_info->slug ) ) {
	$plugin_slug = $package_info->slug;
}

$plugin_archive_dir  = dirname( __FILE__ );
$plugin_archive_dest = dirname( $plugin_archive_dir ) . '/' . trim( $plugin_slug, '.' ) . '.zip';

if ( file_exists( $plugin_archive_dest ) ) {
	unlink( $plugin_archive_dest );
}

$skip_files = array(
	'Gruntfile.js',
	'package.json',
	'phpunit.xml.dist',
);

$skip_dirs = array(
	'.git',
	'.svn',
	'node_modules',
	'tests',
);

$plugin_files = new RecursiveIteratorIterator(
	new RecursiveDirectoryIterator( $plugin_archive_dir ),
	RecursiveIteratorIterator::LEAVES_ONLY
);

$zip = new ZipArchive;
$zip->open( $plugin_archive_dest, ZipArchive::CREATE | ZipArchive::OVERWRITE );

foreach ( $plugin_files as $name => $file ) {
	if ( $file->isDir() ) {
		continue;
	}

	// Excluded files
	if ( false !== array_search( str_replace( $plugin_archive_dir . '/', '', $name ), $skip_files )
		|| 0 === strpos( basename( $name ), '.' )
		|| 0 === strpos( basename( $name ), '_' )
	) {
		continue;
	}

	foreach ( $skip_dirs as $dir ) {
		if ( 0 === strpos( $name, $plugin_archive_dir . '/' . $dir ) ) {
			continue 2;
		}
	}

	$file_path     = $file->getRealPath();
	$relative_path = substr( $file_path, strlen( $plugin_archive_dir ) + 1 );
	$zip->addFile( $file_path, $relative_path);
}

$zip->close();

echo "Plugin Zip archive generated.\n";
