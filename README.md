ClusterPress
============

> Supercharge your WordPress publishing network and head it to the front!

+ Enjoy the four ClusterPress Clusters to highlight your contents and their authors.
+ Users will finally have their own places into the front-end of your site to manage their profile, their account and all their contents (Posts and comments).
+ You are in charge! Choose to open the access to the users directory to any visitors, restrict it to your members or to completely hide it.
+ You are using a Multisite config? Very good choice, the sites of your network are automatically added in a Sites directory you can browse using Site categories and they all get nice discovery pages (in other words a Site's Profile).
+ Spice it up! Activate the Interactions Cluster so that users can mention each others and like the sites contents.
+ Requires WordPress 4.7

Visit The [Cluster.Press](https://cluster.press) website to read more about ClusterPress.
