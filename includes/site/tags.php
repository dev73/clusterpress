<?php
/**
 * ClusterPress Site Tags.
 *
 * @package ClusterPress\site
 * @subpackage tags
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Get the global used for site object loops.
 *
 * As these loops can be used in many contexts, we are
 * using a filter so that potentially any Cluster can own
 * the loop global. By Default It's owned by the Site CLuster.
 *
 * @since 1.0.0
 *
 * @return object The Class of the Cluster holding the loop global.
 */
function cp_site_get_loop_global() {
	$cp      = clusterpress();
	$cluster = null;

	if ( isset( $cp->site ) ) {
		$cluster = $cp->site;
	}

	/**
	 * Filter here to edit the loop global owner
	 *
	 * @since 1.0.0
	 *
	 * @param object $cluster The Class of the Cluster holding the loop global.
	 */
	return apply_filters( 'cp_site_get_loop_global', $cluster );
}

/**
 * In Multisite, make sure to restore the current blog if the Posts
 * or Comments loop was displaying items from another site.
 *
 * @since 1.0.0
 */
function cp_site_restore_current_site() {
	remove_action( current_action(), 'cp_site_restore_current_site', 100 );

	restore_current_blog();
}

/** Posts Loop Template Tags *************************************************/

/**
 * Init the ClusterPress Posts loop.
 *
 * @since 1.0.0
 *
 * @param array $args {
 *   An array of arguments.
 *   @see WP_Query::parse_query() for a complete description.
 * }
 * @return bool True if Posts were found. False otherwise.
 */
function cp_site_has_posts( $args = array() ) {
	$loop_global = cp_site_get_loop_global();

	$args = wp_parse_args( $args, array( 'post_type' => 'post' ) );

	if ( empty( $args['is_widget'] ) ) {
		$args = array_merge( array(
			'author' => cp_get_displayed_user_id(),
		), cp_get_filter_var() );
	}

	/**
	 * Filter here to edit Posts loop arguments.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args The Posts loop arguments.
	 */
	$args = apply_filters( 'cp_site_has_posts_args', $args );

	if ( ! empty( $args['filter'] ) ) {
		$site_id = (int) $args['filter'];
		if ( ! cp_is_main_site( $site_id ) ) {
			switch_to_blog( $site_id );

			add_action( 'cp_post_loop_end', 'cp_site_restore_current_site', 100 );
		}
	}

	// Setup the global query loop
	$loop_global->site_posts = new CP_Site_Posts_Loop( $args );

	/**
	 * Filter here if you need to edit the has_items() result.
	 *
	 * @since  1.0.0
	 *
	 * @param  bool $value True if there are Posts to display. False otherwise.
	 */
	return apply_filters( 'cp_site_has_posts', $loop_global->site_posts->has_items() );
}

/**
 * Whether there are more Posts available in the loop to iterate on.
 *
 * @since 1.0.0
 *
 * @return WP_Post The Post Object.
 */
function cp_site_the_posts() {
	$loop_global = cp_site_get_loop_global();

	return $loop_global->site_posts->items();
}

/**
 * Load the current Post into the loop.
 *
 * @since 1.0.0
 *
 * @return WP_Post The current Post Object.
 */
function cp_site_the_post() {
	$loop_global = cp_site_get_loop_global();

	return $loop_global->site_posts->the_item();
}

/**
 * Get the current Post into the loop.
 *
 * @since 1.0.0
 *
 * @return WP_Post The current Post Object.
 */
function cp_site_get_current_post() {
	global $post;
	$loop_global = cp_site_get_loop_global();

	// Get the post to reset at the end of the loop.
	if ( ! isset( $loop_global->site_posts->reset_post ) ) {
		$loop_global->site_posts->reset_post = $post;
	}

	$post = get_post( $loop_global->site_posts->post );

	if ( ! cp_is_main_site( get_current_blog_id() ) ) {
		$post->parent_site_id = (int) get_current_blog_id();
	}

	/**
	 * Filter here to edit/add properties to the Post object.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Post $post The current Post Object.
	 */
	return apply_filters( 'cp_site_get_current_post', $post );
}

/**
 * Output the Post title.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_the_title() {
	echo cp_site_get_title();
}

	/**
	 * Get the Post title.
	 *
	 * @since 1.0.0
	 *
	 * @return string The Post title.
	 */
	function cp_site_get_title() {
		return get_the_title( cp_site_get_current_post() );
	}

/**
 * Output the Post Classes.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_the_post_class() {
	echo cp_site_get_post_class();
}

	/**
	 * Get the Post Classes.
	 *
	 * @since 1.0.0
	 *
	 * @return string A space separated list of Post classes.
	 */
	function cp_site_get_post_class() {
		$classes = get_post_class( 'cp-user-posts', cp_site_get_current_post() );

		/**
		 * Filter here to add/remove Post classes.
		 *
		 * @since 1.0.0
		 *
		 * @param string $value   A space separated list of Post classes.
		 * @param array  $classes The list of classes.
		 */
		return apply_filters( 'cp_site_get_post_class', join( ' ', $classes ), $classes );
	}

/**
 * Output the Post permalink.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_the_post_permalink() {
	echo esc_url( cp_site_get_post_permalink() );
}

	/**
	 * Get the Post permalink.
	 *
	 * @since 1.0.0
	 *
	 * @param  WP_Post $post The Post Object.
	 * @return string        The Post permalink.
	 */
	function cp_site_get_post_permalink( $post = null ) {
		if ( empty( $post->ID ) ) {
			$post = cp_site_get_current_post();
		}

		/**
		 * Filter here to edit the Post permalink.
		 *
		 * @since 1.0.0
		 *
		 * @param string $value The Post permalink.
		 */
		return apply_filters( 'cp_site_get_post_permalink', get_permalink( $post ) );
	}

/**
 * Check if the current Post has a Featured image.
 *
 * @since 1.0.0
 *
 * @return bool True if the current Post has a Featured image. False otherwise.
 */
function cp_site_has_post_thumbnail() {
	return apply_filters( 'cp_site_has_post_thumbnail', has_post_thumbnail( cp_site_get_current_post() ) );
}

/**
 * Output the Post's featured image.
 *
 * @since 1.0.0
 *
 * {@see cp_site_get_post_thumbnail() for details about parameters.}}
 * @param string|array $size Optional
 * @param string|array $attr Optional.
 * @return string HTML Output.
 */
function cp_site_the_post_thumbnail( $size = 'post-thumbnail', $attr = '' ) {
	echo cp_site_get_post_thumbnail( $size, $attr );
}

	/**
	 * Get the Post's featured image.
	 *
	 * @since 1.0.0
	 *
	 * @param string|array $size Optional. Image size to use. Accepts any valid image size, or
	 *                           an array of width and height values in pixels (in that order).
	 *                           Default 'post-thumbnail'.
	 * @param string|array $attr Optional. Query string or array of attributes. Default empty.
	 * @return string            The Post's featured image.
	 */
	function cp_site_get_post_thumbnail( $size = 'post-thumbnail', $attr = '' ) {
		return apply_filters( 'cp_site_get_post_thumbnail', sprintf( '
			<a class="post-thumbnail" href="%1$s" aria-hidden="true">
				%2$s
			</a>',
			cp_site_get_post_permalink(),
			get_the_post_thumbnail( cp_site_get_current_post(), $size, $attr )
		) );
	}

/**
 * Output the Post's excerpt.
 *
 * @since 1.0.0
 *
 * @param array $args {
 *    An array of arguments.
 *    @see cp_site_get_post_excerpt() for details about arguments.
 * }}
 * @return string HTML Output.
 */
function cp_site_the_post_excerpt( $args = array() ) {
	echo cp_site_get_post_excerpt( $args );
}

	/**
	 * Get the Post's excerpt.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args {
	 *    An array of arguments.
	 *    @type int    $length The number of words to keep in the Post Excerpt.
	 *    @type string $more   The more text to use at the end of the Post Excerpt.
	 * }}
	 * @return string The Post excerpt.
	 */
	function cp_site_get_post_excerpt( $args = array() ) {
		$post = cp_site_get_current_post();

		// Password protected
		if ( post_password_required( $post ) ) {
			$excerpt = __( 'L\'extrait n\'est pas affiché car l\'article est protégé.', 'clusterpress' );
		} else {

			$r = wp_parse_args( $args, array(
				'length' => 55,
				'more'   => ' [&hellip;]',
			) );

			/**
			 * Filter here to edit the Post content text.
			 *
			 * @since 1.0.0
			 *
			 * @param string  $text The content without shortcodes.
			 * @param WP_Post $post The Post object.
			 */
			$text = apply_filters( 'cp_site_get_post_excerpt_text', strip_shortcodes( $post->post_content ), $post );

			$text = str_replace( ']]>', ']]&gt;', $text );

			/**
			 * Filter the number of words in an excerpt.
			 */
			$excerpt_length = apply_filters( 'excerpt_length', $r['length'] );
			/**
			 * Filter the string in the "more" link displayed after a trimmed excerpt.
			 */
			$excerpt_more = apply_filters( 'excerpt_more', $r['more'] );

			$excerpt = wp_trim_words( $text, $excerpt_length, $excerpt_more );
		}

		/**
		 * Used internally to sanitize output
		 *
		 * @since 1.0.0
		 *
		 * @param string  $excerpt The Post excerpt.
		 * @param WP_Post $post    The Post object.
		 */
		return apply_filters( 'cp_site_get_post_excerpt', $excerpt, $post );
	}

/**
 * Output the Post's action Buttons.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_the_post_actions() {
	echo cp_site_get_post_actions();
}

	/**
	 * Get the Post's action Buttons.
	 *
	 * @since 1.0.0
	 *
	 * @return string The Post's action Buttons.
	 */
	function cp_site_get_post_actions() {
		$actions   = array();
		$post      = cp_site_get_current_post();
		$output    = '';
		$edit_link =  get_edit_post_link( $post );

		if ( ! empty( $edit_link ) ) {
			$actions['edit'] = array(
				'wrapper_class' => 'edit-link',
				'dashicon'      => 'dashicons-edit',
				'content'       => sprintf(
					__( '<a href="%1$s" class="post-edit-link">Modifier<span class="screen-reader-text"> "%2$s"</span></a>', 'clusterpress' ),
					$edit_link,
					cp_site_get_title()
				),
			);
		}

		if ( ! post_password_required( $post ) && ( comments_open( $post ) || get_comments_number( $post ) ) ) {
			$actions['comments'] = array(
				'wrapper_class' => 'comments-link',
				'dashicon'      => 'dashicons-admin-comments',
				'content'       => sprintf(
					__( '<a href="%1$s">%2$s<span class="screen-reader-text"> sur %3$s</span></a>', 'clusterpress' ),
					get_comments_link( $post ),
					get_comments_number_text(),
					cp_site_get_title()
				),
			);
		}

		/**
		 * Filter here to edit/add/remove Post actions.
		 *
		 * @since 1.0.0
		 *
		 * @param array $actions The list of available actions for the Pots.
		 */
		$actions = apply_filters( 'cp_site_get_post_actions', $actions );

		if ( empty( $actions ) ) {
			return;
		}

		return cp_cluster_actions_group( array(
			'class'       => 'cp-site-post-actions',
			'action_type' => 'post',
			'actions'     => $actions,
		) );
	}

/**
 * Output a user feedback to inform no posts were found.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_no_posts_found() {
	echo cp_site_get_no_posts_found();
}

	/**
	 * Get the user feedback to inform no posts were found.
	 *
	 * @since 1.0.0
	 *
	 * @return string The user feedback message.
	 */
	function cp_site_get_no_posts_found() {
		$message = __( 'Aucun article n\'a été trouvé sur le site.', 'clusterpress' );

		clusterpress()->feedbacks->add_feedback(
			'site[user-posts-empty]',
			/**
			 * Use this filter if you wish to edit the no posts message
			 *
			 * @since 1.0.0
			 *
			 * @param string $message The user feedback to inform no posts were found.
			 */
			apply_filters( 'cp_site_get_no_posts_found', $message ),
			'info'
		);

		cp_get_template_part( 'assets/feedbacks' );

		if ( is_multisite() && ms_is_switched() ) {
			restore_current_blog();
		}
	}

/**
 * Check if the Posts loop has pagination.
 *
 * @since 1.0.0
 *
 * @return bool True if the Posts loop has pagination. False otherwise.
 */
function cp_site_posts_has_pagination() {
	$loop_global = cp_site_get_loop_global();
	$loop        = $loop_global->site_posts;

	/**
	 * Filter here to enable/disable pagination.
	 *
	 * @since 1.0.0
	 *
	 * @param bool $value True if the Posts loop has pagination. False otherwise.
	 */
	return apply_filters( 'cp_site_posts_has_pagination', empty( $loop->no_pagination ) );
}

/**
 * Output the total count for the current Posts loop.
 *
 * @since 1.0.0
 *
 * @return string HTML output.
 */
function cp_site_posts_total_count() {
	echo esc_html( cp_site_get_posts_total_count() );
}
	/**
	 * Return the total count for the current Posts loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string HTML for the total count.
	 */
	function cp_site_get_posts_total_count() {
		$loop_global = cp_site_get_loop_global();
		$loop        = $loop_global->site_posts;

		$displayed = number_format_i18n( $loop->post_count );
		$total     = number_format_i18n( $loop->total_post_count );

		$total_count  = sprintf(
			_n( 'Total %1$s article, %2$s affiché.', 'Total %1$s articles, %2$s affiché(s).', $total, 'clusterpress' ),
			$total,
			$displayed
		);

		/**
		 * Filter here to edit the total Posts count to output.
		 *
		 * @since 1.0.0
		 *
		 * @param  string $total_count the total Posts count to output
		 */
		return apply_filters( 'cp_site_get_posts_total_count', $total_count );
	}

/**
 * Check if the Posts loop has pagination links.
 *
 * @since 1.0.0
 *
 * @return bool True if the Posts loop has pagination links. False otherwise.
 */
function cp_site_posts_has_pagination_links() {
	$loop_global = cp_site_get_loop_global();
	$loop        = $loop_global->site_posts;

	return ! empty( $loop->pag_links ) && cp_site_posts_has_pagination();
}

/**
 * Output the pagination links for the current Posts loop.
 *
 * @since 1.0.0
 *
 * @return string HTML output.
 */
function cp_site_posts_pagination_links() {
	echo cp_site_get_posts_pagination_links();
}

	/**
	 * Return the pagination links for the current Posts loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string output for the pagination links.
	 */
	function cp_site_get_posts_pagination_links() {
		$loop_global = cp_site_get_loop_global();
		$loop        = $loop_global->site_posts;

		/**
		 * Filter here to edit the paginate to output for posts.
		 *
		 * @since 1.0.0
		 *
		 * @param string $pag_links The pagination links to output for posts.
		 */
		return apply_filters( 'cp_site_get_posts_pagination_links', $loop->pag_links );
	}

/**
 * Once the Posts loop ended, reset WordPress globals.
 *
 * @since 1.0.0
 */
function cp_site_reset_post() {
	global $post;
	$loop_global = cp_site_get_loop_global();

	if ( isset( $loop_global->site_posts->reset_post ) ) {
		$post = $loop_global->site_posts->reset_post;
	}
}
add_action( 'cp_post_loop_end', 'cp_site_reset_post' );

/** Comments Loop Template Tags **********************************************/

/**
 * Init the ClusterPress Comments loop.
 *
 * @since 1.0.0
 *
 * @param array $args {
 *   An array of arguments.
 *   @see WP_Comment_Query::parse_query() for a complete description.
 * }
 * @return bool True if there are comments to display. False otherwise.
 */
function cp_site_has_comments( $args = array() ) {
	$loop_global = cp_site_get_loop_global();

	$args = wp_parse_args( $args, array( 'status' => 'approve' ) );

	if ( empty( $args['is_widget'] ) ) {
		if ( cp_is_user() ) {
			$args['user_id'] = cp_get_displayed_user_id();
		}

		if ( cp_user_is_self_profile() || is_super_admin() ) {
			$args['status'] = 'all';
		}

		$args = array_merge( $args, cp_get_filter_var() );
	}

	/**
	 * Filter here to edit Comments loop arguments.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args The Comments loop arguments.
	 */
	$args = apply_filters( 'cp_site_has_comments_args', $args );

	if ( ! empty( $args['filter'] ) ) {
		$site_id = (int) $args['filter'];
		if ( ! cp_is_main_site( $site_id ) ) {
			switch_to_blog( $site_id );

			add_action( 'cp_comment_loop_end', 'cp_site_restore_current_site', 100 );
		}
	}

	// Setup the global query loop
	$loop_global->site_comments = new CP_Site_Comments_Loop( $args );

	/**
	 * Filter here if you need to edit the has_items() result.
	 *
	 * @since  1.0.0
	 *
	 * @param  bool $value True if there are Comments to display. False otherwise.
	 */
	return apply_filters( 'cp_site_has_comments', $loop_global->site_comments->has_items() );
}

/**
 * Whether there are more Comments available in the loop to iterate on.
 *
 * @since 1.0.0
 *
 * @return WP_Comment The Comment Object.
 */
function cp_site_the_comments() {
	$loop_global = cp_site_get_loop_global();

	return $loop_global->site_comments->items();
}

/**
 * Load the current Comment into the loop.
 *
 * @since 1.0.0
 *
 * @return WP_Comment The current Comment Object.
 */
function cp_site_the_comment() {
	$loop_global = cp_site_get_loop_global();

	return $loop_global->site_comments->the_item();
}

/**
 * Get the current Comment into the loop.
 *
 * @since 1.0.0
 *
 * @return WP_Comment The current Comment Object.
 */
function cp_site_get_current_comment() {
	global $comment;
	$loop_global = cp_site_get_loop_global();

	// Get the comment to reset at the end of the loop.
	if ( ! isset( $loop_global->site_comments->reset_comment ) ) {
		$loop_global->site_comments->reset_comment = 'null';
	}

	$comment = get_comment( $loop_global->site_comments->comment );

	if ( ! cp_is_main_site( get_current_blog_id() ) ) {
		$comment->parent_site_id = (int) get_current_blog_id();
	}

	/**
	 * Filter here to edit/add properties to the Comment object.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Comment $post The current Comment Object.
	 */
	return apply_filters( 'cp_site_get_current_comment', $comment );
}

/**
 * Output the current Comment ID.
 *
 * @since 1.0.0
 *
 * @return string HTML output.
 */
function cp_site_the_comment_ID() {
	echo esc_attr( cp_site_get_comment_ID() );
}

	/**
	 * Get the current Comment ID.
	 *
	 * @since 1.0.0
	 *
	 * @return int The comment ID.
	 */
	function cp_site_get_comment_ID() {
		/**
		 * Filter here to edit the comment ID.
		 *
		 * @since 1.0.0
		 *
		 * @param int $comment_ID The Comment ID.
		 */
		return apply_filters( 'cp_site_get_comment_ID', cp_site_get_current_comment()->comment_ID );
	}

/**
 * Output the current parent post ID.
 *
 * @since 1.0.0
 *
 * @return string HTML output.
 */
function cp_site_the_comment_post_ID() {
	echo esc_attr( cp_site_get_comment_post_ID() );
}

	/**
	 * Get the current parent post ID.
	 *
	 * @since 1.0.0
	 *
	 * @return int The parent post ID.
	 */
	function cp_site_get_comment_post_ID() {
		/**
		 * Filter here to edit the parent post ID.
		 *
		 * @since 1.0.0
		 *
		 * @param int $comment_post_ID The parent post ID.
		 */
		return apply_filters( 'cp_site_get_comment_post_ID', cp_site_get_current_comment()->comment_post_ID );
	}

/**
 * Output the current comment classes.
 *
 * @since 1.0.0
 *
 * @return string HTML output.
 */
function cp_site_the_comment_classes() {
	echo join( ' ', cp_site_get_comment_classes() );
}

	/**
	 * Get the current comment classes.
	 *
	 * @since 1.0.0
	 *
	 * @return string The current comment classes.
	 */
	function cp_site_get_comment_classes() {
		$comment_ID = cp_site_get_comment_ID();
		$post_ID    = cp_site_get_comment_post_ID();

		$comment_classes = get_comment_class( 'hentry cp-user-comment-' . $comment_ID, $comment_ID, $post_ID );

		/**
		 * Filter here to edit the current comment classes.
		 *
		 * @since 1.0.0
		 *
		 * @param array $comment_classes The current comment classes.
		 * @param int   $comment_ID      The Comment ID.
		 * @param int   $post_ID         The parent post ID.
		 */
		return apply_filters( 'cp_site_get_comment_classes', $comment_classes, $comment_ID, $post_ID );
	}

/**
 * Output the current comment date.
 *
 * @since 1.0.0
 *
 * @return string HTML output.
 */
function cp_site_the_comment_date() {
	echo cp_site_get_comment_date();
}

	/**
	 * Get the current comment date.
	 *
	 * @since 1.0.0
	 *
	 * @return string The current comment date.
	 */
	function cp_site_get_comment_date() {
		$comment      = cp_site_get_current_comment();
		$comment_date = get_comment_date( '', $comment );
		$comment_time = get_comment_time();

		if ( ! empty( $comment->comment_parent ) ) {
			$date = __( 'A répondu à un commentaire le %1$s à %2$s', 'clusterpress' );
		} else {
			$date = __( 'A posté un commentaire le %1$s à %2$s', 'clusterpress' );
		}

		/**
		 * Filter here to edit the current comment date.
		 *
		 * @since 1.0.0
		 *
		 * @param string     $value        The current comment date.
		 * @param string     $comment_date The date for the comment.
		 * @param string     $comment_time The time for the comment.
		 * @param WP_Comment $comment      The Comment object.
		 */
		return apply_filters( 'cp_site_get_comment_date', sprintf( $date, $comment_date, $comment_time ), $comment_date, $comment_time, $comment );
	}

/**
 * Check if the comment is still in the moderation queue.
 *
 * @since 1.0.0
 *
 * @return bool True if the comment is still in the moderation queue. False otherwise.
 */
function cp_site_the_comment_is_awaiting_moderation() {
	return 0 === (int) cp_site_get_current_comment()->comment_approved;
}

/**
 * Output the current comment excerpt.
 *
 * @since 1.0.0
 *
 * @return string HTML output.
 */
function cp_site_the_comment_excerpt() {
	echo cp_site_get_comment_excerpt();
}

	/**
	 * Get the current comment excerpt.
	 *
	 * @since 1.0.0
	 *
	 * @return string The current comment excerpt.
	 */
	function cp_site_get_comment_excerpt() {
		$comment = cp_site_get_current_comment();
		$excerpt = get_comment_excerpt( $comment );

		/**
		 * Filter here to edit the current comment excerpt.
		 *
		 * @since 1.0.0
		 *
		 * @param string       $excerpt The current comment excerpt.
		 * @param WP_Comment   $comment The Comment object.
		 */
		return apply_filters( 'cp_site_get_comment_excerpt', $excerpt, $comment );
	}

/**
 * Output the current comment action buttons.
 *
 * @since 1.0.0
 *
 * @return string HTML output.
 */
function cp_site_the_comment_actions() {
	echo cp_site_get_comment_actions();
}

	/**
	 * Get the current comment action buttons.
	 *
	 * @since 1.0.0
	 *
	 * @return string The current comment action buttons.
	 */
	function cp_site_get_comment_actions() {
		$comment      = cp_site_get_current_comment();
		$actions      = array();
		$edit_link    = get_edit_comment_link( $comment );
		$awaiting_mod = cp_site_the_comment_is_awaiting_moderation();

		if ( ! empty( $edit_link ) ) {
			$actions['edit'] = array(
				'wrapper_class' => 'edit-link',
				'dashicon'      => 'dashicons-edit',
				'content'       => sprintf(
					__( '<a href="%1$s" class="comment-edit-link">Modifier</a>', 'clusterpress' ),
					$edit_link
				),
			);
		}

		if ( ! $awaiting_mod ) {
			$actions['view_link'] = array(
				'wrapper_class' => 'view-link',
				'dashicon'      => 'dashicons-visibility',
				'content'       => sprintf(
					__( '<a href="%s" class="comment-view-link">Voir commentaire</a>', 'clusterpress' ),
					get_comment_link( $comment )
				),
			);
		}

		/**
		 * Filter here to edit the current comment actions.
		 *
		 * @since 1.0.0
		 *
		 * @param array $actions      The current comment actions.
		 * @param bool  $awaiting_mod True if the comment is still in the moderation queue. False otherwise.
		 */
		$actions = apply_filters( 'cp_site_get_comment_actions', $actions, $awaiting_mod );

		if ( empty( $actions ) ) {
			return;
		}

		return cp_cluster_actions_group( array(
			'class'       => 'cp-site-comment-actions',
			'action_type' => 'comment',
			'actions'     => $actions,
		) );
	}

/**
 * Output a user feedback to inform no comments were found.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_no_comments_found() {
	echo cp_site_get_no_comments_found();
}

	/**
	 * Get a user feedback to inform no comments were found.
	 *
	 * @since 1.0.0
	 *
	 * @return string The user feedback to inform no comments were found.
	 */
	function cp_site_get_no_comments_found() {
		$message = __( 'Aucun commentaire n\'a été trouvé sur ce site.', 'clusterpress' );

		clusterpress()->feedbacks->add_feedback(
			'site[user-comments-empty]',
			/**
			 * Use this filter if you wish to edit the no comments message
			 *
			 * @since 1.0.0
			 *
			 * @param string $message The user feedback to inform no comments were found.
			 */
			apply_filters( 'cp_site_get_no_comments_found', $message ),
			'info'
		);

		cp_get_template_part( 'assets/feedbacks' );

		if ( is_multisite() && ms_is_switched() ) {
			restore_current_blog();
		}
	}

/**
 * Check if the Comments loop has pagination.
 *
 * @since 1.0.0
 *
 * @return bool True if the Comments loop has pagination. False otherwise.
 */
function cp_site_comments_has_pagination() {
	$loop_global = cp_site_get_loop_global();
	$loop        = $loop_global->site_comments;

	/**
	 * Filter here to enable/disable pagination.
	 *
	 * @since 1.0.0
	 *
	 * @param bool $value True if the Comments loop has pagination. False otherwise.
	 */
	return apply_filters( 'cp_site_comments_has_pagination', empty( $loop->no_pagination ) );
}

/**
 * Output the total count for the Comments loop.
 *
 * @since 1.0.0
 *
 * @retun string HTML Output.
 */
function cp_site_comments_total_count() {
	echo esc_html( cp_site_get_comments_total_count() );
}

	/**
	 * Return the total count for the Comments loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string HTML for the total comments count.
	 */
	function cp_site_get_comments_total_count() {
		$loop_global = cp_site_get_loop_global();
		$loop        = $loop_global->site_comments;

		$displayed = number_format_i18n( $loop->comment_count );
		$total     = number_format_i18n( $loop->total_comment_count );

		$total_count  = sprintf(
			_n( 'Total %1$s commentaire, %2$s affiché.', 'Total %1$s commentaires, %2$s affiché(s).', $total, 'clusterpress' ),
			$total,
			$displayed
		);

		/**
		 * Filter here to edit the total comments count.
		 *
		 * @since 1.0.0
		 *
		 * @param  string $total_count the total count to output
		 */
		return apply_filters( 'cp_site_get_comments_total_count', $total_count );
	}

/**
 * Check if the Comments loop has pagination links.
 *
 * @since 1.0.0
 *
 * @return bool True if the Comments loop has pagination links. False otherwise.
 */
function cp_site_comments_has_pagination_links() {
	$loop_global = cp_site_get_loop_global();
	$loop        = $loop_global->site_comments;

	return ! empty( $loop->pag_links ) && cp_site_comments_has_pagination();
}

/**
 * Output the pagination links for the Comments loop.
 *
 * @since 1.0.0
 *
 * @retun string HTML Output.
 */
function cp_site_comments_pagination_links() {
	echo cp_site_get_comments_pagination_links();
}

	/**
	 * Return the pagination links for the Comments loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string output for the pagination links.
	 */
	function cp_site_get_comments_pagination_links() {
		$loop_global = cp_site_get_loop_global();
		$loop        = $loop_global->site_comments;

		/**
		 * Filter here to edit the pagination links of the comments loop.
		 *
		 * @since 1.0.0
		 *
		 * @param  string $value the pagination links to output
		 */
		return apply_filters( 'cp_site_get_comments_pagination_links', $loop->pag_links );
	}

/**
 * Once the Comments loop ended, reset WordPress globals.
 *
 * @since 1.0.0
 */
function cp_site_reset_comment() {
	global $comment;
	$loop_global = cp_site_get_loop_global();

	if ( isset( $loop_global->site_comments->reset_comment ) ) {
		$comment = null;
	}
}
add_action( 'cp_comment_loop_end', 'cp_site_reset_comment' );
