<?php
/**
 * ClusterPress Site meta functions.
 *
 * @package ClusterPress\site\multisite
 * @subpackage meta
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Get the list of site meta keys ClusterPress is using.
 *
 * @since  1.0.0
 *
 * @return array The list of meta keys keyed according to their corresponding blog meta.
 */
function cp_sites_get_sitemeta_keys() {
	return apply_filters( 'cp_sites_get_sitemeta_keys', clusterpress()->site->meta_keys );
}

/**
 * Registers a site's meta key.
 *
 * @since 1.0.0
 *
 * @param string $meta_key       Meta key to register.
 * @param array  $args {
 *     @see register_meta() for the description of WordPress default argument.
 *
 *     ClusterPress will use 2 other arguments to let you define the manage_display_callback (in sites discover settings page)
 *     and the admin_display_callback (for the Network site's admin screen).
 *
 *     @type string $manage_display_callback Optional. A function or method to call when outputing the html for the meta_key in front-end.
 *     @type string $admin_display_callback  Optional. A function or method to call when outputing the html for the meta_key in back-end.
 * }
 *
 * @return bool True if the meta key was successfully registered in the global array, false if not.
 *                       Registering a meta key with distinct sanitize and auth callbacks will fire those
 *                       callbacks, but will not add to the global registry.
 */
function cp_sites_register_meta( $meta_key = '', $args = array() ) {
	if ( empty( $meta_key ) || registered_meta_key_exists( 'blog', $meta_key ) ) {
		return false;
	}

	$r = wp_parse_args( $args, array(
		'manage_display_callback' => '', // Use false to disallow
		'admin_display_callback'  => '', // Use false to disallow
	) );

	if ( false === $r['manage_display_callback'] ) {
		unset( $r['manage_display_callback'] );
	} elseif ( ! $r['manage_display_callback'] || ! is_callable( $r['manage_display_callback'] ) ) {
		$r['manage_display_callback'] = 'cp_sites_manage_display';
	}

	if ( false === $r['admin_display_callback'] ) {
		unset( $r['admin_display_callback'] );
	} elseif ( ! $r['admin_display_callback'] || ! is_callable( $r['admin_display_callback'] ) ) {
		$r['admin_display_callback'] = 'cp_sites_admin_display';
	}

	// Mirror, as it is not possible to store our own args in WordPress global meta
	if ( register_meta( 'blog', $meta_key, $r ) ) {
		clusterpress()->site->meta_screens[ $meta_key ] = array_intersect_key( $r, array(
			'manage_display_callback' => true,
			'admin_display_callback'  => true,
		) );

		if ( ! empty( $r['_builtin'] ) ) {
			clusterpress()->site->meta_screens[ $meta_key ]['_builtin'] = true;
		}
	}
}

/**
 * Sanitize the Site name and description site meta
 *
 * @since  1.0.0
 *
 * @param  string $value    The value to be sanitized.
 * @param  string $meta_key The correspondin meta key (name or description)
 * @return string           The sanitized site info.
 */
function cp_sites_sanitize_site_info( $value = '', $meta_key = '' ) {
	global $wpdb;
	$original_value = $value;

	if ( 'public' === $meta_key ) {
		return (int) $value;
	} elseif ( 'icon' === $meta_key ) {
		return esc_url_raw( $value );
	}

	// There's no WordPress blogmeta table for now but it can change!
	if ( isset( $wpdb->blogmeta ) ) {
		$reset_blogmeta = $wpdb->blogmeta;
	}

	// Use the ClusterPress metadata table
	$wpdb->blogmeta = $wpdb->base_prefix . 'cp_blogmeta';

	$value = $wpdb->strip_invalid_text_for_column( $wpdb->blogmeta, 'meta_value', $value );
	if ( $value !== $original_value ) {
		$value = $wpdb->strip_invalid_text_for_column( $wpdb->blogmeta, 'meta_value', wp_encode_emoji( $original_value ) );
	}

	if ( is_wp_error( $value ) ) {
		$value = '';
	} else {
		$value = esc_html( $value );
	}

	if ( ! empty( $reset_blogmeta ) ) {
		$wpdb->blogmeta = $reset_blogmeta;
	}

	/**
	 * Filters the sanitize site info.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value    The sanitized site info (name or description).
	 * @param string $meta_key The corresponding meta key.
	 */
	return apply_filters( 'cp_sites_sanitize_site_info', $value, $meta_key );
}

/**
 * Register the site meta that need to be edited in front-end.
 *
 * @since  1.0.0
 */
function cp_sites_register_default_metas() {
	clusterpress()->site->meta_screens = array();

	$descriptions = array(
		'blog_public'     => __( 'Ne pas permettre aux moteurs de recherche d\'indexer ce site', 'clusterpress' ),
		'blogdescription' => __( 'Slogan pour le site', 'clusterpress' ),
		'blogname'        => __( 'Titre du site', 'clusterpress' ),
		'site_icon'       => __( 'Icône du site', 'clusterpress' ),
	);

	$restricted_meta_keys = array();

	$meta_keys = array_intersect_key( cp_sites_get_sitemeta_keys(), $descriptions );

	foreach ( $meta_keys as $site_meta => $meta_key ) {
		$type = 'string';

		if ( 'site_icon' === $site_meta ) {
			$type = 'url';
		} elseif ( 'blog_public' === $site_meta ) {
			$type = 'bool';
		}

		cp_sites_register_meta( $meta_key, array(
			'type'                    => $type,
			'description'             => $descriptions[ $site_meta ],
			'single'                  => true,
			'sanitize_callback'       => 'cp_sites_sanitize_site_info',
			'manage_display_callback' => 'cp_sites_' . $meta_key . '_manage_display',
			'admin_display_callback'  => false,
			'_builtin'                => true,
		) );

		$restricted_meta_keys[ $meta_key ] = true;
	}

	/**
	 * Use this action to register your site metas
	 *
	 * @since  1.0.0
	 *
	 * @param  array $restricted_meta_keys The meta keys restricted to the Site cluster.
	 */
	do_action( 'cp_sites_register_metas', $restricted_meta_keys );
}

/**
 * Delete a metadata from the DB for a site.
 *
 * @since  1.0.0
 *
 * @global object $wpdb WordPress database access object.
 *
 * @param int         $blog_id    ID of the blog whose metadata is being deleted.
 * @param string|bool $meta_key   Optional. The key of the metadata being deleted. If
 *                                omitted, all BP metadata associated with the blog will
 *                                be deleted.
 * @param string|bool $meta_value Optional. If present, the metadata will only be
 *                                deleted if the meta_value matches this parameter.
 * @param bool        $delete_all Optional. If true, delete matching metadata entries for
 *                                all objects, ignoring the specified blog_id. Otherwise, only
 *                                delete matching metadata entries for the specified blog.
 *                                Default: false.
 * @return bool True on success, false on failure.
 */
function cp_sites_delete_meta( $blog_id, $meta_key = false, $meta_value = false, $delete_all = false ) {
	global $wpdb;

	// There's no WordPress blogmeta table for now but it can change!
	if ( isset( $wpdb->blogmeta ) ) {
		$reset_blogmeta = $wpdb->blogmeta;
	}

	// Use the ClusterPress metadata table
	$wpdb->blogmeta = $wpdb->base_prefix . 'cp_blogmeta';

	$retval = delete_metadata( 'blog', $blog_id, $meta_key, $meta_value, $delete_all );

	if ( ! empty( $reset_blogmeta ) ) {
		$wpdb->blogmeta = $reset_blogmeta;
	}

	return $retval;
}

/**
 * Get metadata for a given site.
 *
 * @since 1.0.0
 *
 * @global object $wpdb WordPress database access object.
 *
 * @param int    $blog_id  ID of the blog whose metadata is being requested.
 * @param string $meta_key Optional. If present, only the metadata matching
 *                         that meta key will be returned. Otherwise, all
 *                         metadata for the blog will be fetched.
 * @param bool   $single   Optional. If true, return only the first value of the
 *                         specified meta_key. This parameter has no effect if
 *                         meta_key is not specified. Default: false.
 * @return mixed The meta value(s) being requested.
 */
function cp_sites_get_meta( $blog_id, $meta_key = '', $single = false ) {
	global $wpdb;

	// There's no WordPress blogmeta table for now but it can change!
	if ( isset( $wpdb->blogmeta ) ) {
		$reset_blogmeta = $wpdb->blogmeta;
	}

	// Use the ClusterPress metadata table
	$wpdb->blogmeta = $wpdb->base_prefix . 'cp_blogmeta';

	$retval = get_metadata( 'blog', $blog_id, $meta_key, $single );

	if ( ! empty( $reset_blogmeta ) ) {
		$wpdb->blogmeta = $reset_blogmeta;
	}

	return $retval;
}

/**
 * Update a piece of site meta.
 *
 * @since 1.0.0
 *
 * @global object $wpdb WordPress database access object.
 *
 * @param int    $blog_id    ID of the blog whose metadata is being updated.
 * @param string $meta_key   Key of the metadata being updated.
 * @param mixed  $meta_value Value to be set.
 * @param mixed  $prev_value Optional. If specified, only update existing
 *                           metadata entries with the specified value.
 *                           Otherwise, update all entries.
 * @return bool|int Returns false on failure. On successful update of existing
 *                  metadata, returns true. On successful creation of new metadata,
 *                  returns the integer ID of the new metadata row.
 */
function cp_sites_update_meta( $blog_id, $meta_key, $meta_value, $prev_value = '' ) {
	global $wpdb;

	// There's no WordPress blogmeta table for now but it can change!
	if ( isset( $wpdb->blogmeta ) ) {
		$reset_blogmeta = $wpdb->blogmeta;
	}

	// Use the ClusterPress metadata table
	$wpdb->blogmeta = $wpdb->base_prefix . 'cp_blogmeta';

	$retval = update_metadata( 'blog', $blog_id, $meta_key, $meta_value, $prev_value );

	if ( ! empty( $reset_blogmeta ) ) {
		$wpdb->blogmeta = $reset_blogmeta;
	}

	return $retval;
}

/**
 * Add a piece of blog metadata.
 *
 * @since 1.0.0
 *
 * @param int    $blog_id    ID of the blog.
 * @param string $meta_key   Metadata key.
 * @param mixed  $meta_value Metadata value.
 * @param bool   $unique     Optional. Whether to enforce a single metadata value
 *                           for the given key. If true, and the object already has a value for
 *                           the key, no change will be made. Default: false.
 * @return int|bool The meta ID on successful update, false on failure.
 */
function cp_sites_add_meta( $blog_id, $meta_key, $meta_value, $unique = false ) {
	global $wpdb;

	// There's no WordPress blogmeta table for now but it can change!
	if ( isset( $wpdb->blogmeta ) ) {
		$reset_blogmeta = $wpdb->blogmeta;
	}

	// Use the ClusterPress metadata table
	$wpdb->blogmeta = $wpdb->base_prefix . 'cp_blogmeta';

	$retval = add_metadata( 'blog', $blog_id, $meta_key, $meta_value, $unique );

	if ( ! empty( $reset_blogmeta ) ) {
		$wpdb->blogmeta = $reset_blogmeta;
	}

	return $retval;
}

/**
 * Get the site IDs present into the Site Metas Table
 *
 * @since  1.0.0
 *
 * @return array The list of site IDs.
 */
function cp_sites_meta_get_sites() {
	global $wpdb;

	// There's no WordPress blogmeta table for now but it can change!
	if ( isset( $wpdb->blogmeta ) ) {
		$reset_blogmeta = $wpdb->blogmeta;
	}

	// Use the ClusterPress metadata table
	$wpdb->blogmeta = $wpdb->base_prefix . 'cp_blogmeta';

	$sites = $wpdb->get_col( "SELECT DISTINCT blog_id FROM {$wpdb->blogmeta} WHERE meta_key = 'name'" );

	if ( ! empty( $reset_blogmeta ) ) {
		$wpdb->blogmeta = $reset_blogmeta;
	}

	return (array) $sites;
}

/**
 * Get a list of values for a given site meta for a list of sites.
 *
 * @since  1.0.0
 *
 * @param  array  $ids      The list of site IDs.
 * @param  string $meta_key The meta key to get values for.
 * @return array            The list of meta values for the list of sites.
 */
function cp_sites_get_meta_for_sites_list( $ids = array(), $meta_key = '' ) {
	global $wpdb;

	if ( empty( $ids ) || empty( $meta_key ) ) {
		return false;
	}

	// There's no WordPress blogmeta table for now but it can change!
	if ( isset( $wpdb->blogmeta ) ) {
		$reset_blogmeta = $wpdb->blogmeta;
	}

	// Use the ClusterPress metadata table
	$wpdb->blogmeta = $wpdb->base_prefix . 'cp_blogmeta';

	$in = join( ', ', array_filter( wp_parse_id_list( $ids ) ) );

	$metas = $wpdb->get_results( "SELECT blog_id, meta_value FROM {$wpdb->blogmeta} WHERE meta_key = 'name' AND blog_id IN ({$in})", OBJECT_K );

	if ( ! empty( $reset_blogmeta ) ) {
		$wpdb->blogmeta = $reset_blogmeta;
	}

	return (array) $metas;
}

/**
 * Listen to Blog Meta changes and synchronize the Site Meta accordingly.
 *
 * @since  1.0.0
 *
 * @param  mixed $old_value  The previous value for the meta key
 * @param  mixed $value      The new value for the meta key
 * @param  string $meta_key  The meta key the value is attached to.
 */
function cp_sites_sync_meta( $old_value = null, $value = null, $meta_key = '' ) {
	$meta_keys = cp_sites_get_sitemeta_keys();

	if ( isset( $meta_keys[ $meta_key ] ) ) {
		$site_id = get_current_blog_id();

		if ( 'site_icon' === $meta_key ) {
			$value = get_site_icon_url( 150 );
		}

		if ( ! $value && 'blog_public' !== $meta_key ) {
			cp_sites_delete_meta( $site_id, $meta_keys[ $meta_key ] );
		} else {
			cp_sites_update_meta( $site_id, $meta_keys[ $meta_key ], $value );
		}

		do_action( 'cp_sites_sync_meta', $site_id, $meta_keys[ $meta_key ], $value );
	}
}

/**
 * Removes all metadata and user associations.
 *
 * @since  1.0.0
 *
 * @param  int $number  The number the query offset (not used)
 * @param  int $blog_id The Site id to reset the site metadata for.
 * @return int 1.
 */
function cp_sites_reset_site_metadata( $number = 0, $blog_id = 0 ) {
	if ( ! empty( $blog_id ) ) {
		$sites = array( $blog_id );

	} else {
		$sites      = get_sites( array( 'fields' => 'ids' ) );
		$meta_sites = cp_sites_meta_get_sites();

		// Make sure to get all Site ids.
		$sites = array_merge( $sites, $meta_sites );
	}

	$meta_keys = cp_sites_get_sitemeta_keys();

	foreach ( $sites as $site_id ) {
		// First delete all meta
		foreach ( $meta_keys as $meta_key ) {
			$key = sanitize_key( $meta_key );
			cp_sites_delete_meta( $site_id, $key, false, empty( $blog_id ) );
		}

		// Then delete all user associations having a level > 0
		cp_sites_remove_user( false, $site_id, 'all' );
	}

	return 1;
}

/**
 * Remove all ClusterPress metadata for the given site.
 *
 * @since  1.0.0
 *
 * @param  int $site_id The site ID.
 * @return int          1.
 */
function cp_sites_remove_site_metadata( $site_id = 0 ) {
	if ( empty( $site_id ) ) {
		return false;
	}

	return cp_sites_reset_site_metadata( 1, $site_id );
}

/**
 * Returns 1, to remove all meta in 1 step only.
 *
 * @since  1.0.0
 *
 * @return int 1.
 */
function cp_sites_reset_site_metadata_get_count() {
	return 1;
}

/**
 * Restore/Init Site metas and user associations for all existing sites.
 *
 * @since  1.0.0
 *
 * @param  int $number  The number of sites to restore/init.
 * @param  int $site_id The Site to restore
 * @return int         The number of sites restored/inited.
 */
function cp_sites_save_site_metadata( $number = 0, $site_id = 0 ) {
	if ( ! empty( $site_id ) ) {
		$site  = get_site( $site_id );

		if ( ! is_a( $site, 'WP_Site' ) ) {
			return false;
		}

		$sites = array( $site->blog_id );

	} else {
		$site__not_in = cp_sites_meta_get_sites();

		if ( empty( $number ) ) {
			$sites = get_sites( array( 'fields' => 'ids', 'site__not_in' => $site__not_in ) );
		} else {
			$sites = get_sites( array( 'fields' => 'ids', 'number' => $number, 'site__not_in' => $site__not_in ) );
		}
	}

	$updated = 0;

	foreach ( $sites as $site_id ) {
		$site_id = (int) $site_id;

		$is_main_site = cp_is_main_site( $site_id );

		if ( ! $is_main_site ) {
			switch_to_blog( $site_id );
		}

		$users = cp_get_users( array(
			'count_total' => false,
			'blog_id'     => $site_id,
		) );

		$user_level  = false;
		$admins      = array();
		$user_levels = wp_list_pluck( cp_sites_get_user_levels(), 'user_level', 'capability' );

		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
				foreach ( array_reverse( $user_levels ) as $lk => $lv ) {
					if ( user_can( $user->ID, $lk ) ) {
						$user_level = $lv;

						if ( $lk === 'manage_options' ) {
							$admins[] = $user->ID;
						}
					}
				}

				if ( ! empty( $user_level ) ) {
					cp_sites_add_user( $user->ID, $site_id, $user_level );
				}
			}
		}

		// Add the site
		cp_sites_add_site( $site_id, $admins );

		if ( ! $is_main_site ) {
			restore_current_blog();
		}

		$updated += 1;
	}

	return $updated;
}

/**
 * Restore all ClusterPress metadatas for the given site.
 *
 * @since  1.0.0
 *
 * @param  int $site_id The site ID.
 * @return false|int    The number of sites restored on success. False otherwise.
 */
function cp_sites_restore_site_metadata( $site_id = 0 ) {
	if ( empty( $site_id ) ) {
		return false;
	}

	return cp_sites_save_site_metadata( 1, $site_id );
}

/**
 * Gets the sites count for the network.
 *
 * @since  1.0.0
 *
 * @return int The sites count.
 */
function cp_sites_save_site_metadata_get_count() {
	$network_id = get_main_network_id();

	return (int) get_network_option( (int) $network_id, 'blog_count', 0 );
}
