<?php
/**
 * ClusterPress Multisite Actions.
 *
 * @package ClusterPress\site\multisite
 * @subpackage actions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Register the user's sites section.
 *
 * @since 1.0.0
 */
function cp_site_mu_register_user_section() {
	// Register the Following user modules
	cp_register_cluster_section( 'user', array(
		'id'            => 'sites',
		'name'          => __( 'Sites', 'clusterpress' ),
		'cluster_id'    => 'user',
		'slug'          => cp_sites_get_user_slug(),
		'rewrite_id'    => cp_sites_get_user_rewrite_id(),
		'template'      => '',
		'template_dir'  => '',
		'toolbar_items' => array(
			'position'   => 70,
			'dashicon'   => 'dashicons-welcome-view-site',
			'capability' => 'cp_read_single_user',
		),
	) );
}
add_action( 'cp_register_cluster_sections', 'cp_site_mu_register_user_section', 11 );

/**
 * Handle Front-end site settings edits.
 *
 * @since 1.0.0
 */
function cp_site_edit_settings_action() {
	if ( ! cp_is_site_manage_settings() ) {
		return;
	}

	if ( empty( $_POST['cp_site_settings_submit'] ) ) {
		return;
	}

	check_admin_referer( 'cp_site_settings' );

	$site = cp_displayed_site();
	if ( ! current_user_can( 'cp_edit_single_site', array( 'site' => $site ) ) || empty( $site->blog_id ) ) {
		cp_feedback_redirect( 'not-allowed-01', 'error' );
	}

	/**
	 * Hook here to perform custom action before Site Categories are updated.
	 *
	 * @since 1.0.0
	 *
	 * @param int $blog_id The site ID.
	 */
	do_action( 'cp_site_before_edit_categories', $site->blog_id );

	$updated     = true;
	$taxonomy_id = cp_sites_get_taxonomy();

	if ( isset( $_POST['tax_input'][ $taxonomy_id ] ) ) {
		$category = get_taxonomy( $taxonomy_id );

		if ( ! current_user_can( $category->cap->assign_terms ) ) {
			cp_feedback_redirect( 'not-allowed-02', 'error' );
		}

		$categories = array();

		if ( ! empty( $_POST['tax_input'][ $taxonomy_id ] ) ) {
			$categories = wp_parse_id_list( $_POST['tax_input'][ $taxonomy_id ] );
		}

		// Remove the null value only if there's a non null selected.
		if ( count( $categories ) > 1 ) {
			$categories = array_filter( $categories );
		}

		$updated = wp_set_object_terms( $site->blog_id, $categories, $taxonomy_id );
	}

	/**
	 * Hook here to perform custom action before Site Categories are updated.
	 *
	 * @since 1.0.0
	 *
	 * @param array|WP_Error $updated Term taxonomy IDs of the affected terms.
	 * @param int            $blog_id The site ID.
	 */
	do_action( 'cp_site_after_edit_categories', $updated, $site->blog_id );

	// Somthing went wrong, stop here and inform the user.
	if ( is_wp_error( $updated ) ) {
		cp_feedback_redirect( 'settings-02', 'error' );
	}

	if ( ! empty( $_POST['cp_site_settings'] ) ) {
		$settings_data = (array) $_POST['cp_site_settings'];
		$meta_keys     = get_registered_meta_keys( 'blog' );
		$cp_meta_keys  = cp_sites_get_sitemeta_keys();

		if ( ! array_diff_key( $meta_keys, $settings_data ) ) {
			cp_feedback_redirect( 'settings-02', 'error' );
		}

		$cp_metas = wp_list_filter( clusterpress()->site->meta_screens, array( '_builtin' => true ) );

		if ( $cp_metas ) {
			$meta_keys = array_diff_key( $meta_keys, $cp_metas );

			/**
			 * As we're going to use our Blog Meta > Site Meta syncing,
			 * let's switch!
			 */
			switch_to_blog( $site->blog_id );

			foreach ( array_keys( $cp_metas ) as $meta_key ) {
				if ( ! isset( $settings_data[ $meta_key ] ) ) {
					continue;
				}

				$option = array_search( $meta_key, $cp_meta_keys );

				if ( false === $option ) {
					continue;
				} else {
					$option = trim( $option );
				}

				$option_value = $settings_data[ $meta_key ];

				if ( ! is_array( $option_value ) ) {
					$option_value = trim( $option_value );
				}

				$option_value = wp_unslash( $option_value );

				// Update!
				update_option( $option, $option_value );
			}

			restore_current_blog();
		}

		if ( ! empty( $meta_keys ) ) {
			/**
			 * Hook here to perform custom actions before updating custom site metas.
			 *
			 * @since 1.0.0
			 *
			 * @param int   $blog_id       The site ID.
			 * @param array $meta_keys     The list of registered meta keys.
			 * @param array $settings_data The list of submitted ClusterPress Site's settings.
			 */
			do_action( 'cp_site_before_edit_custom_metas', $site->blog_id, $meta_keys, $settings_data );

			foreach ( array_keys( $meta_keys ) as $m_key ) {

				// If it's a ClusterPress site settings, move to next meta key.
				if ( ! isset( $settings_data[ $m_key ] ) || true !== $meta_keys[ $m_key ]['single'] ) {
					continue;
				}

				// Update!
				cp_sites_update_meta( $site->blog_id, $m_key, wp_unslash( $settings_data[ $m_key ] ) );
			}

			/**
			 * Hook here to perform custom actions after updating custom site metas.
			 *
			 * @since 1.0.0
			 *
			 * @param int   $blog_id       The site ID.
			 * @param array $meta_keys     The list of registered meta keys.
			 * @param array $settings_data The list of submitted ClusterPress Site's settings.
			 */
			do_action( 'cp_site_after_edit_custom_metas', $site->blog_id, $meta_keys, $settings_data );
		}

		// Redirect with a success message.
		cp_feedback_redirect( 'settings-01', 'updated' );
	}
}
add_action( 'cp_page_actions', 'cp_site_edit_settings_action' );

/**
 * Handle Front-end site members/followers edits.
 *
 * @since 1.0.0
 */
function cp_site_edit_members_action() {
	if ( ! cp_is_site_manage_members() && ! cp_is_site_manage_followers() ) {
		return;
	}

	if ( empty( $_POST['cp-user']['users'] ) || empty( $_POST['cp-user']['action'] ) ) {
		return;
	}

	check_admin_referer( 'cp_user_actions' );

	$site        = cp_displayed_site();
	$users       = wp_parse_id_list( $_POST['cp-user']['users'] );
	$action      = sanitize_key( $_POST['cp-user']['action'] );
	$is_follower = false;

	if ( false !== array_search( get_current_user_id(), $users ) ) {
		cp_feedback_redirect( 'site-member-06', 'error', $redirect );
	}

	if ( cp_is_site_manage_followers() ) {
		$redirect = trailingslashit( cp_site_get_url( array(
			'rewrite_id'    => cp_site_get_manage_rewrite_id(),
			'slug'          => cp_site_get_manage_slug() . '/' .  cp_site_get_followers_slug(),
		), $site ) );

		$is_follower = true;
	} else {
		$redirect = trailingslashit( cp_site_get_url( array(
			'rewrite_id'    => cp_site_get_manage_rewrite_id(),
			'slug'          => cp_site_get_manage_slug() . '/' .  cp_site_get_members_slug(),
		), $site ) );
	}

	if ( ! current_user_can( 'cp_edit_single_site', array( 'site' => $site ) ) || empty( $site->blog_id ) ) {
		cp_feedback_redirect( 'not-allowed-03', 'error', $redirect );
	}

	/**
	 * Hook here to perform actions before members are edited.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Site $site        The Site Object passed by reference.
	 * @param array   $users       The list of user IDs to edit.
	 * @param bool    $is_follower True if users are followers. False otherwise.
	 *
	 */
	do_action_ref_array( 'cp_site_before_edit_members', array( &$site, $users, $is_follower ) );

	// Get role/user level map
	$role_map = cp_sites_get_user_levels();

	// Let's switch to do some additional cap checkings
	switch_to_blog( $site->blog_id );

	if ( ( 'demote' === $action || 'remove' === $action ) && ! current_user_can( 'remove_users' ) ) {
		cp_feedback_redirect( 'not-allowed-03', 'error', $redirect );
	}

	if ( isset( $role_map[ $action ]['user_level'] ) && ! current_user_can( 'promote_users' ) ) {
		cp_feedback_redirect( 'not-allowed-03', 'error', $redirect );
	}

	if ( 'demote' === $action ) {
		$feedback = 'site-follower-01';

		if ( count( $users ) > 1 ) {
			$feedback = 'site-follower-02';
		}

	} elseif ( 'remove' === $action ) {
		$feedback = 'site-member-02';

		if ( count( $users ) > 1 ) {
			$feedback = 'site-member-05';
		}

	} elseif ( isset( $role_map[ $action ]['user_level'] ) ) {
		$role     = $action;
		$feedback = 'site-member-01';

		if ( count( $users ) > 1 ) {
			$feedback = 'site-member-04';
		}
	} else {
		cp_feedback_redirect( 'site-member-03', 'error', $redirect );
	}

	foreach ( $users as $user_id ) {
		// Promote action
		if ( ! empty( $role ) ) {
			// It's a follower
			if ( $is_follower ) {
				add_user_to_blog( $site->blog_id, $user_id, $role );

			// The user is already a member of the site
			} else {
				$u = get_user_by( 'id', $user_id );
				$u->set_role( $role );
			}

		// Demote and remove actions
		} else {
			if ( ! $is_follower ) {
				remove_user_from_blog( $user_id, $site->blog_id );
			} else {
				cp_sites_remove_user( $user_id, $site->blog_id, 0 );
			}

			// Demote is only creating a link with the site (follower).
			if ( 'demote' === $action ) {
				cp_sites_add_user( $user_id, $site->blog_id );
			}
		}
	}

	if ( ms_is_switched() ) {
		restore_current_blog();
	}

	/**
	 * Hook here to perform actions after members were edited.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Site $site        The Site Object passed by reference.
	 * @param array   $users       The list of user IDs to edit.
	 * @param bool    $is_follower True if users are followers. False otherwise.
	 *
	 */
	do_action( 'cp_site_after_edit_members', $site, $users, $is_follower );

	// Finally redirect the admin
	cp_feedback_redirect( $feedback, 'updated', $redirect );
}
add_action( 'cp_page_actions', 'cp_site_edit_members_action' );

/**
 * Clean the Sites cache.
 *
 * @since 1.0.0
 *
 * @param int $site_id The Site ID.
 */
function cp_site_reset_cache( $site_id = 0 ) {
	if ( empty( $site_id ) ) {
		return;
	}

	$site = wp_cache_get( $site_id, 'cp_site_ids' );

	if ( empty( $site->blog_id ) ) {
		return;
	}

	$path_or_domain = $site->path;
	if ( is_subdomain_install() ) {
		$path_or_domain = $site->domain;
	}

	wp_cache_delete( $site->blog_id,  'cp_site_ids'            );
	wp_cache_delete( $path_or_domain, 'cp_site_path_or_domain' );
}
add_action( 'refresh_blog_details',            'cp_site_reset_cache', 10, 1 );
add_action( 'cp_sites_add_user_site',          'cp_site_reset_cache', 10, 1 );
add_action( 'cp_sites_sync_meta',              'cp_site_reset_cache', 10, 1 );
add_action( 'cp_site_after_edit_custom_metas', 'cp_site_reset_cache', 10, 1 );
add_action( 'cp_sites_remove_admin_from_site', 'cp_site_reset_cache', 10, 1 );
