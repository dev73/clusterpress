<?php
/**
 * Tags specific to the Site following feature.
 *
 * @package ClusterPress\site\multisite\following-sites
 * @subpackage tags
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Register scripts for the Site's Following feature.
 *
 * @since  1.0.0
 *
 * @param  string $url     The URL to the JS plugin's dir.
 * @param  string $min     Whether to use the minified version of JS files.
 * @param  string $version Plugin's versoin
 */
function cp_site_register_scripts( $url = '', $min = '', $version = '' ) {

	wp_register_script(
		'clusterpress-sites-following',
		sprintf( '%1$ssites-following%2$s.js', $url, $min ),
		array( 'clusterpress-ajax'),
		$version,
		true
	);
}
add_action( 'cp_register_scripts', 'cp_site_register_scripts', 10, 3 );

/**
 * Enqueue the Site's following script if needed.
 *
 * @since  1.0.0
 */
function cp_site_enqueue_cssjs() {
	if ( ! cp_is_site() && ! cp_is_sites() && ! cp_is_user_manage_followed_sites() && ! cp_is_user_followed_sites() ) {
		return;
	}

	wp_enqueue_script( 'clusterpress-sites-following' );
}
add_action( 'cp_enqueue_cssjs_clusterpress', 'cp_site_enqueue_cssjs' );

/**
 * Get the inline style to format the bubble displaying the number
 * of latest posts of the followed sites.
 *
 * @since  1.0.0
 *
 * @return string CSS Output.
 */
function cp_sites_get_toolbar_style() {
	/**
	 * Filter here to override the bubble inline style.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value The bubble inline style.
	 */
	return apply_filters( 'cp_sites_get_toolbar_style', '
		#cp-cp_user_following a {
			position: relative;
		}

		ul.full #cp-cp_user_following a {
			position: inherit;
		}

		#wp-admin-bar-clusterpress-user-bar li span.pending-count,
		#cp-cp_user_following a span.pending-count {
			display: inline-block;
			background-color: #d54e21;
			color: #fff;
			font-size: 9px;
			line-height: 17px;
			font-weight: 600;
			margin: -1px 0 0 2px;
			vertical-align: text-bottom;
			-webkit-border-radius: 10px;
			border-radius: 10px;
			z-index: 26;
			padding: 0 6px;
		}

		#cp-cp_user_following a span.pending-count {
			position: absolute;
			bottom: 2px;
			right: 4px;
		}

		ul.full #cp-cp_user_following a span.pending-count {
			position: inherit;
		}
	' );
}

/**
 * Add an inline style to format the bubble displaying the number
 * of latest posts of the followed sites.
 *
 * @since  1.0.0
 */
function cp_sites_toolbar_style() {
	wp_add_inline_style( 'admin-bar', cp_sites_get_toolbar_style() );
}
add_action( 'cp_enqueue_scripts',       'cp_sites_toolbar_style', 14 );
add_action( 'cp_admin_enqueue_scripts', 'cp_sites_toolbar_style', 14 );

/**
 * Adapt the Users loop to list the followers of a displayed site.
 *
 * @since 1.0.0
 *
 * @param $args The Users loop args {
 *    An array of arguments.
 *    @see WP_User_Query::prepare_query() for a detailed description.
 * }}
 * @return array The Users loop args.
 */
function cp_site_single_followers_args( $args = array() ) {
	$site_followers = cp_sites_get_site_users( cp_get_displayed_site_id(), 'followers' );

	if ( empty( $site_followers ) ) {
		$site_followers = 0;
	}

	$args['include'] = $site_followers;

	return $args;
}

/**
 * Temporarly add a filter on Users loop args.
 *
 * @since 1.0.0
 */
function cp_site_start_followers_loop() {
	add_filter( 'cp_user_has_users_args', 'cp_site_single_followers_args', 10, 1 );
}

/**
 * Remove the temporary filter on Users loop args.
 *
 * @since 1.0.0
 */
function cp_site_end_followers_loop() {
	remove_filter( 'cp_user_has_users_args', 'cp_site_single_followers_args', 10, 1 );
}

/**
 * Output the Title of the displayed site's followers page.
 *
 * @since 1.0.0
 */
function cp_site_followers_loop_title() {
	esc_html_e( 'Adeptes', 'clusterpress' );
}

/**
 * Adapt the Sites loop to list the sites a displayed user is following.
 *
 * @since 1.0.0
 *
 * @param $args The Sites loop args {
 *    An array of arguments.
 *    @see CP_Site_Query::__construct() for a detailed description.
 * }}
 * @return array The Sites loop args.
 */
function cp_site_user_followed_sites_args( $args = array() ) {
	return array_merge( $args, array(
		'user_id'    => cp_get_displayed_user_id(),
		'user_level' => 0,
	) );

	return $args;
}

/**
 * Temporarly add a filter on Sites loop args.
 *
 * @since 1.0.0
 */
function cp_site_start_followed_sites_loop() {
	add_filter( 'cp_site_has_sites_args', 'cp_site_user_followed_sites_args', 10, 1 );
}

/**
 * Remove the temporary filter on Sites loop args.
 *
 * @since 1.0.0
 */
function cp_site_end_followed_sites_loop() {
	remove_filter( 'cp_site_has_sites_args', 'cp_site_user_followed_sites_args', 10, 1 );
}

/**
 * Output the Title of the displayed user's followed sites latest posts page.
 *
 * @since 1.0.0
 */
function cp_user_followed_sites_loop_title() {
	esc_html_e( 'Derniers articles du site suivi', 'clusterpress' );
}

/**
 * Init the loop to list the latest content of the sites a displayed user is following.
 *
 * @since 1.0.0
 *
 * @return bool True if there are content relationships to display. False otherwise.
 */
function cp_sites_followed_has_posts() {
	$loop_global = cp_site_get_loop_global();

	/**
	 * Filter here to edit the Site Followed loop arguments.
	 *
	 * @since 1.0.0
	 *
	 * @param array $value The Site Followed loop arguments.
	 */
	$args = apply_filters( 'cp_sites_followed_has_posts_args', array(
		'user_id' => cp_get_displayed_user_id(),
	) );

	// Setup the global query loop
	$loop_global->follower_site = new CP_Sites_Followed_Post_Loop( $args );

	/**
	 * Filter here if you need to edit the has_items() result.
	 *
	 * @since  1.0.0
	 *
	 * @param  bool $value True if there are posts to display. False otherwise.
	 */
	return apply_filters( 'cp_sites_followed_has_posts', $loop_global->follower_site->has_items() );
}

/**
 * Whether there are more followed sites latest content available in the loop to iterate on.
 *
 * @since 1.0.0
 *
 * @return object The latest content relationship Object.
 */
function cp_sites_followed_the_posts() {
	$loop_global = cp_site_get_loop_global();

	return $loop_global->follower_site->items();
}

/**
 * Load the current content relationship into the loop.
 *
 * @since 1.0.0
 *
 * @return object The current content relationship Object.
 */
function cp_sites_followed_the_post() {
	$loop_global = cp_site_get_loop_global();

	return $loop_global->follower_site->the_item();
}

/**
 * Get the current content relationship of the loop.
 *
 * @since 1.0.0
 *
 * @return object The current content relationship Object.
 */
function cp_sites_followed_get_current_post() {
	$loop_global = cp_site_get_loop_global();

	if ( empty( $loop_global->follower_site->post ) ) {
		return false;
	}

	/**
	 * Filter here to edit the current content relationship of the loop.
	 *
	 * @since 1.0.0
	 *
	 * @param object $mention The current content relationship of the loop.
	 */
	return apply_filters( 'cp_sites_followed_get_current_post', $loop_global->follower_site->post );
}

/**
 * Check if the displayed user has unread posts for his followed sites.
 *
 * @since 1.0.0
 *
 * @return bool True if the displayed user has unread posts for his followed sites. False otherwise.
 */
function cp_sites_followed_has_unread_post() {
	$unread = cp_sites_get_user_followed_sites_new_posts( cp_get_displayed_user_id() );
	return ! empty( $unread );
}

/**
 * Output a button to mark all unread posts as read.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_sites_followed_read_all_button() {
	echo cp_sites_followed_get_read_all_button();
}

	/**
	 * Get a button to mark all unread posts as read.
	 *
	 * @since 1.0.0
	 *
	 * @return string The HTML for the button to mark all unread posts as read.
	 */
	function cp_sites_followed_get_read_all_button() {
		$user_link = cp_user_get_url( array(
			'rewrite_id' => cp_site_get_user_following_rewrite_id(),
			'slug'       => cp_site_get_user_following_slug(),
		), cp_displayed_user() );

		$user_link = wp_nonce_url( add_query_arg( 'readall', 1, $user_link ), 'cp_sites_followed_bulk_read' );

		return cp_cluster_actions_group( array(
			'class'       => 'cp-sites-followed-actions',
			'action_type' => 'post',
			'actions'     => array(
				array(
					'wrapper_class' => 'bulk-read',
					'dashicon'      => 'dashicons-yes',
					'content'       => sprintf(
						__( '<a href="%s" class="sites-followed-bulk-read">Tout Marquer comme lu.</a>', 'clusterpress' ),
						$user_link
					),
				),
			),
		) );
	}

/**
 * Output content relationship meta infos (posted on).
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_sites_followed_the_post_meta() {
	echo cp_sites_followed_get_post_meta();
}

	/**
	 * Get content relationship meta infos.
	 *
	 * @since 1.0.0
	 *
	 * @return string The content relationship meta infos.
	 */
	function cp_sites_followed_get_post_meta() {
		$cp   = clusterpress();
		$post = cp_sites_followed_get_current_post();

		if ( ! cp_user_relationships_isset() || empty( $post->name ) ) {
			return false;
		}

		$relationship = $cp->user->relationships->get_relationship_object( $post->name );

		if ( empty( $relationship->format_callback ) ) {
			return false;
		}

		$ago = '';
		if ( ! empty( $post->date ) ) {
			$ago = sprintf( __( 'il y a %s', 'clusterpress'), human_time_diff( strtotime( $post->date ) ) );
		}

		$meta = '';
		if ( ! empty( $relationship->format_callback ) ) {
			$meta = sprintf( $relationship->format_callback,
				sprintf( '<a href="%1$s">%2$s</a>',
					esc_url( $post->site_url ),
					esc_html( $post->site_name )
				),
				$ago
			);
		}

		/**
		 * Filter here to edit the content relationship meta infos.
		 *
		 * @since 1.0.0
		 *
		 * @param string $meta The content relationship meta infos.
		 * @param object $post The content relationship object.
		 */
		return apply_filters( 'cp_sites_followed_get_post_meta', $meta, $post );
	}

/**
 * Output a placeholoder for the Post's real content so
 * that our Rest API requests fill it during page load.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_sites_followed_the_post_content() {
	echo cp_sites_followed_get_post_content();
}

	/**
	 * Get the placeholoder for the Post's real content.
	 *
	 * @since 1.0.0
	 *
	 * @return string HTML Output.
	 */
	function cp_sites_followed_get_post_content() {
		$post = cp_sites_followed_get_current_post();

		if ( ! $post ) {
			return;
		}

		$endpoint = '';
		if ( ! empty( $post->id ) ) {
			$endpoint = sprintf( ' data-cp-site-followed-endpoint="%1$s/%2$s/%3$s/%4$s"',
				esc_attr( cp_user_get_slug() ),
				esc_attr( cp_site_get_follow_action_slug() ),
				esc_attr( cp_get_displayed_user_id() ),
				esc_attr( $post->id )
			);
		}

		/**
		 * Filter here to edit the HTML of the post placeholder
		 *
		 * @since 1.0.0
		 *
		 * @param string $value The output Post contnent.
		 * @param object $post  The content relationship object.
		 *
		 */
		return apply_filters( 'cp_sites_followed_get_post_content', sprintf( '<div class="post-content"%s></div>', $endpoint ), $post );
	}

/**
 * Output the current content relationship Action buttons
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_sites_followed_the_post_actions() {
	echo cp_sites_followed_get_post_actions();
}

	/**
	 * Get the current content relationship Action buttons
	 *
	 * @since 1.0.0
	 *
	 * @return string The action buttons.
	 */
	function cp_sites_followed_get_post_actions() {
		$actions   = array();
		$post      = cp_sites_followed_get_current_post();
		$output    = '';

		$user_link = cp_user_get_url( array(
			'rewrite_id' => cp_site_get_user_following_rewrite_id(),
			'slug'       => cp_site_get_user_following_slug(),
		), cp_displayed_user() );

		if ( ! empty( $post->id ) && ! empty( $post->name ) ) {

			$actions = array(
				'view' => array(
					'wrapper_class' => 'view-link',
					'dashicon'      => 'dashicons-visibility',
					'content'       => sprintf(
						__( '<a href="%s" class="followed-site-view-link">Voir l\'article</a>', 'clusterpress' ),
						wp_nonce_url( add_query_arg( 'redirectto', $post->id, $user_link ), 'cp_view_followed_site_post' )
					),
				),
				'read' => array(
					'wrapper_class' => 'read-link',
					'dashicon'      => 'dashicons-yes',
					'content'       => sprintf(
						__( '<a href="%s" class="followed-site-read-link">Marquer comme lu</a>', 'clusterpress' ),
						wp_nonce_url( add_query_arg( 'read', $post->id, $user_link ), 'cp_read_followed_site_post' )
					),
				),
			);
		}

		/**
		 * Use this filter if you wish to add custom actions to the current content relationship.
		 *
		 * @since 1.0.0
		 *
		 * @param array  $actions The list of actions available for the content relationship.
		 */
		$actions = apply_filters( 'cp_sites_followed_get_post_actions', $actions );

		if ( empty( $actions ) ) {
			return;
		}

		return cp_cluster_actions_group( array(
			'class'       => 'cp-sites-followed-actions',
			'action_type' => 'post',
			'actions'     => $actions,
		) );
	}

/**
 * Output a user feedback to inform no content relationships were found.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_sites_followed_no_posts_found() {
	echo cp_sites_followed_get_no_posts_found();
}

	/**
	 * Get the user feedback to inform no content relationships were found.
	 *
	 * @since 1.0.0
	 *
	 * @return string HTML Output.
	 */
	function cp_sites_followed_get_no_posts_found() {
		$message = __( 'Aucun article n\'a été trouvé.', 'clusterpress' );

		clusterpress()->feedbacks->add_feedback(
			'follower[posts-empty]',
			apply_filters( 'cp_sites_followed_get_no_posts_found', $message ),
			'info'
		);

		cp_get_template_part( 'assets/feedbacks' );
	}

/**
 * Output the total count for the current latest posts of the followed sites loop.
 *
 * @since 1.0.0
 *
 * @return string HTML for the total count.
 */
function cp_sites_followed_posts_total_count() {
	echo esc_html( cp_sites_followed_posts_get_total_count() );
}
	/**
	 * Return the total count for the current latest posts of the followed sites loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string HTML for the total count.
	 */
	function cp_sites_followed_posts_get_total_count() {
		$loop_global = cp_site_get_loop_global();
		$loop        = $loop_global->follower_site;

		$displayed = number_format_i18n( $loop->post_count );
		$total     = number_format_i18n( $loop->total_post_count );

		$total_count  = sprintf(
			_n( 'Total %1$s article récent, %2$s affiché.', 'Total %1$s articles récents, %2$s affichés.', $total, 'clusterpress' ),
			$total,
			$displayed
		);

		/**
		 * Filter here to edit the total count.
		 *
		 * @since 1.0.0
		 *
		 * @param  string $total_count the total count to output
		 */
		return apply_filters( 'cp_sites_followed_posts_get_total_count', $total_count );
	}

/**
 * Check if there are latest posts for the sites the user is following before adding pagination links.
 *
 * @since 1.0.0
 *
 * @retun bool True if there are pagination links. False otherwise.
 */
function cp_sites_followed_posts_has_pagination_links() {
	$loop_global = cp_site_get_loop_global();
	$loop        = $loop_global->follower_site;

	return ! empty( $loop->pag_links );
}

/**
 * Output the pagination links for the current latest posts of the followed sites loop.
 *
 * @since 1.0.0
 *
 * @return string HTML for the pagination links.
 */
function cp_sites_followed_posts_pagination_links() {
	echo cp_sites_followed_posts_get_pagination_links();
}

	/**
	 * Return the pagination links for the current latest posts of the followed sites loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string output for the pagination links.
	 */
	function cp_sites_followed_posts_get_pagination_links() {
		$loop_global = cp_site_get_loop_global();
		$loop        = $loop_global->follower_site;

		/**
		 * Filter here to edit the pagination links.
		 *
		 * @since 1.0.0
		 *
		 * @param  string the pagination links to output
		 */
		return apply_filters( 'cp_sites_followed_posts_get_pagination_links', $loop->pag_links );
	}
