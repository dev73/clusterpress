<?php
/**
 * ClusterPress Multisite Tags.
 *
 * @package ClusterPress\site\multisite
 * @subpackage tags
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/** Sites Archive Template Tags ***********************************************/

/**
 * This is not used, but it's an example of filter to
 * add more order options to the sites archive.
 *
 * You can activate it using:
 * add_filter( 'cp_order_get_options', 'cp_site_archive_orders' );
 *
 * By default, let's keep it simple!
 *
 * @since 1.0.0
 *
 * @param  array $options The order options
 * @return array          The order options
 */
function cp_site_archive_orders( $options = array() ) {
	if ( ! cp_is_sites() ) {
		return $options;
	}

	return array(
		array(
			'value' => 'name',
			'text'  => __( 'Nom', 'clusterpress' ),
		),
		array(
			'value' => 'last_updated_DESC',
			'text'  => __( 'Dernièrement mis à jour', 'clusterpress' ),
		),
	);
}

/**
 * Init the Sites archive loop.
 *
 * @since  1.0.0
 *
 * @return bool True if Sites were found. False otherwise.
 */
function cp_site_has_sites() {
	$loop_global = cp_site_get_loop_global();

	/**
	 * Filter here to edit the Sites Loop Args.
	 *
	 * @since  1.0.0
	 *
	 * @param  array $value An array of arguments for the Sites Loop {
	 *     @see CP_Site_Query::__construct() for a detailed description of available arguments.
	 * }}
	 * @return  bool True if there are Sites to display. False otherwise.
	 */
	$args = apply_filters( 'cp_site_has_sites_args', array_merge( array(
		'category' => cp_get_current_term_id(),
		'orderby'  => 'name',
		'order'    => 'ASC',
	), cp_get_filter_var() ) );

	// Setup the global query loop
	$loop_global->network_sites = new CP_Sites_Loop( $args );

	/**
	 * Filter here if you need to edit the has_items() result.
	 *
	 * @since  1.0.0
	 *
	 * @param  bool $value True if there are Sites to display. False otherwise.
	 */
	return apply_filters( 'cp_site_has_sites', $loop_global->network_sites->has_items() );
}

/**
 * Whether there are more sites available in the loop to iterate on.
 *
 * @since 1.0.0
 *
 * @return WP_Site The site Object.
 */
function cp_site_the_sites() {
	$loop_global = cp_site_get_loop_global();

	return $loop_global->network_sites->items();
}

/**
 * Load the current site into the loop.
 *
 * @since 1.0.0
 *
 * @return WP_Site The current site Object.
 */
function cp_site_the_site() {
	$loop_global = cp_site_get_loop_global();

	return $loop_global->network_sites->the_item();
}

/**
 * Get the current site of the loop.
 *
 * @since 1.0.0
 *
 * @return WP_Site The current site Object.
 */
function cp_site_get_current_site( $site = null ) {
	if ( ! $site ) {
		$loop_global = cp_site_get_loop_global();

		$site = get_site( $loop_global->network_sites->site );
	}

	/**
	 * Filter here to edit/add properties to the site object.
	 *
	 * @since  1.0.0
	 *
	 * @param  WP_Site The current site Object.
	 */
	return apply_filters( 'cp_site_get_current_site', $site );
}

/**
 * Get the current site ID of the loop.
 *
 * @since 1.0.0
 *
 * @param  WP_Site $site The site object.
 * @return int           The current site ID.
 */
function cp_site_get_current_site_id( $site = null ) {
	if ( ! $site ) {
		$site = cp_site_get_current_site();
	}

	$id = 0;
	if ( isset( $site->blog_id ) ) {
		$id = (int) $site->blog_id;
	}

	/**
	 * Filter here to edit the site ID.
	 *
	 * @since  1.0.0
	 *
	 * @param  int     The current site ID.
	 * @param  WP_Site The current site Object.
	 */
	return apply_filters( 'cp_site_get_current_site_id', $id, $site );
}

/**
 * Get the displayed category in the Sites archive.
 *
 * @since  1.0.0
 *
 * @return WP_Term The displayed category object.
 */
function cp_sites_get_current_category() {
	$cp = clusterpress();

	if ( ! isset( $cp->site->site_category ) ) {
		$category_object = wp_list_filter( clusterpress()->site->categories, array( 'term_id' => cp_get_current_term_id() ) );

		$category                = reset( $category_object );
		$cp->site->site_category = get_term( $category, cp_sites_get_taxonomy() );
	}

	return $cp->site->site_category;
}

/**
 * Output the Sites category title.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_sites_category_the_title() {
	echo cp_sites_category_get_title();
}

	/**
	 * Get the Sites category title.
	 *
	 * @since  1.0.0
	 *
	 * @return string the Sites category title.
	 */
	function cp_sites_category_get_title() {
		$category = cp_sites_get_current_category();

		if ( empty( $category->name ) ) {
			return '';
		}

		// Apply WordPress filters.
		$category_title = apply_filters( 'single_cat_title', sprintf(
			__( 'Catégorie : %s', 'clusterpress' ),
			$category->name
		) );

		/**
		 * Filter here to edit the Sites category title.
		 *
		 * @since  1.0.0
		 *
		 * @param string  $category_title The Sites category title.
		 * @param WP_Term $category       The Sites category object.
		 */
		return apply_filters( 'cp_sites_category_get_title', $category_title, $category );
	}

/**
 * Check if the Sites category has a description.
 *
 * @since  1.0.0
 *
 * @return bool True if the Sites category has a description. False otherwise.
 */
function cp_sites_category_has_description() {
	$category = cp_sites_get_current_category();

	return ! empty( $category->description );
}

/**
 * Output the Sites category description.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_sites_category_the_description() {
	echo cp_sites_category_get_description();
}

	/**
	 * Get the Sites category description.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Sites category description.
	 */
	function cp_sites_category_get_description() {
		$category = cp_sites_get_current_category();

		if ( empty( $category->description ) ) {
			return '';
		}

		// Apply WordPress filters.
		$category_description = apply_filters( 'term_description', $category->description );

		/**
		 * Filter here to edit the Sites category description.
		 *
		 * @since  1.0.0
		 *
		 * @param string  $category_description The Sites category description.
		 * @param WP_Term $category             The Sites category object.
		 */
		return apply_filters( 'cp_sites_category_get_description', $category_description, $category );
	}

/**
 * Output the Site's discover link.
 *
 * @since  1.0.0
 *
 * @param  WP_Site $site The site object.
 * @return string        HTML Output.
 */
function cp_site_the_site_discover_link( $site = null ) {
	echo esc_url( cp_site_get_site_discover_link( $site ) );
}

	/**
	 * Get the Site's discover link.
	 *
	 * @since  1.0.0
	 *
	 * @param  WP_Site $site The site object.
	 * @return string        The Site's discover link.
	 */
	function cp_site_get_site_discover_link( $site = null ) {
		if ( ! $site ) {
			$site = cp_site_get_current_site();
		}

		$url = '#';

		if ( empty( $site->path ) ) {
			return $url;
		}

		/**
		 * Filter here to edit the Site's discover link.
		 *
		 * @since  1.0.0
		 *
		 * @param  string  $value The Site's discover link.
		 * @param  WP_Site $site  The site object.
		 */
		return apply_filters( 'cp_site_get_site_discover_link', cp_site_get_url( array(), $site ), $site );
	}

/**
 * Output the Site's link.
 *
 * @since  1.0.0
 *
 * @param  WP_Site $site The site object.
 * @return string        HTML Output.
 */
function cp_site_the_site_link( $site = null ) {
	echo esc_url( cp_site_get_site_link( $site ) );
}

	/**
	 * Get the Site's link.
	 *
	 * @since  1.0.0
	 *
	 * @param  WP_Site $site The site object.
	 * @return string        The Site's link.
	 */
	function cp_site_get_site_link( $site = null ) {
		if ( ! $site ) {
			$site = cp_site_get_current_site();
		}

		$url = '#';
		if ( ! empty( $site->url ) ) {
			$url = trailingslashit( $site->url );
		}

		/**
		 * Filter here to edit the Site's link.
		 *
		 * @since  1.0.0
		 *
		 * @param  string  $value The Site's link.
		 * @param  WP_Site $site  The site object.
		 */
		return apply_filters( 'cp_site_get_site_link', $url, $site );
	}

/**
 * Output the Site icon.
 *
 * @since  1.0.0
 *
 * @param  array  $args {
 *     An array of arguments.
 *     @see cp_site_get_site_icon() for a detailed description.
 * }
 * @return string HTML Output.
 */
function cp_site_the_site_icon( $args = array() ) {
	echo cp_site_get_site_icon( $args );
}

	/**
	 * Get the Site icon.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $args {
	 *     An array of arguments.
	 *     @type array $classes The list of classes to add to the img tag.
	 *     @type int   $width   The width in pixels of the Site icon. Defaults to 80.
	 *     @type int   $height  The height in pixels of the Site icon. Defaults to 80.
	 * }
	 * @param WP_Site $site The Site object.
	 * @return string HTML for the Site icon.
	 */
	function cp_site_get_site_icon( $args = array(), $site = null ) {
		if ( ! $site ) {
			$site = cp_site_get_current_site();
		}

		$r = wp_parse_args( $args, array(
			'classes'  => array(),
			'width'    => 80,
			'height'   => 80,
		) );

		$icon_url = '';
		$classes  = array_merge( (array) $r['classes'], array( 'avatar' ) );

		if ( ! empty( $site->icon ) ) {
			$icon_url = $site->icon;
		} else {
			$icon_url = admin_url( 'images/wordpress-logo-white.svg' );
			$classes[] = 'no-icon';
		}

		$class = join( ' ', array_map( 'sanitize_html_class', $classes ) );

		/**
		 * Filter here to edit the HTML for the Site icon.
		 *
		 * @since  1.0.0
		 *
		 * @param string  $icon_url The site icon url.
		 * @param array   $classes  The list of classes to add to the img tag.
		 * @param WP_Site $site     The Site object.
		 */
		return apply_filters( 'cp_site_get_site_icon', sprintf(
			'<img src="%1$s" class="%2$s" width="%3$spx" height="%4$spx">',
			esc_url( $icon_url ),
			$class,
			(int) $r['width'],
			(int) $r['height']
		), $icon_url, $classes, $site );
	}

/**
 * Output the Site's name.
 *
 * @since  1.0.0
 *
 * @param  WP_Site $site The site object.
 * @return string        HTML Output.
 */
function cp_site_the_site_name( $site = null ) {
	echo esc_html( cp_site_get_site_name( $site ) );
}

	/**
	 * Get the Site's name.
	 *
	 * @since  1.0.0
	 *
	 * @param  WP_Site $site The site object.
	 * @return string        The Site's name.
	 */
	function cp_site_get_site_name( $site = null ) {
		if ( ! $site ) {
			$site = cp_site_get_current_site();
		}

		$name = '';
		if ( ! empty( $site->name ) ) {
			$name = $site->name;
		}

		/**
		 * Filter here to edit the Site's name
		 *
		 * @since  1.0.0
		 *
		 * @param  string $name The Site's name.
		 * @param  WP_Site $site The site object.
		 */
		return apply_filters( 'cp_site_get_site_name', $name, $site );
	}

/**
 * Output the Site's last updated info.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_the_site_last_updated() {
	echo esc_html( cp_site_get_site_last_updated() );
}

	/**
	 * Get the Site's last updated info.
	 *
	 * @since  1.0.0
	 *
	 * @param  WP_Site $site The site object.
	 * @return string        the Site's last updated info.
	 */
	function cp_site_get_site_last_updated( $site = null ) {
		if ( ! $site ) {
			$site = cp_site_get_current_site();
		}

		if ( empty( $site->last_updated ) || '0000-00-00 00:00:00' === $site->last_updated ) {
			return __( 'Non mis à jour récemment', 'clusterpress' );
		}

		return sprintf( __( 'Mis à jour il y a %s', 'clusterpress' ), human_time_diff( strtotime( $site->last_updated ) ) );
	}

/**
 * Check if a site is assigned to one or more categories.
 *
 * @since  1.0.0
 *
 * @return bool True if a site is assigned to one or more categories. False otherwise.
 */
function cp_site_has_categories() {
	$site = cp_site_get_current_site();

	return ! empty( $site->categories );
}

/**
 * Output the Site's category list.
 *
 * @since  1.0.0
 *
 * @param  WP_Site $site The site object.
 * @return string        HTML Output.
 */
function cp_site_the_category_list( $site = null ) {
	echo cp_site_get_category_list( $site );
}

	/**
	 * Get the Site's category list.
	 *
	 * @since  1.0.0
	 *
	 * @param  WP_Site $site The site object.
	 * @return string        The Site's category list.
	 */
	function cp_site_get_category_list( $site = null ) {
		if ( ! $site ) {
			$site = cp_site_get_current_site();
		}

		$categories_list = cp_sites_get_site_category_list( $site );

		if ( ! $categories_list ) {
			return;
		}

		$category_links = wp_list_pluck( $categories_list, 'link' );

		/**
		 * Filter here to edit the Site's category list
		 *
		 * @since  1.0.0
		 *
		 * @param  string  $value          The Site's category list.
		 * @param  array   $category_links List of site category links.
		 * @param  WP_Site $site           The site object.
		 */
		return apply_filters( 'cp_site_get_category_list', sprintf( '<span class="cp-category-links"><span class="screen-reader-text">%1$s </span>',
			esc_html( _x( 'Catégories', 'Used before category names.', 'clusterpress' ) )
		) . join( ', ', $category_links ) . '</span>', $categories_list, $site );
	}

/**
 * Output the Site's action buttons.
 *
 * @since  1.0.0
 *
 * @param  WP_Site $site The site object.
 * @return string        HTML Output.
 */
function cp_site_the_site_actions( $site = null ) {
	echo cp_site_get_site_actions( $site );
}

	/**
	 * Get the Site's action buttons.
	 *
	 * @since  1.0.0
	 *
	 * @param  WP_Site $site The site object.
	 * @return string        The Site's action buttons.
	 */
	function cp_site_get_site_actions( $site = null ) {
		$actions   = array();
		$output    = '';
		$site_name = esc_html( cp_site_get_site_name( $site ) );

		$actions['visit'] = array(
			'wrapper_class' => 'visit-link button',
			'dashicon'      => 'dashicons-migrate',
			'content'       => sprintf(
				__( '<a href="%1$s" class="site-visit-link">Visiter<span class="screen-reader-text"> %2$s</span></a>', 'clusterpress' ),
				esc_url( cp_site_get_site_link( $site ) ),
				$site_name
			),
		);

		if ( ! cp_is_site() ) {
			$actions['discover'] = array(
				'wrapper_class' => 'discover-link button',
				'dashicon'      => 'dashicons-welcome-view-site',
				'content'       => sprintf(
					__( '<a href="%1$s" class="site-discover-link">Découvrir<span class="screen-reader-text"> "%2$s"</span></a>', 'clusterpress' ),
					esc_url( cp_site_get_site_discover_link( $site ) ),
					$site_name
				),
			);
		} elseif ( current_user_can( 'cp_edit_single_site', array( 'site' => cp_site_get_current_site( $site ) ) ) ) {
			$site_id    = cp_site_get_current_site_id( $site );
			$admin_link = get_admin_url( $site_id );

			$actions['admin'] = array(
				'wrapper_class' => 'admin-link button',
				'dashicon'      => 'dashicons-wordpress',
				'content'       => sprintf(
					__( '<a href="%1$s" class="site-admin-link">Administrer<span class="screen-reader-text"> "%2$s"</span></a>', 'clusterpress' ),
					esc_url( $admin_link ),
					$site_name
				),
			);
		}

		if ( cp_site_is_following_enabled() && current_user_can( 'cp_can_follow_site', array( 'site' => cp_site_get_current_site( $site ) ) ) ) {
			$current_user = wp_get_current_user();
			$site_id      = cp_site_get_current_site_id( $site );
			$follow_slug  = cp_site_get_follow_action_slug();

			if ( isset( $current_user->sites_level[ $site_id ]->user_level ) && 0 === (int) $current_user->sites_level[ $site_id ]->user_level ) {
				$text = sprintf( __( 'Ne plus suivre<span class="screen-reader-text"> "%s"</span>', 'clusterpress' ),
					$site_name
				);
			} else {
				$text = sprintf( __( 'Suivre<span class="screen-reader-text"> "%s"</span>', 'clusterpress' ),
					$site_name
				);
			}

			$userdata_enpoint = sprintf( ' data-cp-action-endpoint="%1$s/%2$s/%3$s"',
				esc_attr( cp_user_get_slug() ),
				esc_attr( $follow_slug ),
				esc_attr( $site_id )
			);

			$sitedata = '';
			$following_class = '';
			if ( ! empty( $current_user->sites_level[ $site_id ]->id ) ) {
				$sitedata = sprintf( ' data-follow-id="%s"',
					esc_attr( $current_user->sites_level[ $site_id ]->id )
				);

				$following_class = ' following';
			}

			$actions['follow'] = array(
				'wrapper_class' => 'follow-link button',
				'dashicon'      => 'dashicons-tickets-alt',
				'content'       => sprintf(
					'<a href="#" class="site-follow-link%1$s"%2$s%3$s>%4$s</span></a>',
					$following_class,
					$userdata_enpoint,
					$sitedata,
					$text
				),
			);
		}

		/**
		 * Filter here to add/edit/remove Site actions.
		 *
		 * @since  1.0.0
		 *
		 * @param array $actions The Site actions.
		 */
		$actions = apply_filters( 'cp_site_get_site_actions', $actions );

		if ( empty( $actions ) ) {
			return;
		}

		return cp_cluster_actions_group( array(
			'class'       => 'cp-site-site-actions',
			'action_type' => 'site',
			'actions'     => $actions,
		) );
	}

/**
 * Output the "no sites found" feedback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_no_sites_found() {
	echo cp_site_get_no_sites_found();
}

	/**
	 * Gett the "no sites found" feedback.
	 *
	 * @since  1.0.0
	 *
	 * @return string The "no sites found" feedback.
	 */
	function cp_site_get_no_sites_found() {
		$message = __( 'Aucun site n\'a été trouvé.', 'clusterpress' );

		clusterpress()->feedbacks->add_feedback(
			'site[network-empty]',
			/**
			 * Filter here to edit the "no sites found" feedback.
			 *
			 * @since  1.0.0
			 *
			 * @param  string $message The "no sites found" feedback.
			 */
			apply_filters( 'cp_site_get_no_sites_found', $message ),
			'info'
		);

		cp_get_template_part( 'assets/feedbacks' );
	}

/**
 * Reset WordPress objects the way we've found them
 * at the begining of the loop.
 *
 * @since  1.0.0
 */
function cp_site_reset_site() {
	$cp = clusterpress();

	if ( isset( $cp->site->site_category ) ) {
		unset( $cp->site->site_category );
	}
}
add_action( 'cp_site_loop_end', 'cp_site_reset_site' );

/**
 * Output the total count for the current Sites loop.
 *
 * @since 1.0.0
 */
function cp_site_total_count() {
	echo esc_html( cp_site_get_total_count() );
}
	/**
	 * Return the total count for the current Sites loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string HTML for the total count.
	 */
	function cp_site_get_total_count() {
		$loop_global = cp_site_get_loop_global();
		$loop        = $loop_global->network_sites;

		$displayed = number_format_i18n( $loop->site_count );
		$total     = number_format_i18n( $loop->total_site_count );

		$total_count = sprintf(
			_n( 'Total %1$s site, %2$s affiché.', 'Total %1$s sites, %2$s affichés.', $total, 'clusterpress' ),
			$total,
			$displayed
		);

		/**
		 * Filter here to edit the total sites count to output.
		 *
		 * @since  1.0.0
		 *
		 * @param  string $total_count The total sites count to output.
		 */
		return apply_filters( 'cp_site_get_total_count', $total_count );
	}

/**
 * Check if the Sites loop has pagination links.
 *
 * @since  1.0.0
 *
 * @return bool True if the Sites loop has pagination links. False otherwise.
 */
function cp_site_has_pagination_links() {
	$loop_global = cp_site_get_loop_global();
	$loop        = $loop_global->network_sites;

	return ! empty( $loop->pag_links );
}

/**
 * Output the pagination links for the current Sites loop.
 *
 * @since 1.0.0
 *
 * @return  string HTML Output.
 */
function cp_site_pagination_links() {
	echo cp_site_get_pagination_links();
}

	/**
	 * Return the pagination links for the current Sites loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string Output for the pagination links.
	 */
	function cp_site_get_pagination_links() {
		$loop_global = cp_site_get_loop_global();
		$loop        = $loop_global->network_sites;

		/**
		 * Filter here to edit the pagination links to output.
		 *
		 * @since  1.0.0
		 *
		 * @param  string the pagination links to output.
		 */
		return apply_filters( 'cp_site_get_pagination_links', $loop->pag_links );
	}

/** Single Site Template Tags *************************************************/

/**
 * Output the Site icon of the displayed site.
 *
 * @since  1.0.0
 *
 * @param  array  $args {
 *   An array of arguments.
 *   @see  cp_get_site_icon() for a detailed description.
 * }
 * @return string HTML Output.
 */
function cp_site_icon( $args = array() ) {
	echo cp_get_site_icon( $args );
}

	/**
	 * Get the Site icon of the displayed site.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $args {
	 *   An array of arguments.
	 *   @type int   $width         The width in pixels of the Site icon. Defaults to 150.
	 *   @type int   $height        The height in pixels of the Site icon. Defaults to 150.
	 *   @type array $site_url_args An array containing specific url data.
	 * }
	 * @param  WP_Site $site The site object.
	 * @return string        The Site icon of the displayed site.
	 */
	function cp_get_site_icon( $args = array(), $site = null ) {
		if ( ! $site ) {
			$site = cp_displayed_site();
		}

		if ( empty( $site->blog_id ) ) {
			return;
		}

		$r = wp_parse_args( $args, array(
			'width'         => 150,
			'height'        => 150,
			'site_url_args' => array(),
		) );

		$site_url_args = $r['site_url_args'];
		unset( $r['site_url_args'] );

		$icon = cp_site_get_site_icon( $r, $site );

		if ( current_user_can( 'cp_edit_single_site', array( 'site' => $site ) ) ) {
			if ( ! empty( $site_url_args['admin_url'] ) ) {
				$return_url = esc_url_raw( $site_url_args['admin_url'] );
			} else {
				$return_url = esc_url_raw( cp_site_get_url( $site_url_args, $site ) );
			}

			$customizer_link = add_query_arg( array(
				'autofocus[section]' => 'title_tagline',
				'return'             => rawurlencode( $return_url ),
			), get_admin_url( $site->blog_id, 'customize.php' ) );

			$icon = sprintf( '<a href="%1$s" title="%2$s">%3$s%4$s</a>',
				esc_url( $customizer_link ),
				esc_attr__( 'Modifier l\'icône du site', 'clusterpress' ),
				$icon,
				'<div class="dashicons dashicons-edit"></div>'
			);
		}

		/**
		 * Filter here to edit the site icon of the displayed site.
		 *
		 * @since  1.0.0
		 *
		 * @param  string  $icon The site icon output.
		 * @param  WP_Site $site The site object.
		 */
		return apply_filters( 'cp_site_icon', $icon, $site );
	}

/**
 * Output the displayed site description.
 *
 * @since 1.0.0
 *
 * @return  string HTML Output.
 */
function cp_site_description() {
	echo esc_html( cp_site_get_description() );
}

	/**
	 * Get the displayed site description.
	 *
	 * @since 1.0.0
	 *
	 * @return  string The displayed site description.
	 */
	function cp_site_get_description() {
		$site = cp_displayed_site();

		$description = '';

		if ( ! empty( $site->description ) ) {
			$description = $site->description;
		}

		/**
		 * Filter here to edit the description of the displayed site.
		 *
		 * @since  1.0.0
		 *
		 * @param  string  $icon The displayed site description.
		 * @param  WP_Site $site The site object.
		 */
		return apply_filters( 'cp_site_get_description', $description, $site );
	}

/**
 * Output the displayed site last updated info.
 *
 * @since 1.0.0
 *
 * @return  string HTML Output.
 */
function cp_site_last_updated() {
	echo esc_html( cp_site_get_site_last_updated( cp_displayed_site() ) );
}

/**
 * Output the displayed site category list.
 *
 * @since 1.0.0
 *
 * @return  string HTML Output.
 */
function cp_site_categories() {
	$categories_output = cp_site_get_category_list( cp_displayed_site() );

	if ( ! $categories_output ) {
		return;
	}

	printf( '<div class="site-categories"><span class="dashicons dashicons-category"></span> %s</div>', $categories_output );
}

/**
 * Output the displayed site action buttons.
 *
 * @since 1.0.0
 *
 * @return  string HTML Output.
 */
function cp_site_the_actions() {
	cp_site_the_site_actions( cp_displayed_site() );
}

/**
 * Check if the displayed site has sticky posts.
 *
 * @since  1.0.0
 *
 * @return bool True if the displayed site has sticky posts. False otherwise.
 */
function cp_site_has_stickies() {
	$site = cp_displayed_site();

	return ! empty( $site->stickies );
}

/**
 * Override the default Posts loop args.
 *
 * @since 1.0.0
 *
 * @param array $args {
 *   An array of arguments.
 *   @see WP_Query::parse_query() for a complete description.
 * }
 * @return array The overriden loop args for the single site home page.
 */
function cp_site_single_posts_args( $args = array() ) {
	$has_stickies = cp_site_has_stickies();

	$args['no_pagination'] = true;

	if ( $has_stickies ) {
		$args['post__in'] = cp_displayed_site()->stickies;
	} else {
		$args['posts_per_page'] = 3;
	}

	/**
	 * Filter here to edit the single site home page loop args.
	 *
	 * @since  1.0.0
	 *
	 * @param  array $args The single site home page loop args.
	 */
	return apply_filters( 'cp_site_single_posts_args', $args );
}

/**
 * Add a parent_site_id property to posts in the single site home page loop.
 *
 * @since  1.0.0
 *
 * @param  WP_Post $post The post object.
 * @return WP_Post       The post object.
 */
function cp_site_set_post_parent_site( $post = null ) {
	if ( is_a( $post, 'WP_Post' ) ) {
		$post->parent_site_id = cp_get_displayed_site_id();
	}

	return $post;
}

/**
 * Temporarly add filters on Posts loop args.
 *
 * @since 1.0.0
 */
function cp_site_start_posts_loop() {
	switch_to_blog( cp_get_displayed_site_id() );

	add_filter( 'cp_site_has_posts_args',   'cp_site_single_posts_args',     10, 1 );
	add_filter( 'cp_site_get_current_post', 'cp_site_set_post_parent_site',  10, 1 );
}

/**
 * Remove the temporary filters on Posts loop args.
 *
 * @since 1.0.0
 */
function cp_site_end_posts_loop() {
	restore_current_blog();

	remove_filter( 'cp_site_has_posts_args',   'cp_site_single_posts_args',     10, 1 );
	remove_filter( 'cp_site_get_current_post', 'cp_site_set_post_parent_site',  10, 1 );
}

/**
 * Output the Single site home page title.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_posts_loop_title() {
	echo esc_html( cp_site_get_posts_loop_title() );
}

	/**
	 * Get the Single site home page title.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Single site home page title.
	 */
	function cp_site_get_posts_loop_title() {
		$has_stickies = cp_site_has_stickies();
		$loop_title   = __( 'Derniers articles', 'clusterpress' );

		if ( $has_stickies ) {
			$loop_title = __( 'Articles mis en avant', 'clusterpress' );
		}

		/**
		 * Filter here to edit the Single site home page title.
		 *
		 * @since  1.0.0
		 *
		 * @param  string $loop_title   The Single site home page title.
		 * @param  bool   $has_stickies True if the displayed site has stickies. False otherwise.
		 */
		return apply_filters( 'cp_site_get_posts_loop_title', $loop_title, $has_stickies );
	}

/**
 * Output the Single site members page title.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_members_loop_title() {
	esc_html_e( 'Membres', 'clusterpress' );
}

/**
 * Override the default Users loop args.
 *
 * @since 1.0.0
 *
 * @param $args The Users loop args {
 *    An array of arguments.
 *    @see WP_User_Query::prepare_query() for a detailed description.
 * }}
 * @return array The overriden loop args for the single site members/manage members page.
 */
function cp_site_single_members_args( $args = array() ) {
	if ( cp_is_site_manage() ) {
		$site              = cp_displayed_site();
		$site->users_level = cp_sites_get_site_users( $site->blog_id, 'members', true );

		$site_users = array_keys( $site->users_level );
	} else {
		$site_users = cp_sites_get_site_users( cp_get_displayed_site_id(), 'members' );
	}

	if ( empty( $site_users ) ) {
		$site_users = 0;
	}

	$args['include'] = $site_users;

	return $args;
}

/**
 * Temporarly add filters on Users loop args.
 *
 * @since 1.0.0
 */
function cp_site_start_members_loop() {
	add_filter( 'cp_user_has_users_args', 'cp_site_single_members_args', 10, 1 );
}

/**
 * Remove the temporary filters on Users loop args.
 *
 * @since 1.0.0
 */
function cp_site_end_members_loop() {
	remove_filter( 'cp_user_has_users_args', 'cp_site_single_members_args', 10, 1 );
}

/**
 * Output the Single user sites page title.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_sites_loop_title() {
	esc_html_e( 'Sites', 'clusterpress' );
}

/**
 * Override the default Sites loop args.
 *
 * @since 1.0.0
 *
 * @param $args The Sites loop args {
 *    An array of arguments.
 *    @see CP_Site_Query::__construct() for a detailed description.
 * }}
 * @return array The overriden loop args for the single user sites page.
 */
function cp_site_user_sites_args( $args = array() ) {
	return array_merge( $args, array( 'user_id' => cp_get_displayed_user_id() ) );
}

/**
 * Temporarly add filters on Sites loop args.
 *
 * @since 1.0.0
 */
function cp_site_start_user_sites_loop() {
	add_filter( 'cp_site_has_sites_args', 'cp_site_user_sites_args', 10, 1 );
}

/**
 * Remove the temporary filters on Sites loop args.
 *
 * @since 1.0.0
 */
function cp_site_end_user_sites_loop() {
	remove_filter( 'cp_site_has_sites_args', 'cp_site_user_sites_args', 10, 1 );
}

/** Single Site's Navigation Template Tags ************************************/

/**
 * Init the Single site's primary nav.
 *
 * @since  1.0.0
 *
 * @return bool True if the Single site has nav items. False otherwise.
 */
function cp_site_has_primary_nav() {
	return clusterpress()->site->toolbar->has_items();
}

/**
 * Init the Single site's secondary nav.
 *
 * @since  1.0.0
 *
 * @param  array  $args  {
 *     An array of arguments.
 *     @see  CP_Cluster_Toolbar::has_items() for a detailed description.
 * }}
 * @return bool True if the Single site has secondary nav items. False otherwise.
 */
function cp_site_has_secondary_nav( $args = array() ) {
	$cp     = clusterpress();
	$parent = $cp->site->toolbar->get_node_by_slug( cp_current_action() );

	if ( ! empty( $parent->id ) ) {
		$parent = $parent->id;
	}

	$r = wp_parse_args( $args, array(
		'parent' => $parent
	) );

	if ( empty( $r['parent'] ) ) {
		return false;
	}

	return clusterpress()->site->toolbar->has_items( $r );
}

/**
 * Iterate nav items within the Nav loop.
 *
 * @since 1.0.0.
 *
 * @return  object The nav item object.
 */
function cp_site_nav_items() {
	return clusterpress()->site->toolbar->nav_items();
}

/**
 * Set the nav item being iterated on.
 *
 * @since 1.0.0
 */
function cp_site_nav_item() {
	clusterpress()->site->toolbar->nav_item();
}

/**
 * Output the Single site nav item ID.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_nav_item_id() {
	echo esc_attr( cp_site_get_nav_item_id() );
}

	/**
	 * Get the Single site nav item ID.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Single site nav item ID.
	 */
	function cp_site_get_nav_item_id() {
		$cp = clusterpress();

		$nav_id = '';
		if ( ! empty( $cp->site->toolbar->current_item->id ) ) {
			$nav_id = 'cp-' . $cp->site->toolbar->current_item->id;
		}

		/**
		 * Filter here to edit the Single site nav item ID.
		 *
		 * @since  1.0.0
		 *
		 * @param string $nav_id The Single site nav item ID.
		 */
		return apply_filters( 'cp_site_get_nav_item_id', $nav_id );
	}

/**
 * Output the Single site nav classes.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_nav_classes() {
	$classes = cp_site_get_nav_classes();

	if ( empty( $classes ) ) {
		return;
	}

	echo join( ' ', array_map( 'sanitize_html_class', $classes ) );
}

	/**
	 * Get the Single site nav classes.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Single site nav classes.
	 */
	function cp_site_get_nav_classes() {
		$cp = clusterpress();

		$classes = array();
		if ( ! empty( $cp->site->toolbar->current_item->meta['classes'] ) ) {
			$classes = $cp->site->toolbar->current_item->meta['classes'];
		}

		/**
		 * Filter here to edit the Single site nav classes.
		 *
		 * @since  1.0.0
		 *
		 * @param array $classes The Single site nav classes.
		 */
		return apply_filters( 'cp_site_get_nav_classes', $classes );
	}

/**
 * Output the Single site nav link.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_nav_link() {
	echo esc_url( cp_site_get_nav_link() );
}

	/**
	 * Get the Single site nav link.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Single site nav link.
	 */
	function cp_site_get_nav_link() {
		$cp = clusterpress();

		$link = '#';
		if ( ! empty( $cp->site->toolbar->current_item->href ) ) {
			$link = $cp->site->toolbar->current_item->href;
		}

		/**
		 * Filter here to edit the Single site nav link.
		 *
		 * @since  1.0.0
		 *
		 * @param string $link The Single site nav link.
		 */
		return apply_filters( 'cp_site_get_nav_link', $link );
	}

/**
 * Output the Single site nav link title.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_nav_link_title() {
	echo esc_attr( cp_site_get_nav_link_title() );
}

	/**
	 * Get the Single site nav link title.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Single site nav link title.
	 */
	function cp_site_get_nav_link_title() {
		$cp = clusterpress();

		$title = '';
		if ( ! empty( $cp->site->toolbar->current_item->title ) ) {
			$title = $cp->site->toolbar->current_item->title;
		}

		/**
		 * Filter here to edit the Single site nav link title.
		 *
		 * @since  1.0.0
		 *
		 * @param string $title The Single site nav link title.
		 */
		return apply_filters( 'cp_site_get_nav_link_title', $title );
	}

/**
 * Output the Single site nav title.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_nav_title() {
	echo wp_kses( cp_site_get_nav_title(), array( 'span' => array( 'class' => true ) ) );
}

	/**
	 * Get the Single site nav title.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Single site nav title.
	 */
	function cp_site_get_nav_title() {
		$cp = clusterpress();

		$title = '';
		if ( ! empty( $cp->site->toolbar->current_item->title ) ) {
			$title = $cp->site->toolbar->current_item->title;
		}

		if ( ! empty( $cp->site->toolbar->current_item->dashicon ) ) {
			$title = sprintf( '<span class="dashicons %1$s"></span><span class="screen-reader-text">%2$s</span>',
				sanitize_html_class( $cp->site->toolbar->current_item->dashicon ),
				esc_html( $title )
			);
		}

		if ( ! empty( $cp->site->toolbar->current_item->count ) ) {
			$title = sprintf( '%1$s <span class="pending-count">%2$d</span>',
				$title,
				number_format_i18n( $cp->site->toolbar->current_item->count )
			);
		}

		/**
		 * Filter here to edit the Single site nav title.
		 *
		 * @since  1.0.0
		 *
		 * @param string $title The Single site nav title.
		 */
		return apply_filters( 'cp_site_get_nav_title', $title );
	}

/** Single Site Manage Screens Template Tags **********************************/

/**
 * Get the Site's settings category control.
 *
 * @since  1.0.0
 *
 * @param  WP_Site $site The site object.
 * @return string        The Site's settings category control.
 */
function cp_site_get_category_checklist( $site = null ) {
	if ( empty( $site ) ) {
		$site = cp_displayed_site();
	}

	if ( empty( $site->blog_id ) ) {
		return false;
	}

	$args = array(
		'descendants_and_self' => 0,
		'selected_cats'        => false,
		'popular_cats'         => false,
		'taxonomy'             => cp_sites_get_taxonomy(),
		'checked_ontop'        => false,
		'disabled'             => false,
	);

	require_once( ABSPATH . 'wp-admin/includes/class-walker-category-checklist.php' );
	$walker = new Walker_Category_Checklist;

	$category = get_taxonomy( $args['taxonomy'] );

	if ( ! current_user_can( $category->cap->assign_terms ) ) {
		$args['disabled'] = true;
	}

	$args['selected_cats'] = wp_get_object_terms( $site->blog_id, $args['taxonomy'], array( 'fields' => 'ids' ) );
	$categories            = (array) get_terms( $args['taxonomy'], array( 'get' => 'all' ) );

	$checklist = call_user_func_array( array( $walker, 'walk' ), array( $categories, 0, $args ) );

	/**
	 * Filter here to edit The Site's settings category control.
	 *
	 * @since  1.0.0
	 *
	 * @param string $checklist The Site's settings category control.
	 * @param  WP_Site $site The site object.
	 */
	return apply_filters( 'cp_site_get_category_checklist', $checklist, $site );
}

/**
 * Check if one or more Site categories exist.
 *
 * @since  1.0.0
 *
 * @return bool True if one or more Site categories exist. False otherwise.
 */
function cp_site_manage_has_categories() {
	$cp = clusterpress();

	return ! empty( $cp->site->categories );
}

/**
 * Output the Site's settings category control.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_manage_the_category_checklist() {
	echo cp_site_manage_get_category_checklist();
}

	/**
	 * Get the Site's settings category output.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Site's settings category output.
	 */
	function cp_site_manage_get_category_checklist() {
		$site        = cp_displayed_site();
		$taxonomy_id = cp_sites_get_taxonomy();

		// Get selected categories for the site
		$categories = cp_sites_get_site_category_list( $site );

		/**
		 * Filter here to edit The Site's settings category output.
		 *
		 * @since  1.0.0
		 *
		 * @param string $checklist The Site's settings category output.
		 * @param  WP_Site $site The site object.
		 */
		return apply_filters( 'cp_site_manage_get_category_checklist', sprintf(
			'<input type="hidden" name="tax_input[%1$s][]" value="0" />
			<ul id="%2$s_checklist" data-wp-lists="list:%2$s" class="categorychecklist form-no-clear">
				%3$s
			</ul>',
			sanitize_key( $taxonomy_id ),
			esc_attr( $taxonomy_id ),
			cp_site_get_category_checklist( $site )
		), $site );
	}

/**
 * Outputs filter options into some user screens to switch sites.
 *
 * @since 1.0.0
 *
 * @param  array $options The order options
 * @return array          The order options
 */
function cp_site_user_filters( $options = array() ) {
	if ( ! cp_is_user_posts() && ! cp_is_user_comments() && ! cp_is_user_likes() ) {
		return $options;
	}

	$any = false;
	if ( ! cp_is_user_posts() ) {
		$any = 'any';
	}

	$ids = cp_sites_get_user_sites( cp_get_displayed_user_id(), $any );

	// Always include the main site.
	$ids = array_merge( array( get_current_network_id() ), $ids );

	$site_names = wp_list_pluck( cp_sites_get_meta_for_sites_list( $ids, 'name' ), 'meta_value' );

	if ( empty( $site_names ) ) {
		return $options;
	}

	// Let's order this by Site ID
	ksort( $site_names );

	$options = array();
	foreach ( $site_names as $value => $text ) {
		$options[] = array( 'value' => $value, 'text' => $text );
	}

	return $options;
}
add_filter( 'cp_filter_get_options', 'cp_site_user_filters', 10, 1 );

/** Single Site settings loop *************************************************/

/**
 * Init the Single site's settings loop.
 *
 * @since  1.0.0
 *
 * @param  array  $args {
 *   An array of arguments.
 *   @see  CP_Site_Metas_Loop::__construct() for a detailed description.
 * }}
 * @return bool True if settings were found for the site. False otherwise.
 */
function cp_site_has_settings( $args = array() ) {
	$loop_global = cp_site_get_loop_global();

	$args = apply_filters( 'cp_site_has_settings_args', $args );

	// Setup the global query loop
	$loop_global->site_settings = new CP_Site_Metas_Loop( $args );

	return apply_filters( 'cp_site_has_sites', $loop_global->site_settings->has_items() );
}

/**
 * Iterate site setting within the Site's settings loop.
 *
 * @since 1.0.0.
 *
 * @return  object The site setting object.
 */
function cp_site_the_settings() {
	$loop_global = cp_site_get_loop_global();

	return $loop_global->site_settings->items();
}

/**
 * Set the site setting item being iterated on.
 *
 * @since 1.0.0
 *
 * @return  object The current site setting object.
 */
function cp_site_the_setting() {
	$loop_global = cp_site_get_loop_global();

	return $loop_global->site_settings->the_item();
}

/**
 * Get the site setting being iterated on.
 *
 * @since 1.0.0
 *
 * @return  object The current site setting object.
 */
function cp_site_get_current_setting() {
	$loop_global = cp_site_get_loop_global();

	/**
	 * Filter here if you need to edit/add properties to the site's setting object.
	 *
	 * @since  1.0.0
	 *
	 * @param  object The current site setting object.
	 */
	return apply_filters( 'cp_site_get_current_setting', $loop_global->site_settings->site_meta );
}

/**
 * Output a Site's settings field.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_setting_the_field() {
	echo cp_site_setting_get_field();
}

	/**
	 * Get a Site's settings field.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Site's settings field.
	 */
	function cp_site_setting_get_field() {
		$setting = cp_site_get_current_setting();

		if ( empty( $setting->id ) || empty( $setting->manage_display_callback ) ) {
			return false;
		}

		/**
		 * Filter here to edit the Site's settings field output.
		 *
		 * @since  1.0.0
		 *
		 * @param string $value   The Site's settings field output.
		 * @param object $setting The setting object.
		 */
		return apply_filters( 'cp_site_setting_get_field', call_user_func_array( $setting->manage_display_callback, array( $setting ) ), $setting );
	}

/**
 * Output a Site's settings field label into the WordPress network Site's administration screen.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_setting_admin_the_label() {
	echo cp_site_setting_admin_get_label();
}

	/**
	 * Get a Site's settings field label.
	 *
	 * @since  1.0.0
	 *
	 * @return string A Site's settings field label.
	 */
	function cp_site_setting_admin_get_label() {
		$setting = cp_site_get_current_setting();

		if ( empty( $setting->id ) || empty( $setting->description ) ) {
			return false;
		}

		/**
		 * Filter here to edit the Site's settings field label.
		 *
		 * @since  1.0.0
		 *
		 * @param string $value The Site's settings field label.
		 */
		return apply_filters( 'cp_site_setting_admin_get_label', sprintf( '<label for="%1$s">%2$s</label>',
			esc_attr( $setting->id ),
			esc_html( $setting->description )
		) );
	}

/**
 * Output a Site's settings field into the WordPress network Site's administration screen.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_setting_admin_the_field() {
	echo cp_site_setting_admin_get_field();
}

	/**
	 * Get a Site's settings field.
	 *
	 * @since  1.0.0
	 *
	 * @return string A Site's settings field.
	 */
	function cp_site_setting_admin_get_field() {
		$setting = cp_site_get_current_setting();

		if ( empty( $setting->id ) || empty( $setting->admin_display_callback ) ) {
			return false;
		}

		/**
		 * Filter here to edit the Site's settings field.
		 *
		 * @since  1.0.0
		 *
		 * @param string $value   The Site's settings field.
		 * @param object $setting The setting object.
		 */
		return apply_filters( 'cp_site_setting_admin_get_field', call_user_func_array( $setting->admin_display_callback, array( $setting ) ), $setting );
	}

/**
 * Generic callback for site settings output.
 *
 * @since  1.0.0
 *
 * @param  object $setting The setting object.
 */
function cp_sites_manage_display( $setting = null ) {
	$output = '';

	if ( 'string' === $setting->type  ) {
		$label = '';

		if ( ! empty( $setting->description ) ) {
			$label = "\n" . sprintf( '<label class="label" for="cp_field_%1$s">%2$s</label>',
				esc_attr( $setting->id ),
				esc_html( $setting->description )
			);
		}

		$output = sprintf(
			'<input type="text" id="cp_setting_field_%1$s" name="cp_site_settings[%1$s]" value="%2$s">%3$s',
			esc_attr( $setting->id ),
			esc_attr( $setting->value ),
			$label
		);

	// Public (Search engines)
	} elseif ( 'public' === $setting->id ) {
		$output  = '';
		$checked = checked( true, is_numeric( $setting->value ) && 0 === (int) $setting->value, false );

		$output = sprintf( '
			<label class="label" for="cp_setting_field_%1$s">
				<input type="hidden" name="cp_site_settings[%1$s]" value="1">
				<input type="checkbox" id="cp_setting_field_%1$s" name="cp_site_settings[%1$s]" value="0"%2$s> %3$s
			</label>
			',
			esc_attr( $setting->id ),
			$checked,
			$setting->description
		);

	// Site Icon
	} elseif ( 'icon' === $setting->id ) {
		$output = sprintf( '<div class="cp-site-icon">%1$s</div><label class="label">%2$s</label>',
			cp_get_site_icon( array(
				'width'  => 40,
				'height' => 40,
				'site_url_args' => array(
					'slug'       => cp_site_get_manage_slug()  . '/' . cp_site_get_settings_slug(),
					'rewrite_id' => cp_site_get_rewrite_id(),
				),
			) ),
			$setting->description
		);
	}

	return $output;
}

/**
 * Output a user feedback to inform no settings were found.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_no_settings_found() {
	echo cp_site_get_no_settings_found();
}

	/**
	 * Get a user feedback to inform no settings were found.
	 *
	 * @since 1.0.0
	 *
	 * @return string HTML Output.
	 */
	function cp_site_get_no_settings_found() {
		$message = __( 'Aucun réglage de site n\'a été trouvé.', 'clusterpress' );

		clusterpress()->feedbacks->add_feedback(
			'site[settings-empty]',
			/**
			 * Filter here to edit the massage output.
			 *
			 * @since  1.0.0
			 *
			 * @param string $message The user feedbak message.
			 */
			apply_filters( 'cp_site_get_no_settings_found', $message ),
			'info'
		);

		cp_get_template_part( 'assets/feedbacks' );
	}
