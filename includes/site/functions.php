<?php
/**
 * ClusterPress Site Functions.
 *
 * @package ClusterPress\site
 * @subpackage functions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Check if a user can edit posts on the current site or on at least
 * one of the site of the network.
 *
 * @since 1.0.0
 *
 * @param WP_User The user object we need to check the capability for.
 * @return bool True if the user can edit posts. False otherwise.
 */
function cp_sites_user_has_contributor_level( $user = null ) {
	if ( empty( $user->ID ) ) {
		return false;
	}

	$has_contributor_level = user_can( $user, 'edit_posts' );

	if ( is_multisite() && ! $has_contributor_level ) {
		global $wpdb;
		$table = $wpdb->base_prefix . 'cp_user_sites';

		$has_contributor_level = (bool) $wpdb->get_var( $wpdb->prepare(
			"SELECT COUNT( user_id ) FROM {$table} WHERE user_id = %d AND user_level < 5 AND user_level != 0", $user->ID
		) );
	}

	return $has_contributor_level;
}

/**
 * This capability check is really specific.
 *
 * Instead of checking the current user, we need to see if the displayed
 * one can edit posts ont the main site or has a user level upper than the
 * subscriber one on one of the site of the network.
 *
 * @since 1.0.0
 *
 * @param  int   $user_id The user id.
 * @param  array $args    Complementary arguments.
 * @return array          The user's actual capabilities.
 */
function cp_site_post_single_user_required_caps( $user_id = 0, $args = array() ) {
	$caps           = array( 'edit_posts' );
	$displayed_user = cp_displayed_user();

	if ( ! empty( $user_id ) ) {
		$current_user = wp_get_current_user();
		if ( ! isset( $current_user->is_network_contributor ) ) {
			$current_user->is_network_contributor = cp_sites_user_has_contributor_level( $current_user );
		}

		if ( true === $current_user->is_network_contributor ) {
			$caps = array( 'exist' );
		}
	}

	// The Admin Bar is for Loggedin users. So we don't need to check the displayed one anymore.
	if ( ! did_action( 'cp_admin_bar_menu' ) && isset( $displayed_user->ID ) ) {
		if ( ! isset( $displayed_user->is_network_contributor ) ) {
			$displayed_user->is_network_contributor = cp_sites_user_has_contributor_level( $displayed_user );
		}

		if ( true === $displayed_user->is_network_contributor ) {
			$caps = array( 'exist' );
		} else {
			$caps = array( 'do_not_allow' );
		}
	}

	/**
	 * Filter here to edit user caps.
	 *
	 * @since  1.0.0
	 *
	 * @param array $caps    The user's actual capabilities.
	 * @param int   $user_id The user id.
	 * @param array $args    Complementary arguments.
	 */
	return apply_filters( 'cp_user_edit_single_required_caps', $caps, $user_id, $args );
}

/**
 * Register the user's Posts & Comments sections.
 *
 * @since 1.0.0
 */
function cp_site_register_user_section() {

	// Register the posts section
	cp_register_cluster_section( 'user', array(
		'id'            => 'posts',
		'name'          => __( 'Articles', 'clusterpress' ),
		'cluster_id'    => 'user',
		'slug'          => cp_user_get_posts_slug(),
		'rewrite_id'    => cp_user_get_posts_rewrite_id(),
		'template'      => '',
		'template_dir'  => '',
		'toolbar_items' => array(
			'position'   => 20,
			'dashicon'   => 'dashicons-admin-post',
			'capability' => 'cp_post_single_user',
		),
	) );

	// Register the comments section
	cp_register_cluster_section( 'user', array(
		'id'            => 'comments',
		'name'          => __( 'Commentaires', 'clusterpress' ),
		'cluster_id'    => 'user',
		'slug'          => cp_user_get_comments_slug(),
		'rewrite_id'    => cp_user_get_comments_rewrite_id(),
		'template'      => '',
		'template_dir'  => '',
		'toolbar_items' => array(
			'position' => 30,
			'dashicon' => 'dashicons-admin-comments',
		),
	) );
}
add_action( 'cp_register_cluster_sections', 'cp_site_register_user_section' );

/**
 * Override the Author link in favor of the ClusterPress User Posts link.
 *
 * @since 1.0.0
 *
 * @param  string $link      The Url to the Author's Archive.
 * @param  int    $author_id The user ID.
 * @return string            The ClusterPress User Posts link.
 */
function cp_site_get_user_posts_link( $link = '', $author_id = 0 ) {
	$user = get_user_by( 'id', $author_id );

	if ( ! user_can( $user, 'edit_posts' ) ) {
		return $link;
	}

	return cp_user_get_url( array(
		'slug'       => cp_user_get_posts_slug(),
		'rewrite_id' => cp_user_get_posts_rewrite_id(),
	), $user );
}
add_filter( 'author_link', 'cp_site_get_user_posts_link', 10, 2 );
