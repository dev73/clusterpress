<?php
/**
 * ClusterPress Site Category Widget.
 *
 * @package ClusterPress\site\classes
 * @subpackage site-category-widget
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

if ( class_exists( 'WP_Widget_Categories' ) ) :
/**
 * The Site Category Widget Class
 *
 * @since  1.0.0
 */
class CP_Site_Category_Widget extends WP_Widget_Categories {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'                   => 'clusterpress-site-categories',
			'description'                 => __( 'Liste des catégories de site.', 'clusterpress' ),
			'customize_selective_refresh' => true,
		);

		WP_Widget::__construct( false, $name = __( 'CP Catégories de site', 'clusterpress' ), $widget_ops );

		$this->alt_option_name = 'clusterpress_site_categories';
	}

	/**
	 * Register the widget
	 *
	 * @since 1.0.0
	 */
	public static function register_widget() {
		register_widget( 'CP_Site_Category_Widget' );
	}

	/**
	 * Display the widget on front end
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args
	 * @param  array $instance
	 */
	public function widget( $args = array(), $instance = array() ) {
		$title = esc_html__( 'Catégories de site', 'clusterpress' );

		if ( ! empty( $instance['title'] ) ) {
			$title = esc_html( $instance['title'] );
		} else {
			$instance['title'] = $title;
		}

		add_filter( 'widget_categories_args', array( $this, 'site_categories_args' ), 10, 1 );

		parent::widget( $args, $instance );

		remove_filter( 'widget_categories_args', array( $this, 'site_categories_args' ), 10, 1 );
	}

	/**
	 * Override wp_list_categories() args used in WP_Widget_Categories
	 * so that it uses the Site Category taxonomy.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $args {
	 *       An array of arguments.
	 *       @see  wp_list_categories() for a detailed description.
	 * }}
	 * @return array The arguments for wp_list_categories().
	 */
	public function site_categories_args( $args = array() ) {
		$site_args = array(
			'taxonomy'   => cp_sites_get_taxonomy(),
			'hide_empty' => 0,
		);

		$current = cp_get_current_term_id();
		if ( $current ) {
			$site_args['current_category'] = $current;
		}

		return array_merge( $args, $site_args );
	}

	/**
	 * Display the form in Widgets Administration
	 *
	 * @since 1.0.0
	 *
	 * @param  array $instance
	 */
	public function form( $instance = array() ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
		$title    = sanitize_text_field( $instance['title'] );

		$count = false;
		if ( isset( $instance['count'] ) ) {
			$count = (bool) $instance['count'];
		}

		$hierarchical = false;
		if ( isset( $instance['hierarchical'] ) ) {
			$hierarchical = (bool) $instance['hierarchical'];
		}
		?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Titre :', 'clusterpress' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>"<?php checked( $count ); ?> />
		<label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php esc_html_e( 'Afficher le nombre de sites', 'clusterpress' ); ?></label><br />

		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id( 'hierarchical' ); ?>" name="<?php echo $this->get_field_name( 'hierarchical' ); ?>"<?php checked( $hierarchical ); ?> />
		<label for="<?php echo $this->get_field_id( 'hierarchical' ); ?>"><?php esc_html_e( 'Afficher la hiérarchie', 'clusterpress' ); ?></label></p>
		<?php
	}
}
add_action( 'cp_widgets_init', array( 'CP_Site_Category_Widget', 'register_widget' ) );

endif;
