<?php
/**
 * Site Cluster.
 *
 * @package ClusterPress\site\classes
 * @subpackage site-cluster
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * The Site CLuster Class.
 *
 * @since 1.0.0
 */
class CP_Site_Cluster extends CP_Cluster {

	/**
	 * The constructor
	 *
	 * @since 1.0.0
	 *
	 * @param array args The Site Cluster arguments.
	 * {@see CP_Cluster::__construct() for a detailled list of available arguments}
	 */
	public function __construct( $args = array() )  {
		parent::__construct( $args );
	}

	/**
	 * Add the Site Cluster to ClusterPress main instance.
	 *
	 * @since 1.0.0
	 */
	public static function start() {
		$cp = clusterpress();

		if ( empty( $cp->site ) ) {
			$cp->site = new self( array(
				'id'            => 'site',
				'name'          => __( 'Site', 'clusterpress' ),
				'dir'           => 'site',
				'slug'          => cp_site_get_slug(),
				'rewrite_id'    => cp_site_get_rewrite_id(),
				'archive_slug'  => cp_sites_get_slug(),
				'archive_title' => cp_sites_get_title(),
				'has_single'    => true,
			) );
		}

		return $cp->site;
	}

	/**
	 * Set specific hooks for the Site Cluster.
	 *
	 * @since 1.0.0
	 */
	public function set_cluster_hooks() {
		if ( ! is_multisite() ) {
			return;
		}

		$this->meta_keys = array(
			'url'             => 'url',
			'blogname'        => 'name',
			'blogdescription' => 'description',
			'admins'          => 'admins',
			'site_icon'       => 'icon',
			'sticky_posts'    => 'stickies',
			'blog_public'     => 'public',
		);

		foreach ( array_keys( $this->meta_keys ) as $meta_key ) {
			// Do not synchronise these two keys.
			if ( 'url' === $meta_key || 'admins' === $meta_key ) {
				continue;
			}

			// Synchronize the other keys
			add_action( "update_option_{$meta_key}", 'cp_sites_sync_meta', 10, 3 );
		}

		// Synchronize user/site association
		add_action( 'wpmu_new_blog',         'cp_sites_add_site',         10, 2 );
		add_action( 'set_user_role',         'cp_sites_add_user_site',    10, 2 );
		add_action( 'wpmu_new_user',         'cp_sites_remove_user_site', 10, 1 );
		add_action( 'deleted_user',          'cp_sites_remove_user_site', 10, 1 );
		add_action( 'remove_user_from_blog', 'cp_sites_remove_user_site', 10, 1 );

		// Remove Site data when one of these hooks is fired
		foreach ( array( 'make_spam_blog', 'mature_blog', 'archive_blog', 'make_delete_blog' ) as $remove_status ) {
			add_action( $remove_status, 'cp_sites_remove_site_metadata', 10, 1 );
		}

		// Restore Site data when one of these hooks is fired
		foreach ( array( 'make_ham_blog', 'unmature_blog', 'unarchive_blog', 'make_undelete_blog' ) as $restore_status ) {
			add_action( $restore_status, 'cp_sites_restore_site_metadata', 10, 1 );
		}

		// Respect Site's noindex setting
		add_action( 'cp_head', 'cp_site_not_public_header_meta' );

		// Register Site metas
		add_action( 'cp_init', 'cp_sites_register_default_metas', 12 );
	}

	/**
	 * Include the needed files.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $files The list of files and class files
	 */
	public function load_cluster( $files = array() ) {
		$files['files'] = array(
			'functions',
			'tags',
		);

		$files['class_files'] = array(
			'site-posts-loop',
			'site-comments-loop',
		);

		/**
		 * Only include MS files if the active WordPress config
		 * is Multisite.
		 */
		if ( is_multisite() ) {
			$files['files'] = array_merge( $files['files'], array(
				'multisite/meta',
				'multisite/functions',
				'multisite/actions',
				'multisite/tags',
			) );

			$files['class_files'] = array_merge( $files['class_files'], array(
				'site-query',
				'site-screens',
				'sites-loop',
				'site-metas-loop',
			) );

			/**
			 * Only include the other files if the site's following
			 * feature is enabled.
			 */
			if ( cp_site_is_following_enabled() ) {
				$files['class_files'][] = 'sites-followed-post-loop';

				$files['files'] = array_merge( $files['files'], array(
					'multisite/following-sites/functions',
					'multisite/following-sites/tags',
					'multisite/following-sites/actions',
				) );
			}

			if ( cp_is_main_site() ) {
				$files['class_files'][] = 'site-category-widget';
			}
		}

		parent::load_cluster( $files );
	}

	/**
	 * Define cache groups for the cluster
	 *
	 * @since  1.0.0
	 */
	public function set_cluster_cache_groups() {
		if ( ! is_multisite() ) {
			return;
		}

		$cache_groups = array(
			'cp_site_ids',
			'cp_site_path_or_domain',
		);

		// Global groups.
		if ( cp_site_is_following_enabled() ) {
			$cache_groups[] = 'cp_site_followed_new_posts';
		}

		wp_cache_add_global_groups( $cache_groups );

		parent::set_cluster_cache_groups();
	}

	/**
	 * Analyse the WordPress Query to eventually set ClusterPress Site's globals.
	 * Check the user can access to the required single site if requested.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Query $q The WordPress main query.
	 */
	public function parse_query( $q = null ) {
		if ( ! cp_can_alter_posts_query( $q ) ) {
			return;
		}

		$cp                  = clusterpress();
		$prefetch_categories = false;
		$taxonomy_id         = cp_sites_get_taxonomy();

		// Check if a Site category is requested.
		$category_slug = $q->get( $taxonomy_id );

		if ( $category_slug ) {
			$category = term_exists( $category_slug, $taxonomy_id );

			if ( ! empty( $category ) )  {
				$cp->is_cluster                = true;
				$cp->cluster->archive          = $this->archive_slug;
				$cp->cluster->current_taxonomy = (object) $category;

				// Attach the taxonomy
				$cp->cluster->current_taxonomy->taxonomy = $taxonomy_id;

				$archive_cap = sprintf( 'cp_read_%ss_archive', cp_current_cluster() );

				if ( ! current_user_can( $archive_cap ) ) {
					$q->set_404();
					return;
				}
			} else {
				$q->set_404();
				return;
			}
		}

		// Check if we are on a Site archive page.
		if ( is_clusterpress() && cp_is_current_archive( $this->archive_slug ) ) {
			$q->set( 'cp_post_title', $this->archive_title );

			$prefetch_categories = true;
		}

		// Check if we are viewing a single site.
		$site_slug = $q->get( $this->rewrite_id );

		if ( $site_slug && isset( $this->toolbar ) ) {
			$cp->is_cluster = true;

			$site = cp_get_site_by( 'path', $site_slug );

			if ( empty( $site->blog_id ) ) {
				$q->set_404();
				return;

			// Set the displayed site
			} else {
				$cp->cluster->displayed_object = $site;
			}

			if ( ! current_user_can( 'cp_read_single_site' ) ) {
				$q->set_404();
				return;
			}

			// Get the nodes with their URLs
			$site_nodes = $this->toolbar->populate_urls( array(
				'object'          => $site,
				'type'            => 'WP_Site',
				'parent_group'    => 'single-bar',
				'url_callback'    => 'cp_site_get_url',
				'filter'          => 'cp_site_cluster_get_toolbar_urls',
				'capability_args' => array( 'site_id' => $site->blog_id ),
			) );

			if ( ! $site_nodes ) {
				$q->set_404();
				return;
			}

			$validate = array_filter( $this->toolbar->validate_access( array(
				'nodes'         => $site_nodes,
				'object'        => $site,
				'query'         => $q,
				'default_cap'   => 'cp_edit_single_site',
				'filter_prefix' => 'cp_site_cluster_edit',
			) ) );

			if ( ! $validate && count( $q->query ) > 1 ) {
				$q->set_404();
				return;
			}

			$validate = array_merge( array(
				'action'    => 'home',
				'subaction' => null,
			), $validate );

			// If it's home, update the home nav separately.
			if ( 'home' === $validate['action'] ) {
				$home_node = $site_nodes['home'];

				if ( isset( $home_node->meta['classes'] ) ) {
					$home_node->meta['classes'] = array_merge( $home_node->meta['classes'], array( 'current' ) );
				} else {
					$home_node->meta['classes'] = array( 'current' );
				}

				// Edit the node
				$this->toolbar->edit_node( 'home', (array) $home_node );
			}

			$q->set( 'cp_post_title', $site->name );

			// Set the action
			$cp->cluster->action = $validate['action'];

			// Set the subaction
			if ( ! empty( $validate['subaction'] ) ) {
				$cp->cluster->sub_action = $validate['subaction'];
			}

			$prefetch_categories = true;
		}

		if ( $prefetch_categories ) {
			$this->set_site_categories();
		}

		parent::parse_query( $q );
	}

	/**
	 * Register the Post types used by the Site Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @param array $post_types an array containing associative arrays {
	 *     An array of arguments.
	 *     @see CP_Cluster::register_post_types() for a detailled list of available arguments.
	 * }
	 */
	public function register_post_types( $post_types = array() ) {
		/**
		 * Only register a utility post type in network admin to be able to
		 * manage Site categories.
		 */
		if ( is_network_admin() ) {
			$post_types = array(
				array(
					'post_type' => 'cp_site',
					'args'      => array(
						'label'              => 'cp_sites',
						'public'             => false,
						'publicly_queryable' => false,
						'show_ui'            => true,
						'show_in_menu'       => false,
						'show_in_nav_menus'  => false,
						'show_admin_column'  => false,
						'query_var'          => false,
						'rewrite'            => false,
						'has_archive'        => false,
						'hierarchical'       => false,
					),
				),
			);
		}

		parent::register_post_types( $post_types );
	}

	/**
	 * Register the Site Category Taxonomy.
	 *
	 * @since 1.0.0
	 *
	 * @param array $taxonomies an array containing associative arrays {
	 *     An array of arguments.
	 *     @see CP_Cluster::register_taxonomies() for a detailled list of available arguments.
	 * }
	 */
	public function register_taxonomies( $taxonomies = array() ) {
		// Only register the Site Category if it's a multisite config.
		if ( is_multisite() ) {
			$taxonomies = array(
				array(
					'taxonomy'    => cp_sites_get_taxonomy(),
					'object_type' => null,
					'args'        => array(
						'label'  => _x( 'Catégories de Site', 'ClusterPress site category taxonomy', 'clusterpress' ),
						'labels' => array(
							'name'              => _x( 'Catégories de Site', 'ClusterPress site category taxonomy', 'clusterpress' ),
							'menu_name'         => _x( 'Catégories', 'ClusterPress site category taxonomy', 'clusterpress' ),
							'singular_name'     => _x( 'Catégorie', 'ClusterPress site category taxonomy', 'clusterpress' ),
							'all_items'         => _x( 'Toutes les catégories', 'ClusterPress site category taxonomy', 'clusterpress' ),
							'edit_item'         => _x( 'Modifier la catégorie', 'ClusterPress site category taxonomy', 'clusterpress' ),
							'view_item'         => _x( 'Voir la catégorie', 'ClusterPress site category taxonomy', 'clusterpress' ),
							'update_item'       => _x( 'Mettre à jour la catégorie', 'ClusterPress site category taxonomy', 'clusterpress' ),
							'add_new_item'      => _x( 'Ajouter une catégorie', 'ClusterPress site category taxonomy', 'clusterpress' ),
							'new_item_name'     => _x( 'Nouveau nom de catégorie', 'ClusterPress site category taxonomy', 'clusterpress' ),
							'parent_item'       => _x( 'Catégorie parente', 'ClusterPress site category taxonomy', 'clusterpress' ),
							'parent_item_colon' => _x( 'Catégorie parente :', 'ClusterPress site category taxonomy', 'clusterpress' ),
							'search_items'      => _x( 'Rechercher une catégorie', 'ClusterPress site category taxonomy', 'clusterpress' ),
							'not_found'         => _x( 'Aucune catégorie trouvée', 'ClusterPress site category taxonomy', 'clusterpress' ),
						),
						'public'                => true,
						'show_ui'               => is_network_admin(),
						'meta_box_cb'           => is_network_admin() ? 'cp_admin_site_category_meta_box' : null,
						'show_in_menu'          => false,
						'show_in_nav_menus'     => cp_is_main_site(),
						'hierarchical'          => true,
						'update_count_callback' => '_update_generic_term_count',
						'rewrite'               => array(
							'slug'              => trailingslashit( cp_get_root_slug() ) . cp_sites_get_slug() . '/' . cp_site_get_category_slug(),
							'with_front'        => false,
							'hierarchical'      => false,
						),
						'capabilities'          => array(
							'manage_terms'      => 'manage_network_options',
							'edit_terms'        => 'manage_network_options',
							'delete_terms'      => 'manage_network_options',
							'assign_terms'      => 'cp_assign_site_terms',
						),
					),
				),
			);
		}

		parent::register_taxonomies( $taxonomies );
	}

	/**
	 * Add rewrite tags for the Site Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @param array $tags an array containing associative arrays {
	 *     An array of arguments.
	 *     @see CP_Cluster::rewrite_tags() for a detailled list of available arguments.
	 * }
	 */
	public function rewrite_tags( $tags = array() ) {
		parent::rewrite_tags( array(
			array(
				'tag'   => '%' . $this->rewrite_id . '%',
				'regex' => '([^/]+)',
			),
			array(
				'tag'   => '%' . cp_site_get_manage_rewrite_id() . '%',
				'regex' => '([^/]+)',
			),
			array(
				'tag'   => '%' . cp_site_get_followers_rewrite_id() . '%',
				'regex' => '([1]{1,})',
			),
			array(
				'tag'   => '%' . cp_site_get_members_rewrite_id() . '%',
				'regex' => '([1]{1,})',
			),
		) );
	}

	/**
	 * Add rewrite rules for the Site Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @param array $rules an array containing associative arrays {
	 *     An array of arguments.
	 *     @see CP_Cluster::rewrite_rules() for a detailled list of available arguments.
	 * }
	 */
	public function rewrite_rules( $rules = array() ) {
		$site_regex = trailingslashit( cp_get_root_slug() ) . $this->slug;
		$page_slug  = cp_get_paged_slug();
		$page_id    = 'paged';

		parent::rewrite_rules( array(
			array(
				'regex' => $site_regex . '/([^/]+)/' . cp_site_get_members_slug() . '/' . $page_slug . '/?([0-9]{1,})/?$',
				'query' => 'index.php?' . $this->rewrite_id . '=$matches[1]&' . cp_site_get_members_rewrite_id() . '=1&' . $page_id . '=$matches[2]',
			),
			array(
				'regex' => $site_regex . '/([^/]+)/' . cp_site_get_members_slug() . '/?$',
				'query' => 'index.php?' . $this->rewrite_id . '=$matches[1]&' . cp_site_get_members_rewrite_id() . '=1',
			),
			array(
				'regex' => $site_regex . '/([^/]+)/' . cp_site_get_followers_slug() . '/' . $page_slug . '/?([0-9]{1,})/?$',
				'query' => 'index.php?' . $this->rewrite_id . '=$matches[1]&' . cp_site_get_followers_rewrite_id() . '=1&' . $page_id . '=$matches[2]',
			),
			array(
				'regex' => $site_regex . '/([^/]+)/' . cp_site_get_followers_slug() . '/?$',
				'query' => 'index.php?' . $this->rewrite_id . '=$matches[1]&' . cp_site_get_followers_rewrite_id() . '=1',
			),
			array(
				'regex' => $site_regex . '/([^/]+)/' . cp_site_get_manage_slug() . '/([^/]+)/' . $page_slug . '/?([0-9]{1,})/?$',
				'query' => 'index.php?' . $this->rewrite_id . '=$matches[1]&' . cp_site_get_manage_rewrite_id() . '=$matches[2]&' . $page_id . '=$matches[3]',
			),
			array(
				'regex' => $site_regex . '/([^/]+)/' . cp_site_get_manage_slug() . '/([^/]+)/?$',
				'query' => 'index.php?' . $this->rewrite_id . '=$matches[1]&' . cp_site_get_manage_rewrite_id() . '=$matches[2]',
			),
			array(
				'regex' => $site_regex . '/([^/]+)/?$',
				'query' => 'index.php?' . $this->rewrite_id . '=$matches[1]',
			),
		) );
	}

	/**
	 * Add permastruct for the Site Cluster.
	 *
	 * @since 1.0.0
	 *
	 * {@see add_permastruct() for a detailed description of the arguments}}
	 * @param string $name
	 * @param string $struct
	 * @param array  $args
	 */
	public function permastructs( $name = '', $struct = '', $args = array() ) {
		parent::permastructs(
			$this->rewrite_id,
			$this->slug . '/%' . $this->rewrite_id . '%'
		);
	}

	/**
	 * Set the Site's Cluster toolbar
	 *
	 * @since 1.0.0
	 *
	 * @param array $nodes an array containing associative arrays {
	 *     An array of arguments.
	 *     @see CP_Cluster::toolbars() for a detailled list of available arguments.
	 * }
	 */
	public function toolbars( $nodes = array() ) {
		$site_toolbar = array(
			array(
				'type' => 'group', 'args' => array( 'id' => 'archive-bar' ),
			),
			array(
				'type' => 'group', 'args' => array( 'id' => 'single-bar' ),
			),
			array(
				'type' => 'node', 'args' => array(
					'id'           => 'home',
					'slug'         => 'home',
					'parent_group' => 'single-bar',
					'title'        => __( 'Accueil', 'clusterpress' ),
					'href'         => '#',
					'position'     => 10,
					'capability'   => 'cp_read_single_site',
					'dashicon'     => 'dashicons-admin-home',
				),
			),
			array(
				'type' => 'node', 'args' => array(
					'id'           => cp_site_get_members_rewrite_id(),
					'slug'         => cp_site_get_members_slug(),
					'parent_group' => 'single-bar',
					'title'        => __( 'Membres', 'clusterpress' ),
					'href'         => '#',
					'position'     => 20,
					'capability'   => 'cp_read_single_site',
					'dashicon'     => 'dashicons-groups',
				),
			),
			array(
				'type' => 'node', 'args' => array(
					'id'           => cp_site_get_manage_rewrite_id(),
					'slug'         => cp_site_get_manage_slug(),
					'parent_group' => 'single-bar',
					'title'        => __( 'Gérer', 'clusterpress' ),
					'href'         => '#',
					'position'     => 1000,
					'meta'         => array( 'classes' => array( 'last' ) ),
					'capability'   => 'cp_edit_single_site',
					'dashicon'     => 'dashicons-admin-settings'
				),
			),
			array(
				'type' => 'node', 'args' => array(
					'id'           => 'settings',
					'slug'         => cp_site_get_settings_slug(),
					'parent'       => cp_site_get_manage_rewrite_id(),
					'parent_group' => 'single-bar',
					'title'        => __( 'Réglages', 'clusterpress' ),
					'href'         => '#',
					'position'     => 10,
					'capability'   => 'cp_edit_single_site',
				),
			),
			array(
				'type' => 'node', 'args' => array(
					'id'           => 'manage_members',
					'slug'         => cp_site_get_members_slug(),
					'parent'       => cp_site_get_manage_rewrite_id(),
					'parent_group' => 'single-bar',
					'title'        => __( 'Membres', 'clusterpress' ),
					'href'         => '#',
					'position'     => 20,
					'capability'   => 'cp_edit_single_site',
				),
			),
		);

		// Only add some nav items if the site's following feature is enabled.
		if ( cp_site_is_following_enabled() ) {
			$site_toolbar[] = array(
				'type' => 'node', 'args' => array(
					'id'           => cp_site_get_followers_rewrite_id(),
					'slug'         => cp_site_get_followers_slug(),
					'parent_group' => 'single-bar',
					'title'        => __( 'Adeptes', 'clusterpress' ),
					'href'         => '#',
					'position'     => 30,
					'capability'   => 'cp_read_single_site',
					'dashicon'     => 'dashicons-tickets-alt',
				),
			);

			$site_toolbar[] = array(
				'type' => 'node', 'args' => array(
					'id'           => 'manage_followers',
					'slug'         => cp_site_get_followers_slug(),
					'parent'       => cp_site_get_manage_rewrite_id(),
					'parent_group' => 'single-bar',
					'title'        => __( 'Adeptes', 'clusterpress' ),
					'href'         => '#',
					'position'     => 30,
					'capability'   => 'cp_edit_single_site',
				),
			);
		}

		parent::toolbars( $site_toolbar );
	}

	/**
	 * Set the title to use in the document <meta> tag.
	 *
	 * @since 1.0.0
	 *
	 * @param array $title_parts The list of terms to add to the title tag.
	 */
	public function setup_page_title( $title_parts = array() ) {

		if ( cp_is_sites() && ! empty( $this->archive_title ) ) {
			$title_parts['title'] = $this->archive_title;

			$current_site_category = cp_current_taxonomy();

			if ( $current_site_category && ! empty( $this->categories ) ) {
				$site_category_data = wp_list_filter( $this->categories, array( 'term_id' => $current_site_category->term_id ) );

				if ( is_array( $site_category_data ) )  {
					$site_category = reset( $site_category_data );

					if ( ! empty( $site_category->name ) ) {
						$title_parts['category'] = esc_html( $site_category->name );
					}
				}
			}

		} elseif ( cp_is_site() ) {
			$site = cp_displayed_site();

			$title_parts['title'] = $site->name;

			if ( cp_current_action() || cp_current_sub_action() ) {
				$slug         = cp_current_action();
				$site_nodes   = $this->toolbar->get_toolbar_nodes();
				$current_node = wp_list_filter( $site_nodes, array( 'slug' => cp_current_action() ) );

				if ( $current_node ) {
					$current_node = reset( $current_node );
					$title_parts['action'] = $current_node->title;
				}

				if ( cp_current_sub_action() ) {
					$current_subnode = wp_list_filter( $site_nodes, array( 'slug' => cp_current_sub_action() ) );

					if ( $current_subnode ) {
						$current_subnode = reset( $current_subnode );
						$title_parts['subaction'] = $current_subnode->title;
					}
				}
			}
		}

		parent::setup_page_title( $title_parts );
	}

	/**
	 * Return the Site settings
	 *
	 * @since 1.0.0.
	 *
	 * @return  array The list of settings for the Cluster.
	 */
	public function get_settings() {
		$settings = array(
			'sections' => array(
				'site_cluster_main_settings' => array(
					'title'    => __( 'Réglages principaux', 'clusterpress' ),
					'callback' => 'cp_core_main_settings_callback',
				),
				'site_cluster_slug_settings' => array(
					'title'    => __( 'Personnalisation des URLs', 'clusterpress' ),
					'callback' => 'cp_core_slug_settings_callback',
				),
			),
			'fields' => array(
				'site_cluster_main_settings' => array(
					'clusterpress_sites_archive_title' => array(
						'title'             => __( 'Titre de la page d\'archive', 'clusterpress' ),
						'callback'          => 'cp_site_settings_archive_title',
						'sanitize_callback' => 'sanitize_text_field',
					),
					'clusterpress_site_following_enabled' => array(
						'title'             => __( 'Suivi des Sites', 'clusterpress' ),
						'callback'          => 'cp_site_settings_follow_feature',
						'sanitize_callback' => 'intval',
					),
				),
				'site_cluster_slug_settings' => array(
					'clusterpress_site_follow_action_slug' => array(
						'title'             => __( 'Identifiant de l\'action pour déclencher le suivi des sites.', 'clusterpress' ),
						'callback'          => 'cp_site_settings_follow_action_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_sites_slug' => array(
						'title'             => __( 'Portion d\'URL pour l\'Archive des sites.', 'clusterpress' ),
						'callback'          => 'cp_site_settings_archive_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_site_category_slug' => array(
						'title'             => __( 'Portion d\'URL racine pour les catégories de sites.', 'clusterpress' ),
						'callback'          => 'cp_site_settings_category_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_site_slug' => array(
						'title'             => __( 'Portion d\'URL racine pour les pages de découverte du site.', 'clusterpress' ),
						'callback'          => 'cp_site_settings_site_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_site_members_slug' => array(
						'title'             => __( 'Portion d\'URL pour la page affichant les membres du site dans ses pages de découverte ainsi que dans ses pages de gestion.', 'clusterpress' ),
						'callback'          => 'cp_site_settings_members_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_site_followers_slug' => array(
						'title'             => __( 'Portion d\'URL pour la page affichant les adeptes du site dans ses pages de découverte ainsi que dans ses pages de gestion.', 'clusterpress' ),
						'callback'          => 'cp_site_settings_followers_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_site_manage_slug' => array(
						'title'             => __( 'Portion d\'URL racine des pages de gestion du site.', 'clusterpress' ),
						'callback'          => 'cp_site_settings_manage_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_site_settings_slug' => array(
						'title'             => __( 'Portion d\'URL de la page de gestion des réglages du site dans ses pages de découverte.', 'clusterpress' ),
						'callback'          => 'cp_site_settings_site_settings_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_user_posts_slug' => array(
						'title'             => __( 'Portion d\'URL pour la page de profil présentant les articles de l\'utilisateur.', 'clusterpress' ),
						'callback'          => 'cp_user_settings_user_posts_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_user_comments_slug' => array(
						'title'             => __( 'Portion d\'URL pour la page de profil présentant les commentaires de l\'utilisateur.', 'clusterpress' ),
						'callback'          => 'cp_user_settings_user_comments_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_user_following_slug' => array(
						'title'             => __( 'Portion d\'URL pour la page de profil présentant les articles non lus des sites que l\'utilisateur suit.', 'clusterpress' ),
						'callback'          => 'cp_site_settings_user_following_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_user_manage_followed_slug' => array(
						'title'             => __( 'Portion d\'URL pour la page de gestion du profil présentant les sites suivis par l\'utilisateur.', 'clusterpress' ),
						'callback'          => 'cp_site_settings_user_followed_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
				),
			),
		);

		// Remove the Slugs settings if not using pretty URLs
		if ( ! clusterpress()->permalink_structure ) {
			// There's nothing to customize in this case.
			if ( ! is_multisite() ) {
				return array();
			} else {
				unset( $settings['sections']['site_cluster_slug_settings'] );
			}
		}

		if ( ! is_multisite() ) {
			unset( $settings['sections']['site_cluster_main_settings'] );
			$settings['fields']['site_cluster_slug_settings'] = array_intersect_key(
				$settings['fields']['site_cluster_slug_settings'],
				array( 'clusterpress_user_posts_slug' => true, 'clusterpress_user_comments_slug' => true )
			);
		} elseif ( ! cp_site_is_following_enabled() ) {
			unset( $settings['fields']['site_cluster_slug_settings']['clusterpress_site_follow_action_slug'] );
			unset( $settings['fields']['site_cluster_slug_settings']['clusterpress_site_followers_slug'] );
			unset( $settings['fields']['site_cluster_slug_settings']['clusterpress_user_following_slug'] );
			unset( $settings['fields']['site_cluster_slug_settings']['clusterpress_user_manage_followed_slug'] );
		}

		return $settings;
	}

	/**
	 * Add the site's followed feature to the "post" post type.
	 *
	 * @since 1.0.0
	 *
	 * @param array $post_types List of post type names.
	 * @param array $features   List of post type features.
	 */
	public function add_post_types_support( $post_types = array(), $features = array() ) {
		parent::add_post_types_support( array( 'post' ), array( 'clusterpress_site_followed_posts' ) );
	}

	/**
	 * Define Rest API routes for the Site's following feature.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list of Rest API Routes.
	 */
	public function get_routes() {
		// Only add Rest routes on the main site.
		if ( ! cp_is_main_site() || ! cp_site_is_following_enabled() ) {
			return parent::get_routes();
		}

		$follow_slug          = cp_site_get_follow_action_slug();
		$user_endpoint        = sprintf( '%1$s/%2$s/(?P<id>\d+)', cp_user_get_slug(), $follow_slug );
		$user_post_endpoint   = $user_endpoint . '/(?P<relationship_id>\d+)' ;
		$user_delete_endpoint = $user_endpoint . '/(?P<follow_id>\d+)' ;

		$args = array(
			'id' => array(
				'validate_callback' => function( $param, $request, $key ) {
					return is_numeric( $param );
				}
			),
		);

		return array(
			array(
				'endpoint'            => $user_post_endpoint,
				'methods'             => 'GET',
				'callback'            => 'cp_site_get_user_followed_sites_new_posts',
				'permission_callback' => 'cp_site_user_can_get_followed_site_new_posts',
				'args'                => $args,
			),
			array(
				'endpoint'            => $user_endpoint,
				'methods'             => 'POST',
				'callback'            => 'cp_site_user_follow_site',
				'permission_callback' => 'cp_site_user_can_follow_site',
				'args'                => $args,
			),
			array(
				'endpoint'            => $user_delete_endpoint,
				'methods'             => 'DELETE',
				'callback'            => 'cp_site_user_unfollow_site',
				'permission_callback' => 'cp_site_user_can_follow_site',
				'args'                => $args,
			),
		);
	}

	/**
	 * Return the specific feedbacks for the Site Cluster.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list of feedbacks specific to this Cluster.
	 */
	public function get_feedbacks() {
		return array(
			'settings-01'       => __( 'Réglages mis à jour avec succès.', 'clusterpress' ),
			'settings-02'       => __( 'Une erreur est survenue lors de la mise à jour des réglages.', 'clusterpress' ),
			'not-allowed-01'    => __( 'Vous n\'êtes pas autorisé à modifier les réglages du site.', 'clusterpress' ),
			'not-allowed-02'    => __( 'Vous n\'êtes pas autorisé à modifier les catégories du site.', 'clusterpress' ),
			'not-allowed-03'    => __( 'Vous n\'êtes pas autorisé à modifier les membres du site.', 'clusterpress' ),
			'site-member-01'    => __( 'Le nouveau rôle de l\'utilisateur a été sauvegardé avec succès.', 'clusterpress' ),
			'site-member-02'    => __( 'L\'utilisateur a été supprimé du site.', 'clusterpress' ),
			'site-member-03'    => __( 'Une erreur est survenue durant la modification du ou des utilisateurs.', 'clusterpress' ),
			'site-member-04'    => __( 'Le nouveau rôle des utilisateurs sélectionnés a été sauvegardé avec succès.', 'clusterpress' ),
			'site-member-05'    => __( 'Les utilisateurs ont été supprimé du site.', 'clusterpress' ),
			'site-member-06'    => __( 'Vous ne pouvez pas vous auto-gérer depuis cet écran.', 'clusterpress' ),
			'site-follower-01'  => __( 'L\' utilisateur a été retrogradé en tant qu\'adepte.', 'clusterpress' ),
			'site-follower-02'  => __( 'Les utilisateurs ont été retrogradés en tant qu\'adepte.', 'clusterpress' ),
			'site-followed-01'  => array( 'cluster' => 'user', 'message' => __( 'L\'article n\'a pas été trouvé.', 'clusterpress' ) ),
			'site-followed-02'  => array( 'cluster' => 'user', 'message' => __( 'L\'article n\'a pas été marqué comme lu.', 'clusterpress' ) ),
			'site-followed-03'  => array( 'cluster' => 'user', 'message' => __( 'L\'article a été marqué comme lu.', 'clusterpress' ) ),
			'site-followed-04'  => array( 'cluster' => 'user', 'message' => __( 'Les articles n\'ont pas été trouvés.', 'clusterpress' ) ),
			'site-followed-05'  => array( 'cluster' => 'user', 'message' => __( 'Les articles n\'ont pas été marqués comme lus.', 'clusterpress' ) ),
			'site-followed-06'  => array( 'cluster' => 'user', 'message' => __( 'Les articles ont été marqués comme lus.', 'clusterpress' ) ),
		);
	}

	/**
	 * Return the specific cap maps for the Site Cluster.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list cap maps specific to this Cluster.
	 */
	public function get_caps_map() {
		return array(
			'cp_read_sites_archive' => 'cp_sites_archive_required_caps',
			'cp_read_single_site'   => 'cp_site_read_single_required_caps',
			'cp_edit_single_site'   => 'cp_site_edit_single_required_caps',
			'cp_post_single_user'   => 'cp_site_post_single_user_required_caps',
			'cp_assign_site_terms'  => 'cp_site_edit_single_required_caps',
			'cp_can_follow_site'    => 'cp_site_follow_required_caps',
		);
	}

	/**
	 * Install some SQL tables on Multisite configs.
	 *
	 * @since  1.0.0
	 */
	public function install() {
		global $wpdb;

		// We only need to add tables on multisite configs.
		if ( ! is_multisite() ) {
			return;
		}

		$sql     = array();
		$charset = $wpdb->get_charset_collate();
		$prefix  = $wpdb->base_prefix;

		$sql[] = "CREATE TABLE {$prefix}cp_user_sites (
				id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				user_id bigint(20) NOT NULL,
				site_id bigint(20) NOT NULL,
				user_level tinyint(1) NOT NULL DEFAULT '0',
				KEY user_id (user_id),
				KEY site_id (site_id),
				KEY user_level (user_level),
				UNIQUE KEY user_sites ( user_id, site_id )
			) {$charset};";

		$sql[] = "CREATE TABLE {$prefix}cp_blogmeta (
				meta_id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				blog_id bigint(20) NOT NULL,
				meta_key varchar(255) DEFAULT NULL,
				meta_value longtext DEFAULT NULL,
				KEY blog_id (blog_id),
				KEY meta_key (meta_key(191))
			) {$charset};";

		dbDelta( $sql );
	}

	/**
	 * Return the Post install batch tasks required by the Site Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @return array The list of Post install batch tasks to run.
	 */
	public function get_cluster_tasks() {
		if ( ! is_multisite() ) {
			return parent::get_cluster_tasks();
		}

		$network_id         = get_main_network_id();
		$network_site_count = get_network_option( (int) $network_id, 'blog_count', 0 );

		if ( 1 >= (int) $network_site_count ) {
			return parent::get_cluster_tasks();
		}

		$meta_sites = cp_sites_meta_get_sites();

		if ( (int) $network_site_count === count( $meta_sites ) ) {
			return parent::get_cluster_tasks();
		}

		return array(
			array(
				'callback'  => 'cp_sites_reset_site_metadata',
				'count'     => 'cp_sites_reset_site_metadata_get_count',
				'message'   => _x( 'Restauration des méta données du ou des sites.', 'ClusterPress Batch Processor feedback message', 'clusterpress' ),
				'number'    => 1,
			),
			array(
				'callback'  => 'cp_sites_save_site_metadata',
				'count'     => 'cp_sites_save_site_metadata_get_count',
				'message'   => _x( 'Sauvegarde des méta données du ou des sites cours - %d site(s) à mettre à jour.', 'ClusterPress Batch Processor feedback message', 'clusterpress' ),
				'number'    => $network_site_count >= 5 ? 5 : 1,
			),
		);
	}

	/**
	 * Prefetch the Site categories if needed.
	 *
	 * @since  1.0.0
	 */
	public function set_site_categories() {
		if ( cp_is_main_site() ) {
			$this->categories = get_terms( array( 'taxonomy' => cp_sites_get_taxonomy(), 'hide_empty' => false ) );
		}
	}
}
