<?php
/**
 * Site Metas loop.
 *
 * @package ClusterPress\site\classes
 * @subpackage site-metas-loop
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Site Metas loop Class.
 *
 * Used inside the Site's settings front-end screen.
 *
 * @since 1.0.0
 */
class CP_Site_Metas_Loop extends CP_Cluster_Loop {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args the loop args {
	 *    An array of arguments.
	 *    @type array $exclude The list of meta keys to exclude.
	 *    @type int   $site_id The Site ID. Defaults to the displayed site's ID.
	 * }
	 */
	public function __construct( $args = array() ) {
		$r = wp_parse_args( $args, array(
			'exclude' => array(),
			'site_id' => cp_get_displayed_site_id(),
		) );

		// Init setting fields and site metas
		$site_settings = array();

		if ( ! empty( $r['site_id'] ) ) {
			// Get registered site metas and existing values.
			$site_meta_keys   = get_registered_meta_keys( 'blog' );
			$site_meta_values = get_registered_metadata( 'blog', (int) $r['site_id'] );

			// Get the meta screens callback
			$cp_meta_screens = clusterpress()->site->meta_screens;

			if ( ! empty( $r['exclude'] ) && ! empty( $site_meta_keys ) ) {
				$exclude = array_fill_keys( (array) $r['exclude'], true );

				$site_meta_keys = array_diff_key( $site_meta_keys, $exclude );
			}

			foreach ( $site_meta_keys as $meta_key => $meta_infos ) {
				$display_callbacks = array();
				$meta_value        = array();

				if ( isset( $cp_meta_screens[ $meta_key ] ) ) {
					$display_callbacks = $cp_meta_screens[ $meta_key ];
				}

				if ( isset( $site_meta_values[ $meta_key ] ) ) {
					if ( 1 === count( $site_meta_values[ $meta_key ] ) && true === $meta_infos['single'] ) {
						$meta_value = array( 'value' => reset( $site_meta_values[ $meta_key ] ) );
					} else {
						$meta_value = array( 'value' => $site_meta_values[ $meta_key ] );
					}
				} else {
					$meta_value = array( 'value' => null );
				}

				$site_settings[] = (object) array_merge(
					array( 'id' => $meta_key ),
					$meta_value,
					$meta_infos,
					$display_callbacks
				);
			}
		}

		// Get count.
		$metas_count = count( $site_settings );

		parent::start( array(
			'plugin_prefix'    => 'cp',
			'item_name'        => 'site_meta',
			'item_name_plural' => 'site_metas',
			'items'            => $site_settings,
			'total_item_count' => $metas_count,
			'page'             => 1,
			'per_page'         => $metas_count,
		) );
	}
}
