<?php
/**
 * Sites Followed Posts loop.
 *
 * @package ClusterPress\site\classes
 * @subpackage sites-followed-posts-loop
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Followed sites latest post loop Class.
 *
 * @since 1.0.0
 */
class CP_Sites_Followed_Post_Loop extends CP_Cluster_Loop {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args the loop args
	 */
	public function __construct( $args = array() ) {
		$r = wp_parse_args( $args, array(
			'offset'  => 0,
			'number'  => 10,
			'user_id' => get_current_user_id(),
		) );

		$paged = get_query_var( 'paged' );

		if ( ! $paged ) {
			if ( ! empty( $_GET['paged'] ) ) {
				$paged = absint( $_GET['paged'] );

			// Default to first page
			} else {
				$paged = 1;
			}
		}

		if ( ! $r['offset'] && ! empty( $r['number'] ) ) {
			$r['offset'] = (int) ( ( $paged - 1 ) * (int) $r['number'] );
		}

		$followed_sites_posts = cp_sites_get_user_followed_sites_new_posts( $r['user_id'] );
		rsort( $followed_sites_posts );

		$followed_sites_posts_paged = array_slice( $followed_sites_posts, $r['offset'], $r['number'] );

		$followed_sites = array();
		$sites          = wp_list_pluck( $followed_sites_posts_paged, 'secondary_id' );
		if ( ! empty( $sites ) ) {
			foreach ( array_unique( $sites ) as $site_id ) {
				$followed_sites[ $site_id ] = (object) array(
					'url'  => cp_sites_get_meta( $site_id, 'url', true ),
					'name' => cp_sites_get_meta( $site_id, 'name', true ),
				);
			}
		}

		// Add Post Url, Site Url & Name to posts.
		foreach ( $followed_sites_posts_paged as $p => $followed_sites_post ) {
			if ( ! isset( $followed_sites[ $followed_sites_post->secondary_id ] ) ) {
				continue;
			}

			$followed_sites_posts_paged[ $p ]->site_url  = trailingslashit( $followed_sites[ $followed_sites_post->secondary_id ]->url );
			$followed_sites_posts_paged[ $p ]->site_name = $followed_sites[ $followed_sites_post->secondary_id ]->name;
		}

		// Defaults to no pretty links.
		$paginate_args = array( 'base' => add_query_arg( 'paged', '%#%' ) );

		// Pretty links
		if ( clusterpress()->permalink_structure ) {
			/**
			 * The paginate base needs to be set using the
			 * 'cp_sites_followed_posts_loop_paginate_args' filter.
			 */
			$paginate_args['base']        = '%_%';
			$paginate_args['format']      = cp_get_paged_slug() . '/%#%/';
			$paginate_args['remove_args'] = array( 'updated', 'error' );
		}

		parent::start( array(
			'plugin_prefix'    => 'cp',
			'item_name'        => 'post',
			'item_name_plural' => 'posts',
			'items'            => $followed_sites_posts_paged,
			'total_item_count' => count( $followed_sites_posts ),
			'page'             => $paged,
			'per_page'         => $r['number'],

		/**
		 * Filter here to edit paginate args according to the context.
		 *
		 * @since 1.0.0
		 *
		 * @param array $paginate_args The arguments of the pagination.
		 */
		), apply_filters( 'cp_sites_followed_posts_loop_paginate_args', $paginate_args ) );
	}
}
