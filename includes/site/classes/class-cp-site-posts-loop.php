<?php
/**
 * Site Posts loop.
 *
 * @package ClusterPress\site\classes
 * @subpackage site-posts-loop
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Site Posts loop Class.
 *
 * @since 1.0.0
 */
class CP_Site_Posts_Loop extends CP_Cluster_Loop {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args the loop args
	 */
	public function __construct( $args = array() ) {
		$paged = get_query_var( 'paged' );

		if ( ! $paged ) {
			if ( ! empty( $_GET['paged'] ) ) {
				$paged = absint( $_GET['paged'] );

			// Default to first page
			} else {
				$paged = 1;
			}
		}

		$r = wp_parse_args( $args, array(
			'paged'               => $paged,
			'posts_per_page'      => 10,
			'post_type'           => 'post',
			'ignore_sticky_posts' => true,
			'post_status'         => 'publish',
		) );

		// Do not run a query if included posts contains a 0 value
		if ( isset( $r['post__in'] ) && 0 === $r['post__in'] ) {
			$p = array();
			$t = 0;

		} else {
			$q = new WP_Query( $r );

			$p = $q->posts;
			$t = $q->found_posts;
		}

		// Defaults to no pretty links.
		$paginate_args = array( 'base' => add_query_arg( 'paged', '%#%' ) );

		// Pretty links
		if ( clusterpress()->permalink_structure ) {
			/**
			 * The paginate base needs to be set using the
			 * 'cp_sites_posts_loop_paginate_args' filter.
			 */
			$paginate_args['base']   = '%_%';
			$paginate_args['format'] = cp_get_paged_slug() . '/%#%/';
		}

		$loop_args = array(
			'plugin_prefix'    => 'cp',
			'item_name'        => 'post',
			'item_name_plural' => 'posts',
			'items'            => $p,
			'total_item_count' => $t,
			'page'             => $paged,
			'per_page'         => $r['posts_per_page'],
		);

		if ( ! empty( $r['no_pagination'] ) ) {
			$loop_args['no_pagination'] = (bool) $r['no_pagination'];
		}

		parent::start( $loop_args,

		/**
		 * Filter here to edit paginate args according to the context.
		 *
		 * @since 1.0.0
		 *
		 * @param array $paginate_args The arguments of the pagination.
		 */
		apply_filters( 'cp_sites_posts_loop_paginate_args', $paginate_args ) );
	}
}
