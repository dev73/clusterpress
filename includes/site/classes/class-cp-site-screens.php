<?php
/**
 * Site Screens Manager.
 *
 * @package ClusterPress\site\classes
 * @subpackage site-screens
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Replace the content by the site templates
 *
 * @since  1.0.0
 */
class CP_Site_Screens {

	/**
	 * The constructor
	 *
	 * Only filter the content once wp_head was fired.
	 *
	 * @since  1.0.0
	 */
	public function __construct() {
		if ( doing_action( 'wp_head' ) || ( ! cp_is_site() && ! cp_is_sites() ) ) {
			return;
		}

		add_filter( 'cp_the_content', array( $this, 'site_content' ), 10, 1 );
	}

	/**
	 * Launch the Site's Screens manager.
	 *
	 * @since 1.0.0
	 *
	 * @return CP_Sites_Screen $screens The instance of the class.
	 */
	public static function start() {
		$cp = clusterpress();

		if ( empty( $cp->site->screens ) ) {
			$cp->site->screens = new self();
		}

		return $cp->site->screens;
	}

	/**
	 * Buffer the Site's root template.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $content The Post content.
	 * @return string          The ClusterPress Site's content.
	 */
	public function site_content( $content = '' ) {
		$template_part = 'site/archive/index';

		if ( cp_is_site() ) {
			$template_part = 'site/single/index';
		}

		return cp_buffer_template_part( $template_part, null, false );
	}
}
add_action( 'cp_do_templates', array( 'CP_Site_Screens', 'start' ), 10 );
