<?php
/**
 * Sites loop.
 *
 * @package ClusterPress\site\classes
 * @subpackage sites-loop
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Sites loop Class.
 *
 * @since 1.0.0
 */
class CP_Sites_Loop extends CP_Cluster_Loop {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args the loop args
	 */
	public function __construct( $args = array() ) {
		$r = wp_parse_args( $args, array(
			'offset'     => 0,
			'number'     => 10,
			'user_level' => false,
		) );

		$paged = get_query_var( 'paged' );

		if ( ! $paged ) {
			if ( ! empty( $_GET['paged'] ) ) {
				$paged = absint( $_GET['paged'] );

			// Default to first page
			} else {
				$paged = 1;
			}
		}

		if ( ! $r['offset'] && ! empty( $r['number'] ) ) {
			$r['offset'] = (int) ( ( $paged - 1 ) * (int) $r['number'] );
		}

		$users = cp_get_sites( $r );

		// Defaults to no pretty links.
		$paginate_args = array( 'base' => add_query_arg( 'paged', '%#%' ) );

		// Pretty links
		if ( clusterpress()->permalink_structure ) {
			$paginate_args['base'] = trailingslashit( cp_get_archive_link( 'site' ) ) . '%_%';

			// Are we displaying a site category ?
			if ( cp_current_taxonomy() ) {
				$paginate_args['base'] = trailingslashit( cp_sites_get_category_link( cp_get_current_term_id() ) ) . '%_%';
			}

			$paginate_args['format'] = cp_get_paged_slug() . '/%#%/';
		}

		parent::start( array(
			'plugin_prefix'    => 'cp',
			'item_name'        => 'site',
			'item_name_plural' => 'sites',
			'items'            => $users['sites'],
			'total_item_count' => $users['total'],
			'page'             => $paged,
			'per_page'         => $r['number'],

		/**
		 * Filter here to edit paginate args according to the context.
		 *
		 * @since 1.0.0
		 *
		 * @param array $paginate_args The arguments of the pagination.
		 */
		), apply_filters( 'cp_sites_loop_paginate_args', $paginate_args ) );
	}
}
