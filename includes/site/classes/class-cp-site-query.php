<?php
/**
 * ClusterPress Site Query Class.
 *
 * @package ClusterPress\site\classes
 * @subpackage site-query
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * ClusterPress class used for querying sites.
 *
 * @since 1.0.0
 *
 * @see CP_Site_Query::__construct() for accepted arguments.
 */
class CP_Site_Query extends WP_Site_Query {

	/**
	 * Get sites of a network according to a given request.
	 *
	 * @since  1.0.0
	 *
	 * @param string|array $query {
	 *     Optional. Array or query string of site query parameters. Default empty.
	 *     @see WP_Site_Query::__construct() for WordPress arguments.
	 *     Below are arguments specific to ClusterPress.
	 *
	 *     @type int         $user_id        Optional. ID of the user whose blogs are being
	 *                                       retrieved. Default: 0.
	 *     @type string|bool $search_terms   Optional. Search by text stored in
	 *                                       blogmeta (such as the blog name). Default: false.
	 * }
	 */
	public function __construct( $query = '' ) {
		// Set filters.
		$this->set_filters();

		// Set defaults query vars.
		parent::__construct();

		// Set ClusterPress specific query vars.
		$this->set_cp_query( $query );

		// If sites are not already set, run the query
		if ( ! isset( $this->sites ) ) {
			// Get the matching sites
			$this->get_sites();

			// Get extra data for each found sites.
			$this->get_site_metas();
		}
	}

	/**
	 * Set custom filters to edit WordPress query
	 *
	 * @since  1.0.0
	 */
	private function set_filters() {
		add_filter( 'sites_clauses', array( $this, 'edit_query' ), 10, 1 );
	}

	/**
	 * Get sites of a network according to a given request.
	 *
	 * @since  1.0.0
	 *
	 * @param string|array $query Array or string of CP_Site_Query arguments. See CP_Site_Query::__construct().
	 */
	private function set_cp_query( $query = '' ) {
		$this->query_vars = wp_parse_args( $query, array(
			'offset'   => 0,
			'number'   => 20,
			/**
			 * Make sure to not display
			 * the following site status:
			 */
			'mature'   => 0,
			'spam'     => 0,
			'archived' => 0,
			'deleted'  => 0,
		) );

		// Get total for loop queries.
		if ( ! empty( $this->query_vars['count_total'] ) ) {
			$this->query_vars['no_found_rows'] = false;
		}

		/**
		 * Use the WP_Site_Query 'site__in' argument to limit fetched sites to the ones
		 * the user is a member or a follower of.
		 */
		if ( ! empty( $this->query_vars['user_id'] ) ) {

			$user_level = false;
			if ( isset( $this->query_vars['user_level'] ) ) {
				$user_level = $this->query_vars['user_level'];
			}

			$user_sites = cp_sites_get_user_sites( $this->query_vars['user_id'], $user_level );

			if ( empty( $user_sites ) ) {
				$this->sites       = array();
				$this->found_sites = 0;
				return;
			}

			if ( ! empty( $this->query_vars['site__in'] ) ) {
				$intersect = array_intersect( $user_sites, $this->query_vars['site__in'] );

				if ( ! empty( $intersect ) ) {
					$user_sites = $intersect;
				}
			}

			$this->query_vars['site__in'] = $user_sites;
		}

		/**
		 * Use the WP_Site_Query 'site__in' argument to limit fetched sites to the ones
		 * that are assigned to the queried category.
		 */
		if ( ! empty( $this->query_vars['category'] ) ) {
			$category_sites = get_objects_in_term( $this->query_vars['category'], cp_sites_get_taxonomy() );

			if ( empty( $category_sites ) ) {
				$this->sites       = array();
				$this->found_sites = 0;
				return;
			}

			if ( ! empty( $this->query_vars['site__in'] ) ) {
				$intersect = array_intersect( $category_sites, $this->query_vars['site__in'] );

				if ( ! empty( $intersect ) ) {
					$category_sites = $intersect;
				}
			}

			$this->query_vars['site__in'] = $category_sites;
		}

		/**
		 * In case of a search, build a Meta Query to look inside 'name' and
		 * 'description' metadatas.
		 */
		if ( ! empty( $this->query_vars['cp_search'] ) ) {
			$this->query_vars['meta_query'] = array(
				'relation' => 'OR',
				array(
					'key'     => 'name',
					'value'   => $this->query_vars['cp_search'],
					'compare' => 'LIKE',
				),
				array(
					'key'     => 'description',
					'value'   => $this->query_vars['cp_search'],
					'compare' => 'LIKE',
				),
			);
		}

		/**
		 * We won't display the main site in the sites loop
		 * or in the single site view.
		 */
		$exclude_main_site = array( (int) get_current_network_id() );

		if ( ! empty( $this->query_vars['site__not_in'] ) ) {
			$this->query_vars['site__not_in'] = array_merge( $exclude_main_site, (array) $this->query_vars['site__not_in'] );
		} else {
			$this->query_vars['site__not_in'] = $exclude_main_site;
		}
	}

	/**
	 * Loop in found sites to attach metadatas.
	 *
	 * @since  1.0.0
	 */
	private function get_site_metas() {
		if ( is_null( $this->sites ) || ( isset( $this->query_vars['fields'] ) && 'ids' === $this->query_vars['fields'] ) ) {
			return;
		}

		$site_ids       = $this->get_ids();
		$cp             = clusterpress();
		$has_categories = ! empty( $cp->site->categories );

		// It can happen when the main requests is only fetching site ids.
		if ( empty( $site_ids ) ) {
			return;
		}

		$sites = array();

		foreach ( $this->sites as $ak => $site ) {

			$site_metas = cp_sites_get_meta( $site->blog_id );

			foreach ( $site_metas as $key => $meta ) {
				// Don't fetch private sitemeta
				if ( is_protected_meta( $key ) ) {
					continue;
				}

				if ( 1 === count( $meta ) ) {
					$site->{$key} = maybe_unserialize( $meta[0] );
				} else {
					$site->{$key} = array_map( 'maybe_unserialize', $meta );
				}
			}

			// @todo Find a way to cache ?
			if ( $has_categories ) {
				$site->categories = wp_get_object_terms( $site->blog_id, cp_sites_get_taxonomy(), array( 'fields' => 'ids' ) );
			}

			// Reindex for easier matching.
			$sites[ $site->blog_id ] = $site;
		}

		$this->sites = apply_filters( 'cp_site_query_after_metas', $sites );
	}

	/**
	 * Edit the query to add ClusterPress custom orders or search features.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $pieces A compacted array of site query clauses.
	 * @return array          The same/edited compacted array of site query clauses.
	 */
	public function edit_query( $pieces = array() ) {
		global $wpdb;

		// Bail if we don't need to edit the query
		if ( empty( $this->query_vars['meta_query'] ) && empty( $this->query_vars['orderby'] ) ) {
			return $pieces;
		}

		// There's no WordPress blogmeta table for now but it can change!
		if ( isset( $wpdb->blogmeta ) ) {
			$reset_blogmeta = $wpdb->blogmeta;
		}

		// Use the ClusterPress metadata table
		$wpdb->blogmeta = $wpdb->base_prefix . 'cp_blogmeta';

		// Add the main table name to each existing $where clauses to avoid SQL warnings.
		if ( ! empty( $pieces['where'] ) && ! empty( $this->sql_clauses['where'] ) && $pieces['where'] === join( ' AND ', $this->sql_clauses['where'] ) ) {
			$possible_fields = array_diff_key( $this->query_var_defaults, array(
				'fields'            => false,
				'number'            => false,
				'offset'            => false,
				'no_found_rows'     => false,
				'orderby'           => false,
				'order'             => false,
				'search_columns'    => false,
				'count'             => false,
				'date_query'        => false,
			) );

			$sql_clauses = array();
			foreach ( array_keys( $possible_fields ) as $key ) {
				if ( empty( $this->sql_clauses['where'][$key] ) ) {
					continue;
				}

				$kc = $key;

				if ( 0 === strpos( $key, 'site' ) ) {
					$kc = 'blog_id';
				} elseif ( 0 === strpos( $key, 'network' ) ) {
					$kc = 'site_id';
				}

				$sql_clauses[$key] = str_replace( $kc, $wpdb->blogs . '.' . $kc,  $this->sql_clauses['where'][$key] );
			}

			if ( ! empty( $sql_clauses ) ) {
				$pieces['where'] = join( ' AND ', $sql_clauses );
			}
		}

		/** Meta Queries *****************************************************/

		if ( ! empty( $this->query_vars['meta_query'] ) ) {
			$meta_query = new WP_Meta_Query( $this->query_vars['meta_query'] );

			$parts = $meta_query->get_sql( 'blog', $wpdb->blogs, 'blog_id' );

			if ( 'blog_id' === $pieces['fields'] ) {
				$pieces['fields'] = "{$wpdb->blogs}.blog_id";
			}

			if ( ! empty( $parts['join'] ) ) {
				if ( ! isset( $pieces['join'] ) ) {
					$pieces['join'] = $parts['join'];
				} else {
					$pieces['join'] .= $parts['join'];
				}
			}

			if ( ! empty( $parts['where'] ) ) {
				if ( empty( $pieces['where'] ) ) {
					$pieces['where'] = '1=1' . $parts['where'];
				} else {
					$pieces['where'] .= $parts['where'];
				}
			}
		}

		/** Alphabetical order ******************************************************/

		if ( ! empty( $this->query_vars['orderby'] ) && 'name' === $this->query_vars['orderby'] ) {
			if ( 'blog_id' === $pieces['fields'] ) {
				$pieces['fields'] = "{$wpdb->blogs}.blog_id";
			}

			$pieces['fields'] .= sprintf( ', bm.meta_value as %s', $this->query_vars['orderby'] );
			$join = " LEFT JOIN {$wpdb->blogmeta} bm ON ({$wpdb->blogs}.blog_id = bm.blog_id )";

			if ( empty( $pieces['join'] ) ) {
				$pieces['join']  = $join;
			} else {
				$pieces['join'] .= $join;
			}

			$where = sprintf( " AND bm.meta_key = '%s'", $this->query_vars['orderby'] );

			if ( empty( $pieces['where'] ) ) {
				$pieces['where'] = '1=1' . $where;
			} else {
				$pieces['where'] .= $where;
			}

			$pieces['orderby'] = ' ' . $this->query_vars['orderby'] . ' ' . $this->query_vars['order'] ;
		}

		// Reset the global blogmeta table if needed.
		if ( ! empty( $reset_blogmeta ) ) {
			$wpdb->blogmeta = $reset_blogmeta;
		}

		return $pieces;
	}

	/**
	 * Get results.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list of sites.
	 */
	public function get_results() {
		if ( isset( $this->query_vars['fields'] ) && 'ids' === $this->query_vars['fields'] ) {
			$this->get_ids();
		}

		return array_values( $this->sites );
	}

	/**
	 * Get the total for the query.
	 *
	 * @since  1.0.0
	 *
	 * @return int The total.
	 */
	public function get_total() {
		if ( $this->query_vars['no_found_rows'] ) {
			return false;
		}

		return $this->found_sites;
	}

	/**
	 * Return the site ids found.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list of site ids.
	 */
	public function get_ids() {
		return array_map( 'intval', wp_list_pluck( $this->sites, 'blog_id' ) );
	}
}
