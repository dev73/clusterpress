<?php
/**
 * Site Comments loop.
 *
 * @package ClusterPress\site\classes
 * @subpackage site-comments-loop
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Site Comments loop Class.
 *
 * @since 1.0.0
 */
class CP_Site_Comments_Loop extends CP_Cluster_Loop {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args the loop args
	 */
	public function __construct( $args = array() ) {
		$offset = 0;
		$number = 10;

		$r = wp_parse_args( $args, array(
			'offset'    => $offset,
			'number'    => $number,
			'status'    => 'approve',
			'post_type' => 'post',
		) );

		$paged = get_query_var( 'paged' );

		if ( ! $paged ) {
			if ( ! empty( $_GET['paged'] ) ) {
				$paged = absint( $_GET['paged'] );

			// Default to first page
			} else {
				$paged = 1;
			}
		}

		if ( ! $r['offset'] && ! empty( $r['number'] ) ) {
			$r['offset'] = (int) ( ( $paged - 1 ) * (int) $r['number'] );
		}

		// Do not run a query if included comments contains a 0 value
		if ( isset( $r['comment__in'] ) && 0 === $r['comment__in'] ) {
			$c = array();
			$t = 0;

		} else {
			$c = get_comments( $r );
			$t = get_comments( array_merge( $r, array( 'offset' => $offset, 'number' => $number, 'count' => true ) ) );
		}

		// Defaults to no pretty links.
		$paginate_args = array( 'base' => add_query_arg( 'paged', '%#%' ) );

		// Pretty links
		if ( clusterpress()->permalink_structure ) {
			/**
			 * The paginate base needs to be set using the
			 * 'cp_sites_comments_loop_paginate_args' filter.
			 */
			$paginate_args['base']   = '%_%';
			$paginate_args['format'] = cp_get_paged_slug() . '/%#%/';
		}

		parent::start( array(
			'plugin_prefix'    => 'cp',
			'item_name'        => 'comment',
			'item_name_plural' => 'comments',
			'items'            => $c,
			'total_item_count' => $t,
			'page'             => $paged,
			'per_page'         => $r['number'],

		/**
		 * Filter here to edit paginate args according to the context.
		 *
		 * @since 1.0.0
		 *
		 * @param array $paginate_args The arguments of the pagination.
		 */
		), apply_filters( 'cp_sites_comments_loop_paginate_args', $paginate_args ) );
	}
}
