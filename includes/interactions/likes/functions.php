<?php
/**
 * ClusterPress Likes Functions.
 *
 * @package ClusterPress\interactions\likes
 * @subpackage functions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Check if Likes are enabled for the current site.
 *
 * @since  1.0.0
 *
 * @return bool True if likes are enabled. False otherwise.
 */
function cp_interactions_are_likes_enabled() {
	$return = cp_interactions_is_like_enabled();

	/**
	 * If the site cluster is not active, don't show the Like
	 * button on sub sites.
	 */
	if ( true === $return && ! cp_is_main_site() ) {
		$return = cp_cluster_is_enabled( 'site' );
	}

	return $return;
}

/**
 * Add a new like relationship.
 *
 * @since  1.0.0
 *
 * @param  array  $args {
 *     An array of arguments.
 *     @type int    $user_id      The user ID who liked the content.
 *     @type int    $primary_id   The object ID (Post or Comment).
 *     @type int    $secondary_id The site ID
 *     @type string $name         The name of the registered relationship.
 * }
 * @return false|int The relationship ID on success. False otherwise.
 */
function cp_interactions_add_like( $args = array() ) {
	$r = wp_parse_args( $args, array(
		'user_id'      => get_current_user_id(),
		'primary_id'   => 0,
		'secondary_id' => get_current_blog_id(),
		'name'         => 'liked_post',
	) );

	if ( empty( $r['user_id'] ) || empty( $r['primary_id'] ) || empty( $r['secondary_id'] ) || empty( $r['name'] ) ) {
		return false;
	}

	$like_id = cp_user_add_relationship( array(
		'user_id'        => (int) $r['user_id'],
		'primary_id'     => (int) $r['primary_id'],
		'secondary_id'   => (int) $r['secondary_id'],
		'name'           => sanitize_key( $r['name'] ),
		'date'           => current_time( 'mysql', 1 ),
		'last_insert_id' => true,
	) );

	/**
	 * Hook here to perform custom actions once the mention is recorded
	 *
	 * @since 1.0.0
	 *
	 * @param int   $like_id The Likes relationship ID.
	 * @param array $r       The LIkes relationship arguments.
	 */
	do_action( 'cp_interactions_add_like', $like_id, $r );

	return $like_id;
}

/**
 * Get the a list of likes relationship.
 *
 * @since  1.0.0
 *
 * @param  array  $where   {
 *     An array of arguments.
 *     @type int    $user_id      The user ID who liked the content.
 *     @type int    $primary_id   The object ID (Post or Comment).
 *     @type int    $secondary_id The site ID
 *     @type string $name         The name of the registered relationship.
 * }
 * @param  array  $columns a list of table columns to get in the SQL results.
 * @return array  The list of likes relationship.
 */
function cp_interactions_get_likes( $where = array(), $columns = array() ) {
	$r = wp_parse_args( $where, array(
		'user_id'      => 0,
		'primary_id'   => 0,
		'secondary_id' => get_current_blog_id(),
		'name'         => 'liked_post',
	) );

	if ( empty( $r['primary_id'] ) && empty( $r['user_id'] ) ) {
		return false;
	}

	$likes = array();

	if ( ! cp_user_relationships_isset() || empty( $r['name'] ) || empty( $columns ) ) {
		return $likes;
	}

	$likes = clusterpress()->user->relationships->get( $columns, array_filter( $r ) );

	return $likes;
}

/**
 * Get the total count for a like relationship on a site.
 *
 * @since  1.0.0
 *
 * @param  string $object The type of object
 * @return int            The likes count for the specific relationship name.
 */
function cp_interactions_get_object_likes_count( $object = 'post' ) {
	if ( empty( $object ) ) {
		return false;
	}

	$where = array(
		'secondary_id' => get_current_blog_id(),
		'name'         => sprintf( 'liked_%s', $object ),
	);

	$likes = array();

	if ( ! cp_user_relationships_isset() ) {
		return $likes;
	}

	$likes = clusterpress()->user->relationships->count( 'id', $where, 'primary_id' );

	return $likes;
}

/**
 * Delete a like relationship entry from the table.
 *
 * @since  1.0.0
 *
 * @param  array  $args {
 *     An array of arguments.
 *     @type int    $id           The like relationship ID.
 *     @type int    $user_id      The user ID who liked the content.
 *     @type int    $primary_id   The object ID (Post or Comment).
 *     @type int    $secondary_id The site ID
 *     @type string $name         The name of the registered relationship.
 * }
 * @return bool   True when the relationship is deleted. False otherwise.
 */
function cp_interactions_delete_like( $args = array() ) {
	$r = wp_parse_args( $args, array(
		'id'           => 0,
		'user_id'      => 0,
		'primary_id'   => 0,
		'secondary_id' => 0,
		'name'         => '',
	) );

	$r = array_filter( $r );

	if ( empty( $r ) ) {
		return false;
	}

	$deleted = cp_user_remove_relationship( $r );

	if ( $deleted ) {
		/**
		 * Hook here to perform custom actions once the like is deleted.
		 *
		 * @since 1.0.0
		 *
		 * @param array $r The Likes relationship arguments of the deleted like.
		 */
		do_action( 'cp_interactions_deleted_like', $r );
	}

	return $deleted;
}

/**
 * User Capabilities callbacks to check the user can view likes.
 *
 * @since  1.0.0
 *
 * @param  int   $user_id The user ID.
 * @param  array $args    A list of contextual arguments.
 * @return array $caps    The user's actual capabilities.
 */
function cp_interactions_read_user_likes( $user_id = 0, $args = array() ) {

	// By default use the default user ClusterPress caps.
	$caps = cp_user_read_single_required_caps( $user_id, $args );

	/**
	 * Filter here to edit the user capabilites
	 *
	 * @since  1.0.0
	 *
	 * @param array $caps    The user's actual capabilities.
	 * @param int   $user_id The user ID.
	 * @param array $args    A list of contextual arguments.
	 */
	return apply_filters( 'cp_interactions_read_user_likes', $caps, $user_id, $args );
}

/**
 * User Capabilities callbacks to check the user can add new likes.
 *
 * @since  1.0.0
 *
 * @param  int   $user_id The user ID.
 * @param  array $args    A list of contextual arguments.
 * @return array $caps    The user's actual capabilities.
 */
function cp_interactions_post_user_likes( $user_id = 0, $args = array() ) {
	$caps = array( 'do_not_allow' );

	if ( ! empty( $args[0]['user_id'] ) && (int) $user_id === (int) $args[0]['user_id'] ) {
		$caps = array( 'exist' );
	}

	/**
	 * Filter here to edit the user capabilites
	 *
	 * @since  1.0.0
	 *
	 * @param array $caps    The user's actual capabilities.
	 * @param int   $user_id The user ID.
	 * @param array $args    A list of contextual arguments.
	 */
	return apply_filters( 'cp_interactions_post_user_likes', $caps, $user_id, $args );
}

/**
 * User Capabilities callbacks to check the user can delete likes.
 *
 * @since  1.0.0
 *
 * @param  int   $user_id The user ID.
 * @param  array $args    A list of contextual arguments.
 * @return array $caps    The user's actual capabilities.
 */
function cp_interactions_delete_user_likes( $user_id = 0, $args = array() ) {
	$caps = array( 'do_not_allow' );

	if ( ! empty( $args[0]['user_id'] ) && (int) $user_id === (int) $args[0]['user_id'] ) {
		$caps = array( 'exist' );
	}

	/**
	 * Filter here to edit the user capabilites
	 *
	 * @since  1.0.0
	 *
	 * @param array $caps    The user's actual capabilities.
	 * @param int   $user_id The user ID.
	 * @param array $args    A list of contextual arguments.
	 */
	return apply_filters( 'cp_interactions_delete_user_likes', $caps, $user_id, $args );
}

/**
 * Rest API permission to view likes callback.
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return bool True if the user can view likes. False otherwise.
 */
function cp_interactions_user_likes_can_get( WP_REST_Request $request ) {
	return current_user_can( 'cp_read_user_likes' );
}

/**
 * Rest API permission to add likes callback.
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return bool True if the user can add likes. False otherwise.
 */
function cp_interactions_user_likes_can_post( WP_REST_Request $request ) {
	$user_id = (int) $request->get_param( 'id' );

	return current_user_can( 'cp_post_user_like', array( 'user_id' => $user_id ) );
}

/**
 * Rest API permission to delete likes callback.
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return bool True if the user can delete likes. False otherwise.
 */
function cp_interactions_user_likes_can_delete( WP_REST_Request $request ) {
	$user_id = (int) $request->get_param( 'id' );

	return current_user_can( 'cp_delete_user_like', array( 'user_id' => $user_id ) );
}

/**
 * Rest API reply: the likes of a user.
 *
 * NB: Not used for now.
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return array
 */
function cp_interactions_get_user_likes( WP_REST_Request $request ) {
	return $request->get_params();
}

/**
 * Handle Rest API POST a new like requests.
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return int                      The like ID just created.
 */
function cp_interactions_post_user_like( WP_REST_Request $request ) {
	$user_id      = (int) $request->get_param( 'id' );
	$name         = sanitize_key( $request->get_param( 'object_type' ) );
	$primary_id   = (int) $request->get_param( 'object_id' );
	$secondary_id = get_current_blog_id();
	$r_site_id    = $request->get_param( 'site_id' );

	if ( $r_site_id ) {
		$secondary_id = (int) $r_site_id;
	}

	if ( $name ) {
		$name = 'liked_' . $name;
	}

	return array( 'like_id' => cp_interactions_add_like( array(
		'user_id'      => $user_id,
		'primary_id'   => $primary_id,
		'secondary_id' => $secondary_id,
		'name'         => $name,
	) ) );
}

/**
 * Handle Rest API DELETE a new like requests.
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return bool                     True on success. False otherwise.
 */
function cp_interactions_delete_user_like( WP_REST_Request $request ) {
	$user_id = (int) $request->get_param( 'id' );
	$like_id = (int) $request->get_param( 'like_id' );

	return cp_interactions_delete_like( array(
		'user_id' => $user_id,
		'id'      => $like_id,
	) );
}

/**
 * Handle Rest API GET likes for a requested Post ID
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return array                    The list of Users who liked the Post.
 */
function cp_interactions_get_post_likes( WP_REST_Request $request ) {
	$primary_id   = (int) $request->get_param( 'id' );
	$secondary_id = get_current_blog_id();
	$r_site_id    = $request->get_param( 'site_id' );

	if ( $r_site_id ) {
		$secondary_id = (int) $r_site_id;
	}

	return cp_interactions_get_likes( array(
		'primary_id'   => $primary_id,
		'secondary_id' => $secondary_id,
		'name'         => 'liked_post',
	), array( 'id', 'user_id' ) );
}

/**
 * Handle Rest API GET likes for a requested Comment ID
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return array                    The list of Users who liked the Comment.
 */
function cp_interactions_get_comment_likes( WP_REST_Request $request ) {
	$primary_id   = (int) $request->get_param( 'id' );
	$secondary_id = get_current_blog_id();
	$r_site_id    = $request->get_param( 'site_id' );

	if ( $r_site_id ) {
		$secondary_id = (int) $r_site_id;
	}

	return cp_interactions_get_likes( array(
		'primary_id'   => $primary_id,
		'secondary_id' => $secondary_id,
		'name'         => 'liked_comment',
	), array( 'id', 'user_id' ) );
}

/**
 * Build and return the content of the Likes shortcode to list liked content.
 *
 * @since  1.0.0
 *
 * @param  array|string $atts {
 *    The shortcode optional attributes.
 *    @type int    $max     Maximum number of posts/comments to get.
 *                          Default: 0.
 *    @type int    $number  Amount of posts/comments to get.
 *                          Default: 10.
 *    @type string $type    The kind of template to use (mini or expanded).
 *                          Default: expanded.
 *    @type string $object  The object type.
 *                          Default: post.
 * }
 * @return string HTML Output.
 */
function cp_interactions_likes_shortcode( $atts = '' ) {
	$sc = new CP_Interactions_Likes_Shortcode( $atts );

	return $sc->content();
}
add_shortcode( 'clusterpress_likes', 'cp_interactions_likes_shortcode' );

/**
 * Build and return the content of the Likes shortcode for the like button.
 *
 * @since  1.0.0
 *
 * @param  string $atts  This shortcode has no attributes.
 * @return string        HTML Output.
 */
function cp_interactions_likes_button_shortcode( $atts = '' ) {
	// There's no attributes for this shortcode.
	$post = get_post();

	// Only print likes for the post post type.
	if ( ! isset( $post->post_type ) || ! post_type_supports( $post->post_type, 'clusterpress_post_likes' ) ) {
		return;
	}

	$actions = apply_filters( 'cp_site_get_post_actions', array() );

	if ( empty( $actions ) ) {
		return '';
	}

	// Only enqueue script and style if we need it
	wp_enqueue_style ( 'clusterpress-likes' );
	wp_enqueue_script( 'clusterpress-likes' );

	return "\n" . cp_cluster_actions_group( array(
		'class'       => 'cp-site-post-actions',
		'action_type' => 'post',
		'actions'     => $actions,
	) );
}
add_shortcode( 'clusterpress_likes_button', 'cp_interactions_likes_button_shortcode' );
