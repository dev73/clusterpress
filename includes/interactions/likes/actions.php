<?php
/**
 * ClusterPress Likes Actions.
 *
 * @package ClusterPress\interactions\likes
 * @subpackage actions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Register the User Likes section.
 *
 * @since 1.0.0
 */
function cp_interactions_register_likes_section() {

	cp_register_cluster_section( 'user', array(
		'id'            => 'likes',
		'name'          => __( 'Likes', 'clusterpress' ),
		'cluster_id'    => 'user',
		'slug'          => cp_user_get_likes_slug(),
		'rewrite_id'    => cp_user_get_likes_rewrite_id(),
		'template'      => '',
		'template_dir'  => '',
		'toolbar_items' => array(
			'position' => 40,
			'dashicon' => 'dashicons-heart',
			'children' => array(
				array(
					'id'           => 'liked_posts',
					'slug'         => cp_user_get_posts_slug(),
					'parent'       => cp_user_get_likes_rewrite_id(),
					'parent_group' => 'single-bar',
					'title'        => __( 'Articles', 'clusterpress' ),
					'href'         => '#',
					'position'     => 10,
					'capability'   => 'cp_read_single_user',
				),
				array(
					'id'           => 'liked_comments',
					'slug'         => cp_user_get_comments_slug(),
					'parent'       => cp_user_get_likes_rewrite_id(),
					'parent_group' => 'single-bar',
					'title'        => __( 'Commentaires', 'clusterpress' ),
					'href'         => '#',
					'position'     => 20,
					'capability'   => 'cp_read_single_user',
				),
			),
		),
	) );
}
add_action( 'cp_register_cluster_sections', 'cp_interactions_register_likes_section' );

/**
 * Register the User likes relationships.
 *
 * @since 1.0.0
 */
function cp_interactions_register_likes_relationships() {

	cp_user_register_relationships( 'user', array(
		array(
			'name'             => 'liked_post',
			'cluster'          => 'user',
			'singular'         => __( 'like', 'clusterpress'  ),
			'plural'           => __( 'likes', 'clusterpress' ),
			'description'      => __( 'Likes d\'article', 'clusterpress' ),
			'format_callback'  => '',
		),
		array(
			'name'             => 'liked_comment',
			'cluster'          => 'user',
			'singular'         => __( 'like', 'clusterpress'  ),
			'plural'           => __( 'likes', 'clusterpress' ),
			'description'      => __( 'Likes de commentaire', 'clusterpress' ),
			'format_callback'  => '',
		),
	) );
}
add_action( 'cp_register_relationships', 'cp_interactions_register_likes_relationships' );

/**
 * Removes User likes when a post/comment or site is removed.
 *
 * @since  1.0.0
 *
 * @param  int $object_id The post/comment or site ID.
 */
function cp_interactions_remove_likes_for_object( $object_id = 0 ) {
	if ( empty( $object_id ) ) {
		return;
	}

	$current_action = current_action();

	switch ( $current_action ) {
		case 'delete_blog' :
			// Remove all likes about posts.
			cp_interactions_delete_like( array(
				'secondary_id' => $object_id,
				'name'         => 'liked_post',
			) );

			// Remove all likes about comments.
			cp_interactions_delete_like( array(
				'secondary_id' => $object_id,
				'name'         => 'liked_comment',
			) );
			break;

		case 'delete_post'    :
		case 'delete_comment' :
			$site_id = get_current_blog_id();

			if ( $site_id ) {
				// Remove all likes about comments.
				cp_interactions_delete_like( array(
					'primary_id'   => $object_id,
					'secondary_id' => $site_id,
					'name'         => $current_action === 'delete_post' ? 'liked_post' : 'liked_comment',
				) );
			}

			break;
	}
}
add_action( 'delete_blog',    'cp_interactions_remove_likes_for_object', 10, 1 );
add_action( 'delete_post',    'cp_interactions_remove_likes_for_object', 10, 1 );
add_action( 'delete_comment', 'cp_interactions_remove_likes_for_object', 10, 1 );
