<?php
/**
 * ClusterPress Likes Tags.
 *
 * @package ClusterPress\interactions\likes
 * @subpackage tags
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Register the Likes Style & Javascript
 *
 * @since  1.0.0
 *
 * @param  string $url     Url to the ClusterPress JS directory.
 * @param  string $min     Whether to use the minified version of scripts/css
 * @param  string $version The active ClusterPress version.
 */
function cp_interactions_register_likes_scripts( $url = '', $min = '', $version = '' ) {
	wp_register_script(
		'clusterpress-likes',
		sprintf( '%1$slikes%2$s.js', $url, $min ),
		array( 'clusterpress-ajax'),
		$version,
		true
	);

	wp_register_style(
		'clusterpress-likes',
		cp_get_template_asset( 'css/likes' . $min , 'css' ),
		array(),
		$version
	);
}
add_action( 'cp_register_scripts', 'cp_interactions_register_likes_scripts', 10, 3 );

/**
 * Add a feedback string for not logged in users.
 *
 * @since 1.0.0.
 *
 * @param  array $scripts_data The list of localized JS Data.
 * @return array               The list of localized JS Data.
 */
function cp_interactions_likes_scripts_data( $scripts_data = array() ) {
	return array_merge( $scripts_data, array(
		'requiresLogin' => __( 'Vous devez être connecté pour ajouter votre like à ce contenu', 'clusterpress' ),
	) );
}
add_filter( 'cp_scripts_data', 'cp_interactions_likes_scripts_data', 10, 1 );

/**
 * Conditionally Enqueue the Style and Javascript for Likes.
 *
 * @since  1.0.0
 */
function cp_enqueue_likes_cssjs() {
	if ( ( cp_is_user() && ( cp_is_current_action( cp_user_get_posts_slug() )
		|| cp_is_current_action( cp_user_get_comments_slug() )
		|| cp_is_current_action( cp_user_get_likes_slug() ) )
		) || cp_is_site_home() ) {
		wp_enqueue_style ( 'clusterpress-likes' );
		wp_enqueue_script( 'clusterpress-likes' );
	}
}
add_action( 'cp_enqueue_cssjs_clusterpress', 'cp_enqueue_likes_cssjs' );

/** Liked Posts Template Tags *************************************************/

/**
 * Build and return the Likes action button for a post.
 *
 * @since  1.0.0
 *
 * @param  array  $actions The ClusterPress available actions for the post.
 * @return array           The ClusterPress available actions for the post.
 */
function cp_site_get_post_likes_action( $actions = array() ) {
	if ( ! cp_interactions_are_likes_enabled() ) {
		return $actions;
	}

	$post = get_post();

	if ( ! post_type_supports( $post->post_type, 'clusterpress_post_likes' ) ) {
		return $actions;
	}

	$likes_slug = cp_user_get_likes_slug();

	$postdata_enpoint = '';
	$postdata_object  = '';
	$sitedata_id      = '';
	if ( ! empty( $post->ID ) ) {
		$postdata_enpoint = sprintf( ' data-cp-action-endpoint="post/%1$s/%2$s"',
			esc_attr( $post->ID ),
			esc_attr( $likes_slug )
		);

		$postdata_object = sprintf( ' data-cp-action-object="%s"',
			esc_attr( $post->ID )
		);

		if ( ! empty( $post->parent_site_id ) ) {
			$sitedata_id = sprintf( ' data-cp-action-site="%s"',
				esc_attr( $post->parent_site_id )
			);
		}
	}

	$userdata_enpoint = '';
	if ( is_user_logged_in() ) {
		$userdata_enpoint = sprintf( ' data-cp-user-endpoint="%1$s/%2$s/%3$s"',
			esc_attr( cp_user_get_slug() ),
			esc_attr( get_current_user_id() ),
			esc_attr( $likes_slug )
		);
	}

	$actions['likes'] = array(
		'wrapper_class' => 'likes-link',
		'content'       => sprintf(
			__( '<a href="#" %1$s%2$s%3$s%4$s><span class="cp-icon"></span><span class="cp-count">0</span></a>', 'clusterpress' ),
			$postdata_object,
			$postdata_enpoint,
			$userdata_enpoint,
			$sitedata_id
		),
	);

	return $actions;
}
add_filter( 'cp_site_get_post_actions', 'cp_site_get_post_likes_action', 10, 1 );

/**
 * Adds a shortcode at the end of the content of the post.
 * If this place does not fit with your theme, you can use:
 *
 * remove_filter( 'the_content', 'cp_interactions_likes_post_actions_container',  5 );
 *
 * and choose a better place.
 *
 * @since 1.0.0
 *
 * @param  string $content The content of the post.
 * @return string          The content of the post with the likes shortcode appended.
 */
function cp_interactions_likes_post_actions_container( $content = '' ) {
	$post = get_post();

	if ( empty( $post->post_type ) || ! post_type_supports( $post->post_type, 'clusterpress_post_likes' ) ) {
		return $content;
	}

	$content .= '[clusterpress_likes_button]';
	return $content;
}
add_filter( 'the_content', 'cp_interactions_likes_post_actions_container',  5 );

/**
 * Output the number of likes for the current post in the loop.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_posts_liked_widget_count() {
	printf( '(%s)', esc_html( cp_interactions_get_posts_liked_widget_count() ) );
}

	/**
	 * Get the number of likes for the current post in the loop.
	 *
	 * @since 1.0.0
	 *
	 * @return int The number of likes the post received.
	 */
	function cp_interactions_get_posts_liked_widget_count() {
		$post = cp_site_get_current_post();

		$count = 0;
		if ( ! empty( $post->likes_count ) ) {
			$count = $post->likes_count;
		}

		/**
		 * Use this filter to edit the likes count.
		 *
		 * @since 1.0.0
		 *
		 * @param  int     $count The number of likes the post received.
		 * @param  WP_Post $post  The current post object in the loop.
		 */
		return apply_filters( 'cp_interactions_get_posts_liked_widget_count', $count, $post );
	}

/**
 * Override the default Posts loop args.
 *
 * @since 1.0.0
 *
 * @param array $args {
 *   An array of arguments.
 *   @see WP_Query::parse_query() for a complete description.
 * }
 * @return array The overriden loop args for the liked posts.
 */
function cp_interactions_set_post_user_likes_args( $args = array() ) {
	// Make sure to be on the user's like page
	if ( ! cp_is_user() || ! cp_is_current_action( cp_user_get_likes_slug() ) ) {
		return $args;
	}

	$site_id = get_current_network_id();
	if ( ! empty( $args['filter'] ) ) {
		$site_id = $args['filter'];
	}

	$post_liked = cp_interactions_get_likes( array(
		'user_id'      => cp_get_displayed_user_id(),
		'secondary_id' => (int) $site_id,
		'name'         => 'liked_post',
	), array( 'primary_id' ) );

	if ( ! empty( $post_liked ) ) {
		$args['post__in']  = wp_list_pluck( $post_liked, 'primary_id' );
		$args['post_type'] = get_post_types_by_support( 'clusterpress_post_likes' );
	} else {
		$args['post__in'] = 0;
	}

	/**
	 * Make sure all likes will be displayed
	 * Not only the post the user published!
	 */
	if ( isset( $args['author'] ) ) {
		unset( $args['author'] );
	}

	return $args;
}
add_filter( 'cp_site_has_posts_args', 'cp_interactions_set_post_user_likes_args', 10, 1 );

/**
 * Override the no Posts found message of the loop.
 *
 * @since 1.0.0
 *
 * @param  string $message The message displayed when no posts were found.
 * @return string $message The message displayed when no liked posts were found.
 */
function cp_interactions_no_post_likes_found( $message = '' ) {
	// Make sure to be on the user's like page
	if ( ! cp_is_user() || ! cp_is_current_action( cp_user_get_likes_slug() ) ) {
		return $message;
	}

	return __( 'Cet utilisateur n\'a pas &quot;liké&quot; de contenu jusqu\'à présent.', 'clusterpress' );
}
add_filter( 'cp_site_get_no_posts_found', 'cp_interactions_no_post_likes_found', 10, 1 );

/**
 * In case a theme is using the excerpt, make sure the Likes button is not
 * added as its html tags will be stripped in this case by wp_trim_words()
 *
 * @since  1.0.0
 *
 * @param  string $excerpt The Post excerpt content.
 * @return string          The unchanged Post excerpt.
 */
function cp_interactions_likes_remove_content_filter( $excerpt = '' ) {
	remove_filter( 'the_content', 'cp_interactions_likes_post_actions_container', 5 );
	return $excerpt;
}
add_filter( 'get_the_excerpt', 'cp_interactions_likes_remove_content_filter', 8 );

/**
 * In case a theme is using the excerpt, make sure the Likes button is
 * restored once the content has been get by get_the_excerpt().
 *
 * @since  1.0.0
 *
 * @param  string $excerpt The Post excerpt content.
 * @return string          The unchanged Post excerpt.
 */
function cp_interactions_likes_restore_content_filter( $excerpt = '' ) {
	add_filter( 'the_content', 'cp_interactions_likes_post_actions_container', 5 );
	return $excerpt;
}
add_filter( 'the_excerpt', 'cp_interactions_likes_restore_content_filter' );

/** Liked Comments Template Tags **********************************************/

/**
 * Build and return the Likes action button for a comment.
 *
 * @since  1.0.0
 *
 * @param  array  $actions      The ClusterPress available actions for the comment.
 * @param  bool   $awaiting_mod True if the comment is awating moderation. False otherwise.
 * @return array                The ClusterPress available actions for the comment.
 */
function cp_site_get_comment_likes_action( $actions = array(), $awaiting_mod = false, $post_type = '' ) {
	if ( ! cp_interactions_are_likes_enabled() || true === $awaiting_mod ) {
		return $actions;
	}

	$comment    = get_comment();
	$likes_slug = cp_user_get_likes_slug();

	// Get the post type if not available
	if ( empty( $post_type ) ) {
		$post_type  = get_post_field( 'post_type', $comment->comment_post_ID );
	}

	if ( ! post_type_supports( $post_type, 'clusterpress_comment_likes' ) ) {
		return $actions;
	}

	$commentdata_enpoint = '';
	$commentdata_object  = '';
	$sitedata_id         = '';
	if ( ! empty( $comment->comment_ID ) ) {
		$commentdata_enpoint = sprintf( ' data-cp-action-endpoint="comment/%1$s/%2$s"',
			esc_attr( $comment->comment_ID ),
			esc_attr( $likes_slug )
		);

		$commentdata_object = sprintf( ' data-cp-action-object="%s"',
			esc_attr( $comment->comment_ID )
		);

		if ( ! empty( $comment->parent_site_id ) ) {
			$sitedata_id = sprintf( ' data-cp-action-site="%s"',
				esc_attr( $comment->parent_site_id )
			);
		}
	}

	$userdata_enpoint = '';
	if ( is_user_logged_in() ) {
		$userdata_enpoint = sprintf( ' data-cp-user-endpoint="%1$s/%2$s/%3$s"',
			esc_attr( cp_user_get_slug() ),
			esc_attr( get_current_user_id() ),
			esc_attr( $likes_slug )
		);
	}

	$actions['likes'] = array(
		'wrapper_class' => 'likes-link',
		'content'       => sprintf(
			__( '<a href="#" %1$s%2$s%3$s%4$s><span class="cp-icon"></span><span class="cp-count">0</span></a>', 'clusterpress' ),
			$commentdata_object,
			$commentdata_enpoint,
			$userdata_enpoint,
			$sitedata_id
		),
	);

	return $actions;
}
add_filter( 'cp_site_get_comment_actions', 'cp_site_get_comment_likes_action', 10, 3 );

/**
 * Add A Group of buttons after the comment reply link.
 * Use it to add the Likes action button.
 *
 * @since 1.0.0
 *
 * @param string $reply_link       The reply link.
 * @param array  $args {
 *    An array of arguments.
 *    @see get_comment_reply_link() for a detailed description.
 * }
 * @param  int|WP_Comment $comment Comment being replied to. Default current comment.
 * @param  int|WP_Post    $post    Post ID or WP_Post object the comment is going to be displayed on.
 * @return string HTML Output      The reply link and the ClusterPress action buttons if needed.
 */
function cp_interactions_comment_actions_container( $reply_link = '', $args = array(), $comment = null, $post = null ) {
	// Validate the comment & its status
	$comment      = get_comment( $comment );
	$awaiting_mod = 0 === (int) $comment->comment_approved;

	// Validate the post type
	if ( ! empty( $post->ID ) ) {
		$post      = get_post( $post );
		$post_type = $post->post_type;

	} else {
		$post_type = get_post_field( 'post_type', $comment->comment_post_ID );
	}

	if ( ! post_type_supports( $post_type, 'clusterpress_comment_likes' ) ) {
		return $reply_link;
	}

	$actions = apply_filters( 'cp_site_get_comment_actions', array(), $awaiting_mod, $post_type );

	if ( empty( $actions ) ) {
		return $reply_link;
	}

	// Only enqueue script and style if we need it
	wp_enqueue_style ( 'clusterpress-likes' );
	wp_enqueue_script( 'clusterpress-likes' );

	return $reply_link . "\n" . cp_cluster_actions_group( array(
		'class'       => 'cp-site-comment-actions',
		'action_type' => 'comment',
		'actions'     => $actions,
	) );
}
add_filter( 'comment_reply_link', 'cp_interactions_comment_actions_container', 10, 4 );

/**
 * Output the number of likes for the current comment in the loop.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_comments_liked_widget_count() {
	printf( '(%s)', esc_html( cp_interactions_get_comments_liked_widget_count() ) );
}

	/**
	 * Get the number of likes for the current comment in the loop.
	 *
	 * @since 1.0.0
	 *
	 * @return int The number of likes the comment received.
	 */
	function cp_interactions_get_comments_liked_widget_count() {
		$comment = cp_site_get_current_comment();

		$count = 0;
		if ( ! empty( $comment->likes_count ) ) {
			$count = $comment->likes_count;
		}

		/**
		 * Use this filter to edit the likes count.
		 *
		 * @since 1.0.0
		 *
		 * @param  int        $count The number of likes the post received.
		 * @param  WP_Comment $post  The current comment object in the loop.
		 */
		return apply_filters( 'cp_interactions_get_comments_liked_widget_count', $count, $comment );
	}

/**
 * Output the author link for the current comment in the loop.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_comments_liked_the_author_link() {
	$comment = cp_site_get_current_comment();

	printf( '%s on ', get_comment_author_link( $comment ) );
}

/**
 * Output the comment link for the current comment in the loop.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_comments_liked_the_permalink() {
	echo esc_url( cp_interactions_comments_liked_get_permalink() );
}

	/**
	 * Get the comment link for the current comment in the loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string The comment link for the current comment in the loop.
	 */
	function cp_interactions_comments_liked_get_permalink() {
		$comment = cp_site_get_current_comment();

		return get_comment_link( $comment );
	}

/**
 * Output the liked title for the current comment in the loop.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_comments_liked_the_title() {
	echo esc_html( cp_interactions_comments_liked_get_title() );
}

	/**
	 * Get the post title for the current comment in the loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string HTML string.
	 */
	function cp_interactions_comments_liked_get_title() {
		$comment = cp_site_get_current_comment();

		return get_the_title( $comment->comment_post_ID );
	}

/**
 * Override the default Comments loop args.
 *
 * @since 1.0.0
 *
 * @param array $args {
 *   An array of arguments.
 *   @see WP_Comment_Query::parse_query() for a complete description.
 * }
 * @return array The overriden loop args for the liked comments.
 */
function cp_interactions_set_comment_user_likes_args( $args = array() ) {
	// Make sure to be on the user's like page
	if ( ! cp_is_user() || ! cp_is_current_action( cp_user_get_likes_slug() ) ) {
		return $args;
	}

	$site_id = get_current_network_id();
	if ( ! empty( $args['filter'] ) ) {
		$site_id = $args['filter'];
	}

	$comment_liked = cp_interactions_get_likes( array(
		'user_id'      => cp_get_displayed_user_id(),
		'secondary_id' => (int) $site_id,
		'name'         => 'liked_comment',
	), array( 'primary_id' ) );

	if ( ! empty( $comment_liked ) ) {
		$args['comment__in'] = wp_list_pluck( $comment_liked, 'primary_id' );
		$args['status']      = 'approve';
	} else {
		$args['comment__in'] = 0;
	}

	/**
	 * Make sure all likes will be displayed
	 * Not only the comment the user published!
	 */
	if ( isset( $args['user_id'] ) ) {
		unset( $args['user_id'] );
	}

	return $args;
}
add_filter( 'cp_site_has_comments_args', 'cp_interactions_set_comment_user_likes_args', 10, 1 );

/**
 * Override the no Comments found message of the loop.
 *
 * @since 1.0.0
 *
 * @param  string $message The message displayed when no comments were found.
 * @return string $message The message displayed when no liked comments were found.
 */
function cp_interactions_no_comment_likes_found( $message = '' ) {
	// Make sure to be on the user's like page
	if ( ! cp_is_user() || ! cp_is_current_action( cp_user_get_likes_slug() ) ) {
		return $message;
	}

	return __( 'Cet utilisateur n\'a pas &quot;liké&quot; de commentaire jusqu\'à présent.', 'clusterpress' );
}
add_filter( 'cp_site_get_no_comments_found', 'cp_interactions_no_comment_likes_found', 10, 1 );

/**
 * Use a more elaborate string for the comment date to use in the Likes loop.
 *
 * @since 1.0.0
 *
 * @param  string     $date_text    The WordPress generated text for the date.
 * @param  string     $comment_date The comment's date.
 * @param  string     $comment_time The comment's time.
 * @param  WP_Comment $comment      The comment object.
 * @return string                   A more elaborate string for the comment date to use in the Likes loop.
 */
function cp_interactions_likes_get_comment_date( $date_text = '', $comment_date = '', $comment_time = '', $comment = null ) {
	if ( cp_is_current_action( cp_user_get_comments_slug() ) || empty( $comment->comment_author ) ) {
		return $date_text;
	}

	if ( ! empty( $comment->comment_parent ) ) {
		$date = __( '%1$s a répondu à un commentaire le %2$s à %3$s', 'clusterpress' );
	} else {
		$date = __( '%1$s a posté un commentaire le %2$s à %3$s', 'clusterpress' );
	}

	$username = esc_html( $comment->comment_author );
	$user_url = get_comment_author_url( $comment );

	if ( ! empty( $user_url ) ) {
		$rel = '';
		if ( false === strpos( $user_url, site_url() ) ) {
			$rel = ' rel="nofollow"';
		}

		$username = sprintf( '<a href="%1$s"%2$s>%3$s</a>',
			esc_url( $user_url ),
			$rel,
			$username
		);
	}

	/**
	 * Use this filter to edit the comment date to use in the Likes loop.
	 *
	 * @since 1.0.0
	 *
	 * @param  string     $value        The ClusterPress generated text for the comment date.
	 * @param  string     $comment_date The comment's date.
	 * @param  string     $comment_time The comment's time.
	 * @param  WP_Comment $comment      The comment object.
	 * @param  string     $username     The comment's author username.
	 */
	return apply_filters( 'cp_interactions_get_comment_date', sprintf( $date, $username, $comment_date, $comment_time ), $comment_date, $comment_time, $comment, $username );
}
add_filter( 'cp_site_get_comment_date', 'cp_interactions_likes_get_comment_date', 10, 4 );
