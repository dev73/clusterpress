<?php
/**
 * ClusterPress Mentions Functions.
 *
 * @package ClusterPress\interactions\mentions
 * @subpackage functions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Get the unread user mentions
 *
 * @since 1.0.0
 *
 * @param  int   $user_id  The user ID.
 * @return array $mentions The list of unread mentions for the user.
 */
function cp_interactions_get_user_mentions( $user_id = 0 ) {
	$mentions = array();

	if ( ! cp_user_relationships_isset() || empty( $user_id ) ) {
		return $mentions;
	}

	$mentions = wp_cache_get( $user_id, 'cp_user_mentions' );

	if ( false === $mentions ) {
		$mentions = clusterpress()->user->relationships->get( array(
			'id',
			'primary_id',
			'secondary_id',
			'name',
		), array(
			'user_id' => $user_id,
			'name'    => array_merge( cp_interactions_get_post_type_mention_names(), array( 'comment_mentions' ) ),
			'date'    => 'null',
		), false, false, true );

		wp_cache_set( $user_id, $mentions, 'cp_user_mentions' );
	}

	return $mentions;
}

/**
 * Edit the Toolbar Mentions node to append the unread mentions count.
 *
 * @since 1.0.0
 *
 * @param  array   $nodes The User toolbar nav items.
 * @param  WP_User $user  The User object.
 * @return array          The User toolbar nav items.
 */
function cp_interactions_get_user_mention_count( $nodes = array(), $user = null ) {
	if ( ! empty( $user->ID ) && (int) $user->ID === (int) get_current_user_id() ) {
		$id = cp_user_get_mentions_rewrite_id();

		if ( empty( $nodes[ $id ] ) ) {
			return $nodes;
		}

		$mentions = cp_interactions_get_user_mentions( $user->ID );

		if ( $mentions ) {
			$nodes[ $id ]->count = count( $mentions );
		}
	}

	return $nodes;
}
add_filter( 'cp_user_cluster_get_toolbar_urls', 'cp_interactions_get_user_mention_count', 10, 2 );

/**
 * "Send" a new mention to the user.
 *
 * @since 1.0.0
 *
 * @param  WP_User $user         The user object.
 * @param  int     $primary_id   The ID of the content type (Post type or Comment).
 * @param  int     $secondary_id The ID of the content parent (Site).
 * @param  string  $name         The name of the relationship.
 * @return bool                  True when a mention a successfully added. False otherwise.
 */
function cp_interactions_add_mention( $user = null, $primary_id = 0, $secondary_id = 0, $name = 'post_mentions' ) {
	if ( empty( $user->ID ) || empty( $primary_id ) || empty( $secondary_id ) || empty( $name ) ) {
		return false;
	}

	$mentioned = cp_user_add_relationship( array(
		'user_id'      => (int) $user->ID,
		'primary_id'   => (int) $primary_id,
		'secondary_id' => (int) $secondary_id,
		'name'         => sanitize_key( $name ),
	) );

	/**
	 * Hook here to perform custom actions once the mention is recorded
	 *
	 * @since 1.0.0
	 *
	 * @param int     $user_id      The ID of the mentionned user.
	 * @param int     $mentioned    The ID of the mention.
	 * @param WP_User $user         The user object.
	 * @param int     $primary_id   The ID of the content type (Post type or Comment).
	 * @param int     $secondary_id The ID of the content parent (Site).
	 * @param string  $name         The name of the relationship.
	 */
	do_action( 'cp_interactions_add_mention', $user->ID, $mentioned, $user, $primary_id, $secondary_id, $name );

	return true;
}

/**
 * Search mentions into the given content
 *
 * @since 1.0.0
 *
 * @param  string $text The content to search mentions in.
 * @return array        The list of the user_nicenames to mention.
 */
function cp_interactions_parse_mentions( $text = '' ) {
	if ( empty( $text ) ) {
		return false;
	}

	$mention_regex = '/[@]+([A-Za-z0-9-_\.@]+)\b/';
	preg_match_all( $mention_regex, $text, $user_nicenames );

	// Avoid duplicates
	return array_unique( $user_nicenames[1] );
}

/**
 * Delete all mentions for a post or a site.
 *
 * @todo  Use a generic function for this and the site_followed relationship ?
 *
 * @since  1.0.0
 *
 * @param  array  $args {
 *   An array of arguments.
 *   @type int    $primary_id   The ID of the content (Post type or Comment).
 *   @type int    $secondary_id The ID of the content parent (Site). Optional.
 *                              Defaults to the current site id.
 *   @type string $name         The name of the relationship.
 * }}
 * @return bool True on success. False otherwise.
 */
function cp_interactions_delete_mentions( $args = array() ) {
	$r = wp_parse_args( $args, array(
		'primary_id'   => 0,
		'secondary_id' => (int) get_current_blog_id(),
		'name'         => '',
	) );

	if ( empty( $r['name'] ) || ( empty( $r['primary_id'] ) && empty( $r['secondary_id'] ) ) ) {
		return false;
	}

	$mentions = cp_user_get_relationship( array( 'id', 'user_id' ), array_filter( $r ) );

	/**
	 * Hook here if you need to perform custom actions before mentions are deleted.
	 *
	 * @since 1.0.0
	 *
	 * @param array $mentions The list of mentions which will be deleted.
	 */
	do_action( 'cp_interactions_before_delete_mentions', $mentions );

	// Init deleted to false
	$deleted = false;

	if ( ! empty( $mentions ) ) {
		$user_ids = wp_list_pluck( $mentions, 'user_id', 'id' );
		$arg      = array( 'id' => array_keys( $user_ids ) );

		if ( ! empty( $arg['id'] ) ) {
			$user_ids = array_unique( $user_ids );

			if ( cp_user_remove_relationship( $arg ) ) {
				// It's a success!
				$deleted = true;

				// Clean each user's mention cache.
				foreach ( $user_ids as $user_id ) {
					/**
					 * Hook here if you need to perform custom actions once the mention is deleted.
					 *
					 * @since 1.0.0
					 *
					 * @param int $user_id The User ID who was mentionned.
					 */
					do_action( 'cp_interactions_delete_mention_for_user', $user_id );
				}
			}
		}
	}

	/**
	 * Hook here if you need to perform custom actions after mentions are deleted.
	 *
	 * @since 1.0.0
	 *
	 * @param bool $deleted True if mentions were deleted. False otherwise.
	 */
	do_action( 'cp_interactions_after_delete_mentions', $deleted );

	return $deleted;
}

/**
 * Mark all unread mentions as read.
 *
 * @since 1.0.0
 *
 * @param  int $user_id The user ID to mark all mentions as read
 * @return bool         True on success. False otherwise.
 */
function cp_interactions_mark_all_mentions_read( $user_id = 0 ) {
	if ( empty( $user_id ) ) {
		return false;
	}

	$date          = current_time( 'mysql', 1 );
	$mention_names = array_merge( cp_interactions_get_post_type_mention_names(), array( 'comment_mentions' ) );

	foreach ( $mention_names as $name ) {
		cp_user_update_relationship( array(
			'date'    => $date,
		), array(
			'user_id' => $user_id,
			'name'    => $name,
			'date'    => NULL,
		) );
	}

	/**
	 * Hook here if you need to perform custom actions once all user's mentions
	 * were marked as read.
	 *
	 * @since 1.0.0
	 *
	 * @param int $user_id The user ID.
	 */
	do_action( 'cp_interactions_mention_all_read', $user_id );

	return true;
}

/**
 * Get all supported Post type mention relationship names.
 *
 * @since 1.0.0
 *
 * @return array A list of mention relationship names.
 */
function cp_interactions_get_post_type_mention_names() {
	$supported_post_types = (array) get_post_types_by_support( 'clusterpress_post_mentions' );

	$names = array();
	foreach ( $supported_post_types as $post_type ) {
		$names[] = sanitize_key( $post_type ) . '_mentions';
	}

	return $names;
}

/**
 * Mentions are not using the post type or comment content but
 * a generic string.
 *
 * @since 1.0.0
 *
 * @param string $mention_name The name of the relationship.
 * @return array The list of generic string to use as content for mentions.
 */
function cp_interactions_get_default_mention_content( $mention_name = '' ) {
	/**
	 * Filter here to edit the generic strings according to the given mention name.
	 *
	 * @since 1.0.0
	 *
	 * @param string $mention_name The name of the relationship.
	 */
	return apply_filters( 'cp_interactions_get_default_mention_content',  array(
		'selfnew' => esc_html__( 'Vous avez été nouvellement mentionné dans un contenu.', 'clusterpress' ),
		'self'    => esc_html__( 'Vous avez été mentionné dans un contenu et en avez pris connaissance %s.', 'clusterpress' ),
		'read'    => esc_html__( '%1$s a été mentionné dans un contenu et en a pris connaissance %2$s.', 'clusterpress' ),
		'unread'  => esc_html__( '%s a été mentionné dans un contenu mais n\'en a pas pris connaissance jusqu\'à présent.', 'clusterpress' ),
	), $mention_name );
}
