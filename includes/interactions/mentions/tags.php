<?php
/**
 * ClusterPress Likes Tags.
 *
 * @package ClusterPress\interactions\likes
 * @subpackage tags
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Get the inline style to format the unread mentions count bubble into toolbars.
 *
 * @since 1.0.0
 */
function cp_interactions_get_toolbar_style() {
	/**
	 * Filter here if you need to edit the mentions count bubble style.
	 *
	 * @since 1.0.0
	 *
	 * @param string $value The inline style for the mentions count bubble.
	 */
	return apply_filters( 'cp_interactions_get_toolbar_style', '
		#cp-cp_mentions a {
			position: relative;
		}

		ul.full #cp-cp_mentions a {
			position: inherit;
		}

		#wp-admin-bar-clusterpress-user-bar li span.pending-count,
		#cp-cp_mentions a span.pending-count {
			display: inline-block;
			background-color: #d54e21;
			color: #fff;
			font-size: 9px;
			line-height: 17px;
			font-weight: 600;
			margin: -1px 0 0 2px;
			vertical-align: text-bottom;
			-webkit-border-radius: 10px;
			border-radius: 10px;
			z-index: 26;
			padding: 0 6px;
		}

		#cp-cp_mentions a span.pending-count {
			position: absolute;
			bottom: 2px;
			right: 4px;
		}

		ul.full #cp-cp_mentions a span.pending-count {
			position: inherit;
		}
	' );
}

/**
 * Hooked to front and admin scripts, This adds the inline style for the mentions count bubble
 *
 * @since 1.0.0
 */
function cp_interactions_toolbar_style() {
	wp_add_inline_style( 'admin-bar', cp_interactions_get_toolbar_style() );
}
add_action( 'cp_enqueue_scripts',       'cp_interactions_toolbar_style', 12 );
add_action( 'cp_admin_enqueue_scripts', 'cp_interactions_toolbar_style', 12 );

/**
 * Format mentions inside the output content.
 *
 * @since 1.0.0
 *
 * @param string          $text The content in which we need to highlight mentions.
 * @param null|WP_Comment The comment object if available (in case of comment mentions).
 * @return                $text The content having mentions highlighted.
 */
function cp_interactions_set_mention_links( $text = '', $comment = null ) {
	$current_filter = current_filter();

	/**
	 * If you wish to use The ClusterPress mentions for your post type,
	 * and are not using 'the_content' filter to sanitize it you can
	 * apply the 'cp_mentions_custom_post_type' filter.
	 */
	$post_type_filters = array( 'the_content', 'cp_site_get_post_excerpt', 'cp_mentions_custom_post_type' );

	// This content belongs to a post type object.
	if ( false !== array_search( $current_filter, $post_type_filters ) ) {
		$post = get_post();

		if ( empty( $post->ID ) ) {
			return $text;
		}

		$mentions = (array) get_post_meta( $post->ID, '_clusterpress_post_mentions', true );

		if ( ! $mentions ) {
			return $text;
		}

	// This content belongs to a comment object.
	} elseif ( 'comment_text' === $current_filter || 'cp_site_get_comment_excerpt' === $current_filter ) {
		if ( empty( $comment->comment_ID ) ) {
			return $text;
		}

		$mentions = (array) get_comment_meta( $comment->comment_ID, '_clusterpress_comment_mentions', true );

		if ( ! $mentions ) {
			return $text;
		}
	}

	if ( ! empty( $mentions ) ) {
		foreach ( $mentions as $username => $user_link ) {
			$text = str_replace( '@' . $username, sprintf(
				'<a href="%1$s">@%2$s</a>',
				esc_url( $user_link ),
				esc_html( $username )
			), $text );
		}
	}

	return $text;
}
add_filter( 'the_content',                  'cp_interactions_set_mention_links', 30, 1 );
add_filter( 'cp_site_get_post_excerpt',     'cp_interactions_set_mention_links', 30, 1 );
add_filter( 'cp_mentions_custom_post_type', 'cp_interactions_set_mention_links', 10, 1 );
add_filter( 'comment_text',                 'cp_interactions_set_mention_links', 30, 2 );
add_filter( 'cp_site_get_comment_excerpt',  'cp_interactions_set_mention_links', 30, 2 );

/** Mentions loop *************************************************************/

/**
 * Output the title of the mentions page.
 *
 * @since 1.0.0
 */
function cp_user_mentions_the_loop_title() {
	esc_html_e( 'Mentions', 'clusterpress' );
}

/**
 * Init the Mentions loop.
 *
 * @since 1.0.0
 *
 * @return bool True if mentions were found. False otherwise.
 */
function cp_interactions_has_mentions() {
	$cp = clusterpress();

	// Setup the global query loop
	$cp->interactions->user_mentions = new CP_Interactions_Mentions_Loop;

	/**
	 * Filter here if you need to edit the has_items() result.
	 *
	 * @since  1.0.0
	 *
	 * @param  bool $value True if there are mentions to display. False otherwise.
	 */
	return apply_filters( 'cp_interactions_has_mentions', $cp->interactions->user_mentions->has_items() );
}

/**
 * Whether there are more mentions available in the loop to iterate on.
 *
 * @since 1.0.0
 *
 * @return object The mention Object.
 */
function cp_interactions_the_mentions() {
	return clusterpress()->interactions->user_mentions->items();
}

/**
 * Load the current mention into the loop.
 *
 * @since 1.0.0
 *
 * @return object The current mention Object.
 */
function cp_interactions_the_mention() {
	return clusterpress()->interactions->user_mentions->the_item();
}

/**
 * Get the current mention of the loop.
 *
 * @since 1.0.0
 *
 * @return object The current mention Object.
 */
function cp_interactions_get_current_mention() {
	/**
	 * Filter here to edit the current mention of the loop.
	 *
	 * @since 1.0.0
	 *
	 * @param object $mention The current mention of the loop.
	 */
	return apply_filters( 'cp_interactions_get_current_mention', clusterpress()->interactions->user_mentions->mention );
}

/**
 * Check if the displayed user has unread mentions.
 *
 * @since 1.0.0
 *
 * @return bool True if the displayed user has unread mentions. False otherwise.
 */
function cp_user_mentions_has_unread() {
	$unread = cp_interactions_get_user_mentions( cp_get_displayed_user_id() );
	return ! empty( $unread );
}

/**
 * Output a button to mark all mentions as read.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_mentions_read_all_button() {
	echo cp_user_mentions_get_read_all_button();
}

	/**
	 * Get a button to mark all mentions as read.
	 *
	 * @since 1.0.0
	 *
	 * @return string The HTML for the button to mark all mentions as read.
	 */
	function cp_user_mentions_get_read_all_button() {
		$user_link = cp_user_get_url( array(
			'rewrite_id' => cp_user_get_mentions_rewrite_id(),
			'slug'       => cp_user_get_mentions_slug(),
		), cp_displayed_user() );

		$user_link = wp_nonce_url( add_query_arg( 'readall', 1, $user_link ), 'cp_mention_bulk_read' );

		return cp_cluster_actions_group( array(
			'class'       => 'cp-interactions-mention-actions',
			'action_type' => 'mention',
			'actions'     => array(
				array(
					'wrapper_class' => 'bulk-read',
					'dashicon'      => 'dashicons-yes',
					'content'       => sprintf(
						__( '<a href="%s" class="mention-bulk-read">Tout Marquer comme lu.</a>', 'clusterpress' ),
						esc_url( $user_link )
					),
				),
			),
		) );
	}

/**
 * Output specific mentions CSS classes.
 *
 * @since 1.0.0
 *
 * @return string The content of the class attribute.
 */
function cp_interactions_the_mention_class() {
	echo join( ' ', array_map( 'sanitize_html_class', cp_interactions_get_mention_class() ) );
}

	/**
	 * Get specific mentions CSS classes.
	 *
	 * @since 1.0.0
	 *
	 * @return string The content of the class attribute.
	 */
	function cp_interactions_get_mention_class() {
		$mention = cp_interactions_get_current_mention();

		$classes = array( 'user-mentions' );

		if ( ! empty( $mention->name ) ) {
			$classes[] = $mention->name;
		}

		if ( empty( $mention->date ) && cp_user_is_self_profile() ) {
			$classes[] = 'new-mention';
		}

		/**
		 * Filter to add/edit mentions classes.
		 *
		 * @since 1.0.0
		 *
		 * @param array  $classes The list of mentions classes.
		 * @param object $mention The current mention in the loop.
		 *
		 */
		return apply_filters( 'cp_interactions_get_mention_class', $classes, $mention );
	}

/**
 * Output the current mention's content.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_the_mention_content() {
	echo cp_interactions_get_mention_content();
}

	/**
	 * Get the current mention's content.
	 *
	 * @since 1.0.0
	 *
	 * @return string the mention's content.
	 */
	function cp_interactions_get_mention_content() {
		$mention = cp_interactions_get_current_mention();

		if ( ! cp_user_relationships_isset() || empty( $mention->name ) ) {
			return false;
		}

		$relationship = cp_user_relationships_get_object( $mention->name );

		if ( ! empty( $relationship->format_callback ) ) {
			$callback = $relationship->format_callback;
		} else {
			$callback = cp_interactions_get_default_mention_content( $mention->name );
		}

		$ago = '';
		if ( ! empty( $mention->date ) ) {
			$ago = sprintf( __( 'il y a %s', 'clusterpress'), human_time_diff( strtotime( $mention->date ) ) );
		}

		if ( cp_user_is_self_profile() ) {
			if ( ! $ago && ! empty( $callback['selfnew'] ) ) {
				$content = $callback['selfnew'];
			} elseif ( $ago && ! empty( $callback['self'] ) ) {
				$content = sprintf( $callback['self'], $ago );
			}
		} elseif ( ! $ago ) {
			$content = sprintf( $callback['unread'], cp_displayed_user()->display_name );
		} else {
			$content = sprintf( $callback['read'], cp_displayed_user()->display_name, $ago );
		}

		/**
		 * Used to format the output. Filter here to edit this output.
		 *
		 * @since 1.0.0
		 *
		 * @param string $content The current mention's content.
		 * @param object $mention The current mention's object.
		 */
		return apply_filters( 'cp_interactions_get_mention_content', $content, $mention );
	}

/**
 * Output the current mention Action buttons
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_the_mention_actions() {
	echo cp_interactions_get_mention_actions();
}

	/**
	 * Get the current mention Action buttons
	 *
	 * @since 1.0.0
	 *
	 * @return string The action buttons.
	 */
	function cp_interactions_get_mention_actions() {
		$mention   = cp_interactions_get_current_mention();
		$actions   = array();
		$user_link = cp_user_get_url( array(
			'rewrite_id' => cp_user_get_mentions_rewrite_id(),
			'slug'       => cp_user_get_mentions_slug(),
		), cp_displayed_user() );

		if ( ! empty( $mention->id ) && ! empty( $mention->name ) ) {
			$view_link = wp_nonce_url( add_query_arg( 'redirectto', $mention->id, $user_link ), 'cp_view_mention' );

			$mention_object = cp_user_relationships_get_object( $mention->name );

			if ( ! empty( $mention_object->singular ) ) {
				$content = sprintf(
					__( '<a href="%1$s" class="mention-view-link">Voir %2$s</a>', 'clusterpress' ),
					esc_url( $view_link ),
					esc_html( $mention_object->singular )
				);
			} else {
				/**
				 * Use this filter if your plugin has not defined a User relationship for its mentions
				 *
				 * @since 1.0.0
				 *
				 * @param string $name      The mention's name
				 * @param string $view_link The link to display the content containing the mention.
				 * @param object $mention   The mention's object.
				 */
				$content = apply_filters( 'cp_interactions_get_mention_action_content', '', $mention->name, $view_link, $mention );
			}

			$actions['view_mention'] = array(
				'wrapper_class' => 'view-link',
				'dashicon'      => 'dashicons-visibility',
				'content'       => $content,
			);
		}

		/**
		 * Use this filter if you wish to add custom actions to the current mention.
		 *
		 * @since 1.0.0
		 *
		 * @param array  $actions The list of actions available for the mention.
		 * @param object $mention The mention's object.
		 */
		$actions = apply_filters( 'cp_interactions_get_mention_actions', $actions, $mention );

		if ( empty( $actions ) ) {
			return;
		}

		return cp_cluster_actions_group( array(
			'class'       => 'cp-interactions-mention-actions',
			'action_type' => 'mention',
			'actions'     => $actions,
		) );
	}

/**
 * Output a user feedback to inform no mentions were found.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_no_mentions_found() {
	echo cp_interactions_get_no_mentions_found();
}

	/**
	 * Get the user feedback to inform no mentions were found.
	 *
	 * @since 1.0.0
	 *
	 * @return string The user feedback message.
	 */
	function cp_interactions_get_no_mentions_found() {
		$message = __( 'Aucune mention n\'a été trouvée pour cet utilisateur.', 'clusterpress' );

		if ( cp_user_is_self_profile() ) {
			$message = __( 'Vous n\'avez pas été mentionné(e) jusqu\'à présent.', 'clusterpress' );
		}

		clusterpress()->feedbacks->add_feedback(
			'interactions[user-mentions-empty]',
			/**
			 * Use this filter if you wish to edit the no mentions message
			 *
			 * @since 1.0.0
			 *
			 * @param string $message The user feedback to inform no mentions were found.
			 */
			apply_filters( 'cp_interactions_get_no_mentions_found', $message ),
			'info'
		);

		cp_get_template_part( 'assets/feedbacks' );
	}

/**
 * Output the total count for the current Mentions loop.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_total_mention_count() {
	echo cp_interactions_get_total_mention_count();
}
	/**
	 * Return the total count for the current Mentions loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string HTML for the total count.
	 */
	function cp_interactions_get_total_mention_count() {
		$loop  = clusterpress()->interactions->user_mentions;

		$displayed = number_format_i18n( $loop->mention_count );
		$total     = number_format_i18n( $loop->total_mention_count );

		if ( ! empty( $loop->new_count ) )  {
			$total += $loop->new_count;
		}

		$total_count  = sprintf(
			_n( 'Total %1$s mention, %2$s affiché.', 'Total %1$s mentions, %2$s affichée(s).', $total, 'clusterpress' ),
			esc_html( $total ),
			esc_html( $displayed )
		);

		/**
		 * Filter here to edit the total mentions count to output.
		 *
		 * @since 1.0.0
		 *
		 * @param  string $total_count the total mentions count to output
		 */
		return apply_filters( 'cp_interactions_get_total_mention_count', $total_count );
	}

/**
 * Check if a mentions loop has pagination links.
 *
 * @since 1.0.0
 *
 * @retun bool True if there are pagination links. False otherwise.
 */
function cp_interactions_mention_has_pagination_links() {
	return ! empty( clusterpress()->interactions->user_mentions->pag_links );
}

/**
 * Output the pagination links for the current Mentions loop.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_mention_pagination_links() {
	echo cp_interactions_mention_get_pagination_links();
}

	/**
	 * Return the pagination links for the current Mentions loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string output for the pagination links.
	 */
	function cp_interactions_mention_get_pagination_links() {
		/**
		 * Filter here to edit the paginate to output for mentions.
		 *
		 * @since 1.0.0
		 *
		 * @param string $pag_links The pagination links to output for mentions.
		 */
		return apply_filters( 'cp_interactions_mention_get_pagination_links', clusterpress()->interactions->user_mentions->pag_links );
	}
