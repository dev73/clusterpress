<?php
/**
 * Mentions loop.
 *
 * @package ClusterPress\interactions\classes
 * @subpackage interactions-mentions-loop
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Mentions loop Class.
 *
 * To avoid too much query overheat, we are not
 * fetching the content of the objects are mentions
 * can be linked to any content of the networK.
 *
 * @since 1.0.0
 */
class CP_Interactions_Mentions_Loop extends CP_Cluster_Loop {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args the loop args
	 */
	public function __construct( $args = array() ) {
		$r = wp_parse_args( $args, array(
			'offset'    => 0,
			'number'    => 10,
			'user_id'   => cp_get_displayed_user_id(),
		) );

		$paged = get_query_var( 'paged' );

		if ( ! $paged ) {
			if ( ! empty( $_GET['paged'] ) ) {
				$paged = absint( $_GET['paged'] );

			// Default to first page
			} else {
				$paged = 1;
			}
		}

		if ( ! $r['offset'] && ! empty( $r['number'] ) ) {
			$r['offset'] = (int) ( ( $paged - 1 ) * (int) $r['number'] );
		}

		if ( (int) get_current_user_id() === (int) $r['user_id'] ) {
			$unread = cp_interactions_get_user_mentions( $r['user_id'] );

			if ( ! empty( $unread ) ) {
				$r['exclude'] = wp_list_pluck( $unread, 'id' );
			}
		}

		$m = array_diff_key( $r, array( 'offset' => false, 'number' => false ) );
		$m = array_merge( array(
			'name' => array_merge( cp_interactions_get_post_type_mention_names(), array( 'comment_mentions' ) )
		), $m );

		$mentions = clusterpress()->user->relationships->get( array(
			'id',
			'primary_id',
			'secondary_id',
			'name',
			'date',
		), $m, $r['offset'], $r['number'], true );

		$total = 0;

		if ( ! empty( $mentions ) ) {
			$total = clusterpress()->user->relationships->count( 'id', $m );
		}

		$new_count = 0;
		if ( ! empty( $unread ) ) {
			if ( 1 === $paged ) {
				$mentions = array_merge( $unread, $mentions );
			}

			$new_count = count( $unread );
		}

		// Defaults to no pretty links.
		$paginate_args = array( 'base' => add_query_arg( 'paged', '%#%' ) );

		// Pretty links
		if ( clusterpress()->permalink_structure ) {
			$paginate_args['base']   = trailingslashit( cp_user_get_url( array(
				'rewrite_id'    => cp_user_get_mentions_rewrite_id(),
				'slug'          => cp_user_get_mentions_slug(),
			), cp_displayed_user() ) ) . '%_%';

			$paginate_args['format'] = cp_get_paged_slug() . '/%#%/';
		}

		parent::start( array(
			'plugin_prefix'    => 'cp',
			'item_name'        => 'mention',
			'item_name_plural' => 'mentions',
			'items'            => $mentions,
			'total_item_count' => $total,
			'page'             => $paged,
			'per_page'         => $r['number'],
			'new_count'        => $new_count,

		/**
		 * Filter here to edit paginate args according to the context.
		 *
		 * @since 1.0.0
		 *
		 * @param array $paginate_args The arguments of the pagination.
		 */
		), apply_filters( 'cp_interactions_mentions_loop_paginate_args', $paginate_args ) );
	}
}
