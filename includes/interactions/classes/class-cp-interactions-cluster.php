<?php
/**
 * Interactions Cluster.
 *
 * @package ClusterPress\interactions\classes
 * @subpackage interactions-cluster
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * The Interactions CLuster Class.
 *
 * @since 1.0.0
 */
class CP_Interactions_Cluster extends CP_Cluster {

	/**
	 * The constructor
	 *
	 * @since 1.0.0
	 *
	 * @param array args The Interactions Cluster arguments.
	 * {@see CP_Cluster::__construct() for a detailled list of available arguments}
	 */
	public function __construct( $args = array() )  {
		parent::__construct( $args );
	}

	/**
	 * Add the Interactions Cluster to ClusterPress main instance.
	 *
	 * @since 1.0.0
	 */
	public static function start() {
		$cp = clusterpress();

		if ( empty( $cp->interactions ) ) {
			$cp->interactions = new self( array(
				'id'            => 'interactions',
				'name'          => __( 'Intéractions', 'clusterpress' ),
				'dir'           => 'interactions',
			) );
		}

		return $cp->interactions;
	}

	/**
	 * Set specific hooks for the interactions Cluster.
	 *
	 * @since 1.0.0
	 */
	public function set_cluster_hooks() {
		// If the site cluster is not enabled, set the Loop global to be a property of this class.
		if ( ! cp_cluster_is_enabled( 'site' ) ) {
			add_filter( 'cp_site_get_loop_global', array( $this, 'set_likes_loop_global' ), 10, 1 );
		}
	}

	/**
	 * Include the needed files.
	 *
	 * NB: It Only includes files for the active
	 * features.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $files The list of files and class files
	 */
	public function load_cluster( $files = array() ) {
		$files['files']       = array();
		$files['class_files'] = array();

		// Defaults to no interactions
		$interactions_enabled = false;

		if ( cp_interactions_are_mentions_enabled() ) {
			$interactions_enabled = true;

			$files['files'] = array_merge( $files['files'], array(
				'mentions/functions',
				'mentions/tags',
				'mentions/actions',
			) );

			$files['class_files'] = array_merge( $files['class_files'], array(
				'interactions-mentions-loop',
			) );
		}

		if ( cp_interactions_is_like_enabled() ) {
			$interactions_enabled = true;

			$files['files'] = array_merge( $files['files'], array(
				'likes/functions',
				'likes/tags',
				'likes/actions',
			) );

			$files['class_files'] = array_merge( $files['class_files'], array(
				'interactions-likes-shortcode',
				'interactions-liked-posts-widget',
				'interactions-liked-comments-widget',
			) );
		}

		/**
		 * Make sure content loops and tags are available even if
		 * the site's Cluster is disabled.
		 */
		if ( $interactions_enabled && ! cp_cluster_is_enabled( 'site' ) ) {
			$files['files'] = array_merge( $files['files'], array(
				'../site/tags',
			) );

			$files['class_files'] = array_merge( $files['class_files'], array(
				'../site/site-posts-loop',
				'../site/site-comments-loop',
			) );
		}

		parent::load_cluster( $files );
	}

	/**
	 * Define cache groups for the cluster
	 *
	 * @since  1.0.0
	 */
	public function set_cluster_cache_groups() {
		if ( cp_interactions_are_mentions_enabled() ) {
			// Global groups.
			wp_cache_add_global_groups( array(
				'cp_user_mentions',
			) );
		}

		parent::set_cluster_cache_groups();
	}

	/**
	 * Return the Interactions settings
	 *
	 * @since 1.0.0.
	 *
	 * @return  array The list of settings for the Cluster.
	 */
	public function get_settings() {
		$settings = array(
			'sections' => array(
				'interactions_cluster_main_settings' => array(
					'title'    => __( 'Réglages principaux', 'clusterpress' ),
					'callback' => 'cp_core_main_settings_callback',
				),
				'interactions_cluster_slug_settings' => array(
					'title'    => __( 'Personnalisation des URLs', 'clusterpress' ),
					'callback' => 'cp_core_slug_settings_callback',
				),
			),
			'fields' => array(
				'interactions_cluster_main_settings' => array(
					'clusterpress_interactions_mentions_enabled' => array(
						'title'             => __( 'Mentions utilisateurs', 'clusterpress' ),
						'callback'          => 'cp_interactions_settings_mentions_feature',
						'sanitize_callback' => 'intval',
					),
					'clusterpress_interactions_like_enabled' => array(
						'title'             => __( '&quot;Likes&quot; pour les contenus', 'clusterpress' ),
						'callback'          => 'cp_interactions_settings_likes_feature',
						'sanitize_callback' => 'intval',
					),
				),
				'interactions_cluster_slug_settings' => array(
					'clusterpress_user_mentions_slug' => array(
						'title'             => __( 'Portion d\'URL pour la page de profil présentant les mentions de l\'utilisateur.', 'clusterpress' ),
						'callback'          => 'cp_interactions_settings_mentions_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_user_likes_slug' => array(
						'title'             => __( 'Portion d\'URL pour la page de profil présentant les &quot;likes&quot; de l\'utilisateur.', 'clusterpress' ),
						'callback'          => 'cp_interactions_settings_likes_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
				),
			),
		);

		// Remove the Slugs settings if not using pretty URLs
		if ( ! clusterpress()->permalink_structure || ( ! cp_interactions_are_mentions_enabled() && ! cp_interactions_is_like_enabled() ) ) {
			unset( $settings['sections']['interactions_cluster_slug_settings'] );
		}

		// Remove the Mentions Slug setting if mentions are not enabled.
		if ( ! cp_interactions_are_mentions_enabled() ) {
			unset( $settings['fields']['interactions_cluster_slug_settings']['clusterpress_user_mentions_slug'] );
		}

		// Remove the Likes Slug setting if likes are not enabled.
		if ( ! cp_interactions_is_like_enabled() ) {
			unset( $settings['fields']['interactions_cluster_slug_settings']['clusterpress_user_likes_slug'] );
		}

		return $settings;
	}

	/**
	 * Add Features to the "Post" post type.
	 *
	 * Using the WordPress Post Type feature is pretty interesting as it gives
	 * the opportunity to add likes or mentions to any post types.
	 *
	 * @since  1.0.0
	 *
	 * @param array $post_types The list of Post Type names.
	 * @param array $features   The list of features to add to the list of Post types.
	 */
	public function add_post_types_support( $post_types = array(), $features = array() ) {
		$post_types = array( 'post' );

		if ( cp_interactions_are_mentions_enabled() ) {
			$features = array_merge( $features, array( 'clusterpress_post_mentions', 'clusterpress_comment_mentions' ) );
		}

		if ( cp_interactions_is_like_enabled() ) {
			$features = array_merge( $features, array( 'clusterpress_post_likes', 'clusterpress_comment_likes' ) );
		}

		if ( ! empty( $features ) ) {
			parent::add_post_types_support( $post_types, $features );
		}
	}

	/**
	 * Define Rest API routes for the Likes feature.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list of Rest API Routes.
	 */
	public function get_routes() {
		if ( ! cp_interactions_is_like_enabled() ) {
			return array();
		}

		$likes_slug           = cp_user_get_likes_slug();
		$user_endpoint        = sprintf( '%1$s/(?P<id>\d+)/%2$s', cp_user_get_slug(), $likes_slug );
		$user_delete_endpoint = $user_endpoint . '/(?P<like_id>\d+)' ;

		$args = array(
			'id' => array(
				'validate_callback' => function( $param, $request, $key ) {
					return is_numeric( $param );
				}
			),
		);

		return array(
			array(
				'endpoint'            => $user_endpoint,
				'methods'             => 'GET',
				'callback'            => 'cp_interactions_get_user_likes',
				'permission_callback' => 'cp_interactions_user_likes_can_get',
				'args'                => $args,
			),
			array(
				'endpoint'            => $user_endpoint,
				'methods'             => 'POST',
				'callback'            => 'cp_interactions_post_user_like',
				'permission_callback' => 'cp_interactions_user_likes_can_post',
				'args'                => $args,
			),
			array(
				'endpoint'            => $user_delete_endpoint,
				'methods'             => 'DELETE',
				'callback'            => 'cp_interactions_delete_user_like',
				'permission_callback' => 'cp_interactions_user_likes_can_delete',
				'args'                => $args,
			),
			array(
				'endpoint'            => sprintf( 'post/(?P<id>\d+)/%s', $likes_slug ),
				'methods'             => 'GET',
				'callback'            => 'cp_interactions_get_post_likes',
				'args'                => $args,
			),
			array(
				'endpoint'            => sprintf( 'comment/(?P<id>\d+)/%s', $likes_slug ),
				'methods'             => 'GET',
				'callback'            => 'cp_interactions_get_comment_likes',
				'args'                => $args,
			),
		);
	}

	/**
	 * Return the specific feedbacks for the interactions Cluster.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list of feedbacks specific to this Cluster.
	 */
	public function get_feedbacks() {
		return array(
			'mention-01'  => array( 'cluster' => 'user', 'message' => __( 'La mention est introuvable.', 'clusterpress' ) ),
			'mention-02'  => array( 'cluster' => 'user', 'message' => __( 'La mention n\'a pas été marquée comme lue.', 'clusterpress' ) ),
			'mention-03'  => array( 'cluster' => 'user', 'message' => __( 'Les mentions sont introuvables.', 'clusterpress' ) ),
			'mention-04'  => array( 'cluster' => 'user', 'message' => __( 'Les mentions n\'ont pas été marquées comme lues.', 'clusterpress' ) ),
			'mention-05'  => array( 'cluster' => 'user', 'message' => __( 'Les mentions ont été marquées comme lues.', 'clusterpress' ) ),
		);
	}

	/**
	 * Return the specific cap maps for the interactions Cluster.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list cap maps specific to this Cluster.
	 */
	public function get_caps_map() {
		if ( ! cp_interactions_is_like_enabled() ) {
			return array();
		}

		return array(
			'cp_read_user_likes'  => 'cp_interactions_read_user_likes',
			'cp_post_user_like'   => 'cp_interactions_post_user_likes',
			'cp_delete_user_like' => 'cp_interactions_delete_user_likes',
		);
	}

	/**
	 * Use the Interactions Cluster Class to globalize content loops (Posts & Comments).
	 *
	 * @since  1.0.0
	 * @param  null $global As the Sites Cluster is not available,
	 *                      The Site Cluster Class is not set.
	 * @return CP_Interactions_Cluster The current Cluster Class.
	 */
	public function set_likes_loop_global( $global = null ) {
		return $this;
	}
}
