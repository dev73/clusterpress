<?php
/**
 * Most Liked Comments Widget.
 *
 * @package ClusterPress\interactions\classes
 * @subpackage interactions-liked-comments-widget
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

if ( class_exists( 'WP_Widget_Recent_Posts' ) ) :

/**
 * Widget class.
 *
 * @since  1.0.0
 */
class CP_Interactions_Liked_Comments_Widget extends WP_Widget_Recent_Posts {

	/**
	 * Widget Constructor
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'                   => 'clusterpress-liked-comments',
			'description'                 => __( 'Liste des commentaires &quot;likés&quot; classé en fonction du nombre de &quot;likes&quot;.', 'clusterpress' ),
			'customize_selective_refresh' => true,
		);

		WP_Widget::__construct( false, $name = __( 'CP Commentaires &quot;likés&quot;', 'clusterpress' ), $widget_ops );

		$this->alt_option_name = 'clusterpress_liked_comments';
	}

	/**
	 * Register the widget
	 *
	 * @since 1.0.0
	 */
	public static function register_widget() {
		register_widget( 'CP_Interactions_Liked_Comments_Widget' );
	}

	/**
	 * Display the widget on front end
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args
	 * @param  array $instance
	 */
	public function widget( $args = array(), $instance = array() ) {

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = __( 'Commentaires les plus appréciés', 'clusterpress' );
		if ( ! empty( $instance['title'] ) )  {
			$title = $instance['title'];
		}

		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = 5;
		if ( ! empty( $instance['number'] ) ) {
			$number = (int) $instance['number'];
		}

		echo $args['before_widget'];

		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		echo do_shortcode( sprintf( '[clusterpress_likes max="%s" object="comment" type="mini"]', $number ) );

		echo $args['after_widget'];
	}

	/**
	 * Display the form in Widgets Administration
	 *
	 * @since 1.0.0
	 *
	 * @param  array $instance
	 */
	public function form( $instance = array() ) {
		$title = '';
		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		}

		$number = 5;
		if ( isset( $instance['number'] ) ) {
			$number = (int) $instance['number'];
		}
		?>

		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Titre :', 'clusterpress' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Nombre de commentaires à afficher :', 'clusterpress' ); ?></label>
		<input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo esc_attr( $number ); ?>" size="3" /></p>

		<?php
	}
}
add_action( 'cp_widgets_init', array( 'CP_Interactions_Liked_Comments_Widget', 'register_widget' ) );

endif;
