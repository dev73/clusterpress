<?php
/**
 * Likes Shortcode handler.
 *
 * @package ClusterPress\interactions\classes
 * @subpackage interactions-likes-shortcode
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Likes Shortcode Class.
 *
 * @since 1.0.0
 */
class CP_Interactions_Likes_Shortcode {

	/**
	 * Specific global to store the loop and avoid
	 * conflicts with Cluster classes globals.
	 *
	 * @var object
	 */
	public $loop = null;

	/**
	 * Attributes of the shortcode.
	 *
	 * @var array
	 */
	public $attributes = array(
		'max'    => 0,
		'number' => 10,
		'type'   => 'expanded',
		'object' => 'post',
	);

	/**
	 * Associative array where keys are Content IDs
	 * and values the likes count.
	 *
	 * @var array
	 */
	public $likes    = array();

	/**
	 * The type of object (Post or Comment).
	 *
	 * @var string
	 */
	public $object   = 'post';

	/**
	 * The specific template file to use.
	 *
	 * @var string
	 */
	public $template = '';

	/**
	 * The constructor.
	 *
	 * @since  1.0.0
	 *
	 * @param array $atts The shortcode attributes. {
	 *    @see  cp_interactions_likes_shortcode() for a detailled description.
	 * }}
	 */
	public function __construct( $atts = array() ) {
		$this->attributes = shortcode_atts( $this->attributes, $atts );

		$templates = array(
			'post' => array(
				'expanded' => 'site/loops/posts',
				'mini'     => 'assets/widgets/posts'
			),
			'comment' => array(
				'expanded' => 'site/loops/comments',
				'mini'     => 'assets/widgets/comments'
			),
		);

		if ( empty( $this->attributes['object'] ) || empty( $this->attributes['type'] )
			|| ! isset( $templates[ $this->attributes['object'] ][ $this->attributes['type'] ] ) ) {
			return '';
		}

		// Set the object.
		$this->object   = $this->attributes['object'];

		// Set the right template according to this object.
		$this->template = $templates[ $this->object ][ $this->attributes['type'] ];

		// Get the most liked object and the corresponding count.
		$this->likes = cp_interactions_get_object_likes_count( $this->object );
	}

	/**
	 * Return the shortcode content.
	 *
	 * @since  1.0.0
	 *
	 * @return string HTML Output.
	 */
	public function content() {
		// Enqueue the likes style
		wp_enqueue_style( 'clusterpress-likes' );

		// Only enqueue the likes script for the expanded templates
		if ( 'expanded' === $this->attributes['type'] ) {
			wp_enqueue_script( 'clusterpress-likes' );
		}

		// Define temporary filters
		add_filter( 'cp_site_get_loop_global',                       array( $this, 'set_loop_global' ),       20, 1 );
		add_filter( 'cp_site_has_' . $this->object . 's_args',       array( $this, 'set_loop_args' ),         20, 1 );
		add_filter( $this->object . 's_clauses',                     array( $this, 'set_clauses' ),           10, 2 );
		add_filter( 'cp_site_' . $this->object . 's_has_pagination', '__return_false'                               );
		add_filter( 'cp_site_get_no_' . $this->object . 's_found',   array( $this, 'set_no_objets_message' ), 10, 1 );

		if ( 'mini' === $this->attributes['type'] ) {
			add_filter( 'cp_site_has_' . $this->object . 's', array( $this, 'set_likes_count' ), 20, 1 );
		}

		/**
		 * Set the content using a template so that the Widget output can be customized
		 * within the active theme.
		 */
		$return = cp_buffer_template_part( $this->template, null, false );

		if ( 'expanded' === $this->attributes['type'] ) {
			$return = '<div class="clusterpress-likes-shortcode">' . $return . '</div>';
		}

		remove_filter( 'cp_site_get_loop_global',                       array( $this, 'set_loop_global' ),       20, 1 );
		remove_filter( 'cp_site_has_' . $this->object .  's_args',      array( $this, 'set_loop_args' ),         20, 1 );
		remove_filter( $this->object . 's_clauses',                     array( $this, 'set_clauses' ),           10, 2 );
		remove_filter( 'cp_site_' . $this->object . 's_has_pagination', '__return_false'                               );
		remove_filter( 'cp_site_get_no_' . $this->object . 's_found',   array( $this, 'set_no_objets_message' ), 10, 1 );

		if ( 'mini' === $this->attributes['type'] ) {
			remove_filter( 'cp_site_has_' . $this->object . 's', array( $this, 'set_likes_count' ), 20, 1 );
		}

		return $return;
	}

	/**
	 * Set the global for the object loop.
	 *
	 * Using a specific global for the loop
	 * will avoid conflicts with regular loops.
	 *
	 * @since 1.0.0
	 *
	 * @param object $global The loop global.
	 */
	public function set_loop_global( $global = null ) {
		if ( ! $this->loop ) {
			$this->loop = new stdClass;
		}

		return $this->loop;
	}

	/**
	 * Set the specific loop args for liked contents.
	 *
	 * @since  1.0.0
	 *
	 * @param   array $args Default content loop arguments.
	 * @return  array       Loop arguments adapted to likes.
	 */
	public function set_loop_args( $args = array() ) {
		if ( ! empty( $this->likes ) ) {
			$args[ $this->object . '__in' ] = array_keys( $this->likes );

			if ( 'post' === $this->object ) {
				$args['post_type'] = get_post_types_by_support( 'clusterpress_post_likes' );
			}
		} else {
			$args[ $this->object . '__in' ] = 0;
		}

		if ( isset( $args['author'] ) ) {
			unset( $args['author'] );
		}

		if ( empty( $this->attributes['max'] ) && ! empty( $this->attributes['number'] ) ) {
			$number_key = 'number';

			if ( 'post' === $this->object ) {
				$number_key = 'posts_per_page';
			}

			$args[ $number_key ] = (int) $this->attributes['number'];
		}

		if ( 'comment' === $this->object && 'mini' === $this->attributes['type'] ) {
			$args['update_comment_post_cache'] = true;
		}

		return $args;
	}

	/**
	 * Set specific SQL Clauses to set order content according to their likes count.
	 *
	 * @since  1.0.0
	 *
	 * @param   array    $clauses      SQL Clauses
	 * @param   WP_Query $object_query The WP_Query instance.
	 * @return  array                  SQL Clauses specific to the liked contents.
	 */
	public function set_clauses( $clauses = array(), $object_query = null ) {
		global $wpdb;

		$column = $wpdb->posts . '.ID';
		if ( 'comment' === $this->object ) {
			$column = $wpdb->comments . '.comment_ID';
		}

		$o_ids     = array_merge( array( $column ), wp_parse_id_list( array_keys( $this->likes ) ) );
		$o_ids_sql = implode( ',', $o_ids );

		// Set the order to respect the Likes count.
		$clauses['orderby'] = 'FIELD(' . $o_ids_sql . ')';

		// Eventually set the limit number to the max
		if ( ! empty( $this->attributes['max'] ) ) {
			$clauses['limits'] = $wpdb->prepare( 'LIMIT 0, %d', $this->attributes['max'] );
		}

		return $clauses;
	}

	/**
	 * Adds the likes count to the loop global.
	 *
	 * @since  1.0.0
	 *
	 * @param   bool $has_objects Whether Liked contents were found or not.
	 * @return  bool              Whether Liked contents were found or not.
	 */
	public function set_likes_count( $has_objects = false ) {
		if ( true !== $has_objects ) {
			return $has_objects;
		}

		$objects  = $this->object . 's';
		$property = 'site_' . $objects;

		$id = 'ID';
		if ( 'comment' === $this->object ) {
			$id = 'comment_ID';
		}

		foreach ( $this->loop->{$property}->{$objects} as $key => $value ) {
			if ( isset( $this->likes[ $value->{$id} ] ) ) {
				$this->loop->{$property}->{$objects}[ $key ]->likes_count = $this->likes[ $value->{$id} ]->count;
			}
		}

		return $has_objects;
	}

	/**
	 * Customize the no content found message in loops.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $message The default content message.
	 * @return string          The message to inform no contents were liked.
	 */
	public function set_no_objets_message( $message = '' ) {
		$message = __( 'Aucun article n\'a été &quot;likés&quot; jusqu\'à présent.', 'clusterpress' );

		if ( 'comment' === $this->object ) {
			$message = __( 'Aucun commentaire n\'a été &quot;likés&quot; jusqu\'à présent.', 'clusterpress' );
		}

		return $message;
	}
}
