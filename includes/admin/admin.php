<?php
/**
 * ClusterPress Admin Bootstrap
 *
 * @since  1.0.0
 *
 * @package    ClusterPress\Admin
 * @subpackage admin
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/** Admin Actions *************************************************************/

/**
 * Add an alias to admin_init for ClusterPress use.
 *
 * @since 1.0.0.
 */
function cp_admin_init() {
	do_action( 'cp_admin_init' );
}
add_action( 'admin_init', 'cp_admin_init', 10 );

/**
 * Add an alias to admin_head for ClusterPress use.
 *
 * @since 1.0.0.
 */
function cp_admin_head() {
	do_action( 'cp_admin_head' );
}
add_action( 'admin_head',  'cp_admin_head', 10 );

/**
 * Add an alias to all_admin_notices for ClusterPress use.
 *
 * @since 1.0.0.
 */
function cp_admin_notices() {
	do_action( 'cp_admin_notices' );
}
add_action( 'all_admin_notices', 'cp_admin_notices', 10 );

/**
 * Add an alias to admin_enqueue_scripts for ClusterPress use.
 *
 * @since 1.0.0.
 */
function cp_admin_enqueue_scripts() {
	do_action( 'cp_admin_enqueue_scripts' );
}
add_action( 'admin_enqueue_scripts', 'cp_admin_enqueue_scripts', 10 );

/**
 * Fire the cp_activation_redirect action.
 *
 * @since 1.0.0.
 */
function cp_activation_redirect() {
	do_action( 'cp_activation_redirect' );
}
add_action( 'cp_admin_init', 'cp_activation_redirect', 1 );

/**
 * Fire the cp_register_settings action.
 *
 * @since 1.0.0.
 */
function cp_register_settings() {
	do_action( 'cp_register_settings' );
}
add_action( 'cp_admin_init', 'cp_register_settings', 11 );

/** Admin Bootstrap ***********************************************************/

/**
 * Load the Main Admin Class and add it to the plugin's main instance.
 *
 * @since  1.0.0
 */
function cp_admin() {
	require_once( cp_get_includes_dir()  . 'admin/classes/class-cp-admin.php' );

	clusterpress()->admin = new CP_Admin;
}
