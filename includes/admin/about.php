<?php
/**
 * Welcome to ClusterPress!
 *
 * @since  1.0.0
 *
 * @package    ClusterPress\Admin
 * @subpackage about
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Displays the Welcome Screen.
 *
 * @since  1.0.0
 *
 * @param  string $tab The current tab being displayed.
 * @return string HTML output.
 */
function cp_admin_welcome_screen( $tab = 'welcome' ) {
	if ( 'welcome' !== $tab ) {
		return;
	}

	$clusters = cp_get_built_in_clusters();
	$required = array_intersect_key( $clusters, array( 'core' => true, 'user' => true ) );
	$optional = array_intersect_key( $clusters, array( 'interactions' => true, 'site' => true ) );
	?>

	<div class="feature-section one-col">
		<h2><?php echo esc_html( _x( 'Survitaminez votre réseau de publication WordPress et partez au "front" !', 'headline about', 'clusterpress' ) ); ?></h2>
		<p><?php esc_html_e( 'Profitez des quatre "Clusters" de ClusterPress pour mettre en valeur l\'intégralité des contenus de votre réseau de sites (ou de votre site) et surtout leurs auteurs.', 'clusterpress' ); ?></p>
	</div>

	<hr />

	<div class="feature-section two-col">
		<h2><?php esc_html_e( 'Les indispensables', 'clusterpress' ); ?></h2>

		<?php foreach ( $required as $r ) : ?>

			<div class="col">
				<img src="<?php echo esc_url( $r['icons']['svg'] ) ;?>" class="cp-about-svg"/>
				<h3><?php echo esc_html( $r['name'] ); ?></h3>
				<p><?php echo esc_html( $r['short_description'] ); ?></p>
			</div>

		<?php endforeach ; ?>
	</div>

	<hr />

	<div class="feature-section two-col">
		<h2><?php esc_html_e( 'Les optionnels', 'clusterpress' ); ?></h2>

		<?php foreach ( $optional as $o ) : ?>

			<div class="col">
				<img src="<?php echo esc_url( $o['icons']['svg'] ) ;?>" class="cp-about-svg"/>
				<h3><?php echo esc_html( $o['name'] ); ?></h3>
				<p><?php echo esc_html( $o['short_description'] ); ?></p>
			</div>

		<?php endforeach ; ?>
	</div>

	<hr />

	<div class="changelog">
		<h2><?php esc_html_e( 'Découvrez de nouveaux horizons !', 'clusterpress' ); ?></h2>

		<div class="under-the-hood three-col">
			<div class="col">
				<h3><?php esc_html_e( 'D\'abord conçu pour le Multisite.', 'clusterpress' ); ?></h3>
				<p>
					<?php esc_html_e( 'Bien qu\'il soit possible d\'utiliser ClusterPress pour équiper votre configuration régulière de WordPress, vous en profiterez pleinement dans les configurations Multisites de WordPress. Son originalité réside dans le "Cluster" des Sites qui enrichit WordPress d\'un répertoire catégorisable et de pages individualisées pour les sites du réseau.', 'clusterpress' ); ?>
				</p>
			</div>
			<div class="col">
				<h3><?php esc_html_e( 'Bien plus qu\'une extension.', 'clusterpress' ); ?></h3>
				<p>
					<?php printf(
						esc_html__( 'ClusterPress est un projet open source, un site, et une tribu formée par vous les utilisateurs et potentiels contributeurs. Participez activement au développement, au design, à la documentation, à la traduction, et au support du projet en créant votre compte sur %s.', 'clusterpress' ),
						sprintf( '<a href="%s">Cluster.Press</a>', esc_url( __( 'https://cluster.press/wp-signup.php', 'clusterpress' ) ) )
					); ?>
				</p>
			</div>
			<div class="col">
				<h3><?php esc_html_e( 'Autonome, pour plus de maîtrise.', 'clusterpress' ); ?></h3>
				<p>
					<?php printf(
						esc_html__( 'ClusterPress gère lui même ses mises à jour et propose de nombreuses API spécifiques pour permettre à tout contributeur de profiter de ces mécanismes et de publier des extensions de ClusterPress. Il est, tout comme ses extensions, disponible en téléchargement gratuit depuis %s.', 'clusterpress' ),
						'<a href="https://cluster.press/tribu/extensions/">' . esc_html__( 'son répertoire d\'extensions', 'clusterpress' ) . '</a>'
					); ?>
				</p>
			</div>
		</div>
	</div>

	<?php
}
add_action( 'cp_admin_about_screen', 'cp_admin_welcome_screen', 10, 1 );

/**
 * Displays the Credits Screen.
 *
 * @since  1.0.0
 *
 * @param  string $tab The current tab being displayed.
 * @return string HTML output.
 */
function cp_admin_credits_screen( $tab = '' ) {
	if ( 'credits' !== $tab ) {
		return;
	}

	$img_dir_url = cp_get_includes_url()  . 'admin/img/';
	$cc          = '<a href="http://creativecommons.org/licenses/by/3.0/">CC 3.0 BY</a>';
	$flaticon    = '<a href="http://www.flaticon.com">www.flaticon.com</a>';
	?>

	<p class="about-description">
		<?php esc_html_e( 'ClusterPress est un projet open source maintenu par une tribu bienveillante et respectueuse des valeurs de la communauté WordPress.', 'clusterpress' ); ?>
	</p>

	<h3 class="wp-people-group"><?php esc_html_e( 'Créateur de ClusterPress.', 'clusterpress' ); ?></h3>
	<ul class="wp-people-group " id="wp-people-creator">
		<li class="wp-person" id="wp-person-imath">
			<a href="https://cluter.press/tribu/tribun/imath"><img src="http://0.gravatar.com/avatar/8b208ca408dad63888253ee1800d6a03?s=60" class="gravatar" alt="imath" /></a>
			<a class="web" href="https://cluter.press/tribu/tribun/imath">Mathieu Viet (imath)</a>
			<span class="title"><?php esc_html_e( 'Créateur', 'clusterpress' ); ?></span>
		</li>
	</ul>

	<h3 class="wp-people-group"><?php esc_html_e( 'Equipe de développement.', 'clusterpress' ); ?></h3>
	<p><?php printf(
		esc_html__( 'Oups ! L\'équipe de développement ne contient qu\'un seul membre : le créateur de ClusterPress %1$s... %2$s !', 'clusterpress' ),
		convert_smilies( ':)' ),
		'<a href="https://cluster.press/rejoindre/">' . esc_html__( 'Faites vos preuves pour la rejoindre', 'clusterpress' ) . '</a>'
	); ?></p>

	<h3 class="wp-people-group"><?php printf( esc_html__( 'Contributeurs de la version %s', 'clusterpress' ), cp_get_plugin_version() ); ?></h3>
	<p class="wp-credits-list">
		<a href="https://cluter.press/tribu/tribun/imath">Mathieu Viet (imath)</a>.
	</p>

	<h3 class="wp-people-group"><?php esc_html_e( 'Inspirations.', 'clusterpress' ); ?></h3>
	<ul class="wp-people-group " id="wp-people-inspirations">
		<li class="wp-person" id="wp-person-buddypress">
			<a href="https://github.com/buddypress/"><img src="https://avatars2.githubusercontent.com/u/805998?v=3&s=60" class="gravatar" alt="BuddyPress" /></a>
			<a class="web" href="https://github.com/buddypress/">BuddyPress</a>
			<span class="title"><?php printf(
				esc_html__( 'Son API "%1$s" a inspiré la %2$s API', 'clusterpress' ),
				'<a href="https://github.com/buddypress/BuddyPress/blob/master/src/bp-core/classes/class-bp-component.php">BP_Component</a>',
				'<code>CP_Cluster</code>'
			); ?></span>
		</li>
		<li class="wp-person" id="wp-person-garyjones">
			<a href="https://github.com/GaryJones"><img src="https://avatars3.githubusercontent.com/u/88371?v=2&s=60" class="gravatar" alt="Gary Jones" /></a>
			<a class="web" href="https://github.com/GaryJones">Gary Jones</a>
			<span class="title"><?php printf(
				esc_html__( 'Sa classe "%1$s" a inspiré la classe %2$s', 'clusterpress' ),
				'<a href="https://github.com/GaryJones/Gamajo-Template-Loader">Gamajo Template Loader</a>',
				'<code>CP_Template_Loader</code>'
			); ?></span>
		</li>
	</ul>

	<h3 class="wp-people-group"><?php esc_html_e( 'Eléments graphiques.', 'clusterpress' ); ?></h3>
	<ul class="wp-people-group " id="wp-people-design">
		<li class="wp-person" id="wp-person-grape">
			<a href="http://www.freepik.com"><img src="<?php echo esc_url( $img_dir_url . 'grape.svg' ); ?>" class="gravatar" alt="Freepik" /></a>
			<span class="title"><?php printf(
				esc_html__( 'Icone créée par %1$s de %2$s et sous licence %3$s.', 'clusterpress' ),
				'<a href="http://www.freepik.com">Freepik</a>',
				$flaticon,
				$cc
			); ?></span>
		</li>
		<li class="wp-person" id="wp-person-core">
			<a href="http://www.freepik.com"><img src="<?php echo esc_url( $img_dir_url . 'core.svg' ); ?>" class="gravatar" alt="Freepik" /></a>
			<span class="title"><?php printf(
				esc_html__( 'Icone créée par %1$s de %2$s et sous licence %3$s.', 'clusterpress' ),
				'<a href="http://www.freepik.com">Freepik</a>',
				$flaticon,
				$cc
			); ?></span>
		</li>
		<li class="wp-person" id="wp-person-users">
			<a href="http://www.flaticon.com/authors/madebyoliver"><img src="<?php echo esc_url( $img_dir_url . 'users.svg' ); ?>" class="gravatar" alt="Madebyoliver" /></a>
			<span class="title"><?php printf(
				esc_html__( 'Icone créée par %1$s de %2$s et sous licence %3$s.', 'clusterpress' ),
				'<a href="http://www.flaticon.com/authors/madebyoliver">Madebyoliver</a>',
				$flaticon,
				$cc
			); ?></span>
		</li>
		<li class="wp-person" id="wp-person-interactions">
			<a href="http://www.flaticon.com/authors/madebyoliver"><img src="<?php echo esc_url( $img_dir_url . 'interactions.svg' ); ?>" class="gravatar" alt="Madebyoliver" /></a>
			<span class="title"><?php printf(
				esc_html__( 'Icone créée par %1$s de %2$s et sous licence %3$s.', 'clusterpress' ),
				'<a href="http://www.flaticon.com/authors/madebyoliver">Madebyoliver</a>',
				$flaticon,
				$cc
			); ?></span>
		</li>
		<li class="wp-person" id="wp-person-site">
			<a href="http://www.freepik.com"><img src="<?php echo esc_url( $img_dir_url . 'site.svg' ); ?>" class="gravatar" alt="Freepik" /></a>
			<span class="title"><?php printf(
				esc_html__( 'Icone créée par %1$s de %2$s et sous licence %3$s.', 'clusterpress' ),
				'<a href="http://www.freepik.com">Freepik</a>',
				$flaticon,
				$cc
			); ?></span>
		</li>
	</ul>

	<?php
}
add_action( 'cp_admin_about_screen', 'cp_admin_credits_screen', 11, 1 );
