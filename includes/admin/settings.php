<?php
/**
 * ClusterPress Settings
 *
 * @since  1.0.0
 *
 * @package    ClusterPress\Admin
 * @subpackage settings
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Main settings section callback.
 *
 * @since  1.0.0
 *
 * @return null.
 */
function cp_core_main_settings_callback() {}

/**
 * Network type settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_core_settings_network_type() {
	$current_type = cp_get_network_type();
	?>
	<select name="clusterpress_network_type" id="clusterpress_network_type">
		<option value="public" <?php selected( $current_type, 'public' );?>><?php esc_html_e( 'Public', 'clusterpress' );?></option>
		<option value="private" <?php selected( $current_type, 'private' );?>><?php esc_html_e( 'Privé', 'clusterpress' );?></option>
		<option value="closed" <?php selected( $current_type, 'closed' );?>><?php esc_html_e( 'Fermé', 'clusterpress' );?></option>
	</select>
	<p class="description">
		<?php esc_html_e( 'En choisissant &quot;Public&quot;, vous permettez à tout visiteur de votre site de consulter votre annuaire d\'utilisateurs et de sites.', 'clusterpress' ); ?>
		<?php esc_html_e( 'En choisissant &quot;Privé&quot;, seuls les utilisateurs connectés de votre site pourront consulter votre annuaire d\'utilisateurs et de sites.', 'clusterpress' ); ?>
		<?php esc_html_e( 'En choisissant &quot;Fermé&quot;, les utilisateurs connectés de votre site pourront consulter leur propre page de profil utilisateur et consulter l\'annuaire de sites.', 'clusterpress' ); ?>
	</p>
	<p class="description">
		<?php printf( esc_html__( '%s : Dans tous les cas de figure, l\'annuaire de sites n\'est consultable qu\'à partir du moment où le Cluster Site est activé.', 'clusterpress' ), sprintf( '<strong><u>%s</u></strong>', esc_html__( 'NB', 'clusterpress' ) ) ); ?>
	</p>
	<?php
}

/**
 * Slugs settings section callback.
 *
 * @since  1.0.0
 *
 * @return null.
 */
function cp_core_slug_settings_callback() {}

/**
 * Root slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_core_settings_root_slug() {
	?>

	<input name="clusterpress_root_slug" id="clusterpress-root-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_get_root_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL racine du réseau est la portion qui sera ajoutée juste après le domaine de votre site ex: %s', 'clusterpress' ),
			'<code>' . esc_url( home_url( cp_get_root_slug() ) ) . '</code>'
		); ?>
		<?php esc_html_e( 'Toutes les pages générées par ClusterPress contiendront cette racine.', 'clusterpress' ); ?>
	</p>
	<?php
}

/**
 * Users Archive title settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_settings_archive_title() {
	?>

	<input name="clusterpress_users_archive_title" id="clusterpress_users_archive_title" type="text" class="regular-text code" value="<?php echo esc_attr( cp_users_get_title() ); ?>" />

	<?php
}

/**
 * Profile settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_settings_profile_fields() {
	$fields  = cp_user_get_profile_fields();
	$enabled = cp_user_get_enabled_fields();

	if ( false === $enabled ) {
		$enabled = $fields;
	} else {
		$enabled = array_flip( (array) $enabled );
	}
	?>
	<ul>
		<li>
			<label for="clusterpress-toggle-all">
				<input type="checkbox" id="clusterpress-toggle-all" checked /> <?php _e( 'Tout Cocher/ Tout décocher', 'clusterpress' );?>
			</label>
		</li>
		<?php foreach( $fields as $field_key => $field_name ):?>

			<li style="display:inline-block;width:45%;margin-right:1em">
				<label for="clusterpress-profile-field-cb-<?php echo esc_attr( $field_key ); ?>">
					<input type="checkbox" class="checkbox" id="clusterpress-profile-field-cb-<?php echo esc_attr( $field_key ); ?>" value="<?php echo esc_attr( $field_key ); ?>" name="clusterpress_user_profile_fields[]" <?php checked( isset( $enabled[ $field_key ] ) ); ?>>
					<?php echo esc_html( $field_name ); ?>
				</label>
			</li>

		<?php endforeach;?>
	</ul>
	<?php
}

/**
 * Users archive slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_settings_archive_slug() {
	?>

	<input name="clusterpress_users_slug" id="clusterpress-users-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_users_get_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL de l\'archive des utilisareurs est la portion qui sera ajoutée juste après la portion racine du réseau de votre site ex: %s', 'clusterpress' ),
			'<code>' . esc_url( home_url( cp_get_root_slug() . '/' . cp_users_get_slug() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Single User slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_settings_user_slug() {
	?>

	<input name="clusterpress_user_slug" id="clusterpress-user-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_user_get_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL des pages de profil de l\'utilisareur est la portion qui sera ajoutée juste après la portion racine du réseau et juste avant le nom d\'utilisateur du membre affiché. Ex: %s', 'clusterpress' ),
			'<code>' . esc_url( cp_user_get_url( array(), wp_get_current_user() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Single User Manage slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_settings_user_manage_slug() {
	?>

	<input name="clusterpress_user_manage_slug" id="clusterpress-user-manage-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_user_get_manage_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL pour les pages de gestion est la portion qui sera ajoutée juste après le nom d\'utilisateur du membre affiché afin de lui permettre de gérer ses préférences. Ex: %s', 'clusterpress' ),
			'<code>' . esc_url( cp_user_get_url( array( 'slug' => cp_user_get_manage_slug(), 'rewrite_id' => cp_user_get_manage_rewrite_id() ), wp_get_current_user() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Single User Manage Profile slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_settings_user_profile_slug() {
	?>

	<input name="clusterpress_user_profile_slug" id="clusterpress-user-profile-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_user_get_manage_profile_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL pour la page de gestion de profil est la portion qui sera ajoutée juste après celle des pages de gestion du membre afin de lui permettre de personnaliser ses champs de profil. Ex: %s', 'clusterpress' ),
			'<code>' . esc_url( cp_user_get_url( array( 'slug' => cp_user_get_manage_slug() . '/' . cp_user_get_manage_profile_slug(), 'rewrite_id' => cp_user_get_manage_rewrite_id() ), wp_get_current_user() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Single User Manage Account slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_settings_user_account_slug() {
	?>

	<input name="clusterpress_user_account_slug" id="clusterpress-user-account-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_user_get_manage_account_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL pour la page de gestion du compte est la portion qui sera ajoutée juste après celle des pages de gestion du membre afin de lui permettre de gérer son compte. Ex: %s', 'clusterpress' ),
			'<code>' . esc_url( cp_user_get_url( array( 'slug' => cp_user_get_manage_slug() . '/' . cp_user_get_manage_account_slug(), 'rewrite_id' => cp_user_get_manage_rewrite_id() ), wp_get_current_user() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Sites Archive title settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_settings_archive_title() {
	?>

	<input name="clusterpress_sites_archive_title" id="clusterpress_sites_archive_title" type="text" class="regular-text code" value="<?php echo esc_attr( cp_sites_get_title() ); ?>" />

	<?php
}

/**
 * Follow Sites feature settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_settings_follow_feature() {
	?>
	<input name="clusterpress_site_following_enabled" id="clusterpress-site-following-enabled" type="checkbox" value="1" <?php checked( cp_site_is_following_enabled() ); ?> />
	<label for="clusterpress-site-following-enabled"><?php esc_html_e( 'Autoriser les utilisateurs à devenir adeptes des sites', 'clusterpress' ); ?></label>
	<p class="description">
		<?php esc_html_e( 'Lorsque le suivi de site est actif, les utilisateurs peuvent suivre les derniers articles publiés par les sites et intégrer sa liste d\'adeptes. Les administrateurs de site pourront promouvoir, s\'ils le souhaitent ces adeptes sur de nouveaux rôles de contribution.', 'clusterpress' ); ?>
	</p>
	<?php
}

/**
 * Follow Sites action slug (Rest API) settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_settings_follow_action_slug() {
	?>

	<input name="clusterpress_site_follow_action_slug" id="clusterpress-site-follow-action-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_site_get_follow_action_slug() ); ?>" />
	<p class="description">
		<?php esc_html__( 'Cette identifiant permet d\'enregistrer l\'action de suivi et d\'arrêt du suivi des sites par les utilisateurs (AJAX et REST API)', 'clusterpress' ); ?>
	</p>

	<?php
}

/**
 * Sites archive slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_settings_archive_slug() {
	?>

	<input name="clusterpress_sites_slug" id="clusterpress-sites-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_sites_get_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL de l\'archive des sites est la portion qui sera ajoutée juste après la portion racine du réseau de votre site. Ex: %s', 'clusterpress' ),
			'<code>' . esc_url( home_url( cp_get_root_slug() . '/' . cp_sites_get_slug() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Sites category slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_settings_category_slug() {
	?>

	<input name="clusterpress_site_category_slug" id="clusterpress-site-category-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_site_get_category_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL racine des catégories de site est la portion qui sera ajoutée juste après la portion de l\'archive des sites et juste avant la portion d\'URL de la catégorie affichée. Ex: %s', 'clusterpress' ),
			'<code>' . esc_url( home_url( cp_get_root_slug() . '/' . cp_sites_get_slug() . '/' . cp_site_get_category_slug() . '/plugins' ) ) . '</code>'
		); ?>
	</p>
	<p class="description">
		<?php printf( esc_html__( '%s : L\'ajout de nouvelles catégories s\'effectue depuis le sous menu Catégories du menu Sites de l\'Administration du réseau. L\'assignation d\'une ou de plusieurs catégories s\'opère depuis la page de gestion de l\'environnement découverte du site ou depuis l\'onglet option du site de la zone d\'édition des sites au sein de l\'Administration du réseau.', 'clusterpress' ), sprintf( '<strong><u>%s</u></strong>', esc_html__( 'NB', 'clusterpress' ) ) ); ?>
	</p>

	<?php
}

/**
 * Single site slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_settings_site_slug() {
	?>

	<input name="clusterpress_site_slug" id="clusterpress-site-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_site_get_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL racine des pages découverte du site est la portion qui sera ajoutée juste après la portion racine du réseau de votre site et juste avant le domaine du site. Ex: %s', 'clusterpress' ),
			'<code>' . esc_url( home_url( cp_get_root_slug() . '/' . cp_site_get_slug() ) ) . '/domain</code>'
		); ?>
	</p>
	<p class="description">
		<?php printf( esc_html__( '%s : Les pages découverte sont accessibles depuis l\'archive des sites en cliquant sur le nom, l\'icône ou le bouton &quot;Découvrir&quot; du site.', 'clusterpress' ), sprintf( '<strong><u>%s</u></strong>', esc_html__( 'NB', 'clusterpress' ) ) ); ?>
	</p>
	<?php
}

/**
 * Single site members slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_settings_members_slug() {
	?>

	<input name="clusterpress_site_members_slug" id="clusterpress-site-members-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_site_get_members_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL de la page présentant les membres du site au sein de ses pages de découverte est la portion qui sera ajoutée juste après le domaine du site. Ex: %s', 'clusterpress' ),
			'<code>' . esc_url( home_url( cp_get_root_slug() . '/' . cp_site_get_slug() . '/domain/' . cp_site_get_members_slug() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Single site followers slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_settings_followers_slug() {
	?>

	<input name="clusterpress_site_followers_slug" id="clusterpress-site-followers-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_site_get_followers_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL de la page présentant les adeptes du site au sein de ses pages de découverte est la portion qui sera ajoutée juste après le domaine du site. Ex: %s', 'clusterpress' ),
			'<code>' . esc_url( home_url( cp_get_root_slug() . '/' . cp_site_get_slug() . '/domain/' . cp_site_get_followers_slug() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Single site manage slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_settings_manage_slug() {
	?>

	<input name="clusterpress_site_manage_slug" id="clusterpress-site-manage-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_site_get_manage_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL racine des pages de gestion du site au sein de ses pages de découverte est la portion qui sera ajoutée juste après le domaine du site. Ex: %s', 'clusterpress' ),
			'<code>' . esc_url( home_url( cp_get_root_slug() . '/' . cp_site_get_slug() . '/domain/' . cp_site_get_manage_slug() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Single site manage options slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_settings_site_settings_slug() {
	?>

	<input name="clusterpress_site_settings_slug" id="clusterpress-site-settings-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_site_get_settings_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL de la page de gestion des réglages du site est la portion qui sera ajoutée juste après la portion d\'URL racine des pages de gestion. Ex: %s. A partir de cette page, il est possible d\'assigner le site à une ou plusieurs catégories.', 'clusterpress' ),
			'<code>' . esc_url( home_url( cp_get_root_slug() . '/' . cp_site_get_slug() . '/domain/' . cp_site_get_manage_slug() . '/' . cp_site_get_settings_slug() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Single user posts slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_settings_user_posts_slug() {
	?>

	<input name="clusterpress_user_posts_slug" id="clusterpress-user-posts-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_user_get_posts_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL pour la page de profil présentant les articles est la portion qui sera ajoutée juste après le nom d\'utilisateur du contributeur affiché. Ex: %s', 'clusterpress' ),
			'<code>' . esc_url( cp_user_get_url( array( 'slug' => cp_user_get_posts_slug(), 'rewrite_id' => cp_user_get_posts_rewrite_id() ), wp_get_current_user() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Single user comments slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_settings_user_comments_slug() {
	?>

	<input name="clusterpress_user_comments_slug" id="clusterpress-user-comments-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_user_get_comments_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL pour la page de profil présentant les commentaires est la portion qui sera ajoutée juste après le nom d\'utilisateur du membre affiché. Ex %s', 'clusterpress' ),
			'<code>' . esc_url( cp_user_get_url( array( 'slug' => cp_user_get_comments_slug(), 'rewrite_id' => cp_user_get_comments_rewrite_id() ), wp_get_current_user() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Single user Followed sites posts slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_settings_user_following_slug() {
	?>

	<input name="clusterpress_user_following_slug" id="clusterpress-user-following-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_site_get_user_following_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL pour la page de profil présentant les articles non lus des sites suivis par l\'utilisateur est la portion qui sera ajoutée juste après le nom d\'utilisateur du membre affiché. Ex %s', 'clusterpress' ),
			'<code>' . esc_url( cp_user_get_url( array( 'slug' => cp_site_get_user_following_slug(), 'rewrite_id' => cp_site_get_user_following_rewrite_id() ), wp_get_current_user() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Single user manage Followed sites slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_site_settings_user_followed_slug() {
	?>

	<input name="clusterpress_user_manage_followed_slug" id="clusterpress-user-manage-followed-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_site_get_user_manage_followed_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL pour la page de gestion du profil présentant les les sites suivis par l\'utilisateur est la portion qui sera ajoutée juste après la portion d\'URL racine des pages de gestion du membre affiché. Ex %s', 'clusterpress' ),
			'<code>' . esc_url( cp_user_get_url( array( 'slug' => cp_user_get_manage_slug() . '/' . cp_site_get_user_manage_followed_slug(), 'rewrite_id' => cp_user_get_manage_rewrite_id() ), wp_get_current_user() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Mentions feature settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_settings_mentions_feature() {
	?>
	<input name="clusterpress_interactions_mentions_enabled" id="clusterpress-interactions-mentions-enabled" type="checkbox" value="1" <?php checked( cp_interactions_are_mentions_enabled() ); ?> />
	<label for="clusterpress-interactions-mentions-enabled"><?php esc_html_e( 'Autoriser les utilisateurs à mentionner d\'autres utilisateurs.', 'clusterpress' ); ?></label>
	<p class="description">
		<?php esc_html_e( 'Lorsque les mentions sont autorisées, les utilisateurs peuvent &quot;pinger&quot; d\'autres utilisateurs à l\'aide de mentions @nomutilisateur dans le contenu des articles ou des commentaires.', 'clusterpress' ); ?>
	</p>
	<?php
}

/**
 * Likes feature settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_settings_likes_feature() {
	?>
	<input name="clusterpress_interactions_like_enabled" id="clusterpress-interactions-like-enabled" type="checkbox" value="1" <?php checked( cp_interactions_is_like_enabled() ); ?> />
	<label for="clusterpress-interactions-like-enabled"><?php esc_html_e( 'Autoriser les utilisateurs à &quot;liker&quot; des contenus.', 'clusterpress' ); ?></label>
	<p class="description">
		<?php esc_html_e( 'Lorsque les &quot;likes&quot; sont autorisées, les utilisateurs peuvent montrer leur appréciation des articles ou des commentaires.', 'clusterpress' ); ?>
	</p>
	<?php
}

/**
 * Single user Mentions slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_settings_mentions_slug() {
	?>

	<input name="clusterpress_user_mentions_slug" id="clusterpress-user-mentions-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_user_get_mentions_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL pour la page de profil présentant les mentions reçus par l\'utilisateur est la portion qui sera ajoutée juste après le nom d\'utilisateur du membre affiché. Ex %s', 'clusterpress' ),
			'<code>' . esc_url( cp_user_get_url( array( 'slug' => cp_user_get_mentions_slug(), 'rewrite_id' => cp_user_get_mentions_rewrite_id() ), wp_get_current_user() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Single user Likes root slug settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_interactions_settings_likes_slug() {
	?>

	<input name="clusterpress_user_likes_slug" id="clusterpress-user-likes-slug" type="text" class="regular-text code" value="<?php echo esc_attr( cp_user_get_likes_slug() ); ?>" />
	<p class="description">
		<?php printf(
			esc_html__( 'La portion d\'URL pour la page de profil présentant les &quot;likes&quot; attribués par l\'utilisateur sur des contenus du site est la portion qui sera ajoutée juste après le nom d\'utilisateur du membre affiché. Ex %s', 'clusterpress' ),
			'<code>' . esc_url( cp_user_get_url( array( 'slug' => cp_user_get_likes_slug(), 'rewrite_id' => cp_user_get_likes_rewrite_id() ), wp_get_current_user() ) ) . '</code>'
		); ?>
	</p>
	<?php
}

/**
 * Profile field sanitization callback.
 *
 * @since  1.0.0
 *
 * @return array The sanitized profile field keys.
 */
function cp_sanitize_enabled_profile_fields( $option ) {
	return array_map( 'sanitize_key', (array) $option );
}

/**
 * Slug sanitization callback.
 *
 * @since  1.0.0
 *
 * @return string The sanitized slug.
 */
function cp_sanitize_slug( $option ) {
	// Remove accents
	$slug = remove_accents( $option );

	// Sanitize for the url
	$slug = sanitize_title_with_dashes( $slug );

	// Don't allow multiple slashes in a row
	$slug = preg_replace( '#/+#', '/', str_replace( '#', '', $slug ) );

	// Strip out unsafe or unusable chars
	$slug = esc_url_raw( $slug );

	// esc_url_raw() adds a scheme via esc_url(), so let's remove it
	$slug = str_replace( 'http://', '', $slug );
	$slug = str_replace( 'https://', '', $slug );

	// Trim off first and last slashes.
	$slug = ltrim( $slug, '/' );
	$slug = rtrim( $slug, '/' );

	/**
	 * Filter here to add sanitizations to slugs
	 *
	 * @since 1.0.0
	 *
	 * @param string $slug   the sanitized slug
	 * @param string $option The slug choosed by the admin
	 */
	return apply_filters( 'cp_sanitize_slug', $slug, $option );
}
