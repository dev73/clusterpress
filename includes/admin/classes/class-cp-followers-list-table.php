<?php
/**
 * ClusterPress Followers List Table
 *
 * @since  1.0.0
 *
 * @package    ClusterPress\Admin\classes
 * @subpackage followers-list-table
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Class extending the WP_List_Table to display a managing
 * followers list inside sites network admin.
 *
 * @since  1.0.0
 */
class CP_Followers_List_Table extends WP_List_Table {

	/**
	 * The Site ID
	 *
	 * @var int The Site ID
	 */
	public $site_id;

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 *
	 * @see WP_List_Table::__construct() for more information on default arguments.
	 *
	 * @param array $args An associative array of arguments.
	 */
	public function __construct( $args = array() ) {
		parent::__construct( array(
			'singular' => 'follower',
			'plural'   => 'followers',
			'screen'   => isset( $args['screen'] ) ? $args['screen'] : null,
		) );

		$this->site_id = 0;
		if ( isset( $_REQUEST['id'] ) ) {
			$this->site_id = (int) $_REQUEST['id'];
		}
	}

	/**
	 * Check the current user's permissions.
	 *
 	 * @since 1.0.0
	 *
	 * @return bool
	 */
	public function ajax_user_can() {
		return current_user_can( 'manage_sites' );
	}

	/**
	 * Prepare the followers list for display.
	 *
	 * @since 1.0.0
	 */
	public function prepare_items() {
		$followersearch = '';
		if ( isset( $_REQUEST['s'] ) ) {
			$followersearch = wp_unslash( trim( $_REQUEST['s'] ) );
		};

		$followers_per_page = $this->get_items_per_page( 'sites_page_clusterpress_followers_network_per_page' );

		$paged = $this->get_pagenum();

		$site_followers = 0;

		if ( cp_cluster_is_enabled( 'site' ) && cp_site_is_following_enabled() ) {
			$followers = cp_sites_get_site_users( $this->site_id, 'followers' );

			if ( ! empty( $followers ) ) {
				$site_followers = $followers;
			}
		}

		/**
		 * Filters the query arguments used to retrieve the followers of the list table.
		 *
		 * @since 1.0.0
		 *
		 * @param array $args Arguments passed to CP_User_Query to retrieve items for the
		 *                    followers list table.
		 */
		$args = apply_filters( 'cp_followers_list_table_query_args', array(
			'orderby'   => array( 'display_name' => 'ASC' ),
			'include'   => $site_followers,
			'number'    => $followers_per_page,
			'offset'    => ( $paged-1 ) * $followers_per_page,
			'cp_search' => $followersearch,
		) );

		if ( empty( $args['include'] ) ) {
			$this->items = array();
			$t = 0;

		// Query the followers
		} else {
			$qf          = cp_get_users( $args );
			$this->items = $qf['users'];
			$t           = $qf['total'];
		}

		$this->set_pagination_args( array(
			'total_items' => $t,
			'per_page'    => $followers_per_page,
		) );
	}

	/**
	 * Output 'no followers' message.
	 *
	 * @since 1.0.0
	 */
	public function no_items() {
		esc_html_e( 'Aucun adepte n\'a été trouvé.', 'clusterpress' );
	}

	/**
	 * Return an empty array as we don't need any views for this table.
	 *
	 * @since  1.0.0
	 *
	 * @return array An empty array.
	 */
	protected function get_views() {
		return array();
	}

	/**
	 * Retrieve an associative array of bulk actions available on this table.
	 *
	 * @since  1.0.0
	 *
	 * @return array Array of bulk actions.
	 */
	protected function get_bulk_actions() {
		$actions = array();

		if ( ! cp_cluster_is_enabled( 'site' ) || ! cp_site_is_following_enabled() ) {
			return $actions;
		}

		if ( current_user_can( 'remove_users' ) ) {
			$actions = array( 'remove' => __( 'Supprimer', 'clusterpress' ) );
		}

		/**
		 * Filter here to add ClusterPress specific bulk actions
		 *
		 * @since  1.0.0
		 *
		 * @param  array $actions The ClusterPress Bulk actions.
		 */
		return apply_filters( 'cp_followers_bulk_actions', $actions );
	}

	/**
	 * Output the controls to allow the superadmin to promote Followers.
	 *
	 * @since 1.0.0
	 *
	 * @param string $which Whether this is being invoked above ("top")
	 *                      or below the table ("bottom").
	 */
	protected function extra_tablenav( $which ) {
		$id = 'promote';
		if ( 'bottom' === $which ) {
			$id = 'promote2';
		}
		?>

		<div class="alignleft actions">
			<?php if ( current_user_can( 'promote_users' ) && $this->has_items() ) : ?>

				<label class="screen-reader-text" for="<?php echo $id ?>">
					<?php esc_html_e( 'Promouvoir en tant que&hellip;', 'clusterpress' ) ?>
				</label>

				<select name="<?php echo $id ?>" id="<?php echo $id ?>">
					<option value=""><?php esc_html_e( 'Promouvoir en tant que&hellip;', 'clusterpress' ) ?></option>
					<?php wp_dropdown_roles(); ?>
				</select>

				<?php submit_button( __( 'Promouvoir', 'clusterpress' ), 'button', 'promoteit', false );

			endif;

			/**
			 * Fires just before the closing div containing the bulk role-promote controls
			 * in the Followers list table.
			 *
			 * @since 1.0.0.
			 *
			 * @param string $which The location of the extra table nav markup: 'top' or 'bottom'.
			 */
			do_action( 'cp_restrict_manage_followers', $which ); ?>
		</div>

		<?php
	}

	/**
	 * Capture the bulk action required, and return it.
	 *
	 * @since  1.0.0
	 *
	 * @return string The bulk action required.
	 */
	public function current_action() {
		if ( isset( $_REQUEST['promoteit'] ) &&
			( ! empty( $_REQUEST['promote'] ) || ! empty( $_REQUEST['promote2'] ) ) ) {
			return 'promote';
		}

		return parent::current_action();
	}

	/**
	 * Get a list of columns for the list table.
	 *
	 * @since  1.0.0
	 *
	 * @return array Array in which the key is the ID of the column,
	 *               and the value is the description.
	 */
	public function get_columns() {
		return array(
			'cb'       => '<input type="checkbox" />',
			'username' => __( 'Nom d\'utilisateur', 'clusterpress' ),
			'name'     => __( 'Nom', 'clusterpress' ),
			'email'    => __( 'Courriel', 'clusterpress' ),
		);
	}

	/**
	 * No sortable columns available for the list table.
	 *
	 * @since 1.0.0
	 *
	 * @return array An empty array to disable sortable columns.
	 */
	protected function get_sortable_columns() {
		return array();
	}

	/**
	 * Generate the list table rows.
	 *
	 * @since 1.0.0
	 */
	public function display_rows() {
		foreach ( $this->items as $followerid => $follower_object ) {
			echo "\n\t" . $this->single_row( $follower_object );
		}
	}

	/**
	 * Generate HTML for a single row for the follower.
	 *
	 * @since 1.0.0
	 *
	 * @param object $follower_object The current follower object.
	 * @return string Output for a single row.
	 */
	public function single_row( $follower_object ) {
		$email = $follower_object->user_email;

		// Set up the hover actions for this user
		$actions = array();
		$checkbox = '';

		// Check if the user for this row is editable
		if ( current_user_can( 'list_users' ) ) {
			// Set up the user editing link
			$edit_link = esc_url( add_query_arg( 'wp_http_referer', urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ), get_edit_user_link( $follower_object->ID ) ) );

			if ( current_user_can( 'edit_user', $follower_object->ID ) ) {
				$edit        = sprintf( '<strong><a href="%1$s">%2$s</a></strong><br />', $edit_link, esc_html( $follower_object->user_login ) );
				$remove_link = esc_url( wp_nonce_url( add_query_arg( array(
					'page'      => 'clusterpress-followers',
					'id'        => $this->site_id,
					'action'    => 'remove_follower',
					'followers' => $follower_object->ID,
				), network_admin_url( 'sites.php' ) ), 'cp-remove-follower' ) );

				$actions = array(
					'edit'   => sprintf( '<a href="%1$s">%2$s</a>', $edit_link, esc_html__( 'Modifier', 'clusterpress' ) ),
					'remove' => sprintf( '<a href="%1$s" class="delete">%2$s</a>', $remove_link, esc_html__( 'Supprimer', 'clusterpress' ) ),
				);
			} else {
				$edit = sprintf( '<strong>%s</strong><br />', esc_html( $follower_object->user_login ) );
			}

			/**
			 * Filters the action links displayed under each user in the Users list table.
			 *
			 * @since 1.0.0
			 *
			 * @param array   $actions        An array of action links to be displayed.
			 *                                Default 'Edit'.
			 * @param object $follower_object Follower object for the currently-listed follower.
			 */
			$actions = apply_filters( 'cp_follower_row_actions', $actions, $follower_object );

			// Set up the checkbox ( because the user is editable, otherwise it's empty )
			$checkbox = '<label class="screen-reader-text" for="user_' . $follower_object->ID . '">' . sprintf( __( 'Sélectionner %s', 'clusterpress' ), $follower_object->user_login ) . '</label>'
						. "<input type='checkbox' name='followers[]' id='follower_{$follower_object->ID}' value='{$follower_object->ID}' />";

		} else {
			$edit = '<strong>' . $follower_object->user_login . '</strong>';
		}

		$avatar = get_avatar( $follower_object->ID, 32 );

		$r = "<tr id='follower-$follower_object->ID'>";

		list( $columns, $hidden, $sortable, $primary ) = $this->get_column_info();

		foreach ( $columns as $column_name => $column_display_name ) {
			$classes = "$column_name column-$column_name";
			if ( $primary === $column_name ) {
				$classes .= ' has-row-actions column-primary';
			}

			if ( in_array( $column_name, $hidden ) ) {
				$classes .= ' hidden';
			}

			$data = 'data-colname="' . wp_strip_all_tags( $column_display_name ) . '"';

			$attributes = "class='$classes' $data";

			if ( 'cb' === $column_name ) {
				$r .= "<th scope='row' class='check-column'>$checkbox</th>";
			} else {
				$r .= "<td $attributes>";
				switch ( $column_name ) {
					case 'username':
						$r .= "$avatar $edit";
						break;
					case 'name':
						$r .= "$follower_object->display_name";
						break;
					case 'email':
						$r .= sprintf( '<a href="%1$s">%2$s</a>', esc_url( "mailto:$email" ), $email );
						break;

					default:
						/**
						 * Filters the display output of custom columns in the Followers list table.
						 *
						 * @since 1.0.0
						 *
						 * @param string $output      Custom column output. Default empty.
						 * @param string $column_name Column name.
						 * @param int    $user_id     ID of the currently-listed follower.
						 */
						$r .= apply_filters( 'cp_manage_followers_custom_column', '', $column_name, $follower_object->ID );
				}

				if ( $primary === $column_name ) {
					$r .= $this->row_actions( $actions );
				}

				$r .= "</td>";
			}
		}

		$r .= '</tr>';

		return $r;
	}

	/**
	 * Gets the name of the default primary column.
	 *
	 * @since 1.0.0
	 *
	 * @return string Name of the default primary column, in this case, 'username'.
	 */
	protected function get_default_primary_column_name() {
		return 'username';
	}
}
