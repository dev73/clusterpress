<?php
/**
 * ClusterPress Administration
 *
 * @since  1.0.0
 *
 * @package    ClusterPress\Admin\classes
 * @subpackage admin
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * The ClusterPress Admin Class.
 *
 * Mainly deals with needed Administration screens.
 *
 * @since  1.0.0
 */
final class CP_Admin {

	/**
	 * The admin class's constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		$this->setup_globals();
		$this->setup_actions();
	}

	/**
	 * Set admin globals.
	 *
	 * @since 1.0.0
	 */
	private function setup_globals() {

		// Paths and URLs
		$this->admin_dir  = trailingslashit( cp_get_includes_dir()  . 'admin' );
		$this->admin_url  = trailingslashit( cp_get_includes_url()  . 'admin' );
		$this->images_url = trailingslashit( $this->admin_url       . 'img'   );
		$this->css_url    = trailingslashit( $this->admin_url       . 'css'   );
		$this->js_url     = trailingslashit( $this->admin_url       . 'js'    );
		$this->min        = cp_get_minified_suffix();

		// Default settings page & required capability.
		$this->settings_page      = 'options-general.php';
		$this->capability         = 'manage_options';
		$this->menu_hook          = 'admin_menu';
		$this->site_options       = null;
		$this->site_followers     = null;
		$this->site_category_slug = '';

		// Updates available ?
		$updates = get_site_transient( 'clusterpress_updates' );
		$this->cp_updates = false;

		if ( ! empty( $updates ) && 'no_updates' !== current( $updates ) ) {
			$this->cp_updates = $updates;
		}

		// Multisite specific settings page & required capability.
		if ( is_multisite() ) {
			$this->settings_page      = 'settings.php';
			$this->capability         = 'manage_network_options';
			$this->menu_hook          = 'network_admin_menu';
			$this->post_install_tasks = cp_admin_get_cluster_post_install_tasks();
		}
	}

	/**
	 * Set Up Admin Actions.
	 *
	 * @since 1.0.0
	 */
	private function setup_actions() {
		// Admin menus.
		add_action( $this->menu_hook,                        array( $this, 'admin_menus'             )     );
		add_action( 'cp_admin_head',                         array( $this, 'admin_head'              ), 10 );
		add_action( 'admin_head-settings_page_clusterpress', array( $this, 'settings_menu_highlight' )     );

		// CSS/JS.
		add_action( 'cp_admin_enqueue_scripts', array( $this, 'register_scripts' ),  1 );
		add_action( 'cp_admin_enqueue_scripts', array( $this, 'global_css'       ), 10 );

		// Add a ClusterPress WP Nav Menu Accordion Box.
		if ( cp_is_main_site() ) {
			add_action( 'load-nav-menus.php', array( $this, 'menu_accordion' ), 10, 1 );
		}

		// Site Edit Screens
		if ( cp_cluster_is_enabled( 'site' ) ) {
			// Site options & Categories
			add_action( 'admin_head-sites_page_clusterpress-categories', array( $this, 'sites_categories_menu_highlight' )        );
			add_action( 'admin_head-sites_page_clusterpress-options',    array( $this, 'site_subnav_menu_highlight'      )        );
			add_filter( 'manage_sites_action_links',                     array( $this, 'set_sites_action_links'          ), 10, 2 );
			add_filter( 'network_edit_site_nav_links',                   array( $this, 'site_clusterpress_nav'           ), 10, 1 );
			add_filter( 'term_updated_messages',                         'cp_admin_site_category_updated_messages',         10, 1 );
			add_filter( 'ms_sites_list_table_query_args',                'cp_admin_sites_table_query_args',                 10, 1 );
			add_action( 'load-sites.php',                                array( $this, 'prepare_category_dropdown'       ), 10, 1 );

			// Followers
			if ( cp_site_is_following_enabled() ) {
				add_action( 'admin_head-sites_page_clusterpress-followers',  array( $this, 'site_subnav_menu_highlight'  )        );
				add_filter( 'set-screen-option',                             array( $this, 'set_followers_screen_option' ), 10, 3 );
				add_filter( 'user_row_actions',                              array( $this, 'set_demote_action'           ), 10, 2 );
				add_filter( 'bulk_actions-site-users-network',               array( $this, 'set_demote_bulk_action'      ), 10, 1 );
				add_filter( 'handle_bulk_actions-site-users-network',        array( $this, 'handle_demote_bulk_action'   ), 10, 4 );
				add_action( 'load-site-users.php',                           array( $this, 'handle_demote_action'        ), 10    );
			}
		}

		// Display notices if needed.
		add_action( 'cp_admin_notices', array( $this, 'notices' ) );
	}

	/**
	 * Set Admin menus.
	 *
	 * @since  1.0.0
	 */
	public function admin_menus() {
		$screen_ids = array();

		$updates_bubble  = '';
		$main_menu_title = __( 'ClusterPress', 'clusterpress' );

		if ( ! empty( $this->cp_updates ) && is_array( $this->cp_updates ) ) {
			$updates_bubble  = '%1$s <span class="cluster-tasks">%2$s</span>';
			$main_menu_title = sprintf( $updates_bubble,
				$main_menu_title,
				number_format_i18n( count( $this->cp_updates ) )
			);
		}

		// This submenu will be removed later
		$screen_ids['cluster_settings'] = add_submenu_page(
			$this->settings_page,
			__( 'ClusterPress', 'clusterpress' ),
			__( 'ClusterPress', 'clusterpress' ),
			$this->capability,
			'clusterpress',
			array( $this, 'settings_screen' )
		);

		// Just after Sites in network admin, and after Users in regular admin.
		$screen_ids['main'] = add_menu_page(
			__( 'ClusterPress', 'clusterpress' ),
			$main_menu_title,
			$this->capability,
			'clusterpress-main',
			array( $this, 'admin_screen' ),
			$this->images_url . 'grape-w.svg',
			is_network_admin() ? 7 : 70
		);

		if ( ! empty( $updates_bubble ) ) {
			$updates_menu_title = __( 'Mises à jour', 'clusterpress' );
			$u_menu_title       = sprintf( $updates_bubble,
				$updates_menu_title,
				number_format_i18n( count( $this->cp_updates ) )
			);

			$screen_ids['updates'] = add_submenu_page(
				'clusterpress-main',
				$updates_menu_title,
				$u_menu_title,
				$this->capability,
				'clusterpress-upgrades',
				array( $this, 'cluster_updates' )
			);
		}

		$screen_ids['settings'] = add_submenu_page(
			'clusterpress-main',
			__( 'Réglages', 'clusterpress' ),
			__( 'Réglages', 'clusterpress' ),
			$this->capability,
			'clusterpress-settings',
			array( $this, 'settings_screen' )
		);

		// Add screens to manage Site categories and site options.
		if ( is_network_admin() && cp_cluster_is_enabled( 'site' ) ) {
			$screen_ids['sites_categories'] = add_submenu_page(
				'sites.php',
				__( 'Catégories', 'clusterpress' ),
				__( 'Catégories', 'clusterpress' ),
				$this->capability,
				'clusterpress-categories',
				array( $this, 'sites_categories_screen' )
			);

			$screen_ids['site_options'] = add_submenu_page(
				'sites.php',
				__( 'Options', 'clusterpress' ),
				__( 'Options', 'clusterpress' ),
				$this->capability,
				'clusterpress-options',
				array( $this, 'site_options_screen' )
			);

			$this->site_options  = $screen_ids['site_options'];

			if ( cp_site_is_following_enabled() ) {
				$screen_ids['site_followers'] = add_submenu_page(
					'sites.php',
					__( 'Gérer les adeptes', 'clusterpress' ),
					__( 'Gérer les adeptes', 'clusterpress' ),
					$this->capability,
					'clusterpress-followers',
					array( $this, 'site_followers_screen' )
				);

				$this->site_followers = $screen_ids['site_followers'];
			}
		}

		// Checks if some post install tasks are required.
		if ( ! empty( $this->post_install_tasks ) ) {
			$task_menu_title = __( 'Tâches de Cluster', 'clusterpress' );
			$menu_title      = sprintf( '%1$s <span class="cluster-tasks">%2$s</span>',
				$task_menu_title,
				number_format_i18n( count( $this->post_install_tasks ) )
			);

			$screen_ids['tasks'] = add_submenu_page(
				'clusterpress-main',
				$task_menu_title,
				$menu_title,
				$this->capability,
				'clusterpress-tasks',
				array( $this, 'cluster_tasks' )
			);
		}

		// Do specific things to prepare/handle the ClusterPress screen
		foreach ( $screen_ids as $screen_key => $screen_id ) {
			add_action( "load-{$screen_id}", array( $this, "load_{$screen_key}" ) );
		}

		// Welcome Screen
		add_submenu_page(
			'index.php',
			__( 'Bienvenue dans ClusterPress', 'clusterpress' ),
			__( 'Bienvenue dans ClusterPress', 'clusterpress' ),
			$this->capability,
			'clusterpress-about',
			array( $this, 'about_screen' )
		);
	}

	/**
	 * Handle Clusters activation and deactivation.
	 *
	 * @since  1.0.0
	 */
	public function load_main() {
		if ( ! empty( $_GET ) ) {
			$redirect         = add_query_arg( array(
				'page'     => 'clusterpress-main',
			), self_admin_url( 'admin.php' ) );
		}

		/**
		 * Custom Clusters are regular WordPress plugins with specific Header tags.
		 *
		 * When a custom Cluster is activated or deactivated, we need to first activate,
		 * deactivate the WordPress plugin before enabling/disabling the Cluster.
		 */
		if ( ! empty( $_GET['plugin'] ) && ! empty( $_GET['cluster'] ) && ! empty( $_GET['action']) ) {
			$plugin  = $_GET['plugin'];
			$cluster = $_GET['cluster'];
			$action  = $_GET['action'];

			check_admin_referer( 'cp_' . $action . '_plugin_' . $plugin );

			// Activate the plugin before enabling the Cluster.
			if ( 'enable' === $action ) {
				if ( ! current_user_can( 'activate_plugins' ) ) {
					wp_die( __( 'Désolé vous n\'êtes pas autorisé à activer des extensions sur ce site.', 'clusterpress' ) );
				}

				$redirect_error = add_query_arg( 'error', 'error-while-plugin-activation', $redirect );

				$activated = activate_plugin( $plugin, $redirect_error, is_network_admin() );
				if ( is_wp_error( $activated) ) {
					wp_safe_redirect( $redirect_error );
					exit;
				}

				// Mimic what WordPress does once the plugin is active
				if ( ! is_network_admin() ) {
					$recent = (array) get_option( 'recently_activated' );
					unset( $recent[ $plugin ] );
					update_option( 'recently_activated', $recent );
				} else {
					$recent = (array) get_site_option( 'recently_activated' );
					unset( $recent[ $plugin ] );
					update_site_option( 'recently_activated', $recent );
				}

			// Deactivate the plugin before disabling the Cluster.
			} elseif ( 'disable' === $action ) {
				deactivate_plugins( $plugin, false, is_network_admin() );

				if ( ! is_network_admin() ) {
					update_option( 'recently_activated', array( $plugin => time() ) + (array) get_option( 'recently_activated' ) );
				} else {
					update_site_option( 'recently_activated', array( $plugin => time() ) + (array) get_site_option( 'recently_activated' ) );
				}
			}

			// Redirect to enable/disable the Cluster
			wp_safe_redirect( add_query_arg( $action, $cluster, $redirect ) );
			exit;

		// Manage built in Clusters.
		} elseif ( ! empty( $_GET['enable'] ) || ! empty( $_GET['disable'] ) ) {
			// Get already enabled
			$enabled_clusters = cp_get_enabled_clusters();
			$check_enabled    = array_flip( $enabled_clusters );

			// Enable?
			if ( ! empty( $_GET['enable'] ) ) {
				$cluster = sanitize_key( $_GET['enable'] );

				if ( isset( $check_enabled[$cluster] ) ) {
					wp_safe_redirect( add_query_arg( 'status', 'error-already-enabled', $redirect ) );
					exit;
				}

				$enabled_clusters[] = $cluster;
				update_network_option( 0, 'cp_active_clusters', $enabled_clusters );

				$cp = clusterpress();
				$cp->set_cluster_class( $cluster );

				do_action( 'cp_set_clusters' );

				if ( isset( $cp->{$cluster} ) ) {
					// Include the WordPress Upgrader.
					require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

					// Install the cluster
					$cp->{$cluster}->install();

					// Make sure rewrite rules are updated on next page load
					cp_delete_rewrite_rules();
				}

				wp_safe_redirect( add_query_arg( 'status', 'enabled', $redirect ) );
				exit;

			// Disable?
			} elseif ( ! empty( $_GET['disable'] ) ) {
				$cluster = sanitize_key( $_GET['disable'] );

				$src = array_search( $cluster, $enabled_clusters );

				if ( false === $src ) {
					wp_safe_redirect( add_query_arg( 'status', 'error-already-disabled', $redirect ) );
					exit;
				}

				unset( $enabled_clusters[$src] );
				update_network_option( 0, 'cp_active_clusters', $enabled_clusters );

				// Make sure rewrite rules are updated on next page load
				cp_delete_rewrite_rules();

				wp_safe_redirect( add_query_arg( 'status', 'disabled', $redirect ) );
				exit;
			}
		}

		/**
		 * we've been redirected after a plugin install, but the new plugin
		 * is not listed unless the Plugins cache is cleaned.
		 */
		if ( ! empty( $_GET['installed'] ) ) {
			wp_clean_plugins_cache();
		}

		// Make sure the List Table our Clusters List Table is extending is available.
		if ( ! class_exists( 'WP_Plugin_Install_List_Table' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/class-wp-plugin-install-list-table.php' );
		}

		// Load our Clusters List Table
		require_once( $this->admin_dir . 'classes/class-cp-manage-clusters.php' );

		// Instantiate it and globalize it for a later use in the main Administration screen.
		$this->clusters_table = new CP_Manage_Clusters( array(
			'plural'   => __( 'clusters', 'clusterpress' ),
			'singular' => __( 'cluster', 'clusterpress' ),
			'screen'   => get_current_screen(),
		) );

		// Prepare the Clusters to list in the Table.
		$this->clusters_table->prepare_items();

		// Add WordPress specific JS files.
		wp_enqueue_script( 'plugin-install' );
		add_thickbox();
		wp_enqueue_script( 'updates' );

		// Add our the plugin-install-php class so that these JS files work fine.
		add_filter( 'admin_body_class', array( $this, 'admin_body_class' ) );
	}

	/**
	 * Adds the same body class WordPress uses to manage its plugins
	 * as Clusters are primarly plugins.
	 *
	 * @since 1.0.0
	 */
	public function admin_body_class() {
		return 'plugin-install-php';
	}

	/**
	 * Do needed menu customization.
	 * Mainly remove submenus.
	 *
	 * @since  1.0.0
	 */
	public function admin_head() {
		remove_submenu_page( $this->settings_page, 'clusterpress' );

		if ( ! empty( $this->site_options ) ) {
			remove_submenu_page( 'sites.php', 'clusterpress-options' );
		}

		if ( ! empty( $this->site_followers ) ) {
			remove_submenu_page( 'sites.php', 'clusterpress-followers' );
		}

		// Always hide the Welcome Screen.
		remove_submenu_page( 'index.php', 'clusterpress-about' );
	}

	/**
	 * Display the main Administration screen.
	 *
	 * It's used to let the Admin enable/disable built-in and custom Clusters.
	 *
	 * @since  1.0.0
	 *
	 * @return string HTML Output.
	 */
	public function admin_screen() {
		remove_filter( 'admin_body_class', array( $this, 'admin_body_class' ) );
		?>
		<div class="wrap">
			<h1><?php esc_html_e( 'Gérer les Clusters', 'clusterpress' ); ?></h1>

			<?php
			/**
			 * User feedbacks.
			 */
			if ( ! empty( $_GET['status'] ) ) {
				$type = 'error';

				switch ( $_GET['status'] ) {
					case 'error-already-enabled' :
						$message = __( 'Ce Cluster est déjà activé.', 'clusterpress' );
						break;

					case 'error-already-disabled' :
						$message = __( 'Ce Cluster est déjà désactivé.', 'clusterpress' );
						break;

					case 'enabled' :
						$type = 'updated';
						$message = __( 'Le Cluster a été activé avec succès.', 'clusterpress' );
						break;

					case 'disabled' :
						$type = 'updated';
						$message = __( 'Le Cluster a été désactivé avec succès.', 'clusterpress' );
						break;
				}

				?>
				<div id="message" class="<?php echo $type ;?> notice is-dismissible"><p><?php echo esc_html( $message ) ?></p></div>
				<?php
			}

			$this->clusters_table->views(); ?>

			<br class="clear" />
			<span class="spinner"></span>

			<form id="cluster-filter" method="post">
				<?php $this->clusters_table->display(); ?>
			</form>
		</div>

		<?php
		// Enqueue the script to Ajax install new Clusters.
		if ( ! empty( $_GET['tab'] ) && 'cluster.press' === $_GET['tab'] ) {
			wp_enqueue_script( 'clusterpress-upgrade' );
		}
	}

	/**
	 * Redirect to the settings page when it's required.
	 *
	 * @since 1.0.0
	 */
	public function load_settings() {
		wp_safe_redirect( add_query_arg( 'page', 'clusterpress', self_admin_url( $this->settings_page ) ) );
		exit();
	}

	/**
	 * Include options head file to enjoy WordPress settings feedback.
	 *
	 * @since 1.0.0
	 */
	public function restore_settings_feedback() {
		require( ABSPATH . 'wp-admin/options-head.php' );
	}

	/**
	 * Handle settings changes.
	 *
	 * @since  1.0.0
	 */
	public function load_cluster_settings() {
		add_action( 'all_admin_notices', array( $this, 'restore_settings_feedback' ) );

		if ( ! empty( $_POST['clusterpress_settings'] ) && ! empty( $_POST['option_page'] ) ) {
			$option_page = $_POST['option_page'];

			check_admin_referer( $option_page . '-options' );

			$options = apply_filters( 'whitelist_options', array() );

			if ( isset( $options[ $option_page ] ) ) {

				foreach ( $options[$option_page] as $option ) {

					$option = trim( $option );
					$value = null;

					if ( isset( $_POST[ $option ] ) ) {
						$value = $_POST[ $option ];

						if ( ! is_array( $value ) ) {
							$value = trim( $value );
						}

						$value = wp_unslash( $value );
					}

					update_network_option( 0, $option, $value );
				}

				// Make sure rewrite rules are updated on next page load
				cp_delete_rewrite_rules();
			}

			wp_redirect( add_query_arg( 'updated', 'true',  wp_get_referer() ) );
			exit;
		}
	}

	/**
	 * Customize the Setting menu so that the good submenu is highlighted.
	 *
	 * @since 1.0.0
	 */
	public function settings_menu_highlight() {
		global $parent_file, $pagenow, $submenu_file;

		$parent_file  = $pagenow = 'clusterpress-main';
		$submenu_file = 'clusterpress-settings';

		// Above is not enough! Here comes Javascript to the rescue!
		cp_admin_add_inline_menu_script();
	}

	/**
	 * Display the Settings screen.
	 *
	 * It's a tabbed UI generated by Cluster bootstrap classes.
	 *
	 * @since  1.0.0
	 *
	 * @return string HTML Output.
	 */
	public function settings_screen() {
		$tab = 'core';

		if ( ! empty( $_GET['tab'] ) ) {
			$tab = $_GET['tab'];
		}

		$form_url = add_query_arg( 'page', 'clusterpress', self_admin_url( $this->settings_page ) );

		// Add the inline script to select/deselect all checkboxes
		if ( 'user' === $tab ) {
			cp_admin_add_inline_profile_field_script();
		}
		?>
		<div class="wrap">
			<h1><?php esc_html_e( 'Personnaliser les réglages', 'clusterpress' ); ?></h1>

			<?php if ( ! empty( $this->settings_tabs ) ) : ?>
				<div class="wp-filter">
					<ul class="filter-links">

						<?php foreach ( $this->settings_tabs as $key => $name ) :
							$url = add_query_arg( 'tab', $key, $form_url );
							$class = '';

							if ( $tab === $key ) {
								$class    = ' class="current"';
								$form_url = $url;
							}
						?>
							<li class="cluster-<?php echo esc_attr( $key ); ?>">
								<a href="<?php echo esc_url( $url ); ?>"<?php echo $class; ?>><?php echo esc_html( $name ); ?></a>
							</li>
						<?php endforeach ; ?>

					</ul>
				</div>
			<?php endif ;?>

			<form action="<?php echo esc_url( $form_url ); ?>" method="post">

				<?php settings_fields( 'clusterpress_' . $tab ); ?>

				<?php do_settings_sections( 'clusterpress_' . $tab ); ?>

				<p class="submit">
					<input type="submit" name="clusterpress_settings" class="button-primary" value="<?php esc_attr_e( 'Sauvegarder', 'clusterpress' ); ?>" />
				</p>
			</form>
		</div>
		<?php
	}

	/**
	 * Register the Upgrade UI scripts.
	 *
	 * @since 1.0.0
	 */
	public function register_scripts() {
		wp_register_script(
			'clusterpress-upgrade',
			$this->js_url . "upgrade{$this->min}.js",
			array( 'jquery' ),
			cp_get_plugin_version(),
			true
		);

		wp_localize_script( 'clusterpress-upgrade', 'clusterPress', array(
			'nonce'      => wp_create_nonce( 'updates' ),
			'installing' => __( 'Installation en cours...', 'clusterpress' ),
			'ajaxUrl'    => admin_url( 'admin-ajax.php', 'relative' ),
			'redirect'   => esc_url_raw( add_query_arg( 'page', 'clusterpress-main', self_admin_url( 'admin.php' ) ) ),
		) );
	}

	/**
	 * Enqueue the needed CSS rules.
	 *
	 * @since 1.0.0
	 */
	public function global_css() {
		wp_enqueue_style(
			'clusterpress-admin',
			$this->css_url . "admin{$this->min}.css",
			array( 'dashicons' ),
			cp_get_plugin_version()
		);
	}

	/**
	 * If the admin confirmed it, load the Scripts required to AJAX update
	 * ClusterPress and Clusters hosted on Cluster.Press.
	 *
	 * @since  1.0.0
	 */
	public function load_updates() {
		if ( empty( $_GET['doupdate'] ) ) {
			return;
		}

		$update_tasks = array();

		foreach ( $this->cp_updates as $update ) {
			$name = $update->name;

			if ( 'clusterpress' === $update->slug ) {
				$name = 'ClusterPress';
			}

			$update_tasks[ $update->slug ] = array(
				'callback'  => $update->slug,
				'count'     => 1,
				'message'   => sprintf( _x( 'Mise à jour de %s.', 'ClusterPress Batch Processor feedback message', 'clusterpress' ), $name ),
				'number'    => 1,
			);
		}

		/**
		 * If Main plugin is in, only perform its upgrade
		 * as it will be deleted after the upgrade and we need it back
		 * to upgrade custom Clusters!
		 */
		if ( ! empty( $update_tasks['clusterpress'] ) ) {
			$update_tasks = array( 'clusterpress' => $update_tasks['clusterpress'] );
		}

		// Enqueue the Tasks script
		wp_enqueue_script( 'clusterpress-tasks-js',
			cp_get_includes_js_url() . "tasks{$this->min}.js",
			array( 'jquery', 'json2', 'wp-backbone' ),
			cp_get_plugin_version(),
			true
		);

		// Localize it.
		wp_localize_script( 'clusterpress-tasks-js', 'ClusterPress_l10n', array(
			'tasks'     => array_values( $update_tasks ),
			'feedbacks' => array(
				'updated'  => __( 'La (les) mise(s) à jour est (sont) terminée(s). Merci pour votre patience.', 'clusterpress' ),
				'redirect' => __( 'ClusterPress est à jour, vous allez être redirigé vers la note de version dans quelques secondes...', 'clusterpress' ),
			),
			'nonce'               => wp_create_nonce( 'clusterpress-bulk-update' ),
			'action'              => 'cp_process_updates',
			'coreUpgradeRedirect' => esc_url_raw( add_query_arg( 'page', 'clusterpress-about', self_admin_url( 'index.php' ) ) ),
		) );
	}

	/**
	 * Screen to ask for Upgrade confirmation / perform the confirmed upgrades.
	 *
	 * @since  1.0.0
	 *
	 * @return string HTML Output.
	 */
	public function cluster_updates() {
		$has_core = false;

		// Build an array to check current versions against the updates ones.
		$clusters = array_merge( cp_admin_get_custom_clusters(), array(
			'clusterpress' => array(
				'version'        => cp_get_plugin_version(),
				'wp_plugin_slug' => 'clusterpress',
			)
		) );

		$clusters     = wp_list_pluck( $clusters, 'version', 'wp_plugin_slug' );
		$check_amount = 0;

		if ( ! empty( $this->cp_updates ) ) {
			// Count updates before eventually unsetting some.
			$check_amount = count( $this->cp_updates );

			foreach ( $this->cp_updates as $ku => $update ) {
				// Check if a core update is required.
				if ( 'clusterpress' === $update->slug ) {
					$has_core = true;
				}

				// Then check if the user updated manually to eventually unset some updates.
				if ( isset( $clusters[ $update->slug ] ) && $update->version === $clusters[ $update->slug ] ) {
					unset( $this->cp_updates[ $ku ] );
				}
			}
		}

		$updates_amount = count( $this->cp_updates );

		if ( 0 === $updates_amount ) {
			set_site_transient( 'clusterpress_updates', array( 'no_updates' ), 3 * DAY_IN_SECONDS );

		// The amount has changed, update the Clusters to upgrade.
		} elseif ( $check_amount !== $updates_amount ) {
			set_site_transient( 'clusterpress_updates', $this->cp_updates, 3 * DAY_IN_SECONDS );
		}
		?>
		<div class="wrap">
			<h1><?php esc_html_e( 'Mises à jour', 'clusterpress' ); ?></h1>

			<?php

			if ( $has_core && $updates_amount > 1 ) {
				printf( '<p class="description">%s</p>', sprintf(
					esc_html__( 'Bien qu\'il y ait %s mises à jour à réaliser, seule celle concernant ClusterPress sera opérée dans un premier temps.', 'clusterpress' ), $updates_amount )
				);
			}

			if ( ! empty( $_GET['doupdate'] ) ) {
				cp_get_template_part( 'assets/batch' );

			// No updates? The user must have done it manually!
			} elseif ( 0 === $updates_amount ) {
				printf( '<p class="description">%s</p>', esc_html__( 'Toutes les mises à jour semble avoir été réalisées.', 'clusterpress' ) );
			} else {
				?>
				<p class="description"><strong><?php esc_html_e( 'Les mises à jour sont des opérations délicates, merci de veiller à faire une copie de sauvegarde de votre base de données et des plugins que vous allez mettre à jour.', 'clusterpress' ); ?></strong></p>

				<div style="text-align:center;"><a href="?page=clusterpress-upgrades&doupdate=1" class="button-primary"><?php esc_html_e( 'Lancer la mise à jour', 'clusterpress' ); ?> </a></div>
				<?php
			}
			?>
		</div>
		<?php
	}

	/**
	 * Display notices to alert the Admin about specific events.
	 *
	 * Mainly used to warn him about Post Install/Upgrade tasks.
	 *
	 * @since  1.0.0
	 *
	 * @return string HTML Output.
	 */
	public function notices() {
		$notices = array();

		if ( ! empty( $this->post_install_tasks ) ) {
			$current_screen = get_current_screen();

			if ( ! empty( $current_screen->id ) && false === strpos( $current_screen->id, 'clusterpress_page_clusterpress-tasks' ) ) {
				$tasks     = count( $this->post_install_tasks );
				$notices[] = sprintf( _n(
					'ClusterPress: %s tâche complétant l\'installation ou la mise à jour doivent être effectuée. Merci de sauvegarder votre base de données avant de poursuivre.',
					'ClusterPress: %s tâches complétant l\'installation ou la mise à jour doivent être effectuée. Merci de sauvegarder votre base de données avant de poursuivre.',
					$tasks,
					'clusterpress'
				), number_format_i18n( $tasks ) );

				$url = add_query_arg( 'page', 'clusterpress-tasks', self_admin_url( 'admin.php' ) );

				if ( is_multisite() && ! is_network_admin() ) {
					$url = add_query_arg( 'page', 'clusterpress-tasks', network_admin_url( 'admin.php' ) );
				}

				$notices[] = sprintf(
					__( 'Une fois la base de données sauvegardée, cliquez sur ce %s ou le menu des Tâches de Cluster.', 'clusterpress' ),
					'<a href="' . esc_url( $url ) . '">' . esc_html__( 'lien', 'clusterpress' ) . '</a>'
				);
			}
		}

		if ( $notices && is_super_admin() ) {
			$notices = array_map( 'cp_kses_notices', $notices );
			printf( '<div class="update-nag notice is-dismissible">%s</div>', '<p>' . join( '</p><p>', $notices ) . '</p>' );
		}
	}

	/**
	 * Mainly used by the Site Cluster to synchronize some site options into
	 * our custom tables.
	 *
	 * NB: Any Cluster can use it for their upgrade tasks.
	 *
	 * @since  1.0.0
	 */
	public function load_tasks() {
		$tasks = array();

		foreach ( $this->post_install_tasks as $cluster_task ) {
			if ( ! empty( $cluster_task['count'] ) && is_callable( $cluster_task['count'] ) ) {
				$cluster_task['count'] = call_user_func( $cluster_task['count'] );

				// If nothing needs to be ugraded, remove the task.
				if ( ! empty( $cluster_task['count'] ) ) {
					$tasks[ $cluster_task['callback'] ] = $cluster_task;
					$tasks[ $cluster_task['callback'] ]['message'] = sprintf( $cluster_task['message'], $cluster_task['count'] );
				}
			}
		}

		wp_enqueue_script( 'clusterpress-tasks-js',
			cp_get_includes_js_url() . "tasks{$this->min}.js",
			array( 'jquery', 'json2', 'wp-backbone' ),
			cp_get_plugin_version(),
			true
		);

		wp_localize_script( 'clusterpress-tasks-js', 'ClusterPress_l10n', array(
			'tasks'     => array_values( $tasks ),
			'feedbacks' => array(
				'updated' => __( 'La (les) tâche(s) complétant l\'installation ou la mise à jour task(s) sont terminées. Merci pour votre patience.', 'clusterpress' ),
			),
			'nonce'     => wp_create_nonce( 'clusterpress-batch' ),
		) );
	}

	/**
	 * Display the Tasks UI.
	 *
	 * @since  1.0.0
	 *
	 * @return string HTML Output.
	 */
	public function cluster_tasks() {
		?>
		<div class="wrap">
			<h1><?php esc_html_e( 'Tâches complétant l\'installation ou la mise à jour', 'clusterpress' ); ?></h1>

			<?php cp_get_template_part( 'assets/batch' ); ?>
		</div>
		<?php
	}

	/**
	 * The sites.php is loading, let's add the category
	 * dropdown, now we are sure of it.
	 *
	 * NB: Unfortunately, WordPress doesn't provide any hook to do it the proper way.
	 * So we are calling Javascript to the rescue.
	 *
	 * @since  1.0.0
	 */
	public function prepare_category_dropdown() {
		cp_admin_add_inline_category_dropdown_script();
		add_action( 'in_admin_footer', 'cp_admin_sites_category_dropdown' );
	}

	/**
	 * Add a link to display the discovery page of a site into the Sites list table.
	 *
	 * @since   1.0.0
	 *
	 * @param   array   $actions List of Site's action links.
	 * @param   int     $site_id The site ID.
	 * @return  array            List of Site's action links.
	 */
	public function set_sites_action_links( $actions = array(), $site_id = 0 ) {
		if ( empty( $site_id ) || cp_is_main_site( $site_id ) ) {
			return $actions;
		}

		$actions['discover'] = sprintf( '<a href="%1$s">%2$s</a>',
			esc_url( cp_site_get_url( array( 'site_id' => $site_id ) ) ),
			esc_html__( 'Découvrir', 'clusterpress' )
		);

		return $actions;
	}

	/**
	 * Manage Site categories (Add/edit/delete).
	 *
	 * @since 1.0.0
	 */
	public function load_sites_categories() {
		global $page_hook;

		$post_type = 'cp_site';
		$taxonomy  = cp_sites_get_taxonomy();
		$taxnow    = $taxonomy;

		$current_screen            = get_current_screen();
		$page_hook                 = $current_screen->id;
		$current_screen->post_type = $post_type;
		$current_screen->taxonomy  = $taxonomy;

		$cheating = __( 'Sérieusement ?', 'clusterpress' );
		$doaction = cp_admin_get_current_action();

		/**
		 * Eventually deal with actions before including the edit-tags.php file
		 */
		if ( ! empty( $doaction ) ) {
			$category    = get_taxonomy( $taxonomy );
			$redirect_to = add_query_arg( 'page', 'clusterpress-categories', network_admin_url( 'sites.php' ) );

			if ( ! $category ) {
				wp_die( __( 'Taxinomie invalide', 'clusterpress' ) );
			}

			switch ( $doaction ) {
				case 'add-tag':

					check_admin_referer( 'add-tag', '_wpnonce_add-tag' );

					if ( ! current_user_can( $category->cap->edit_terms ) ) {
						wp_die( $cheating );
					}

					$inserted = wp_insert_term( $_POST['tag-name'], $category->name, $_POST );

					if ( ! empty( $inserted ) && ! is_wp_error( $inserted ) ){
						$redirect_to = add_query_arg( 'message', 1, $redirect_to );
					} else {
						$redirect_to = add_query_arg( 'message', 4, $redirect_to );
					}
					wp_redirect( $redirect_to );
				exit;

				case 'delete':
				case 'bulk-delete':
					$category_ids = array();
					$query_args = array();

					if ( empty( $_REQUEST['tag_ID'] ) && empty( $_REQUEST['delete_tags'] ) ) {
						wp_redirect( $redirect_to );
						exit;
					} else if ( ! empty( $_REQUEST['tag_ID'] ) ) {
						$category_id = absint( $_REQUEST['tag_ID'] );

						check_admin_referer( 'delete-tag_' . $category_id );

						$category_ids          = array( $category_id );
						$query_args['message'] = 2;

					} else {
						check_admin_referer( 'bulk-tags' );

						$category_ids          = wp_parse_id_list( $_REQUEST['delete_tags'] );
						$query_args['message'] = 6;
					}

					if ( ! current_user_can( $category->cap->delete_terms ) ) {
						wp_die( $cheating );
					}

					foreach ( $category_ids as $c_id ) {
						wp_delete_term( $c_id, $category->name );
					}

					$redirect_to = add_query_arg( $query_args, $redirect_to );
					wp_redirect( $redirect_to );
				exit;

				case 'edit':
					cp_admin_add_inline_category_script();
					$wp_http_referer = '';

					if ( ! empty( $_REQUEST['wp_http_referer'] ) ) {
						$wp_http_referer = $_REQUEST['wp_http_referer'];
					}

					require_once( ABSPATH . 'wp-admin/term.php' );
					exit;

				case 'editedtag':
					$category_id = (int) $_POST['tag_ID'];
					check_admin_referer( 'update-tag_' . $category_id );

					if ( ! current_user_can( $category->cap->edit_terms ) )
						wp_die( $cheating );

					$c = get_term( $category_id, $category->name );
					if ( ! $c ) {
						wp_die( __( 'Vous avez tenté de supprimer un élément qui n\'existe plus.', 'clusterpress' ) );
					}

					$updated = wp_update_term( $category_id, $category->name, $_POST );

					if ( ! empty( $updated ) && ! is_wp_error( $updated ) ) {
						$redirect_to = add_query_arg( 'message', 3, $redirect_to );
					} else {
						$redirect_to = add_query_arg( 'message', 5, $redirect_to );
					}

					wp_redirect( $redirect_to );
				exit;
			}

		/**
		 * Make sure to "javascript change" some form attributes
		 * in edit-tags.php
		 */
		} else {
			cp_admin_add_inline_categories_script();
		}

		require_once( ABSPATH . 'wp-admin/edit-tags.php' );
		exit();
	}

	/**
	 * Customize the Amdin menu as we are using a specific sites.php submenu
	 * for the site categories.
	 *
	 * @since  1.0.0
	 */
	public function sites_categories_menu_highlight() {
		global $submenu_file;

		$submenu_file = 'clusterpress-categories';
	}

	/**
	 * Not used, as we are using the non MS edit-tags.php/term.php screens.
	 * @see  CP_Admin::load_sites_categories().
	 *
	 * @since  1.0.0
	 */
	public function sites_categories_screen() {}

	/**
	 * Make sure the Sites subnav we're adding are highlihted.
	 *
	 * @since  1.0.0
	 */
	public function site_subnav_menu_highlight() {
		global $submenu_file;

		$submenu_file = 'sites.php';
	}

	/**
	 * Used to display some feedback about sites manipulation in our custom subnavs.
	 *
	 * @since  1.0.0
	 */
	function display_clusterpress_notice() {
		if ( ! empty( $_REQUEST['status'] ) ) {
			$cp = clusterpress();

			$type = 'updated';
			if ( 'site-member-03' === $_REQUEST['status'] || 'settings-02' === $_REQUEST['status'] ) {
				$type = 'error';
			}

			$cp->feedbacks->set_feedback( 'site[' . $_REQUEST['status'] . ']', $type );
			$message = cp_get_feedback_data( 'message' );

			printf( '<div id="message" class="%1$s notice is-dismissible"><p>%2$s</p></div>',
				$type,
				esc_html( $message )
			);
		}
	}

	/**
	 * Add custom nav items to the sites.php network admin nav.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $nav The sites network admin nav.
	 * @return array       The sites network admin nav.
	 */
	public function site_clusterpress_nav( $nav = array() ) {
		// Do not output the nav if it's the main site.
		if ( ! empty( $_REQUEST['id'] ) ) {
			$id = (int) $_REQUEST['id'];

			if ( cp_is_main_site( $id ) ) {
				return $nav;
			}
		}

		$options_url = esc_url_raw( add_query_arg( 'page', 'clusterpress-options',  'sites.php' ) );

		$cp_nav = array(
			'site-options' => array( 'label' => __( 'Options', 'clusterpress' ), 'url' => $options_url, 'cap' => 'manage_sites' )
		);

		if ( cp_site_is_following_enabled() ) {
			$follower_url = esc_url_raw( add_query_arg( 'page', 'clusterpress-followers', 'sites.php' ) );
			$cp_nav['site-followers'] = array( 'label' => __( 'Adeptes', 'clusterpress' ),  'url' => $follower_url, 'cap' => 'manage_sites' );
		}

		$nav_keys  = array_keys( $nav );
		$users_pos = array_search( 'site-users', $nav_keys );

		if ( false === $users_pos || ! cp_site_is_following_enabled() ) {
			return array_merge( $nav, $cp_nav );
		} else {
			$nav_chunks = array_chunk( $nav, $users_pos + 1, true );
			$nav       = array();
			foreach ( $nav_chunks as $kc => $chunk ) {
				if ( 0 === $kc ) {
					$nav_chunks[ $kc ] = array_merge( $chunk, array( 'site-followers' => $cp_nav['site-followers'] ) );
					$nav = $nav + $nav_chunks[ $kc ];
				}

				if ( count( $nav_chunks ) - 1 === $kc ) {
					$nav_chunks[ $kc ] = array_merge( $chunk, array( 'site-options' => $cp_nav['site-options'] ) );
					$nav = $nav + $nav_chunks[ $kc ];
				}
			}
		}

		return $nav;
	}

	/**
	 * Handle ClusterPress site options (Categories, Logo..).
	 *
	 * @since 1.0.0
	 */
	public function load_site_options() {
		$taxonomy_id = cp_sites_get_taxonomy();

		if ( ! empty( $_POST['site']['id'] ) ) {
			$site_id = (int) $_POST['site']['id'];

			check_admin_referer( 'cp-edit-site-options_' . $site_id );

			// Set the base url for redirection.
			if ( ! empty( $_POST['_wp_http_referer'] ) ) {
				$referer = $_POST['_wp_http_referer'];
			} else {
				$referer = add_query_arg( array( 'page' => 'clusterpress-options', 'id' => $site_id, network_admin_url( 'sites.php' ) ) );
			}

			do_action( 'cp_site_before_edit_categories', $site_id );

			$categories = array();
			if ( ! empty( $_POST['tax_input'][ $taxonomy_id ] ) ) {
				$categories = wp_parse_id_list( $_POST['tax_input'][ $taxonomy_id ] );
			}

			// Remove the null value only if there's a non null selected.
			if ( count( $categories ) > 1 ) {
				$categories = array_filter( $categories );
			}

			$updated = wp_set_object_terms( $site_id, $categories, $taxonomy_id );

			do_action( 'cp_site_after_edit_categories', $updated, $site_id );

			if ( is_wp_error( $updated ) ) {
				$referer = add_query_arg( 'status', 'settings-02', $referer );
			} else {

				// Check for custom settings
				if ( ! empty( $_POST['cp_site_settings'] ) ) {
					$settings_data = (array) $_POST['cp_site_settings'];
					$meta_keys     = get_registered_meta_keys( 'blog' );

					if ( ! array_diff_key( $meta_keys, $settings_data ) ) {
						$referer = add_query_arg( 'status', 'settings-02', $referer );
					} else {
						$cp_metas  = wp_list_filter( clusterpress()->site->meta_screens, array( '_builtin' => true ) );
						$meta_keys = array_diff_key( $meta_keys, $cp_metas );

						if ( empty( $meta_keys ) ) {
							$referer = add_query_arg( 'status', 'settings-02', $referer );

						} else {
							do_action( 'cp_site_before_edit_custom_metas', $meta_keys, $settings_data, $site_id );

							foreach ( array_keys( $meta_keys ) as $m_key ) {
								if ( ! isset( $settings_data[ $m_key ] ) || true !== $meta_keys[ $m_key ]['single'] ) {
									continue;
								}

								// Update!
								cp_sites_update_meta( $site_id, $m_key, wp_unslash( $settings_data[ $m_key ] ) );
							}

							do_action( 'cp_site_after_edit_custom_metas', $meta_keys, $settings_data, $site_id );

							$referer = add_query_arg( 'status', 'settings-01', $referer );
						}
					}
				} else {
					$referer = add_query_arg( 'status', 'settings-01', $referer );
				}
			}

			wp_safe_redirect( $referer );
			exit();
		}

		add_meta_box( 'submitdiv', _x( 'Sauvegarder', 'site admin edit screen', 'clusterpress' ), 'cp_admin_site_options_submit_box', get_current_screen()->id, 'side', 'high' );
		add_meta_box( 'clustepress_site_icon_div', _x( 'Icône du site', 'site admin edit screen', 'clusterpress' ), 'cp_admin_site_icon_box', get_current_screen()->id, 'side', 'core' );
		add_meta_box( 'clustepress_options_div', _x( 'Options', 'site admin edit screen', 'clusterpress' ), 'cp_admin_site_options_box', get_current_screen()->id, 'advanced', 'high' );

		$taxonomy = get_taxonomy( $taxonomy_id );

		if ( $taxonomy->show_ui && false !== $taxonomy->meta_box_cb ) {
			add_meta_box( $taxonomy_id . '_div', $taxonomy->labels->name, $taxonomy->meta_box_cb, null, 'normal', 'core', array( 'taxonomy' => $taxonomy_id ) );
		}

		wp_enqueue_script( 'postbox' );
		wp_enqueue_script( 'dashboard' );
	}

	/**
	 * Display the ClusterPress site options screen.
	 *
	 * @since  1.0.0
	 *
	 * @return string HTML Output.
	 */
	public function site_options_screen() {
		$site = null;
		$id = 0;
		if ( ! empty( $_REQUEST['id'] ) ) {
			$id = (int) $_REQUEST['id'];
		}

		$messages = array();

		if ( ! empty( $_REQUEST['status'] ) ) {
			if ( 'success' === $_REQUEST['status'] ) {
				$messages['updated'] = __( 'Les Catégories ont été assignées au site avec succès.', 'clusterpress' );
			} else {
				$messages['error'] = __( 'Un problème est survenu lors de l\'assignation de catégories pour ce site.', 'clusterpress' );
			}
		}

		$admin_url = get_admin_url( $id );
		$site      = cp_get_site_by( 'id', $id );
		$title     = __( 'Erreur pour le site', 'clusterpress' );
		$form_url  = add_query_arg( array(
			'page' => 'clusterpress-options',
			'id'   => $id,
		), network_admin_url( 'sites.php' ) );

		if ( ! $site || ! is_a( $site, 'WP_Site' ) ) {
			$url = get_home_url( $id, '/' );

		} else {
			$title = sprintf( __( 'Modifier le site : %s', 'clusterpress' ), $site->name );
			$url   = $site->url;
		}

		// Display notices
		$this->display_clusterpress_notice();
		?>

		<div class="wrap">
			<h1 id="edit-site"><?php echo esc_html( $title ); ?></h1>
			<p class="edit-site-actions">
				<a href="<?php echo esc_url( $url ); ?>"><?php _e( 'Visiter', 'clusterpress' ); ?></a> |
				<a href="<?php echo esc_url( $admin_url ); ?>"><?php esc_html_e( 'Tableau de bord', 'clusterpress' ); ?></a>
			</p>
			<?php

			network_edit_site_nav( array(
				'blog_id'  => $id,
				'selected' => 'site-options'
			) );

			if ( ! cp_is_main_site( (int) $site->blog_id ) ) : ?>

			<div id="poststuff">

				<div id="post-body" class="metabox-holder columns-<?php echo 1 == get_current_screen()->get_columns() ? '1' : '2'; ?>">
					<form action="<?php echo esc_url( $form_url ); ?>" id="cp-site-options-edit-form" method="post">
						<input type="hidden" name="site[id]" value="<?php echo esc_attr( $id ); ?>"/>

						<div id="postbox-container-1" class="postbox-container">
							<?php do_meta_boxes( get_current_screen()->id, 'side', $site ); ?>
						</div>

						<div id="postbox-container-2" class="postbox-container">
							<?php do_meta_boxes( get_current_screen()->id, 'advanced', $site ); ?>
							<?php do_meta_boxes( get_current_screen()->id, 'normal', $site ); ?>
						</div>

						<?php wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', false ); ?>
						<?php wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', false ); ?>
						<?php wp_nonce_field( 'cp-edit-site-options_' . $id ); ?>
					</form>
				</div>
			</div>

			<?php endif ; ?>

		</div>
		<?php
	}

	/**
	 * Handle the user demote action (Change a member to a follower).
	 *
	 * @since 1.0.0
	 */
	public function handle_demote_action() {
		if ( ! empty( $_GET['status'] ) ) {
			add_action( 'network_admin_notices', array( $this, 'display_clusterpress_notice' ) );
		}

		// First check if a user is to demote as a follower
		if ( ! empty( $_GET['cp_action'] ) && 'demote' === $_GET['cp_action'] && ! empty( $_GET['user'] ) && ! empty( $_GET['id'] ) ) {

			check_admin_referer( 'cp_demote_user' );

			if ( ! current_user_can( 'manage_sites' ) ) {
				wp_die( __( 'Vous ne pouvez pas rétrograder des utilisateurs.', 'clusterpress' ) );
			}

			$update = 'site-member-03';
			if ( remove_user_from_blog( $_GET['user'], $_GET['id'] ) ) {
				cp_sites_add_user( $_GET['user'], $_GET['id'] );

				$update = 'site-follower-01';
			}

			wp_safe_redirect( add_query_arg( array( 'status' => $update, 'id' => $_GET['id'] ), network_admin_url( 'site-users.php' ) ) );
			exit();
		}
	}

	/**
	 * Add a site member's action link to demote a member.
	 *
	 * @since  1.0.0
	 *
	 * @param   array   $actions     List of site member's actions.
	 * @param   WP_User $user_object The User object.
	 * @return  array                List of site member's actions.
	 */
	public function set_demote_action( $actions = array(), $user_object = null ) {
		$current_screen = get_current_screen();

		if ( empty( $current_screen->id ) || $current_screen->id !== 'site-users-network' || empty( $_GET['id'] ) || empty( $user_object->ID ) ) {
			return $actions;
		}

		if ( (int) get_current_user_id() === (int) $user_object->ID || is_super_admin( $user_object->ID ) ) {
			return $actions;
		}

		$demote_link = wp_nonce_url( add_query_arg( array(
			'cp_action'  => 'demote',
			'id'         => $_GET['id'],
			'user'       => $user_object->ID,
		), network_admin_url( 'site-users.php' ) ), 'cp_demote_user' );

		$actions['demote'] = sprintf( '<a href="%1$s" class="delete">%2$s</a>',
			esc_url( $demote_link ),
			esc_html__( 'Rétrograder comme adepte', 'clusterpress' )
		);

		return $actions;
	}

	/**
	 * Handle the demote bulk actions.
	 *
	 * NB: Only available since WordPress 4.7
	 *
	 * @since  1.0.0
	 *
	 * @param  string  $referer The referer url.
	 * @param  string  $action  The requested bulk action.
	 * @param  array   $userids The list of user ids
	 * @param  int     $id      The site's ID.
	 * @return string           The custom redirect url.
	 */
	public function handle_demote_bulk_action( $referer = '', $action = '', $userids = array(), $id = 0 ) {
		if ( 'demote' !== $action || empty( $id ) ) {
			return $referer;
		}

		$user_ids = array_filter( wp_parse_id_list( $userids ) );

		if ( empty( $user_ids ) ) {
			return $referer;
		}

		$updated = array();

		$update = 'site-member-03';
		foreach ( $user_ids as $user_id ) {
			if ( remove_user_from_blog( $user_id, $id ) ) {
				$updated[] = cp_sites_add_user( $user_id, $id );
			}
		}

		if ( count( $updated ) === count( $user_ids ) ) {
			$update = 'site-follower-01';

			if ( count( $user_ids ) > 1 ) {
				$update = 'site-follower-02';
			}
		}

		return add_query_arg( array( 'status' => $update, 'id' => $id ), network_admin_url( 'site-users.php' ) );
	}

	/**
	 * Add a new Bulk action to demote a list of members as followers.
	 *
	 * NB: Only available since WordPress 4.7
	 *
	 * @since  1.0.0
	 *
	 * @param   array $actions The site's members bulk actions.
	 * @return  array          The site's members bulk actions.
	 */
	public function set_demote_bulk_action( $actions = array() ) {
		$actions['demote'] = __( 'Rétrograder', 'clusterpress' );

		return $actions;
	}

	/**
	 * Make sure user preferences about the Followers UI are saved to database.
	 *
	 * @since  1.0.0
	 *
	 * @param  bool|mixed $return  The value for the user preference.
	 * @param  string     $option  The name of the user preference setting.
	 * @param  int        $value   The value for the user preference.
	 */
	public function set_followers_screen_option( $return = false, $option = '', $value = '' ) {
		if ( 'sites_page_clusterpress_followers_network_per_page' === $option ) {
			$value = (int) $value;

			if ( $value < 1 || $value > 999 ) {
				return $return;
			} else {
				$return = $value;
			}
		}

		return $return;
	}

	/**
	 * Handle Followers Promote/remove actions.
	 *
	 * @since 1.0.0
	 */
	public function load_site_followers() {
		if ( ! class_exists( 'CP_Followers_List_Table' ) ) {
			require_once( $this->admin_dir. 'classes/class-cp-followers-list-table.php' );
		}

		$this->followers_table = new CP_Followers_List_Table( array( 'screen' => get_current_screen() ) );
		$this->followers_table->prepare_items();

		$action = $this->followers_table->current_action();

		if ( $action ) {
			$referer = add_query_arg( array(
				'page' => 'clusterpress-followers',
				'id'   => $this->followers_table->site_id,
			), network_admin_url( 'sites.php' ) );

			$update  = '';
			$followers = array();

			if ( ! empty( $_REQUEST['followers'] ) ) {
				$followers = wp_parse_id_list( $_REQUEST['followers'] );
			}

			if ( empty( $followers ) ) {
				return;
			}

			if ( 'remove' === $action || 'remove_follower' === $action ) {
				if ( ! current_user_can( 'remove_users' ) ) {
					wp_die( __( 'Vous ne pouvez pas supprimer des adeptes.', 'clusterpress' ) );
				}

				$nonce = 'cp-remove-follower';
				if ( $action === 'remove' ) {
					$nonce = 'bulk-followers';
				}

				check_admin_referer( $nonce );

				$updated_followers = array();

				foreach ( $followers as $user_id ) {
					if ( cp_sites_remove_user( $user_id, $this->followers_table->site_id, 0 ) ) {
						$updated_followers[] = $user_id;
					}
				}

				if ( count( $updated_followers ) !== count( $followers ) ) {
					$update = 'site-member-03';

					if ( 1 === count( $followers ) ) {
						$update = 'site-member-02';
					}
				} else {
					$update = 'site-member-05';
				}

			} elseif ( 'promote' === $action ) {
				if ( ! current_user_can( 'remove_users' ) ) {
					wp_die( __( 'Vous ne pouvez pas supprimer des utilisateurs.', 'clusterpress' ) );
				}

				check_admin_referer( 'bulk-followers' );

				$editable_roles = get_editable_roles();

				if ( empty( $_REQUEST['promote'] ) || empty( $editable_roles[ $_REQUEST['promote'] ] ) ) {
					wp_die( __( 'Vous ne pouvez pas attribuer ce rôle aux utilisateurs.', 'clusterpress' ) );
				}

				$updated_followers = array();

				foreach ( $followers as $user_id ) {
					if ( add_user_to_blog( $this->followers_table->site_id, $user_id, $_REQUEST['promote'] ) ) {
						$updated_followers[] = $user_id;
					}
				}

				if ( count( $updated_followers ) !== count( $followers ) ) {
					$update = 'site-member-03';
				} else {
					$update = 'site-member-04';
				}

			} else {
				$update = apply_filters( 'cp_site_followers_admin_action', $action, $referer );
			}

			wp_safe_redirect( add_query_arg( 'status', $update, $referer ) );
			exit();
		}

		add_screen_option( 'per_page' );
	}

	/**
	 * Display the site followers administation screen.
	 *
	 * @since  1.0.0
	 *
	 * @return string HTML Output.
	 */
	public function site_followers_screen() {
		if ( empty( $this->followers_table->site_id ) ) {
			return;
		}

		$site = cp_get_site_by( 'id', $this->followers_table->site_id );

		if ( ! is_a( $site, 'WP_Site' ) ) {
			return;
		}

		$form_url = add_query_arg( array(
			'page' => 'clusterpress-followers',
			'id'   => $site->blog_id,
		), network_admin_url( 'sites.php' ) );

		// Display notices
		$this->display_clusterpress_notice();
		?>

		<div class="wrap">
			<h1 id="edit-site"><?php echo esc_html( sprintf( __( 'Modifier le site : %s', 'clusterpress' ), $site->name ) ); ?></h1>
			<p class="edit-site-actions">
				<a href="<?php echo esc_url( $site->url ); ?>"><?php _e( 'Visiter', 'clusterpress' ); ?></a> |
				<a href="<?php echo esc_url( get_admin_url( $site->blog_id ) ); ?>"><?php esc_html_e( 'Tableau de bord', 'clusterpress' ); ?></a>
			</p>
			<?php

			network_edit_site_nav( array(
				'blog_id'  => $site->blog_id,
				'selected' => 'site-followers'
			) );

			if ( ! cp_is_main_site( (int) $site->blog_id ) ) : ?>

				<form class="search-form" method="get" action="<?php echo esc_url( network_admin_url( 'sites.php' ) ); ?>">
					<?php $this->followers_table->search_box( __( 'Rechercher des adeptes', 'clusterpress' ), 'follower' ); ?>
					<input type="hidden" name="id" value="<?php echo esc_attr( $site->blog_id ) ?>" />
					<input type="hidden" name="page" value="clusterpress-followers" />
				</form>

			<?php $this->followers_table->views(); ?>

			<form method="post" action="<?php echo esc_url( $form_url ); ?>">

				<input type="hidden" name="id" value="<?php echo esc_attr( $site->blog_id ) ?>" />

				<?php $this->followers_table->display(); ?>

			</form>

			<?php
			/**
			 * Fires after the list table on the Followers nav of the Multisite Network Site Admin.
			 *
			 * @since 1.0
			 */
			do_action( 'cp_followers_after_list_table' );

			endif; ?>

		</div>

		<?php
	}

	/**
	 * Add a new Accordion the the WP Nav Menu Admin screen to let the Admin
	 * easily add links to the ClusterPress areas.
	 *
	 * @since  1.0.0
	 */
	public function menu_accordion() {
		add_meta_box(
			'clusterpress-nav-menu',
			__( 'ClusterPress', 'clusterpress' ),
			array( $this, 'do_accordion' ),
			'nav-menus',
			'side',
			'default'
		);
	}

	/**
	 * Output the ClusterPress WP Nav Menu Accordion.
	 *
	 * @since  1.0.0
	 *
	 * @return string HTML Output.
	 */
	public function do_accordion() {
		global $_nav_menu_placeholder, $nav_menu_selected_id;

		if ( 0 > $_nav_menu_placeholder ) {
			$_nav_menu_placeholder = $_nav_menu_placeholder - 1;
		}  else {
			$_nav_menu_placeholder = -1;
		}

		$archive_nav_items = cp_get_archive_wp_nav_items();

		if ( ! $archive_nav_items ) {
			return;
		}
		?>
		<div class="clusterpress" id="clusterpress">

			<div id="tabs-panel-posttype-clusterpress" class="tabs-panel tabs-panel-active">
				<ul id="clusterpress-menu-checklist" class="categorychecklist form-no-clear">

					<?php foreach( $archive_nav_items as $archive_nav_item ) : ?>

						<li>
							<label class="menu-item-title">
								<input type="checkbox" class="menu-item-checkbox" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-object-id]" value="-1"> <?php echo esc_html( $archive_nav_item->title ) ; ?>
								<input type="hidden" class="menu-item-object" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-object]" value="<?php echo esc_attr( $archive_nav_item->object ) ; ?>" />
								<input type="hidden" class="menu-item-url" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-url]" value="<?php echo esc_url( $archive_nav_item->url ) ; ?>">
								<input type="hidden" class="menu-item-title" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-title]" value="<?php echo esc_attr( $archive_nav_item->title ) ; ?>">
								<input type="hidden" class="menu-item-type" name="menu-item[<?php echo $_nav_menu_placeholder; ?>][menu-item-type]" value="clusterpress" />
							</label>
						</li>

					<?php endforeach; ?>

				</ul>
			</div>

			<p class="button-controls wp-clearfix">
				<span class="add-to-menu">
					<input type="submit"<?php wp_nav_menu_disabled_check( $nav_menu_selected_id ); ?> class="button-secondary submit-add-to-menu right" value="<?php esc_attr_e( 'Ajouter au menu', 'clusterpress' ); ?>" name="add-clusterpress-menu-item" id="submit-clusterpress" />
					<span class="spinner"></span>
				</span>
			</p>

		</div><!-- /.clusterpress -->
	<?php
	}

	/**
	 * The ClusterPress about screen.
	 *
	 * @since 1.0.0
	 */
	public function about_screen() {
		require_once( $this->admin_dir . 'about.php' );

		$tab = 'welcome';
		if ( ! empty( $_GET['tab'] ) ) {
			$tab = sanitize_key( $_GET['tab'] );
		}

		$is_install = false;
		if ( ! empty( $_GET['is_install'] ) ) {
			$is_install = true;
		}

		$display_version  = cp_get_plugin_version();
		$base_url         = add_query_arg( 'page', 'clusterpress-about', self_admin_url( 'index.php' ) );
		$main_url         = add_query_arg( 'page', 'clusterpress-main', self_admin_url( 'admin.php' ) );

		$screens = array(
			'welcome' => array(
				'title' => __( 'Quoi de neuf ?', 'clusterpress' ),
				'url'   => add_query_arg( 'tab', 'welcome', $base_url ),
			),
			'credits' => array(
				'title' => __( 'Crédits', 'clusterpress' ),
				'url'   => add_query_arg( 'tab', 'credits', $base_url ),
			),
		);
		?>

		<div class="wrap about-wrap">
			<h1><?php printf( esc_html__( 'Bienvenue dans ClusterPress&nbsp;%s', 'clusterpress' ), $display_version ); ?></h1>

			<p class="about-text">
				<?php if ( ! $is_install ) :
					printf( esc_html__( 'Merci d\'avoir choisi de mettre à jour ClusterPress. La version %s embarque des fonctionnalités originales pour maximiser votre expérience en terme de mise en valeur des contenus publiés et de leurs auteurs dans votre WordPress.', 'clusterpress' ), $display_version );
				else :
					printf( esc_html__( 'Merci d\'avoir choisi d\'installer ClusterPress. La version %s embarque des fonctionnalités originales pour maximiser votre expérience en terme de mise en valeur des contenus publiés et de leurs auteurs dans votre WordPress.', 'clusterpress' ), $display_version );

				endif; ?>
			</p>
			<div class="wp-badge cp-badge"><?php printf( esc_html__( 'Version %s', 'clusterpress' ), $display_version ); ?></div>

			<h2 class="nav-tab-wrapper wp-clearfix">

				<?php foreach ( $screens as $ks => $vs ) : ?>

					<a href="<?php echo esc_url( $vs['url'] ); ?>" class="nav-tab<?php echo ( $ks === $tab ) ? ' nav-tab-active' : ''; ?>">
						<?php echo esc_html( $vs['title'] ); ?>
					</a>

				<?php endforeach ; ?>
			</h2>

			<?php
			/**
			 * Used to display the Welcome, Credit or License tab
			 */
			do_action( 'cp_admin_about_screen', $tab );

			if ( current_user_can( $this->capability ) ) : ?>
				<hr />

				<div class="return-to-dashboard">
					<a href="<?php echo esc_url( $main_url ); ?>"><?php esc_html_e( 'Configurez ClusterPress', 'clusterpress' ); ?></a>
				</div>

			<?php endif ; ?>

		</div>
		<?php
	}
}
