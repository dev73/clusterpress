<?php
/**
 * ClusterPress Admin functions
 *
 * @since  1.0.0
 *
 * @package    ClusterPress\Admin
 * @subpackage functions
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Get Clusters post install/upgrade tasks.
 *
 * @since  1.0.0
 *
 * @return array The list of post install/upgrade tasks.
 */
function cp_admin_get_cluster_post_install_tasks() {
	/**
	 * This filter is used by the CP_Cluster API to populate  post install/upgrade tasks.
	 *
	 * @since  1.0.0
	 *
	 * @param  array $value The list of post install/upgrade tasks.
	 */
	return (array) apply_filters( 'cp_admin_get_cluster_post_install_tasks', array() );
}

/**
 * Sanitizes Admin Notice output, allowing links to be added to their content.
 *
 * @since  1.0.0
 *
 * @param  string $notices The notices content.
 * @return string          The notices content.
 */
function cp_kses_notices( $notices = '' ) {
	return wp_kses( $notices, array(
		'a' => array( 'href' => true ),
	) );
}

/**
 * Add an inline script to polish the Admin Menu settings items.
 *
 * @since  1.0.0
 */
function cp_admin_add_inline_menu_script() {
	wp_add_inline_script( 'common', '
		( function($) {
			$( \'#menu-settings\' ).removeClass( \'wp-has-current-submenu\', \'wp-menu-open\' ).addClass( \'wp-not-current-submenu\' );
			$( \'#menu-settings a\' ).removeClass( \'wp-has-current-submenu\', \'wp-menu-open\' ).addClass( \'wp-not-current-submenu\' );
		} )( jQuery );
	' );
}

/**
 * Add an inline script to allow the admin to select/unselect all available Profile fields.
 *
 * @since  1.0.0
 */
function cp_admin_add_inline_profile_field_script() {
	wp_add_inline_script( 'common', '
		( function($) {
			$( \'#clusterpress-toggle-all\' ).on( \'click\', function( e ) {
				$.each( $( \'[name="clusterpress_user_profile_fields\[\]"]\' ), function( cb, checkbox ) {
					$( checkbox ).prop( \'checked\', $( event.currentTarget ).prop( \'checked\' ) );
				} );
			} );
		} )( jQuery );
	' );
}

/**
 * Add an inline script to polish the Sites Category Administration screens.
 *
 * @since  1.0.0
 */
function cp_admin_add_inline_categories_script() {
	$admin_sites_url = network_admin_url( 'sites.php' );
	$taxonomy_id     = cp_sites_get_taxonomy();

	wp_add_inline_script( 'admin-tags', sprintf(
		'( function( $ ) {

			if ( $( \'#addtag\' ).length ) {
				$( \'#addtag\' ).prop( \'action\', \'%1$s\' );
			}

			if ( $( \'.search-form\' ).length ) {
				$( \'.search-form\' ).prepend( \'<input type="hidden" name="page" value="clusterpress-categories"/>\' );
				$( \'.search-form input[name="taxonomy"]\').remove();
				$( \'.search-form input[name="post_type"]\').remove();
			}

			/**
			 * This is needed to be able to bulk delete categories.
			 */
			if ( $( \'#posts-filter\' ).length ) {
				$( \'#posts-filter input[name="taxonomy"]\').remove();
				$( \'#posts-filter input[name="post_type"]\').remove();
			}

			if ( $( \'.wp-list-table .column-posts a\' ).length ) {
				$.each( $( \'.wp-list-table td.column-posts a\' ), function( i, link ) {
					var found = $( link ).prop( \'href\' ).split( \'%2$s=\' )[1];
					var category = null;

					if ( found ) {
						category = decodeURIComponent( found.split(\'&\')[0] );
					}

					$( link ).prop( \'href\', \'%3$s?%4$s=\' + category );
				} );
			}

		} )( jQuery );',
		esc_url_raw( add_query_arg( 'page', 'clusterpress-categories', $admin_sites_url ) ),
		$taxonomy_id,
		$admin_sites_url,
		$taxonomy_id
	) );
}

/**
 * Add an inline script to edit the Sites Category form action.
 *
 * @since  1.0.0
 */
function cp_admin_add_inline_category_script() {
	wp_add_inline_script( 'common', sprintf(
		'( function( $ ) {

			if ( $( \'#edittag\' ).length ) {
				$( \'#edittag\' ).prop( \'action\', \'%s\' );
			}

		} )( jQuery );',
		esc_url_raw( add_query_arg( 'page', 'clusterpress-categories', network_admin_url( 'sites.php' ) ) )
	) );
}

/**
 * Add an inline script to output the Category filter inside the Network Sites List Table.
 *
 * @since  1.0.0
 */
function cp_admin_add_inline_category_dropdown_script() {
	wp_add_inline_script( 'common', sprintf(
		'( function( $ ) {

			if ( $( \'#form-site-list\' ).length ) {
				if ( $( \'#form-site-list .tablenav.top .bulkactions\' ).length ) {
					$( \'#form-site-list .tablenav.top .bulkactions\' ).after( $( \'#cp_sites_category_dropdown_container\' ) );
				} else {
					$( \'#form-site-list .tablenav.top .tablenav-pages\' ).before( $( \'#cp_sites_category_dropdown_container\' ) );
				}


				$( \'#cp_sites_category_dropdown_container\' ).on( \'click\', \'a.button\', function( e ) {
					e.preventDefault();

					var selected = $( e.delegateTarget ).find( \'select\' ).val();

					if ( ! selected || \'0\' === selected ) {
						location.href = $( e.currentTarget ).prop( \'href\' );
					} else {
						location.href = $( e.currentTarget ).prop( \'href\' ) + \'?%s=\' + selected;
					}
				} );
			}

		} )( jQuery );',
		cp_sites_get_taxonomy()
	) );
}

/**
 * Get The Current action for ClusterPress List Tables.
 *
 * @since  1.0.0
 *
 * @return string The current List Table action.
 */
function cp_admin_get_current_action() {
	$action = '';
	if ( ! empty( $_REQUEST['action'] ) ) {
		$action = $_REQUEST['action'];
	}

	// If the bottom is set, let it override the action
	if ( ! empty( $_REQUEST['action2'] ) && $_REQUEST['action2'] !== "-1" ) {
		$action = $_REQUEST['action2'];
	}

	return $action;
}

/**
 * Filter the regular WordPress Category feedbacks for Site Categories use.
 *
 * @since  1.0.0
 *
 * @param  array  $messages The regular WordPress Category feedbacks.
 * @return array            Sites Category feedbacks.
 */
function cp_admin_site_category_updated_messages( $messages = array() ) {
	$messages[ cp_sites_get_taxonomy() ] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Catégorie ajoutée.',                     'clusterpress' ),
		2 => __( 'Catégorie supprimée.',                   'clusterpress' ),
		3 => __( 'Catégorie mise à jour.',                 'clusterpress' ),
		4 => __( 'La catégorie n\'a pu être ajoutée.',     'clusterpress' ),
		5 => __( 'La catégorie n\'a pu être mise à jour.', 'clusterpress' ),
		6 => __( 'Catégorie supprimée.',                   'clusterpress' ),
	);

	return $messages;
}

/**
 * Filter the Multisite List Table items query args so that it is
 * possible to filter Sites according to their assigned Site categories.
 *
 * @since  1.0.0
 *
 * @param  array  $args The prepare items query args.
 * @return array        The prepare items query args.
 */
function cp_admin_sites_table_query_args( $args = array() ) {
	$taxonomy_id = cp_sites_get_taxonomy();

	if ( empty( $_REQUEST[ $taxonomy_id ] ) ) {
		return $args;
	}

	$category = term_exists( $_REQUEST[ $taxonomy_id ], $taxonomy_id );

	if ( empty( $category ) ) {
		return $args;
	}

	if ( is_array( $category ) ) {
		$term_id = reset( $category );
		clusterpress()->admin->site_category_slug = $_REQUEST[ $taxonomy_id ];
	}

	$category_sites = get_objects_in_term( $term_id, $taxonomy_id );

	// No sites for the category? Try to find the site having the 0 id!
	if ( empty( $category_sites ) ) {
		return array( 'site__in' => array( '0' ) );
	}

	if ( ! empty( $args['site__in'] ) ) {
		$intersect = array_intersect( $category_sites, $args['site__in'] );

		if ( ! empty( $intersect ) ) {
			$category_sites = $intersect;
		}
	}

	return array_merge( $args, array( 'site__in' => $category_sites ) );
}

/**
 * This is outputing a dropdown in the sites.php footer as there are
 * no hooks available to put it elsewhere. JS will put it at the right
 * place {@see cp_admin_add_inline_category_dropdown_script()}}
 *
 * (WP_MS_Sites_List_Table has no extra_tablenav method)
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_admin_sites_category_dropdown() {
	$current     = '';
	$cp          = clusterpress();
	$taxonomy_id = cp_sites_get_taxonomy();

	if ( ! empty( $cp->admin->site_category_slug ) ) {
		$current = $cp->admin->site_category_slug;
	}

	$dropdown_options = array(
		'show_option_all' => get_taxonomy( $taxonomy_id )->labels->all_items,
		'hide_empty'      => 0,
		'hierarchical'    => 1,
		'show_count'      => 0,
		'orderby'         => 'name',
		'selected'        => $current,
		'name'            => $taxonomy_id,
		'echo'            => 0,
		'taxonomy'        => $taxonomy_id,
		'value_field'     => 'slug',
	);

	printf(
		'<div id="cp_sites_category_dropdown_container" class="alignleft actions">
			<label class="screen-reader-text" for="%1$s">%2$s</label>
			%3$s
			<a href="%4$s" class="button secondary-button">%5$s</a>
		</div>',
		$taxonomy_id,
		esc_html__( 'Filtrer par catégorie', 'clusterpress' ),
		wp_dropdown_categories( $dropdown_options ),
		esc_url( network_admin_url( 'sites.php' ) ),
		esc_html__( 'Filtrer', 'clusterpress' )
	);
}

/**
 * Filter the tag row actions so that actions are adapted to Site Categories.
 *
 * @since  1.0.0
 *
 * @param  array   $actions  The list of Tag row actions.
 * @param  WP_Term $category The Term Object.
 * @return array             The list of Site Categories row actions.
 */
function cp_admin_site_category_row_actions( $actions = array(), $category = null ) {
	if ( empty( $category ) || ! is_a( $category, 'WP_Term') ) {
		return $actions;
	}

	$taxonomy_id = cp_sites_get_taxonomy();

	if ( $taxonomy_id !== $category->taxonomy ) {
		return $actions;
	}

	$query_args = array(
		'page'   => 'clusterpress-categories',
		'tag_ID'   => $category->term_id,
	);

	$uri = $_SERVER['REQUEST_URI'];
	if ( wp_doing_ajax() ) {
		$uri = wp_get_referer();
	}

	$edit_args   = array_merge( $query_args, array(
		'action'          => 'edit',
		'wp_http_referer' => urlencode( wp_unslash( $uri ) ),
	) );

	$delete_args = array_merge( $query_args, array(
		'action'   => 'delete',
		'taxonomy' => cp_sites_get_taxonomy(), // This is required for the Ajax delete action.
	) );

	$admin_sites_url = network_admin_url( 'sites.php' );
	$edit_link       = esc_url_raw( add_query_arg( $edit_args, $admin_sites_url ) );
	$delete_link     = esc_url_raw( wp_nonce_url(
		add_query_arg( $delete_args, $admin_sites_url ),
		'delete-tag_' . $category->term_id
	) );

	$actions['edit']   = preg_replace( '/href=\"\S*/', 'href="' . $edit_link . '"', $actions['edit'] );
	$actions['delete'] = preg_replace( '/href=\"\S*/', 'href="' . $delete_link  . '"', $actions['delete'] );

	return $actions;
}
add_filter( 'tag_row_actions', 'cp_admin_site_category_row_actions', 10, 2 );

/** Metaboxes for the ClusterPress site Options Administration screen *********/

/**
 * Major Publishing box for the ClusterPress site Options.
 *
 * @since  1.0.0
 *
 * @param  WP_Site $site The Site Object being edited.
 * @return string HTML Output.
 */
function cp_admin_site_options_submit_box( $site = null ) {
	?>
	<div id="submitsite" class="submitbox">
		<div id="major-publishing-actions">

			<?php do_action( 'cp_admin_site_category_submit_box_before_button', $site ); ?>

			<div id="publishing-action">
				<?php submit_button( __( 'Sauvegarder', 'clusterpress' ), 'primary', 'site[save]', false ); ?>
			</div>
			<div class="clear"></div>
		</div><!-- #major-publishing-actions -->
	</div><!-- #submitsite -->
	<?php
}

/**
 * Site Icon box for the ClusterPress site Options.
 *
 * @since  1.0.0
 *
 * @param  WP_Site $site The Site Object being edited.
 * @return string HTML Output.
 */
function cp_admin_site_icon_box( $site = null ) {
	if ( empty( $site->blog_id ) ) {
		return;
	}

	$return_url = add_query_arg( array(
		'id'   => $site->blog_id,
		'page' => 'clusterpress-options',
	), network_admin_url( 'sites.php' ) );

	printf( '<div class="cp-site-icon">%1$s</div>',
		cp_get_site_icon( array(
			'width'  => 150,
			'height' => 150,
			'site_url_args' => array(
				'admin_url'  => $return_url,
			),
		), $site )
	);
}

/**
 * Site Options box for the ClusterPress site Options.
 *
 * NB: This is mainly used for the registered site meta custom Clusters
 * can add.
 *
 * @since  1.0.0
 *
 * @param  WP_Site $site The Site Object being edited.
 * @return string HTML Output.
 */
function cp_admin_site_options_box( $site = null ) {
	if ( empty( $site->blog_id ) ) {
		return;
	}

	$site_settings = add_query_arg( array( 'id' => $site->blog_id ), network_admin_url( 'site-settings.php' ) );
	$editables     = array(
		'blog_public'     => true,
		'blogdescription' => true,
		'blogname'        => true,
		'site_icon'       => true,
	);

	$meta_keys = array_intersect_key( cp_sites_get_sitemeta_keys(), $editables );
	?>
	<p class="description">
		<?php printf( esc_html__( 'Le titre, le slogan ainsi que la visibilité vis à vis des moteurs de recherche pour le site sont personnalisables depuis %s.', 'clusterpress' ),
			sprintf( '<a href="%1$s">%2$s</a>',
				esc_url( $site_settings ),
				esc_html__( 'les réglages globaux du site', 'clusterpress' )
			)
		); ?>
	</p>

	<?php if ( cp_site_has_settings( array( 'exclude' => $meta_keys, 'site_id' => $site->blog_id ) ) ) : ?>

		<table class="form-table">

			<?php while ( cp_site_the_settings() ) : cp_site_the_setting() ; ?>

				<tr>
					<th scope="row">
						<?php cp_site_setting_admin_the_label(); ?>
					</th>
					<td>
						<?php cp_site_setting_admin_the_field(); ?>
					</td>
				</tr>

			<?php endwhile ; ?>

		</table>

	<?php endif ;
}

/**
 * Site Categories box for the ClusterPress site Options.
 *
 * NB: This is used to assign site categories to the site being edited.
 *
 * @since  1.0.0
 *
 * @param  WP_Site $site The Site Object being edited.
 * @return string HTML Output.
 */
function cp_admin_site_category_meta_box( $site = null ) {
	$taxonomy_id = cp_sites_get_taxonomy();
	?>
	<p class="description"><?php esc_html_e( 'Utilisez les cases à cocher ci-dessous pour assigner une ou plusieurs catégories au site.', 'clusterpress' ); ?></p>

	<div id="taxonomy-<?php echo esc_attr( $taxonomy_id ); ?>" class="categorydiv">
		<div id="<?php echo esc_attr( $taxonomy_id ); ?>-all" class="tabs-panel">
			<input type="hidden" name='tax_input[<?php echo sanitize_key( $taxonomy_id ); ?>][]' value='0' />
			<ul id="<?php echo esc_attr( $taxonomy_id ); ?>_checklist" data-wp-lists="list:<?php echo esc_attr( $taxonomy_id ); ?>" class="categorychecklist form-no-clear">
				<?php echo cp_site_get_category_checklist( $site ); ?>
			</ul>
		</div>
	</div>
	<?php
}

/** Clusters API **************************************************************/

/**
 * Add specific Plugin headers to recognize custom Clusters.
 *
 * NB: Clusters are WordPress plugins with specific header tags.
 *
 * @since  1.0.0
 *
 * @param  array  $headers The WordPress plugin headers.
 * @return array           The ClusterPress Cluster specific headers.
 */
function cp_admin_extra_plugin_headers( $headers = array() ) {
	$headers = array_merge( $headers, array(
		'cluster_id'       => 'cluster_id',
		'cluster_name'     => 'cluster_name',
		'cluster_icon'     => 'cluster_icon',
		'cluster_requires' => 'cluster_requires',
		'Author URI'       => 'Author URI',
	) );

	return $headers;
}

/**
 * Translate the Custom Clusters data.
 *
 * @since  1.0.0
 *
 * @param  string $plugin_file The plugin file.
 * @param  array  $plugin_data The plugin header data.
 * @return array               The plugin header data.
 */
function cp_admin_translate_custom_cluster_data( $plugin_file = '', $plugin_data = array() ) {
	// Sanitize the plugin filename to a WP_PLUGIN_DIR relative path
	$plugin_file = plugin_basename( $plugin_file );

	// Set default values
	$textdomain  = false;
	$domain_path = '';

	// Set textdomain if available
	if ( ! empty( $plugin_data['TextDomain'] ) ) {
		$textdomain = $plugin_data['TextDomain'];
	}

	// Set domainpath if available
	if ( ! empty( $plugin_data['DomainPath'] ) ) {
		$domain_path = $plugin_data['DomainPath'];
	}

	if ( ! $textdomain ) {
		return $plugin_data;
	}

	if ( ! is_textdomain_loaded( $textdomain ) ) {
		load_plugin_textdomain( $textdomain, false, dirname( $plugin_file ) . $domain_path );
	}

	foreach ( array( 'Name', 'cluster_name', 'Description' ) as $field ) {
		if ( ! isset( $plugin_data[ $field ] ) ) {
			continue;
		}

		$translated = translate( $plugin_data[ $field ], $textdomain );

		if ( empty( $translated ) ) {
			continue;
		}

		$plugin_data[ $field ] = wp_kses( $translated, array() );
		$translated = '';
	}

	return $plugin_data;
}

/**
 * Get all custom Clusters or a specific one.
 *
 * @since  1.0.0
 *
 * @param  string $cluster_id The custom Cluster ID. Optional.
 * @param  string $return     The type of data to return. Optional.
 * @return array              The custom Cluster(s) data or slugs.
 */
function cp_admin_get_custom_clusters( $cluster_id = '', $return = 'data' ) {
	add_filter( 'extra_plugin_headers', 'cp_admin_extra_plugin_headers', 10, 1 );

	$plugins = get_plugins();

	remove_filter( 'extra_plugin_headers', 'cp_admin_extra_plugin_headers', 10, 1 );

	$clusters = array();

	foreach ( $plugins as $plugin => $plugin_data ) {
		if ( empty( $plugin_data['cluster_id'] ) ) {
			continue;
		}

		$is_active = false;
		if ( ( is_multisite() && is_plugin_active_for_network( $plugin ) ) || is_plugin_active( $plugin ) ) {
			$is_active = true;
		}

		$cluster_slug = trim( wp_basename( $plugin ), '.php' );
		$plugin_slug  = $cluster_slug;

		// This is what we check on the UI to see if the plugin is active
		if ( ! $is_active ) {
			$cluster_slug = '';
		}

		$cluster_author = $plugin_data['Author'];
		if ( ! empty( $plugin_data['AuthorURI'] ) ) {
			$cluster_author = sprintf( '<a href="%1$s">%2$s</a>', $plugin_data['AuthorURI'], $plugin_data['Author'] );
		} elseif ( ! empty( $plugin_data['Author URI'] ) ) {
			$cluster_author = sprintf( '<a href="%1$s">%2$s</a>', $plugin_data['Author URI'], $plugin_data['Author'] );
		}

		$cluster_requires = 0;
		if ( ! empty( $plugin_data['cluster_requires'] ) ) {
			$cluster_requires = $plugin_data['cluster_requires'];
		}

		$cluster_icon = cp_get_includes_url()  . 'admin/img/grape-w.svg';
		if ( ! empty( $plugin_data['cluster_icon'] ) ) {
			$cluster_icon = plugins_url( $plugin_data['cluster_icon'], plugin_basename( $plugin ) );
		}

		$short_description = $plugin_data['Description'];

		if ( 'slugs' === $return ) {
			$clusters[] = $plugin_slug;
		} else {
			$plugin_data = cp_admin_translate_custom_cluster_data( $plugin, $plugin_data );

			$cluster_name = $plugin_data['Name'];
			if ( ! empty( $plugin_data['cluster_name'] ) ) {
				$cluster_name = $plugin_data['cluster_name'];
			}

			$clusters[ $plugin_data['cluster_id'] ] = array(
				'wp_plugin_id'        => $plugin,
				'wp_plugin_slug'      => $plugin_slug,
				'wp_plugin_is_active' => $is_active,
				'cluster_id'          => $plugin_data['cluster_id'],
				'name'                => $cluster_name,
				'slug'                => $cluster_slug,
				'short_description'   => $plugin_data['Description'],
				'version'             => $plugin_data['Version'],
				'author'              => $cluster_author,
				'requires'            => $cluster_requires,
				'icons'               => array(
					'svg' => $cluster_icon,
				),
			);
		}
	}

	if ( ! empty( $cluster_id ) ) {
		if ( isset( $clusters[ $cluster_id ] ) ) {
			$cluster = $clusters[ $cluster_id ];
		} else {
			$cluster = array();
		}

		/**
		 * Filter here if you want to override a specific cluster's data
		 *
		 * @since  1.0.0
		 *
		 * @param  array  $cluster The requested Cluster headers data.
		 * @param  string $return  The type of return requested (slugs or complete data set).
		 */
		return apply_filters( 'cp_admin_get_custom_cluster', $cluster, $return );
	}

	/**
	 * Filter here if you want to override all custom clusters data
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $clusters The list of custom Clusters headers data.
	 * @param  string $return   The type of return requested (slugs or complete data set).
	 */
	return apply_filters( 'cp_admin_get_custom_clusters', $clusters, $return );
}

/**
 * Get available Clusters (Built-in + Customs).
 *
 * @since  1.0.0
 *
 * @return array The available Clusters.
 */
function cp_admin_get_clusters() {
	$enabled_clusters  = array_flip( clusterpress()->active_clusters );
	$built_in_clusters = cp_get_built_in_clusters();

	$available_clusters = array_merge( $built_in_clusters, cp_admin_get_custom_clusters() );

	foreach ( $available_clusters as $cluster_id => $cluster ) {
		if ( ! isset( $enabled_clusters[ $cluster_id ] ) ) {
			$available_clusters[ $cluster_id ]['slug'] = '';
		}
	}

	return $available_clusters;
}
