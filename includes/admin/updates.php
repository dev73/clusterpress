<?php
/**
 * Updates API
 *
 * @since  1.0.0
 *
 * @package    ClusterPress\Admin
 * @subpackage updates
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Use WordPress's Ajax action to install Clusters
 *
 * @since  1.0.0
 *
 * @return string JSON output.
 */
function cp_admin_install_cluster() {
	add_filter( 'plugins_api', 'cp_clusters_api', 10, 3 );

	wp_ajax_install_plugin();
}
add_action( 'wp_ajax_cp_install_cluster', 'cp_admin_install_cluster' );

/**
 * Filters the WordPress Plugin's API to request data about
 * Clusters or a specific Cluster.
 *
 * @since  1.0.0
 *
 * @param  bool    $result The API result to override WordPress's one.
 * @param  string  $action The action to perform.
 * @param  object  $args   The arguments to send to the remote site.
 * @return object          The remote request result.
 */
function cp_clusters_api( $result = false, $action = '', $args = null ) {
	global $wp_version;

	if ( false === array_search( $action, array( 'query_clusters', 'plugin_information' ) ) ) {
		return $result;
	}

	/**
	 * Request for Clusters
	 */
	$url = 'http://cluster.press/tribu/extensions/info/';
	if ( 'plugin_information' === $action && ! empty( $args->slug ) ) {
		/**
		 * Request for a specific Cluster.
		 */
		$url = sprintf( 'http://cluster.press/tribu/extension/%s/info/', $args->slug );
	}

	/**
	 * Use https if possible
	 */
	if ( wp_http_supports( array( 'ssl' ) ) ) {
		$url = set_url_scheme( $url, 'https' );
	}

	$options = array(
		'timeout' => 30,
		'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' ),
	);

	if ( 'query_clusters' === $action ) {
		$options['body']['wp_version'] = (float) $wp_version;
		$options['body']['locale']     = get_locale();
	}

	if ( ! empty( $args->cp_plugins ) ) {
		$options['body']['extensions'] = wp_json_encode( $args->cp_plugins );
	} elseif ( isset( $args->installed_clusters ) ) {
		$options['body']['query_extensions'] = wp_json_encode( $args );
	}

	$request = wp_remote_post( $url, $options );

	$res = new stdClass;

	if ( ! is_wp_error( $request ) ) {
		$res = json_decode( wp_remote_retrieve_body( $request ) );

		if ( ! is_object( $res ) ) {
			$res = (object) $res;
		}
	}

	if ( has_filter( 'plugins_api', 'cp_clusters_api' ) ) {
		// Make sure to remove the filter on the Plugins API.
		remove_filter( 'plugins_api', 'cp_clusters_api', 10, 3 );
	}

	return $res;
}

/**
 * This filter is there for debugging purpose to allow
 * self requests.
 *
 * @since  1.0.0
 *
 * @param  bool    $retval Whether to allow or not self requests.
 * @param  string  $host   The requested host.
 * @return bool            Whether to allow or not self requests.
 */
function cp_admin_allow_clusterpress_host( $retval = false, $host = '' ) {
	/**
	 * Do it only to debug
	 */
	if ( 'cluster.press' === $host ) {
		$retval = defined( 'WP_DEBUG' ) && WP_DEBUG;
	}

	return $retval;
}
add_filter( 'http_request_host_is_external', 'cp_admin_allow_clusterpress_host', 30, 2 );

/**
 * Refresh the Clusters that are to update.
 *
 * @since  1.0.0
 *
 * @param  string $plugin_slug The Plugin slug.
 * @return bool                True on success. False otherwise.
 */
function cp_admin_refresh_clusterpress_updates( $plugin_slug = '' ) {
	if ( empty( $plugin_slug ) ) {
		return false;
	}

	$clusterpress_updates = get_site_transient( 'clusterpress_updates' );

	if ( empty( $clusterpress_updates ) ) {
		return true;
	}

	$get_plugin_key = wp_list_filter( $clusterpress_updates, array( 'slug' => $plugin_slug ) );

	if ( ! $get_plugin_key ) {
		return true;
	}

	$plugin_key = key( $get_plugin_key );
	unset( $clusterpress_updates[ $plugin_key ] );

	if ( empty( $clusterpress_updates ) ) {
		delete_site_transient( 'clusterpress_updates' );
		return true;
	}

	set_site_transient( 'clusterpress_updates', $clusterpress_updates, 3 * DAY_IN_SECONDS );
	return true;
}

/**
 * Check for ClusterPress or ClusterPress plugins updates
 *
 * NB: we are not using WP Cron here as it seems the WP Cron is not executed
 * if callback functions are only loaded in the wp-admin context. To avoid too
 * much requests on Cluster.Press, this is only happening in WP Admin and every
 * 3 days.
 *
 * @since  1.0.0
 */
function cp_admin_check_for_updates() {
	$clusterpress_updates = get_site_transient( 'clusterpress_updates' );

	if ( ! empty( $clusterpress_updates ) ) {
		return;
	}

	$clusters = wp_list_pluck( cp_admin_get_custom_clusters(), 'version', 'wp_plugin_id' );
	$clusters = array_merge( array(
		'clusterpress/clusterpress.php' => cp_get_plugin_version()
	), $clusters );

	$cp_plugins = array();
	foreach ( $clusters as $kp => $vp ) {
		$cp_plugins[ $kp ] = array(
			'name'    => dirname( $kp ),
			'version' => $vp,
		);
	}

	$updates = cp_clusters_api( false, 'query_clusters', (object) array( 'cp_plugins' => $cp_plugins ) );

	$interval = 3 * DAY_IN_SECONDS;

	if ( ! empty( $updates->plugins ) ) {
		set_site_transient( 'clusterpress_updates', $updates->plugins, $interval );
	} else {
		set_site_transient( 'clusterpress_updates', array( 'no_updates' ), $interval );
	}
}
add_action( 'cp_admin_init', 'cp_admin_check_for_updates' );

/**
 * The Plugin_Upgrader class is only using the 'update_plugins'
 * transient to check the plugin to upgrade. This filter is there
 * to allow Clusters upgrades.
 *
 * @since  1.0.0
 *
 * @param  bool   $pre Used to shortcircuit get_transient().
 * @return object      The "Clusters" available on the site.
 */
function cp_admin_temporarly_override_update_plugins_transient( $pre = false ) {
	$updates  = clusterpress()->admin->cp_updates;
	$clusters = array_merge( array(
		'clusterpress' => array( 'wp_plugin_id' => 'clusterpress/clusterpress.php', 'wp_plugin_slug' => 'clusterpress' ),
	), cp_admin_get_custom_clusters() );

	$clusters = wp_list_pluck( $clusters, 'wp_plugin_id', 'wp_plugin_slug' );

	if ( empty( $updates ) || empty( $clusters ) ) {
		return $pre;
	}

	$update_plugins = new stdClass;
	$update_plugins->response = array();

	foreach ( $updates as $update ) {
		if ( empty( $clusters[ $update->slug ] ) ) {
			continue;
		}

		$update_plugins->response[ $clusters[ $update->slug ] ] = (object) array(
			'package' => $update->download_link,
		);
	}

	remove_filter( 'pre_site_transient_update_plugins', 'cp_admin_temporarly_override_update_plugins_transient', 10, 1 );

	return $update_plugins;
}

/**
 * Ajax upgrades Clusters & this plugin!
 *
 * @since  1.0.0
 *
 * @return string JSON output.
 */
function cp_admin_process_updates() {
	$error = array(
		'message'   => __( 'La mise à jour n\'a pu s\'accomplir en raison d\'une erreur.', 'clusterpress' ),
		'type'      => 'error'
	);

	if ( empty( $_POST['id'] ) || ! isset( $_POST['count'] ) || ! isset( $_POST['done'] ) ) {
		wp_send_json_error( $error );
	}

	// Add the action to the error
	$plugin_slug       = sanitize_text_field( wp_unslash( $_POST['id'] ) );
	$error['callback'] = $plugin_slug;

	// Check nonce
	if ( empty( $_POST['_clusterpress_batch_nonce'] ) || ! wp_verify_nonce( $_POST['_clusterpress_batch_nonce'], 'clusterpress-bulk-update' ) ) {
		wp_send_json_error( $error );
	}

	// Check capability
	if ( ! current_user_can( 'update_plugins' ) ) {
		wp_send_json_error( $error );
	}

	add_filter( 'pre_site_transient_update_plugins', 'cp_admin_temporarly_override_update_plugins_transient', 10, 1 );

	if ( 'clusterpress' === $plugin_slug ) {
		$cluster = (object) array(
			'wp_plugin_id' => 'clusterpress/clusterpress.php',
		);
	} else {
		$cluster = wp_list_filter( cp_admin_get_custom_clusters(), array( 'wp_plugin_slug' => $plugin_slug ) );
		$cluster = reset( $cluster );

		if ( is_array( $cluster ) ) {
			$cluster = (object) $cluster;
		}
	}

	$plugin = plugin_basename( sanitize_text_field( wp_unslash( $cluster->wp_plugin_id ) ) );

	if ( ! current_user_can( 'update_plugins' ) || 0 !== validate_file( $plugin ) ) {
		$error['message'] = __( 'Désolé vous n\'êtes pas autorisé à mettre à jour les extensions de ce site.', 'clusterpress' );
		wp_send_json_error( $error );
	}

	include_once( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );

	$skin     = new WP_Ajax_Upgrader_Skin();
	$upgrader = new Plugin_Upgrader( $skin );
	$result   = $upgrader->bulk_upgrade( array( $plugin ) );

	if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
		$error['debug'] = $skin->get_upgrade_messages();
	}

	if ( is_wp_error( $skin->result ) ) {
		$error['message'] = $skin->result->get_error_message();
		wp_send_json_error( $error );
	} elseif ( $skin->get_errors()->get_error_code() ) {
		$error['message'] = $skin->get_error_messages();
		wp_send_json_error( $error );
	} elseif ( is_array( $result ) && ! empty( $result[ $plugin ] ) ) {
		$plugin_update_data = current( $result );

		if ( true === $plugin_update_data || empty( $plugin_update_data['destination'] ) ) {
			$error['message'] = __( 'La mise à jour de l\'extension a échouée.', 'clusterpress' );
			wp_send_json_error( $error );
		}

		// For a reason i don't get yet, there are some cases where the plugin is removed and added with a random name.
		if ( $plugin_update_data['destination'] !== WP_PLUGIN_DIR . '/' . dirname( $plugin ) . '/' ) {
			rename( untrailingslashit( $plugin_update_data['destination'] ), WP_PLUGIN_DIR . '/' . dirname( $plugin ) );
		}

		// Remove the updated plugin from the ClusterPress Updates.
		cp_admin_refresh_clusterpress_updates( $plugin_slug );

		wp_send_json_success( array( 'done' => 1, 'callback' => $plugin_slug ) );
	} elseif ( false === $result ) {
		global $wp_filesystem;

		$error['message'] = __( 'Connexion au système de fichier refusée. Merci de fournir vos éléments de connexion.', 'clusterpress' );

		// Pass through the error from WP_Filesystem if one was raised.
		if ( $wp_filesystem instanceof WP_Filesystem_Base && is_wp_error( $wp_filesystem->errors ) && $wp_filesystem->errors->get_error_code() ) {
			$error['message'] = esc_html( $wp_filesystem->errors->get_error_message() );
		}

		wp_send_json_error( $error );
	}

	// An unhandled error occurred.
	$error['message'] = __( 'La mise à jour de l\'extension a échouée.', 'clusterpress' );
	wp_send_json_error( $error );
}
add_action( 'wp_ajax_cp_process_updates', 'cp_admin_process_updates' );
