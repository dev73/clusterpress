/* global cp, clusterPress */
window.cp = window.cp || {};

( function( exports, $ ) {

	// Bail if not set
	if ( typeof clusterPress === 'undefined' ) {
		return;
	}

	/**
	 * Ajax Class.
	 * @type {Object}
	 */
	cp.Ajax = {

		request: function( endpoint, data, method ) {
			data = data || {};

			if ( ! endpoint || ! method ) {
				return false;
			}

			this.ajaxRequest = $.ajax( {
				url: clusterPress.root_url + endpoint,
				method: method,
				beforeSend: function( xhr ) {
					xhr.setRequestHeader( 'X-WP-Nonce', clusterPress.nonce );
				},
				data: data
			} );

			return this.ajaxRequest;
		},

		get: function( endpoint, data ) {
			return this.request( endpoint, data, 'GET' );
		},

		post: function( endpoint, data ) {
			return this.request( endpoint, data, 'POST' );
		},

		remove: function( endpoint, data ) {
			return this.request( endpoint, data, 'DELETE' );
		}
	};

} )( cp, jQuery );
