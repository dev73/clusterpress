/* global cp, clusterPress */
window.cp = window.cp || {};

( function( exports, $ ) {

	// Bail if not set
	if ( typeof clusterPress === 'undefined' ) {
		return;
	}

	/**
	 * SitesFollowing Class
	 * @type {Object}
	 */
	cp.SitesFollowing = {
		/**
		 * Listen to the dom and act accordingly!
		 * @return {void}
		 */
		start: function() {
			$( document ).ready( this.populatePosts );

			$( '.view-site-followed-posts .post-content' ).on( 'click', 'a', this.forceRedirect );

			$( '.cp-site-site-actions' ).on( 'click', '[data-cp-action-endpoint]', this.toggleFollow );
		},

		populatePosts: function() {
			if ( ! $( '#cp-cp_user_following').hasClass( 'current' ) ) {
				return;
			}
			$.each( $( '[data-cp-site-followed-endpoint]'), function( e, element ) {
				var endpoint = $( element ).data( 'cp-site-followed-endpoint' );

				if ( endpoint ) {
					cp.Ajax.get( endpoint ).done( function( response ) {
						if ( response.error ) {
							$( element ).html( response.error );
						} else {
							$( element ).html( '<h3>' + response.post_title + '</h3>' + '\n' + '<p>' + response.post_content + '</p>' );
						}
					} );
				}
			} );
		},

		forceRedirect: function( event ) {
			event.preventDefault();

			location.href = $( event.delegateTarget ).parent().find( '.followed-site-view-link' ).prop( 'href' );
		},

		toggleFollow: function( event ) {
			var link = $( event.currentTarget ), endpoint = link.data( 'cp-action-endpoint' );

			event.preventDefault();

			if ( ! endpoint ) {
				return false;
			}

			if ( link.hasClass( 'following' ) ) {
				var follow_id = link.data( 'follow-id' );

				if ( ! follow_id ) {
					return false;
				}

				cp.Ajax.remove( endpoint + '/' + follow_id ).done( function( response ) {
					link.removeClass( 'following' );
					link.attr( 'data-like-id', '' );

					if ( response.text ) {
						link.html( response.text );
					}

					if ( $( '#cp-cp_site_followers' ).hasClass( 'current' ) || $( 'body' ).hasClass( 'cp-self-profile' ) ) {
						return window.location.href = window.location.href;
					}
				} );
			} else {
				cp.Ajax.post( endpoint ).done( function( response ) {
					if ( response.follow_id ) {
						link.attr( 'data-follow-id', response.follow_id );
					}

					if ( response.text ) {
						link.html( response.text );
					}

					link.addClass( 'following' );

					if ( $( '#cp-cp_site_followers' ).hasClass( 'current' ) ) {
						return window.location.href = window.location.href;
					}
				} );
			}
		}
	};

	// Launch ClusterPress Sites following
	cp.SitesFollowing.start();

} )( cp, jQuery );
