/* global wp, cp, ClusterPress_l10n */
window.wp = window.wp || {};
window.cp = window.cp || {};

( function( exports, $ ) {

	if ( typeof ClusterPress_l10n === 'undefined' ) {
		return;
	}

	_.extend( cp, _.pick( wp, 'Backbone', 'ajax', 'template' ) );

	// Init Models and Collections
	cp.Models      = cp.Models || {};
	cp.Collections = cp.Collections || {};

	// Init Views
	cp.Views = cp.Views || {};

	/**
	 * The Batch Processor
	 */
	cp.batchProcessor = {
		/**
		 * Launcher
		 */
		start: function() {
			this.tasks = new cp.Collections.Tasks();
			this.completed = false;

			// Create the task list view
			var task_list = new cp.Views.Processor( { collection: this.tasks } );

			task_list.inject( '#clusterpress-batch' );

			this.setUpTasks();
		},

		/**
		 * Populate the tasks collection
		 */
		setUpTasks: function() {
			var self = this;

			_.each( ClusterPress_l10n.tasks, function( task, index ) {
				if ( ! _.isObject( task ) ) {
					return;
				}

				self.tasks.add( {
					id      : task.callback,
					order   : index,
					message : task.message,
					count   : task.count,
					number  : task.number,
					done    : 0,
					active  : false
				} );
			} );
		}
	};

	/**
	 * The Tasks collection
	 */
	cp.Collections.Tasks = Backbone.Collection.extend( {
		proceed: function( options ) {
			options         = options || {};
			options.context = this;
			options.data    = options.data || {};

			options.data = _.extend( options.data, {
				action                      : ClusterPress_l10n.action || 'cp_process_batch',
				'_clusterpress_batch_nonce' : ClusterPress_l10n.nonce
			} );

			return cp.ajax.send( options );
		}
	} );

	/**
	 * Extend Backbone.View with .prepare() and .inject()
	 */
	cp.View = cp.Backbone.View.extend( {
		inject: function( selector ) {
			this.render();
			$( selector ).html( this.el );
			this.views.ready();
		},

		prepare: function() {
			if ( ! _.isUndefined( this.model ) && _.isFunction( this.model.toJSON ) ) {
				return this.model.toJSON();
			} else {
				return {};
			}
		}
	} );

	/**
	 * List of tasks view
	 */
	cp.Views.Processor = cp.View.extend( {
		tagName   : 'div',

		initialize: function() {
			this.views.add( new cp.View( { tagName: 'ul', id: 'clusterpress-tasks' } ) );

			this.collection.on( 'add', this.injectTask, this );
			this.collection.on( 'change:active', this.manageQueue, this );
			this.collection.on( 'change:done', this.manageQueue, this );
		},

		taskSuccess: function( response ) {
			var task, next, nextTask;

			if ( response.done && response.callback ) {
				task = this.collection.get( response.callback );

				task.set( 'done', Number( response.done ) + Number( task.get( 'done' ) ) );

				if ( Number( task.get( 'count' ) ) === Number( task.get( 'done' ) ) ) {
					task.set( 'active', false );

					next     = Number( task.get( 'order' ) ) + 1;
					nextTask = this.collection.findWhere( { order: next } );

					if ( ! _.isUndefined( nextTask ) && _.isObject( nextTask ) ) {
						nextTask.set( 'active', true );
					} else {
						this.views.add( new cp.Views.Feedback( {
							value : ClusterPress_l10n.feedbacks.updated,
							type : 'updated'
						} ) );

						if ( $( '#toplevel_page_clusterpress-main a.current span.cluster-tasks' ).length ) {
							$( '#toplevel_page_clusterpress-main a.current span.cluster-tasks' ).remove();
							$( '#toplevel_page_clusterpress-main a.wp-menu-open span.cluster-tasks' ).remove();
						}

						if ( 'clusterpress' === task.get( 'id') && 'cp_process_updates' === ClusterPress_l10n.action ) {
							this.views.add( new cp.Views.Feedback( {
								value : ClusterPress_l10n.feedbacks.redirect,
								type : 'updated'
							} ) );

							window.setTimeout( function() {
								window.location.href = ClusterPress_l10n.coreUpgradeRedirect;
							}, 1500 );
						}
					}
				}
			}
		},

		taskError: function( response ) {
			if ( response.message && response.callback ) {
				if ( 'warning' === response.type ) {
					var task = this.collection.get( response.callback );
					response.message = response.message.replace( '%d', Number( task.get( 'count' ) ) - Number( task.get( 'done' ) ) );
				}

				$( '#' + response.callback + ' .clusterpress-task-progress' ).html( response.message ).addClass( response.type );
			} else {
				this.views.add( new cp.Views.Feedback( {
					value : response.message,
					type : 'error'
				} ) );
			}
		},

		injectTask: function( task ) {
			this.views.add( '#clusterpress-tasks', new cp.Views.Task( { model: task } ) );
		},

		manageQueue: function( task ) {
			if ( true === task.get( 'active' ) && Number( task.get( 'done' ) ) < Number( task.get( 'count' ) ) ) {
				this.collection.proceed( {
					data    : _.pick( task.attributes, ['id', 'count', 'number', 'done'] ),
					success : _.bind( this.taskSuccess, this ),
					error   : _.bind( this.taskError, this )
				} );
			}
		}
	} );

	/**
	 * The task view
	 */
	cp.Views.Task = cp.View.extend( {
		tagName   : 'li',
		template  : cp.template( 'progress-window' ),
		className : 'clusterpress-task',

		initialize: function() {
			this.model.on( 'change:done', this.taskProgress, this );
			this.model.on( 'change:active', this.addClass, this );

			if ( 0 === this.model.get( 'order' ) ) {
				this.model.set( 'active', true );
			}
		},

		addClass: function( task ) {
			if ( true === task.get( 'active' ) ) {
				$( this.$el ).addClass( 'active' );
			}
		},

		taskProgress: function( task ) {
			if ( ! _.isUndefined( task.get( 'done' ) ) && ! _.isUndefined( task.get( 'count' ) ) ) {
				var percent = ( Number( task.get( 'done' ) ) / Number( task.get( 'count' ) ) ) * 100;
				$( '#' + task.get( 'id' ) + ' .clusterpress-task-progress .clusterpress-task-bar' ).css( 'width', percent + '%' );
			}
		}
	} );

	cp.Views.Feedback = cp.View.extend( {
		tagName: 'div',
		className: 'notice is-dismissible',
		id: 'message',

		initialize: function() {
			this.el.className += ' ' + this.options.type;
			this.value = this.options.value;
		},

		render: function() {
			this.$el.html( $( '<p></p>' ).html( this.value ) );
			return this;
		}
	} );

	cp.batchProcessor.start();

} )( cp, jQuery );
