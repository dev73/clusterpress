/* global cp, clusterPress */
window.cp = window.cp || {};

( function( exports, $ ) {

	// Bail if not set
	if ( typeof clusterPress === 'undefined' ) {
		return;
	}

	/**
	 * Common Class
	 * @type {Object}
	 */
	cp.Common = {
		/**
		 * Listen to the dom and act accordingly!
		 * @return {void}
		 */
		start: function() {
			// Clean up the url
			if ( clusterPress.canonical && window.history.replaceState ) {
				window.history.replaceState( null, null, clusterPress.canonical + window.location.hash );
			}

			// Set the document.
			$( document ).ready( this.setDocument );

			$( '[data-cp-nav="primary"]' ).on( 'click', 'li#cp-collapsible a', this.toggleSingleNav );

			$( '.cp-pagination-links' ).on( 'click', 'a.page-numbers', this.submitFilters );

			$( '.cp-mention-at-name' ).on( 'click', 'a[name="cp-mention"]', this.showMentionInfo );

			$( '[data-cp-role="user-actions"]' ).on( 'click', '.cp-select-all', this.selectAll );
		},

		setDocument: function() {
			// Add a collapse control to the primary nav
			if ( $( '[data-cp-nav="primary"]' ).length ) {
				$( '[data-cp-nav="primary"]' ).append(
					$( '<li></li>' ).html(
						'<a name="collapse" title="' + clusterPress.strings.increaseNav + '"><span class="dashicons dashicons-admin-collapse"></span></a>'
					).prop( 'id', 'cp-collapsible' )
				);
			}

			// Fill filters according to posted vars
			if ( $( '[data-cp-role="filters"]' ).length || $( '[data-cp-role="user-actions"]' ).length ) {
				// Order
				if ( clusterPress.filters.orderby ) {
					var orderValue = clusterPress.filters.orderby;

					if ( 'DESC' === clusterPress.filters.order ) {
						orderValue += '_DESC';
					}

					$( '[name="cp_filters\[order\]"] option[value="' + orderValue +'"]' ).prop( 'selected', true );
				}

				// Filter
				if ( clusterPress.filters.filter ) {
					$( '[name="cp_filters\[filter\]"] option[value="' + clusterPress.filters.filter +'"]' ).prop( 'selected', true );
				}

				// Search
				if ( clusterPress.filters.cp_search ) {
					$( '[name="cp_filters\[cp_search\]"]' ).val( clusterPress.filters.cp_search );
				}

				// Check pagination and set the form action to be the first page by default
				if ( $( '.cp-pagination-links a.page-numbers' ).length ) {
					$.each( $( '.cp-pagination-links a.page-numbers' ), function( a, action ) {
						if ( '1' !== $( action ).html() ) {
							return;
						}

						$( '[data-cp-role="filters"]' ).prop( 'action', $( action ).prop( 'href' ) );

						// Only change the pagination if a search is performed
						if ( clusterPress.filters.cp_search ) {
							$( '[data-cp-role="user-actions"]' ).prop( 'action', $( action ).prop( 'href' ) );
						}
					} );
				}
			}
		},

		toggleSingleNav: function( event ) {
			var collapsed = ! $( event.delegateTarget ).hasClass( 'full' );

			event.preventDefault();

			$.each( $( event.delegateTarget ).children( 'li' ), function( l, li ) {
				if ( collapsed ) {
					if ( 'cp-collapsible' === $( li ).prop( 'id' )  ) {
						$( li ).find( 'a' ).prop( 'title', clusterPress.strings.collapseNav );
						return;
					}

					$( li ).find( 'span:nth-child(2)' ).removeClass( 'screen-reader-text' );
					$( event.delegateTarget ).addClass( 'full' );
				} else {
					if ( 'cp-collapsible' === $( li ).prop( 'id' )  ) {
						$( li ).find( 'a' ).prop( 'title', clusterPress.strings.increaseNav );
						return;
					}

					$( li ).find( 'span:nth-child(2)' ).addClass( 'screen-reader-text' );
					$( event.delegateTarget ).removeClass( 'full' );
				}
			} );
		},

		submitFilters: function( event ) {
			var hasFormData = [];

			$.each( $( '[data-cp-role="filters"], [data-cp-role="user-actions"]' ).find( 'input,select' ), function( i, input ) {
				if ( $( input ).val() ) {
					hasFormData.push( $( input ).val() );
				}
			} );

			if ( hasFormData.length > 0 ) {
				if ( $( '[data-cp-role="user-actions"]' ).length && clusterPress.filters.cp_search ) {
					event.preventDefault();
					$( '[data-cp-role="user-actions"]' ).prop( 'action', $( event.currentTarget ).prop( 'href' ) );
					$( '[data-cp-role="user-actions"]' ).submit();
				}

				if ( $( '[data-cp-role="filters"]' ).length ) {
					event.preventDefault();
					$( '[data-cp-role="filters"]' ).prop( 'action', $( event.currentTarget ).prop( 'href' ) );
					$( '[data-cp-role="filters"]' ).submit();
				}

			} else {
				return event;
			}
		},

		showMentionInfo: function( event ) {
			event.preventDefault();

			if ( 'none' === $( event.delegateTarget ).children( '.cp-mention-at-description' ).css( 'display' ) ) {
				$( event.delegateTarget ).children( '.cp-mention-at-description' ).show();
			} else {
				$( event.delegateTarget ).children( '.cp-mention-at-description' ).hide();
			}
		},

		selectAll: function( event ) {
			$.each( $( '.cp-user-checkbox' ), function( cb, checkbox ) {
				$( checkbox ).prop( 'checked', $( event.currentTarget ).prop( 'checked' ) );
			} );

			$( '[data-cp-role="user-actions"] .cp-select-all' ).prop( 'checked', $( event.currentTarget ).prop( 'checked' ) );
		}
	};

	// Launch ClusterPress Sites following
	cp.Common.start();

} )( cp, jQuery );
