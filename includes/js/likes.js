/* global cp, clusterPress */
window.cp = window.cp || {};

( function( exports, $ ) {

	// Bail if not set
	if ( typeof clusterPress === 'undefined' ) {
		return;
	}

	/**
	 * Likes Class
	 * @type {Object}
	 */
	cp.Likes = {
		/**
		 * Listen to the dom and act accordingly!
		 * @return {void}
		 */
		start: function() {
			$( document ).ready( this.populateCount );

			$( '.cp-site-post-actions .likes-link, .cp-site-comment-actions .likes-link' ).on( 'click', '[data-cp-user-endpoint]', this.likeContent );
		},

		populateCount: function() {
			var currentUserId = clusterPress.u;

			$.each( $( '[data-cp-action-type="post"], [data-cp-action-type="comment"]'), function( e, element ) {
				var link = $( element ).find( '[data-cp-action-endpoint]' ),
				    endpoint = link.data( 'cp-action-endpoint' );


				if ( 0 === Number( currentUserId ) ) {
					$( link ).addClass( 'not-logged-in' );
					$( link ).prop( 'title', clusterPress.requiresLogin );
				}

				// Try to put the button at the right of the reply link
				if ( 'comment' === $( element ).data( 'cp-action-type' ) ) {
					var replyDiv = $( element ).prev();

					if ( $( replyDiv ).hasClass( 'reply' ) ) {
						$( replyDiv ).append( $( element ) );
					}
				}

				if ( endpoint ) {
					var data = {};

					if ( link.data( 'cp-action-site' ) ) {
						data.site_id = link.data( 'cp-action-site' );
					}

					cp.Ajax.get( endpoint, data ).done( function( response ) {
						if ( currentUserId ) {
							var like_id = -1;

							$.each( response, function( i, like ) {
								if ( currentUserId === like.user_id ) {
									like_id = like.id;
								}
							} );

							if ( -1 !== like_id ) {
								link.addClass( 'liked' );
								link.attr( 'data-like-id', like_id );
							}
						}

						link.find( 'span.cp-count' ).html( response.length );
					} );
				}
			} );
		},

		likeContent: function( event ) {
			var link = $( event.currentTarget ), endpoint = link.data( 'cp-user-endpoint' ), post = link.data( 'cp-action-object' ),
			    type = link.closest( '[data-cp-action-type]' ).data( 'cp-action-type' ), countSpan = link.find( 'span.cp-count' ),
			    countValue = parseInt( countSpan.html(), 10 ), data = {};

			event.preventDefault();

			if ( ! endpoint ) {
				return false;
			}

			if ( link.data( 'cp-action-site' ) ) {
				data.site_id = link.data( 'cp-action-site' );
			}

			if ( link.hasClass( 'liked' ) ) {
				var like_id = link.data( 'like-id' );

				if ( ! like_id ) {
					return false;
				}

				cp.Ajax.remove( endpoint + '/' + like_id ).done( function() {
					link.removeClass( 'liked' );
					link.attr( 'data-like-id', '' );
					countSpan.html( Number( countValue - 1 ) );

					if ( $( '#cp-cp_user_likes' ).hasClass( 'current' ) && $( 'body' ).hasClass( 'cp-self-profile' ) ) {
						// In case the user has used filters.
						if ( $( '[data-cp-role="filters"]' ).length ) {
							$( '[data-cp-role="filters"]' ).submit();
						} else {
							return window.location.href = window.location.href;
						}
					}
				} );
			} else {
				$.extend( data, {
					object_type: type,
					object_id:   post
				} );

				cp.Ajax.post( endpoint, data ).done( function( response ) {
					if ( response.like_id ) {
						link.attr( 'data-like-id', response.like_id );
					}

					link.addClass( 'liked' );
					countSpan.html( Number( 1 + countValue ) );
				} );
			}
		}
	};

	// Launch ClusterPress likes
	cp.Likes.start();

} )( cp, jQuery );
