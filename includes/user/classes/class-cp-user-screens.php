<?php
/**
 * User Screens Manager.
 *
 * @package ClusterPress\user\classes
 * @subpackage user-screens
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Replace the content by the user templates.
 *
 * @since  1.0.0
 */
class CP_User_Screens {

	/**
	 * The constructor
	 *
	 * Only filter the content once wp_head was fired.
	 *
	 * @since  1.0.0
	 */
	public function __construct() {
		if ( doing_action( 'wp_head' ) || ( ! cp_is_user() && ! cp_is_users() ) ) {
			return;
		}

		add_filter( 'cp_the_content', array( $this, 'user_content' ), 10, 1 );
	}

	/**
	 * Launch the User's Screens manager.
	 *
	 * @since 1.0.0
	 *
	 * @return CP_User_Screen $screens The instance of the class.
	 */
	public static function start() {
		$cp = clusterpress();

		if ( empty( $cp->user->screens ) ) {
			$cp->user->screens = new self();
		}

		return $cp->user->screens;
	}

	/**
	 * Buffer the User's root template.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $content The Post content.
	 * @return string          The ClusterPress User's content.
	 */
	public function user_content( $content = '' ) {
		$template_part = 'user/archive/index';

		if ( cp_is_user() ) {
			$template_part = 'user/single/index';
		}

		return cp_buffer_template_part( $template_part, null, false );
	}
}
add_action( 'cp_do_templates', array( 'CP_User_Screens', 'start' ), 10 );
