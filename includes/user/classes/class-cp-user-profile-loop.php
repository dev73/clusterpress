<?php
/**
 * User Profile Loop.
 *
 * @package ClusterPress\user\classes
 * @subpackage user-profile-loop
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * User Profile loop Class.
 *
 * @since 1.0.0
 */
class CP_User_Profile_Loop extends CP_Cluster_Loop {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args the loop args {
	 *    An array of arguments.
	 *    @type WP_User $user    The user object.
	 *    @type string  $type    The context in which the loop is displayed (view or edit).
	 *    @type array   $include The list of profile fields IDs to include. (Optional)
	 *    @type array   $exclude The list of profile fields IDs to exclude. (Optional)
	 * }
	 */
	public function __construct( $args = array() ) {
		$r = wp_parse_args( $args, array(
			'user'    => '',
			'type'    => 'view',
			'include' => array(),
			'exclude' => array(),
		) );

		$profile_fields = array();
		$fields_count   = 0;

		if ( ! empty( $r['user'] ) && is_a( $r['user'], 'WP_User' ) ) {
			$user        = $r['user'];
			$user_fields = cp_user_get_profile_fields( $user );
			$enabled     = cp_user_get_enabled_fields();

			if ( false !== $enabled ) {
				$user_fields = array_intersect_key( $user_fields, array_flip( $enabled ) );
			}

			if ( ! empty( $r['include'] ) ) {
				$user_fields = array_intersect_key( $user_fields, array_fill_keys( (array) $r['include'], true ) );
			}

			if ( ! empty( $r['exclude'] ) ) {
				$user_fields = array_diff_key( $user_fields, array_fill_keys( (array) $r['exclude'], false ) );
			}

			foreach ( $user_fields as $field_key => $field_name ) {
				$field_value = $user->{$field_key};

				if ( 'edit' !== $r['type'] && empty( $field_value ) ) {
					continue;
				}

				$profile_fields[] = (object) array(
					'field_id'    => $field_key,
					'field_name'  => $field_name,
					'field_value' => $field_value,
				);
			}
		}

		parent::start( array(
			'plugin_prefix'    => 'cp',
			'item_name'        => 'field',
			'item_name_plural' => 'fields',
			'items'            => $profile_fields,
			'total_item_count' => $fields_count,
			'page'             => 1,
			'per_page'         => $fields_count,
		) );
	}
}
