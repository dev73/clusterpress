<?php
/**
 * User Cluster.
 *
 * @package ClusterPress\user\classes
 * @subpackage user-cluster
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * The User CLuster Class.
 *
 * @since 1.0.0
 */
class CP_User_Cluster extends CP_Cluster {

	/**
	 * The constructor
	 *
	 * @since 1.0.0
	 *
	 * @param array args The User Cluster arguments.
	 * {@see CP_Cluster::__construct() for a detailled list of available arguments}
	 */
	public function __construct( $args = array() )  {
		parent::__construct( $args );
	}

	/**
	 * Add the User Cluster to ClusterPress main instance.
	 *
	 * @since 1.0.0
	 */
	public static function start() {
		$cp = clusterpress();

		if ( empty( $cp->user ) ) {
			$cp->user = new self( array(
				'id'            => 'user',
				'name'          => __( 'Utilisateur', 'clusterpress' ),
				'dir'           => 'user',
				'slug'          => cp_user_get_slug(),
				'rewrite_id'    => cp_user_get_rewrite_id(),
				'archive_slug'  => cp_users_get_slug(),
				'archive_title' => cp_users_get_title(),
				'has_single'    => true,
			) );
		}

		return $cp->user;
	}

	/**
	 * Include the needed files.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $files The list of files and class files
	 */
	public function load_cluster( $files = array() ) {
		$files['files'] = array(
			'functions',
			'tags',
		);

		$files['class_files'] = array(
			'user-screens',
			'user-profile-loop',
			'user-query',
			'users-loop',
			'user-relationships'
		);

		// Init User relationships once files are loaded
		add_action( 'cp_user_cluster_loaded', array( $this, 'init_relationships' ) );

		parent::load_cluster( $files );
	}

	/**
	 * Init the User Relationships.
	 *
	 * Other Clusters can use it to register specific
	 * relationships with the user.
	 *
	 * @since 1.0.0
	 */
	public function init_relationships() {
		$this->relationships = new CP_User_Relationships( array(
			'cluster'      => $this->id,
			'relationship' => array(
				'name'        => 'last_active',
				'description' => __( 'Suit la connexion des utilisateurs.', 'clusterpress' ),
			)
		) );
	}

	/**
	 * Analyse the WordPress Query to eventually set ClusterPress User's globals.
	 * Check the user can access to the required single user if requested.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Query $q The WordPress main query.
	 */
	public function parse_query( $q = null ) {
		if ( ! cp_can_alter_posts_query( $q ) ) {
			return;
		}

		$cp = clusterpress();

		if ( is_clusterpress() && cp_is_current_archive( $this->archive_slug ) ) {
			$q->set( 'cp_post_title', $this->archive_title );
		}

		$user_nicename = $q->get( $this->rewrite_id );

		if ( $user_nicename && isset( $this->toolbar ) ) {
			$cp->is_cluster = true;

			$user = get_user_by( 'slug', $user_nicename );

			if ( empty( $user->ID ) ) {
				$q->set_404();
				return;

			// Set the displayed user
			} else {
				$cp->cluster->displayed_object = $user;
				$cp->cluster->displayed_object->last_active = cp_user_get_last_active( $user->ID );
			}

			if ( ! current_user_can( 'cp_read_single_user' ) ) {
				$q->set_404();
				return;
			}

			// Get the nodes with their URLs
			$user_nodes = $this->toolbar->populate_urls( array(
				'object'          => $user,
				'type'            => 'WP_User',
				'parent_group'    => 'single-bar',
				'url_callback'    => 'cp_user_get_url',
				'filter'          => 'cp_user_cluster_get_toolbar_urls',
				'capability_args' => array( 'user_id' => $user->ID, 'toolbar' => 'user_bar' ),
			) );

			if ( ! $user_nodes ) {
				$q->set_404();
				return;
			}

			$validate = array_filter( $this->toolbar->validate_access( array(
				'nodes'         => $user_nodes,
				'object'        => $user,
				'query'         => $q,
				'default_cap'   => 'cp_edit_single_user',
				'filter_prefix' => 'cp_user_cluster_edit',
			) ) );

			if ( ! $validate && count( $q->query ) > 1 ) {
				$q->set_404();
				return;
			}

			$validate = array_merge( array(
				'action'    => 'home',
				'subaction' => null,
			), $validate );

			// If it's home, update the home nav separately.
			if ( 'home' === $validate['action'] ) {
				$home_node = $user_nodes['home'];

				if ( isset( $home_node->meta['classes'] ) ) {
					$home_node->meta['classes'] = array_merge( $home_node->meta['classes'], array( 'current' ) );
				} else {
					$home_node->meta['classes'] = array( 'current' );
				}

				// Edit the node
				$this->toolbar->edit_node( 'home', (array) $home_node );
			}

			// Set the post title
			$q->set( 'cp_post_title', $user->display_name );

			// Set the action
			$cp->cluster->action = $validate['action'];

			// Set the subaction
			if ( ! empty( $validate['subaction'] ) ) {
				$cp->cluster->sub_action = $validate['subaction'];
			}
		}

		parent::parse_query( $q );
	}

	/**
	 * Add rewrite tags for the User Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @param array $tags an array containing associative arrays {
	 *     An array of arguments.
	 *     @see CP_Cluster::rewrite_tags() for a detailled list of available arguments.
	 * }
	 */
	public function rewrite_tags( $tags = array() ) {
		parent::rewrite_tags( array(
			array(
				'tag'   => '%' . $this->rewrite_id . '%',
				'regex' => '([^/]+)',
			),
			array(
				'tag'   => '%' . cp_user_get_manage_rewrite_id() . '%',
				'regex' => '([^/]+)',
			),
		) );
	}

	/**
	 * Add rewrite rules for the User Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @param array $rules an array containing associative arrays {
	 *     An array of arguments.
	 *     @see CP_Cluster::rewrite_rules() for a detailled list of available arguments.
	 * }
	 */
	public function rewrite_rules( $rules = array() ) {
		$user_regex = trailingslashit( cp_get_root_slug() ) . $this->slug;
		$page_slug  = cp_get_paged_slug();
		$page_id    = 'paged';

		parent::rewrite_rules( array(
			array(
				'regex' => $user_regex . '/([^/]+)/' . cp_user_get_manage_slug() . '/([^/]+)/' . $page_slug . '/?([0-9]{1,})/?$',
				'query' => 'index.php?' . $this->rewrite_id . '=$matches[1]&' . cp_user_get_manage_rewrite_id() . '=$matches[2]&' . $page_id . '=$matches[3]',
			),
			array(
				'regex' => $user_regex . '/([^/]+)/' . cp_user_get_manage_slug() . '/([^/]+)/?$',
				'query' => 'index.php?' . $this->rewrite_id . '=$matches[1]&' . cp_user_get_manage_rewrite_id() . '=$matches[2]',
			),
			array(
				'regex' => $user_regex . '/([^/]+)/?$',
				'query' => 'index.php?' . $this->rewrite_id . '=$matches[1]',
			),
		) );
	}

	/**
	 * Add permastruct for the User Cluster.
	 *
	 * @since 1.0.0
	 *
	 * {@see add_permastruct() for a detailed description of the arguments}}
	 * @param string $name
	 * @param string $struct
	 * @param array  $args
	 */
	public function permastructs( $name = '', $struct = '', $args = array() ) {
		parent::permastructs(
			$this->rewrite_id,
			trailingslashit( cp_get_root_slug() ) . $this->slug . '/%' . $this->rewrite_id . '%'
		);
	}

	/**
	 * Set the User's Cluster toolbar
	 *
	 * @since 1.0.0
	 *
	 * @param array $nodes an array containing associative arrays {
	 *     An array of arguments.
	 *     @see CP_Cluster::toolbars() for a detailled list of available arguments.
	 * }
	 */
	public function toolbars( $nodes = array() ) {
		parent::toolbars( array(
			array(
				'type' => 'group', 'args' => array( 'id' => 'archive-bar' ),
			),
			array(
				'type' => 'group', 'args' => array( 'id' => 'single-bar' ),
			),
			array(
				'type' => 'node', 'args' => array(
					'id'           => 'home',
					'slug'         => 'home',
					'parent_group' => 'single-bar',
					'title'        => __( 'Profil', 'clusterpress' ),
					'href'         => '#',
					'position'     => 10,
					'capability'   => 'cp_read_single_user',
					'dashicon'     => 'dashicons-admin-home',
				),
			),
			array(
				'type' => 'node', 'args' => array(
					'id'           => cp_user_get_manage_rewrite_id(),
					'slug'         => cp_user_get_manage_slug(),
					'parent_group' => 'single-bar',
					'title'        => __( 'Gérer', 'clusterpress' ),
					'href'         => '#',
					'position'     => 1000,
					'meta'         => array( 'classes' => array( 'last' ) ),
					'capability'   => 'cp_edit_single_user',
					'dashicon'     => 'dashicons-admin-settings'
				),
			),
			array(
				'type' => 'node', 'args' => array(
					'id'           => 'profile',
					'slug'         => cp_user_get_manage_profile_slug(),
					'parent'       => cp_user_get_manage_rewrite_id(),
					'parent_group' => 'single-bar',
					'title'        => __( 'Profil', 'clusterpress' ),
					'href'         => '#',
					'position'     => 10,
					'capability'   => 'cp_edit_single_user',
				),
			),
			array(
				'type' => 'node', 'args' => array(
					'id'           => 'account',
					'slug'         => cp_user_get_manage_account_slug(),
					'parent'       => cp_user_get_manage_rewrite_id(),
					'parent_group' => 'single-bar',
					'title'        => __( 'Compte', 'clusterpress' ),
					'href'         => '#',
					'position'     => 20,
					'capability'   => 'cp_edit_single_user',
				),
			),
		) );
	}

	/**
	 * Return the User's Cluster Toolbar to create the
	 * My Account menu of the WP Admin Bar.
	 *
	 * @since 1.0.0
	 *
	 * @return array The list of Admin bar items to create.
	 */
	public function get_account_bar_menus() {
		$user = wp_get_current_user();

		if ( empty( $user->ID ) ) {
			return array();
		}

		return $this->toolbar->populate_urls( array(
			'object'          => $user,
			'type'            => 'WP_User',
			'parent_group'    => 'single-bar',
			'url_callback'    => 'cp_user_get_url',
			'filter'          => 'cp_user_cluster_get_toolbar_urls',
			'capability_args' => array( 'user_id' => $user->ID, 'toolbar' => 'admin_bar' ),
		) );
	}

	/**
	 * Set the title to use in the document <meta> tag.
	 *
	 * @since 1.0.0
	 *
	 * @param array $title_parts The list of terms to add to the title tag.
	 */
	public function setup_page_title( $title_parts = array() ) {

		if ( cp_is_users() && ! empty( $this->archive_title ) ) {
			$title_parts['title'] = $this->archive_title;
		} elseif ( cp_is_user() ) {
			$user = cp_displayed_user();

			$title_parts['title'] = $user->display_name;

			if ( cp_current_action() || cp_current_sub_action() ) {
				$slug         = cp_current_action();
				$user_nodes   = $this->toolbar->get_toolbar_nodes();
				$current_node = wp_list_filter( $user_nodes, array( 'slug' => cp_current_action() ) );

				if ( $current_node ) {
					$current_node = reset( $current_node );
					$title_parts['action'] = $current_node->title;
				}

				if ( cp_current_sub_action() ) {
					$current_subnode = wp_list_filter( $user_nodes, array( 'slug' => cp_current_sub_action() ) );

					if ( $current_subnode ) {
						$current_subnode = reset( $current_subnode );
						$title_parts['subaction'] = $current_subnode->title;
					}
				}
			}
		}

		parent::setup_page_title( $title_parts );
	}

	/**
	 * Return the User settings
	 *
	 * @since 1.0.0.
	 *
	 * @return  array The list of settings for the Cluster.
	 */
	public function get_settings() {
		$settings = array(
			'sections' => array(
				'user_cluster_main_settings' => array(
					'title'    => __( 'Réglages principaux', 'clusterpress' ),
					'callback' => 'cp_core_main_settings_callback',
				),
				'user_cluster_slug_settings' => array(
					'title'    => __( 'Personnalisation des URLs', 'clusterpress' ),
					'callback' => 'cp_core_slug_settings_callback',
				),
			),
			'fields' => array(
				'user_cluster_main_settings' => array(
					'clusterpress_users_archive_title' => array(
						'title'             => __( 'Titre de la page d\'archive', 'clusterpress' ),
						'callback'          => 'cp_user_settings_archive_title',
						'sanitize_callback' => 'sanitize_text_field',
					),
					'clusterpress_user_profile_fields' => array(
						'title'             => __( 'Champs de profil actifs.', 'clusterpress' ),
						'callback'          => 'cp_user_settings_profile_fields',
						'sanitize_callback' => 'cp_sanitize_enabled_profile_fields',
					),
				),
				'user_cluster_slug_settings' => array(
					'clusterpress_users_slug' => array(
						'title'             => __( 'Portion d\'URL pour l\'Archive des utilisateurs.', 'clusterpress' ),
						'callback'          => 'cp_user_settings_archive_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_user_slug' => array(
						'title'             => __( 'Portion d\'URL pour les pages de profil de l\'utilisateur.', 'clusterpress' ),
						'callback'          => 'cp_user_settings_user_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_user_manage_slug' => array(
						'title'             => __( 'Portion d\'URL racine pour les pages de gestion de l\'utilisateur.', 'clusterpress' ),
						'callback'          => 'cp_user_settings_user_manage_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_user_profile_slug' => array(
						'title'             => __( 'Portion d\'URL pour la page de gestion du profil utilisateur.', 'clusterpress' ),
						'callback'          => 'cp_user_settings_user_profile_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
					'clusterpress_user_account_slug' => array(
						'title'             => __( 'Portion d\'URL pour la page de gestion du compte utilisateur.', 'clusterpress' ),
						'callback'          => 'cp_user_settings_user_account_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
				),
			),
		);

		// Remove the Slugs settings if not using pretty URLs
		if ( ! clusterpress()->permalink_structure ) {
			unset( $settings['sections']['user_cluster_slug_settings'] );
		}

		return $settings;
	}

	/**
	 * Return the specific feedbacks for the User Cluster.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list of feedbacks specific to this Cluster.
	 */
	public function get_feedbacks() {
		return array(
			'profile-01'  => __( 'Profil mis à jour avec succès.', 'clusterpress' ),
			'profile-02'  => __( 'Une erreur est survenue lors de la mise à jour de votre profil.', 'clusterpress' ),
			'account-01'  => __( 'Compte mis à jour avec succès', 'clusterpress' ),
			'account-02'  => __( 'Une erreur est survenue lors de la mise à jour de votre compte.', 'clusterpress' ),
			'account-03'  => __( 'Ce nom d\'utilisateur est déjà utilisé par un autre, merci d\'en choisir un autre.', 'clusterpress' ),
			'account-04'  => __( 'Merci de vérifier votre courriel. Quelque chose ne convient pas.', 'clusterpress' ),
			'account-05'  => __( 'Ce courriel est déjà utilisé par un autre, merci d\'en choisir un autre.', 'clusterpress' ),
			'not-allowed' => __( 'Vous n\'êtes pas autorisé à effectuer cette action.', 'clusterpress' ),
		);
	}

	/**
	 * Return the specific cap maps for the User Cluster.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list cap maps specific to this Cluster.
	 */
	public function get_caps_map() {
		return array(
			'cp_read_users_archive' => 'cp_user_archive_required_caps',
			'cp_read_single_user'   => 'cp_user_read_single_required_caps',
			'cp_edit_single_user'   => 'cp_user_edit_single_required_caps',
		);
	}

	/**
	 * Install the relationships SQL table.
	 *
	 * @since  1.0.0
	 */
	public function install() {
		global $wpdb;

		$charset = $wpdb->get_charset_collate();
		$prefix  = $wpdb->base_prefix;

		$sql = array(
			"CREATE TABLE {$prefix}cp_relationships (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			user_id bigint(20) NOT NULL,
			primary_id bigint(20) NOT NULL,
			secondary_id bigint(20),
			name varchar(75) NOT NULL,
			date datetime,
			KEY primary_id (primary_id),
			KEY secondary_id (secondary_id),
			KEY user_id (user_id),
			KEY name (name)
			) {$charset};"
		);

		dbDelta( $sql );
	}
}
