<?php
/**
 * ClusterPress User Query.
 *
 * @package ClusterPress\user\classes
 * @subpackage user-query
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * ClusterPress class used for querying users.
 *
 * @since 1.0.0
 *
 * @see WP_User_Query::prepare_query() for accepted arguments.
 */
class CP_User_Query extends WP_User_Query {

	/**
	 * Set the User Query vars
	 *
	 * @since 1.0.0
	 *
	 * @param array $args {
	 *   An array of arguments.
	 *   @see WP_User_Query::prepare_query() for a detailed description.
	 * }}
	 */
	public static function fill_query_vars( $args = array() ) {

		$fields = array(
			'ID',
			'user_login',
			'user_nicename',
			'user_email',
			'user_url',
			'user_registered',
			'user_status',
			'display_name',
		);

		if ( is_multisite() ) {
			$fields = array_merge( $fields, array(
				'spam',
				'deleted',
			) );
		}

		// By default get all members
		$r = array( 'fields' => $fields, 'blog_id' => 0 );

		// If the blog id is set, only get the corresponding users.
		if ( ! empty( $args['blog_id'] ) ) {
			$r['blog_id'] = $args['blog_id'];
		}

		$args = parent::fill_query_vars( array_merge(
			$args,
			$r
		) );

		if ( ! empty( $args['cp_search'] ) ) {
			$args['search'] = $args['cp_search'];

			if ( false === strpos( $args['search'], '*' ) ) {
				$args['search'] = '*' . $args['search'] . '*';
			}

			unset( $args['cp_search'] );
		}

		/**
		 * Filter here to edit the CP_User_Query query vars.
		 *
		 * @since 1.0.0.
		 *
		 * @param array $args {
		 *   An array of arguments.
		 *   @see WP_User_Query::prepare_query() for a detailed description.
		 * }}
		 */
		return apply_filters( 'cp_user_query_fill_args', $args );
	}

	/**
	 * Prepare the ClusterPress User query.
	 *
	 * @since 1.0.0
	 *
	 * @param array $query {
	 *   An array of arguments.
	 *   @see WP_User_Query::prepare_query() for a detailed description.
	 * }}
	 */
	public function prepare_query( $query = array() ) {
		if ( empty( $this->query_vars ) || ! empty( $query ) ) {
			$this->query_limit = null;
			$this->query_vars = $this->fill_query_vars( $query );
		}

		/**
		 * Fires before the CP_User_Query has been parsed.
		 *
		 * @since 1.0.0
		 *
		 * @param CP_User_Query $this The current CP_User_Query instance,
		 *                            passed by reference.
		 */
		do_action( 'cp_pre_get_users', $this );

		parent::prepare_query( $query );

		/**
		 * Fires after the CP_User_Query has been parsed, and before
		 * the query is executed.
		 *
		 * @since 1.0.0
		 *
		 * @param CP_User_Query $this The current CP_User_Query instance,
		 *                            passed by reference.
		 */
		do_action_ref_array( 'cp_pre_user_query', array( &$this ) );
	}

	/**
	 * Return the result of the User Query.
	 *
	 * @since 1.0.0
	 *
	 * @return array The found users.
	 */
	public function get_results() {
		$results = array();

		if ( $this->get( 'role__in' ) ) {
			return $this->results;
		}

		// Reindex by user IDs
		foreach ( $this->results as $user ) {
			$results[ $user->ID ] = $user;
		}

		// Get the list of these ids.
		$ids = array_keys( $results );

		// Get the last active value
		$last_actives = cp_users_get_last_active( $ids );

		if ( ! empty( $last_actives ) ) {
			foreach ( $last_actives as $la ) {
				if ( ! isset( $results[ $la->user_id ] ) ) {
					continue;
				}

				$results[ $la->user_id ]->last_active = $la->date;
			}
		}

		/**
		 * Filter here to edit the results of the ClusterPress User Query.
		 *
		 * @since 1.0.0
		 *
		 * @param array $results The found users.
		 */
		return array_values( apply_filters( 'cp_user_query_results', $results ) );
	}
}
