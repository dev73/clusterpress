<?php
/**
 * ClusterPress User Tags.
 *
 * @package ClusterPress\user
 * @subpackage tags
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Check if displaying user avatar is enabled.
 *
 * @since 1.0.0
 *
 * @return bool True if displaying user avatar is enabled. False otherwise.
 */
function cp_user_show_avatar() {
	/**
	 * Filter here to bypass the WordPress show avatars setting.
	 *
	 * @since  1.0.0
	 *
	 * @param  bool $show_avatars True if displaying user avatar is enabled. False otherwise.
	 */
	return (bool) apply_filters( 'cp_user_show_avatar', clusterpress()->show_avatars );
}

/**
 * Output Title of the User's posts page.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_posts_the_loop_title() {
	esc_html_e( 'Articles', 'clusterpress' );
}

/**
 * Output Title of the User's comments page.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_comments_the_loop_title() {
	esc_html_e( 'Commentaires', 'clusterpress' );
}

/** Single User Template Tags ************************************************/

/**
 * Output the displayed user avatar.
 *
 * @since  1.0.0
 *
 * @param  array  $args {
 *   An array of arguments.
 *   @see cp_get_user_avatar() for a detailed description.
 * }}
 * @return string HTML Output.
 */
function cp_user_avatar( $args = array() ) {
	$avatar = cp_get_user_avatar( $args );

	if ( cp_user_is_self_profile() ) {
		$avatar = sprintf( '<a href="%1$s" title="%2$s" target="_blank">%3$s%4$s</a>',
			esc_url( __( 'https://fr.gravatar.com/', 'clusterpress' ) ),
			esc_attr__( 'Modifier l\'image de profil', 'clusterpress' ),
			$avatar,
			'<div class="dashicons dashicons-external"></div>'
		);
	}

	/**
	 * Filter here to edit the avatar output.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $avatar The user avatar output.
	 */
	echo apply_filters( 'cp_user_avatar', $avatar );
}

/**
 * Get a user avatar.
 *
 * @since  1.0.0
 *
 * @param  array  $args {
 *   An array of arguments.
 *   @type int $width   The avatar width in pixels.
 *   @type int $height  The avatar height in pixels.
 *   @type int $user_id The user ID.
 * }}
 * @return string the user avatar.
 */
function cp_get_user_avatar( $args = array() ) {
	$r = wp_parse_args( $args, array(
		'width'   => 150,
		'height'  => 150,
		'user_id' => 0,
	) );

	if ( ! empty( $r['user_id'] ) ) {
		$id = (int) $r['user_id'];
	} else {
		$id = cp_get_displayed_user_id();
	}

	if ( empty( $id ) ) {
		return false;
	}

	return get_avatar( $id, 150, '', '', $r );
}

/**
 * Output the user's last active info.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_last_active() {
	echo esc_html( cp_get_user_last_active() );
}

	/**
	 * Get the user's last active info.
	 *
	 * @since  1.0.0
	 *
	 * @return string the user's last active info.
	 */
	function cp_get_user_last_active() {
		$user = cp_displayed_user();

		$last_active = __( 'Pas récemment actif.', 'clusterpress' );

		if ( ! empty( $user->last_active ) ) {
			$last_active = sprintf( __( 'Actif il y a %s', 'clusterpress' ), human_time_diff( strtotime( $user->last_active ) ) );
		}

		/**
		 * Filter here to edit the user's last active info.
		 *
		 * @since  1.0.0
		 *
		 * @param  string  $last_active The user's last active info.
		 * @param  WP_User $user        The user object.
		 */
		return apply_filters( 'cp_get_user_last_active', $last_active, $user );
	}

/**
 * Output the user's username.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_at_username() {
	if ( ! cp_interactions_are_mentions_enabled() ) {
		return;
	}

	echo wp_kses( cp_user_get_at_username(), array(
		'div'  => array( 'class' => true ),
		'span' => array( 'class' => true ),
		'code' => true,
		'a'    => array( 'name' => true, 'title' => true, 'class' => true ),
		'p'    => array( 'class' => true ),
	) );
}

	/**
	 * Get the user's username.
	 *
	 * @since  1.0.0
	 *
	 * @return string the user's username.
	 */
	function cp_user_get_at_username() {
		$user = cp_displayed_user();

		/**
		 * Filter here to edit the user's username.
		 *
		 * @since  1.0.0
		 *
		 * @param  string  $last_active The user's username.
		 * @param  WP_User $user        The user object.
		 */
		return apply_filters( 'cp_user_get_at_username', sprintf( '
			<div class="cp-mention-at-name">
				<code>@%1$s</code> <a name="cp-mention" title="%2$s"><span class="dashicons dashicons-editor-help"></span></a>
				<p class="cp-mention-at-description description">%3$s</p>
			</div>
			',
			$user->user_nicename,
			esc_attr__( 'De quoi s\'agit il ?', 'clusterpress' ),
			sprintf(
				esc_html__( 'Utilisez @%1$s dans vos articles et commentaires pour mentionner et &quot;pinger&quot; %2$s.', 'clusterpress' ),
				$user->user_nicename,
				$user->display_name
			)
		), $user );
	}

/** Single User Toolbar Template Tags ****************************************/

/**
 * Init the Single user's primary nav.
 *
 * @since  1.0.0
 *
 * @return bool True if the Single user has nav items. False otherwise.
 */
function cp_user_has_primary_nav() {
	return clusterpress()->user->toolbar->has_items();
}

/**
 * Init the Single user's secondary nav.
 *
 * @since  1.0.0
 *
 * @param  array  $args  {
 *     An array of arguments.
 *     @see  CP_Cluster_Toolbar::has_items() for a detailed description.
 * }}
 * @return bool True if the Single user has secondary nav items. False otherwise.
 */
function cp_user_has_secondary_nav( $args = array() ) {
	$cp     = clusterpress();
	$parent = $cp->user->toolbar->get_node_by_slug( cp_current_action() );

	if ( ! empty( $parent->id ) ) {
		$parent = $parent->id;
	}

	$r = wp_parse_args( $args, array(
		'parent' => $parent
	) );

	if ( empty( $r['parent'] ) ) {
		return false;
	}

	return clusterpress()->user->toolbar->has_items( $r );
}

/**
 * Iterate nav items within the Nav loop.
 *
 * @since 1.0.0.
 *
 * @return  object The nav item object.
 */
function cp_user_nav_items() {
	return clusterpress()->user->toolbar->nav_items();
}

/**
 * Set the nav item being iterated on.
 *
 * @since 1.0.0
 */
function cp_user_nav_item() {
	clusterpress()->user->toolbar->nav_item();
}

/**
 * Output the Single user nav item ID.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_nav_item_id() {
	echo esc_attr( cp_user_get_nav_item_id() );
}

	/**
	 * Get the Single user nav item ID.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Single user nav item ID.
	 */
	function cp_user_get_nav_item_id() {
		$cp = clusterpress();

		$nav_id = '';
		if ( ! empty( $cp->user->toolbar->current_item->id ) ) {
			$nav_id = 'cp-' . $cp->user->toolbar->current_item->id;
		}

		/**
		 * Filter here to edit the Single user nav item ID.
		 *
		 * @since  1.0.0
		 *
		 * @param string $nav_id The Single user nav item ID.
		 */
		return apply_filters( 'cp_user_get_nav_item_id', $nav_id );
	}

/**
 * Output the Single user nav classes.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_nav_classes() {
	$classes = cp_user_get_nav_classes();

	if ( empty( $classes ) ) {
		return;
	}

	echo join( ' ', array_map( 'sanitize_html_class', $classes ) );
}

	/**
	 * Get the Single user nav classes.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Single user nav classes.
	 */
	function cp_user_get_nav_classes() {
		$cp = clusterpress();

		$classes = array();
		if ( ! empty( $cp->user->toolbar->current_item->meta['classes'] ) ) {
			$classes = $cp->user->toolbar->current_item->meta['classes'];
		}

		/**
		 * Filter here to edit the Single user nav classes.
		 *
		 * @since  1.0.0
		 *
		 * @param array $classes The Single user nav classes.
		 */
		return apply_filters( 'cp_user_get_nav_classes', $classes );
	}

/**
 * Output the Single user nav link.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_nav_link() {
	echo esc_url( cp_user_get_nav_link() );
}

	/**
	 * Get the Single user nav link.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Single user nav link.
	 */
	function cp_user_get_nav_link() {
		$cp = clusterpress();

		$link = '#';
		if ( ! empty( $cp->user->toolbar->current_item->href ) ) {
			$link = $cp->user->toolbar->current_item->href;
		}

		/**
		 * Filter here to edit the Single user nav link.
		 *
		 * @since  1.0.0
		 *
		 * @param string $link The Single user nav link.
		 */
		return apply_filters( 'cp_user_get_nav_link', $link );
	}

/**
 * Output the Single user nav link title.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_nav_link_title() {
	echo esc_attr( cp_user_get_nav_link_title() );
}

	/**
	 * Get the Single user nav link title.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Single user nav link title.
	 */
	function cp_user_get_nav_link_title() {
		$cp = clusterpress();

		$title = '';
		if ( ! empty( $cp->user->toolbar->current_item->title ) ) {
			$title = $cp->user->toolbar->current_item->title;
		}

		/**
		 * Filter here to edit the Single user nav link title.
		 *
		 * @since  1.0.0
		 *
		 * @param string $title The Single user nav link title.
		 */
		return apply_filters( 'cp_user_get_nav_link_title', $title );
	}

/**
 * Output the Single user nav title.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_nav_title() {
	echo wp_kses( cp_user_get_nav_title(), array( 'span' => array( 'class' => true ) ) );
}

	/**
	 * Get the Single site nav title.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Single site nav title.
	 */
	function cp_user_get_nav_title() {
		$cp = clusterpress();

		$title = '';
		if ( ! empty( $cp->user->toolbar->current_item->title ) ) {
			$title = $cp->user->toolbar->current_item->title;
		}

		if ( ! empty( $cp->user->toolbar->current_item->dashicon ) ) {
			$title = sprintf( '<span class="dashicons %1$s"></span><span class="screen-reader-text">%2$s</span>',
				sanitize_html_class( $cp->user->toolbar->current_item->dashicon ),
				esc_html( $title )
			);
		}

		if ( ! empty( $cp->user->toolbar->current_item->count ) ) {
			$title = sprintf( '%1$s <span class="pending-count">%2$d</span>',
				$title,
				number_format_i18n( $cp->user->toolbar->current_item->count )
			);
		}

		/**
		 * Filter here to edit the Single user nav title.
		 *
		 * @since  1.0.0
		 *
		 * @param string $title The Single user nav title.
		 */
		return apply_filters( 'cp_user_get_nav_title', $title );
	}

/** The Single User's profile Template Tags **********************************/

/**
 * Output the User's profile page title.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_profile_the_loop_title() {
	esc_html_e( 'Profil', 'clusterpress' );
}

/**
 * Init the Single user's profile loop.
 *
 * @since  1.0.0
 *
 * @param  $args array $args the loop args {
 *    An array of arguments.
 *    @see  CP_User_Profile_Loop::__construct() for a detailed description.
 * }}
 * @return bool True if the Single user has profile fields. False otherwise.
 */
function cp_user_has_fields( $args = array() ) {
	$cp = clusterpress();

	$r = wp_parse_args( $args, array(
		'user' => cp_displayed_user(),
	) );

	if ( cp_is_user_manage_profile() ) {
		$r['type'] = 'edit';
	}

	// Setup the global query loop
	$cp->user->profile_fields = new CP_User_Profile_Loop( $r );

	/**
	 * Filter here if you need to edit the has_items() result.
	 *
	 * @since  1.0.0
	 *
	 * @param  bool  $value True if there are profile fields to display. False otherwise.
	 * @param  array $r     The loop arguments.
	 */
	return apply_filters( 'cp_user_has_fields', $cp->user->profile_fields->has_items(), $r );
}

/**
 * Whether there are more profile fields available in the loop to iterate on.
 *
 * @since 1.0.0
 *
 * @return Object The Profile field object.
 */
function cp_user_the_fields() {
	return clusterpress()->user->profile_fields->items();
}

/**
 * Load the current profile field into the loop.
 *
 * @since 1.0.0
 *
 * @return Object The current profile field Object.
 */
function cp_user_the_field() {
	return clusterpress()->user->profile_fields->the_item();
}

/**
 * Output the profile field ID.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_field_id() {
	echo esc_attr( cp_user_get_field_id() );
}

	/**
	 * Get the profile field ID.
	 *
	 * @since  1.0.0
	 *
	 * @return string the profile field ID
	 */
	function cp_user_get_field_id() {
		$cp = clusterpress();

		$field_id = 0;
		if ( ! empty( $cp->user->profile_fields->field->field_id ) ) {
			$field_id = $cp->user->profile_fields->field->field_id;
		}

		/**
		 * Filter here to edit the profile field ID.
		 *
		 * @since  1.0.0
		 *
		 * @param  string $field_id The profile field ID.
		 */
		return apply_filters( 'cp_user_get_field_id', $field_id );
	}

/**
 * Check if the field is the user description and is not empty.
 *
 * @since  1.0.0
 *
 * @return bool True if the field is the user description and is not empty.
 *              False otherwise.
 */
function cp_user_has_description() {
	$cp = clusterpress();

	if ( empty( $cp->user->profile_fields->field->field_id ) ) {
		return false;
	} else {
		$field = $cp->user->profile_fields->field;
	}

	return ! empty( $field->field_name ) && 'user_description' === $field->field_id && ! empty( $field->field_value );
}

/**
 * Check if the field is the user description.
 *
 * @since  1.0.0
 *
 * @return bool True if the field is the user description. False otherwise.
 */
function cp_user_is_description() {
	$cp = clusterpress();

	if ( empty( $cp->user->profile_fields->field->field_id ) ) {
		return false;
	} else {
		$field = $cp->user->profile_fields->field;
	}

	return 'user_description' === $field->field_id;
}

/**
 * Check if the field has a value.
 *
 * @since  1.0.0
 *
 * @return bool True if the field has a value. False otherwise.
 */
function cp_user_has_field_value() {
	$cp = clusterpress();

	if ( empty( $cp->user->profile_fields->field ) ) {
		return false;
	} else {
		$field = $cp->user->profile_fields->field;
	}

	return ! empty( $field->field_name ) && ! empty( $field->field_value );
}

/**
 * Output the profile field name.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_field_name() {
	echo esc_html( cp_user_get_field_name() );
}

	/**
	 * Get the profile field name.
	 *
	 * @since  1.0.0
	 *
	 * @return string The profile field name.
	 */
	function cp_user_get_field_name() {
		$cp = clusterpress();

		if ( empty( $cp->user->profile_fields->field->field_id ) || empty( $cp->user->profile_fields->field->field_name ) ) {
			return false;
		} else {
			$field = $cp->user->profile_fields->field;
		}

		/**
		 * Filter here to edit the profile field name.
		 *
		 * @since  1.0.0
		 *
		 * @param  string $field_name The profile field name.
		 */
		return apply_filters( 'cp_user_get_field_name_' . $field->field_id, $field->field_name );
	}

/**
 * Output the profile field value.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_field_value() {
	echo cp_user_get_field_value();
}

	/**
	 * Get the profile field value.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $context Inform about the context ('edit' or 'get') to apply filters accordingly.
	 * @return string          The profile field value.
	 */
	function cp_user_get_field_value( $context = 'get' ) {
		$cp = clusterpress();

		if ( empty( $cp->user->profile_fields->field->field_id ) || empty( $cp->user->profile_fields->field->field_value ) ) {
			return false;
		} else {
			$field = $cp->user->profile_fields->field;
		}

		/**
		 * Filter here if you need to edit the field value.
		 *
		 * NB: used internally to sanitize data output.
		 *
		 * @since  1.0.0
		 *
		 * @param  string $field_value The profile field value.
		 * @param  string $field_id    The profile field ID.
		 */
		return apply_filters( 'cp_user_' . $context . '_field_value', $field->field_value, $field->field_id );
	}

/**
 * Output a user feedback to inform no profile fields were found.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_no_profile_found() {
	echo cp_user_get_no_profile_found();
}

	/**
	 * Get the user feedback to inform no profile fields were found.
	 *
	 * @since 1.0.0
	 *
	 * @return string The user feedback message.
	 */
	function cp_user_get_no_profile_found() {
		$message = __( 'Les champs du profil de cet utilisateur ne sont pas disponibles.', 'clusterpress' );

		if ( cp_user_is_self_profile() ) {
			$message = sprintf( __( 'Tous vos champs de profil sont vides. Vous pouvez les modifier à tout moment depuis l\'onglet %s.', 'clusterpress' ),
				sprintf( '<a href="%1$s">%2$s</a>', cp_user_get_url( array(
					'rewrite_id' => cp_user_get_manage_rewrite_id(),
					'slug'       => cp_user_get_manage_slug() . '/' . cp_user_get_manage_profile_slug(),
				), cp_displayed_user() ), __( 'Gérer', 'clusterpress' ) )
			);

			if ( cp_is_user_manage_profile() ) {
				$message = __( 'Tous les champs de profil ont été désactivés par l\'administrateur du site.', 'clusterpress' );
			}
		}

		clusterpress()->feedbacks->add_feedback(
			'user[profile-empty]',
			/**
			 * Use this filter if you wish to edit the no profile fields message
			 *
			 * @since 1.0.0
			 *
			 * @param string $message The user feedback to inform profile fields were found.
			 */
			apply_filters( 'cp_user_get_no_profile_found', $message ),
			'info'
		);

		cp_get_template_part( 'assets/feedbacks' );
	}

/**
 * Output the Account form id attribute.
 *
 * @since  1.0.0
 *
 * @return string the Account form id attribute.
 */
function cp_user_account_form_id() {
	// This ID is required so that the Password Control does not fail.
	echo 'your-profile';
}

/**
 * Output the Account username.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cp_user_account_username() {
	echo esc_attr( cp_user_account_get_username() );
}

	/**
	 * Get the Account username.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Account username.
	 */
	function cp_user_account_get_username() {
		/**
		 * Filter here to edit the Account username.
		 *
		 * @since  1.0.0
		 *
		 * @param string $user_nicename The Account username.
		 */
		return apply_filters( 'cp_user_account_get_username', cp_displayed_user()->user_nicename );
	}

/**
 * Output a disable attribute on the username if not equal to the login.
 * This prevents too much edits on this field (which is part of an url).
 *
 * @since  1.0.0
 *
 * @return string a disable attribute on the username.
 */
function cp_account_username_disabled() {
	$user = cp_displayed_user();
	if ( $user->user_nicename !== $user->user_login ) {
		echo ' disabled="disabled"';
	}
}

/**
 * Output the Account username label.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cp_user_account_username_label() {
	echo cp_user_account_get_username_label();
}

	/**
	 * Get the Account username label.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Account username label.
	 */
	function cp_user_account_get_username_label() {
		/**
		 * Filter here to edit the Account username label.
		 *
		 * @since  1.0.0
		 *
		 * @param string $value The Account username label.
		 */
		return apply_filters( 'cp_user_account_get_username_label', sprintf(
			'%1$s <span class="required">%2$s</span>',
			esc_html__( 'Nom d\'utilisateur', 'clusterpress' ),
			esc_html__( '(obligatoire)', 'clusterpress' )
		) );
	}

/**
 * Output the Account username description.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cp_user_account_username_description() {
	echo cp_user_account_get_username_description();
}
	/**
	 * Get the Account username description.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Account username description.
	 */
	function cp_user_account_get_username_description() {
		$description = array(
			__( 'Votre nom d\'utilisateur est utilisé dans l\'URL de votre profil.', 'clusterpress' ),
		);

		if ( cp_cluster_is_enabled( 'interactions' ) && cp_interactions_are_mentions_enabled() ) {
			$description[] = __( 'Il est également utilisé dans le cadre des mentions @nomutilisateur d\'article ou de commentaire.', 'clusterpress' );
		}

		$user = cp_displayed_user();
		if ( $user->user_nicename === $user->user_login ) {
			$description[] = __( 'Le changer suite à votre inscription peut être une riche idée, dans le cas où ce nom d\'utilisateur est similaire à votre identifiant de connexion.', 'clusterpress' );
		}

		/**
		 * Filter here to edit the Account username description.
		 *
		 * @since  1.0.0
		 *
		 * @param string $value       The Account username description.
		 * @param array  $description A list of description entries.
		 */
		return apply_filters( 'cp_user_account_get_username_description', join( ' ', array_map( 'esc_html', $description ) ), $description );
	}

/**
 * Output the Account display name options.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cp_user_account_display_name_options() {
	echo join( "\n", cp_user_account_get_display_name_options() );
}

	/**
	 * Get the Account display name options.
	 *
	 * @since  1.0.0
	 *
	 * @return array The Account display name options.
	 */
	function cp_user_account_get_display_name_options() {
		$user = cp_displayed_user();

		$options              = array();
		$display_name_options = array(
			'display_nickname' => $user->nickname,
			'display_login'    => $user->user_login,
		);

		if ( $user->user_nicename !== $user->user_login ) {
			$display_name_options['display_username'] = $user->user_nicename;
		}

		if ( ! empty( $user->first_name ) ) {
			$display_name_options['display_firstname'] = $user->first_name;
		}

		if ( ! empty( $user->last_name ) ) {
			$display_name_options['display_lastname'] = $user->last_name;
		}

		if ( ! empty( $user->first_name ) && ! empty( $user->last_name ) ) {
			$display_name_options['display_firstlast'] = $user->first_name . ' ' . $user->last_name;
			$display_name_options['display_lastfirst'] = $user->last_name  . ' ' . $user->first_name;
		}

		if ( false === array_search( $user->display_name, $display_name_options ) ) {
			$display_name_options = array( 'display_displayname' => $user->display_name ) + $display_name_options;
		}

		$display_name_options = array_map( 'trim', $display_name_options );
		$display_name_options = array_unique( $display_name_options );

		foreach ( $display_name_options as $display_name ) {
			$options[] = sprintf( '<option value="%1$s"%2$s>%3$s</option>',
				esc_attr( $display_name ),
				selected( $user->display_name, $display_name, false ),
				esc_html( $display_name )
			);
		}

		/**
		 * Filter here to edit the Account display name options.
		 *
		 * @since  1.0.0
		 *
		 * @param array   $options              The Account display name options.
		 * @param array   $display_name_options The Account display name raw data.
		 * @param WP_User $user                 The user object.
		 */
		return apply_filters( 'cp_user_account_get_display_name_options', $options, $display_name_options, $user );
	}

/**
 * Output the Account display name label.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cp_user_account_display_name_label() {
	echo esc_html( cp_user_account_get_display_name_label() );
}

	/**
	 * Get the Account display name label.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Account display name label.
	 */
	function cp_user_account_get_display_name_label() {
		/**
		 * Filter here to edit the Account display name label.
		 *
		 * @since  1.0.0
		 *
		 * @param string $value The Account display name label.
		 */
		return apply_filters( 'cp_user_account_get_display_name_label', __( 'Choisissez le nom à utiliser publiquement (ex: le titre de votre page de profil...).', 'clusterpress' ) );
	}

/**
 * Output the Account email value.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cp_user_account_email() {
	echo esc_attr( cp_user_account_get_email() );
}

	/**
	 * Get the Account email value.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Account email value.
	 */
	function cp_user_account_get_email() {
		return cp_displayed_user()->user_email;
	}

/**
 * Output the Account email label.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cp_user_account_email_label() {
	echo cp_user_account_get_email_label();
}

	/**
	 * Get the Account email label.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Account email label.
	 */
	function cp_user_account_get_email_label() {
		/**
		 * Filter here to edit the Account email label.
		 *
		 * @since  1.0.0
		 *
		 * @param string $value The Account email label.
		 */
		return apply_filters( 'cp_user_account_get_email_label', sprintf(
			'%1$s <span class="required">%2$s</span>',
			esc_html__( 'Courriel', 'clusterpress' ),
			esc_html__( '(obligatoire)', 'clusterpress' )
		) );
	}

/**
 * Check if an email change has been requested and returns it.
 *
 * @since  1.0.0
 *
 * @return string|bool The email raw data if set. False otherwise.
 */
function cp_user_account_get_new_email() {
	$new_email_pending = get_user_meta( cp_get_displayed_user_id(), '_new_email', true );

	if ( ! empty( $new_email_pending['newemail'] ) ) {
		return is_email( $new_email_pending['newemail'] );
	}

	return false;
}

/**
 * Check there's a pending email change.
 *
 * @since  1.0.0
 *
 * @return bool True if there's a pending email change. False otherwise.
 */
function cp_user_account_new_email_pending() {
	$return = false;

	if ( cp_user_account_get_new_email() && cp_user_account_get_new_email() !== cp_user_account_get_email() ) {
		$return = cp_user_is_self_profile();
	}

	return $return;
}

/**
 * Output the Dismiss email change info/link.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cp_user_account_dismiss_email_change() {
	echo cp_user_account_get_dismiss_email_change();
}

	/**
	 * Get the Dismiss email change info/link.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Dismiss email change info/link.
	 */
	function cp_user_account_get_dismiss_email_change() {
		/**
		 * Filter here to edit the Dismiss email change info/link.
		 *
		 * @since  1.0.0
		 *
		 * @param string $value The Dismiss email change info/link.
		 */
		return apply_filters( 'cp_user_account_get_dismiss_email_change', sprintf(
			__( 'Une demande de changement de votre courriel pour %1$s. %2$s', 'clusterpress' ),
			'<code>' . esc_html( cp_user_account_get_new_email() ) . '</code>',
			sprintf( '<a href="%1$s">%2$s</a>',
				esc_url( wp_nonce_url( add_query_arg(
					'dismiss',
					cp_get_displayed_user_id() . '_new_email',
					cp_user_get_url( array(
						'slug'       => cp_user_get_manage_slug() . '/' . cp_user_get_manage_account_slug(),
						'rewrite_id' => cp_user_get_manage_rewrite_id(),
					), cp_displayed_user() )
				), 'dismiss-' . cp_get_displayed_user_id() . '_new_email' ) ),
				esc_html__( 'Annuler', 'clusterpress' )
			)
		) );
	}

/**
 * Output the Account avatar label.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cp_user_account_avatar_label() {
	echo esc_html( cp_user_account_get_avatar_label() );
}

	/**
	 * Get the Account avatar label.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Account avatar label.
	 */
	function cp_user_account_get_avatar_label() {
		/**
		 * Filter here to edit the Account avatar label.
		 *
		 * @since  1.0.0
		 *
		 * @param string $value The Account avatar label.
		 */
		return apply_filters( 'cp_user_account_get_avatar_label', __( 'Image de profil', 'clusterpress' ) );
	}

/**
 * Output the Account avatar description.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cp_user_account_avatar_description() {
	echo cp_user_account_get_avatar_description();
}

	/**
	 * Get the Account avatar description.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Account avatar description.
	 */
	function cp_user_account_get_avatar_description() {
		// Make sure to apply WordPress filers on the description first.
		$description = apply_filters( 'user_profile_picture_description', sprintf(
			__( 'Vous pouvez modifier votre image de profil depuis <a href="%s">Gravatar</a>.', 'clusterpress' ),
			__( 'https://fr.gravatar.com/', 'clusterpress' )
		), cp_displayed_user() );

		/**
		 * Filter here to edit the Account avatar description.
		 *
		 * @since  1.0.0
		 *
		 * @param string $description The Account avatar description.
		 */
		return apply_filters( 'cp_user_account_get_avatar_description', $description );
	}

/**
 * Output the Account edit password control.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cp_user_account_edit_password() {
	// First enqueue the javascript
	wp_enqueue_script( 'user-profile' );

	// Then output the Password control
	echo cp_user_account_get_edit_password_output();
}

	/**
	 * Get the Account edit password control.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Account edit password control.
	 */
	function cp_user_account_get_edit_password_output() {
		/**
		 * Filter here to edit the Account edit password control.
		 *
		 * @since  1.0.0
		 *
		 * @param string $description The Account edit password control.
		 */
		return apply_filters( 'cp_user_account_get_edit_password_output', sprintf( '
			<div id="password" class="user-pass1-wrap">

				<input class="hidden" value=" " />
				<button type="button" class="button wp-generate-pw hide-if-no-js">%1$s</button>

				<div class="wp-pwd hide-if-js">
					<span class="password-input-wrapper">
						<input type="password" name="pass1" id="pass1" class="regular-text" value="" autocomplete="off" data-pw="%2$s" aria-describedby="pass-strength-result" />
					</span>
					<button type="button" class="button wp-hide-pw hide-if-no-js" data-toggle="0" aria-label="%3$s">
						<span class="dashicons dashicons-hidden"></span>
						<span class="text">%4$s</span>
					</button>
					<button type="button" class="button wp-cancel-pw hide-if-no-js" data-toggle="0" aria-label="%5$s">
						<span class="text">%6$s</span>
					</button>
					<div style="display:none" id="pass-strength-result" aria-live="polite"></div>
				</div>

			</div>

			<div class="user-pass2-wrap hide-if-js">
				<label class="label" for="pass2">%7$s</label>

				<input name="pass2" type="password" id="pass2" class="regular-text" value="" autocomplete="off" />
				<p class="description">%8$s</p>

			</div>

			<div class="pw-weak">
				<label>
					<input type="checkbox" name="pw_weak" class="pw-checkbox" />
					<span id="pw-weak-text-label">%9$s</span>
				</label>
			</div>',
			esc_html__( 'Changer le mot de passe', 'clusterpress' ),
			esc_attr( wp_generate_password( 24 ) ),
			esc_attr__( 'Masquer le mot de passe', 'clusterpress' ),
			esc_html__( 'Masquer', 'clusterpress' ),
			esc_attr__( 'Annuler le changement de mot de passe', 'clusterpress' ),
			esc_html__( 'Annuler', 'clusterpress' ),
			esc_html__( 'Répétez votre nouveau mot de passe', 'clusterpress' ),
			esc_html__( 'Renseignez une nouvelle fois votre mot de passe.', 'clusterpress' ),
			esc_html__( 'Merci de confirmer que vous souhaitez utiliser un mot de passe faible.', 'clusterpress' )
		) );
	}

/**
 * Output the Account submit button.
 *
 * @since  1.0.0
 *
 * @param  string $nonce The nonce to check once the form is submitted.
 * @return string        HTML Output
 */
function cp_account_submit_button( $nonce = '' ) {
	printf( '<input type="hidden" name="user_id" value="%s">', cp_get_displayed_user_id() );
	cp_submit_button( $nonce );
}

/** Users Archive Template Tags **********************************************/

/**
 * Init the ClusterPress Users loop.
 *
 * @since 1.0.0
 *
 * @return bool True if Users were found. False otherwise.
 */
function cp_user_has_users() {
	$cp = clusterpress();

	/**
	 * Filter here to edit The User loop args.
	 *
	 * @since  1.0.0
	 *
	 * @param array $args {
	 *   An array of arguments.
	 *   @see WP_User_Query::parse_query() for a complete description.
	 * }
	 */
	$args = apply_filters( 'cp_user_has_users_args', array_merge( array(
		'orderby' => array( 'display_name' => 'ASC' ),
	), cp_get_filter_var() ) );

	// Setup the global query loop
	$cp->user->users = new CP_Users_Loop( $args );

	/**
	 * Filter here if you need to edit the has_items() result.
	 *
	 * @since  1.0.0
	 *
	 * @param  bool $value True if there are Users to display. False otherwise.
	 */
	return apply_filters( 'cp_user_has_users', $cp->user->users->has_items() );
}

/**
 * Whether there are more Users available in the loop to iterate on.
 *
 * @since 1.0.0
 *
 * @return WP_User The User Object.
 */
function cp_user_the_users() {
	return clusterpress()->user->users->items();
}

/**
 * Load the current User into the loop.
 *
 * @since 1.0.0
 *
 * @return WP_User The current User Object.
 */
function cp_user_the_user() {
	return clusterpress()->user->users->the_item();
}

/**
 * Get the current User into the loop.
 *
 * @since 1.0.0
 *
 * @return WP_User The current User Object.
 */
function cp_user_get_current_user() {
	$cp = clusterpress();

	$user = null;

	if ( isset( $cp->user->users->user->ID ) ) {
		$user = new WP_User( $cp->user->users->user );
	}

	/**
	 * Filter here to edit/add properties to the User object.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_User $user The current User Object.
	 */
	return apply_filters( 'cp_user_get_current_user', $user );
}

/**
 * Output the User ID.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_the_user_ID() {
	echo esc_attr( cp_user_get_user_ID() );
}

	/**
	 * Get the User ID.
	 *
	 * @since 1.0.0
	 *
	 * @return string The User ID.
	 */
	function cp_user_get_user_ID() {
		$user    = cp_user_get_current_user();
		$user_id = 0;

		if ( $user ) {
			$user_id = $user->ID;
		}

		/**
		 * Filter here to edit the User ID.
		 *
		 * @since 1.0.0
		 *
		 * @param int     $user_id The User ID.
		 * @param WP_User $user    The User object.
		 */
		return apply_filters( 'cp_user_get_user_ID', $user_id, $user );
	}

/**
 * Output the User link.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_the_user_link() {
	echo esc_url( cp_user_get_user_link() );
}

	/**
	 * Get the User link.
	 *
	 * @since 1.0.0
	 *
	 * @return string The User link.
	 */
	function cp_user_get_user_link() {
		$user = cp_user_get_current_user();
		$link = '#';

		if ( $user ) {
			$link = cp_user_get_url( array(), $user );
		}

		/**
		 * Filter here to edit the User link.
		 *
		 * @since 1.0.0
		 *
		 * @param string  $link The User link.
		 * @param WP_User $user The User object.
		 */
		return apply_filters( 'cp_user_get_user_link', $link, $user );
	}

/**
 * Output the User name.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_the_user_name() {
	echo esc_html( cp_user_get_user_name() );
}

	/**
	 * Get the User name.
	 *
	 * @since 1.0.0
	 *
	 * @return string The User name.
	 */
	function cp_user_get_user_name() {
		$user = cp_user_get_current_user();
		$name = '';

		if ( ! empty( $user->display_name ) ) {
			$name = $user->display_name;
		} elseif ( ! empty( $user->user_nicename ) ) {
			$name = $user->user_nicename;
		} elseif ( ! empty( $user->user_login ) ) {
			$name = $user->user_login;
		}

		/**
		 * Filter here to edit the User name.
		 *
		 * @since 1.0.0
		 *
		 * @param string  $name The User name.
		 * @param WP_User $user The User object.
		 */
		return apply_filters( 'cp_user_get_user_name', $name, $user );
	}

/**
 * Output the last active date.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_the_user_last_active() {
	echo esc_html( cp_user_get_user_last_active() );
}

	/**
	 * Get the last active date.
	 *
	 * @since 1.0.0
	 *
	 * @return string The last active date.
	 */
	function cp_user_get_user_last_active() {
		$user        = cp_user_get_current_user();
		$last_active = __( 'Pas récemment actif.', 'clusterpress' );

		if ( ! empty( $user->last_active ) ) {
			$last_active = sprintf( __( 'Actif il y a %s', 'clusterpress' ), human_time_diff( strtotime( $user->last_active ) ) );
		}

		/**
		 * Filter here to edit the last active date.
		 *
		 * @since 1.0.0
		 *
		 * @param string  $name The last active date.
		 * @param WP_User $user The User object.
		 */
		return apply_filters( 'cp_user_get_user_last_active', $last_active, $user );
	}

/**
 * Output the user avatar.
 *
 * @since 1.0.0
 *
 * @param  array $args {
 *   @see cp_user_get_user_avatar() for a detailed description.
 * }
 * @return string HTML Output.
 */
function cp_user_the_user_avatar( $args = array() ) {
	echo cp_user_get_user_avatar( $args );
}

	/**
	 * Get the user avatar.
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args {
	 *   @type int $user_id The user ID.
	 *   @type int $width   The avatar image width in pixels. Defauls to 80.
	 *   @type int $height  The avatar image height in pixels. Defauls to 80.
	 * }
	 * @return string The user avatar.
	 */
	function cp_user_get_user_avatar( $args = array() ) {
		$r = wp_parse_args( $args, array(
			'user_id' => cp_user_get_user_ID(),
			'width'   => 80,
			'height'  => 80,
		) );

		/**
		 * Filter here to edit the user avatar.
		 *
		 * @since 1.0.0
		 *
		 * @param string  $value The user avatar.
		 */
		return apply_filters( 'cp_user_get_user_avatar', cp_get_user_avatar( $r ) );
	}

/**
 * Output the user form action.
 *
 * @since  1.0.0
 *
 * @return stringe HTML Output.
 */
function cp_user_form_action() {
	/**
	 * Filter here to edit the user form action.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $value The user form action.
	 */
	echo esc_url( apply_filters( 'cp_user_form_action', '' ) );
}

/**
 * Output the user table headers.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_table_headers() {
	echo cp_user_get_table_headers();
}
	/**
	 * Get the user table headers.
	 *
	 * @since 1.0.0
	 *
	 * @param  bool $use_foot True to use the tfoot tag. False otherwise.
	 * @return string The user table headers.
	 */
	function cp_user_get_table_headers( $use_foot = true ) {
		$output = '';

		$headers = array(
			'cb'     => '<input type="checkbox" class="cp-select-all">',
			'member' => esc_html__( 'membre',   'clusterpress' ),
			'role'   => esc_html__( 'Rôle',     'clusterpress' ),
			'email'  => esc_html__( 'Courriel', 'clusterpress' ),
		);

		if ( ! cp_is_site_manage_members() ) {
			unset( $headers['role'] );
		}

		$header_cells = '';
		foreach ( $headers as $kh => $vh ) {
			if ( 'cb' === $kh ) {
				$header_cells .= sprintf( '<th class="cp-checkbox">%s</th>', $vh ) . "\n";
			} else {
				$header_cells .= sprintf( '<th>%s</th>', $vh ) . "\n";
			}
		}

		$output = sprintf( "<thead>\n\t<tr>%s</tr>\n</thead>", $header_cells );

		if ( true === $use_foot ) {
			$output .= sprintf( "\n<tfoot>\n\t<tr>%s</tr>\n</tfoot>", $header_cells );
		}

		/**
		 * Filter here to edit the user table headers.
		 *
		 * @since 1.0.0
		 *
		 * @param string  $output   The user table headers.
		 * @param array   $headers  The column headers.
		 * @param bool    $use_foot True to use the tfoot tag. False otherwise.
		 */
		return apply_filters( 'cp_user_get_table_headers', $output, $headers, $use_foot );
	}

/**
 * Output the user email link.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_the_user_email() {
	echo wp_kses( cp_user_get_user_email(), array(
		'a' => array( 'href' => true ),
	) );
}

	/**
	 * Get the user email link.
	 *
	 * @since 1.0.0
	 *
	 * @return string The user email link.
	 */
	function cp_user_get_user_email() {
		$user = cp_user_get_current_user();
		$email = '#';

		if ( $user ) {
			$email = $user->user_email;

			if ( is_email( $email ) ) {
				$email = sprintf( '<a href="mailto:%1$s">%2$s</a>',
					esc_attr( $user->user_email ),
					esc_html( $user->user_email )
				);
			}
		}

		/**
		 * Filter here to edit the user email link.
		 *
		 * @since 1.0.0
		 *
		 * @param string  $email The user email link.
		 * @param WP_User $user  The User object.
		 */
		return apply_filters( 'cp_user_get_user_email', $email, $user );
	}

/**
 * Output the user role.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_the_user_role() {
	echo esc_html( cp_user_get_user_role() );
}

	/**
	 * Get the user role.
	 *
	 * @since 1.0.0
	 *
	 * @return string The user role.
	 */
	function cp_user_get_user_role() {
		$user = cp_user_get_current_user();
		$role = _x( 'Aucun', 'no site roles', 'clusterpress' );

		if ( empty( $user->site_role ) ) {
			if ( cp_is_site_manage_followers() ) {
				$role = _x( 'Adepte', 'site follower (not a role)', 'clusterpress' );
			}
		} else {
			$wp_roles = wp_roles();

			if ( ! empty( $wp_roles->role_names[ $user->site_role ] ) ) {
				$role = translate_user_role( $wp_roles->role_names[ $user->site_role ] );
			}
		}

		/**
		 * Filter here to edit the user role.
		 *
		 * @since 1.0.0
		 *
		 * @param string  $role The user role.
		 * @param WP_User $user The User object.
		 */
		return apply_filters( 'cp_user_get_user_role', $role, $user );
	}

/**
 * Output the user action options.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_the_user_actions() {
	echo cp_user_get_user_actions();
}

	/**
	 * Get the user action options.
	 *
	 * @since 1.0.0
	 *
	 * @return string The user action options.
	 */
	function cp_user_get_user_actions() {
		$wp_roles     = wp_roles();
		$roles        = array_map( 'translate_user_role', $wp_roles->role_names );
		$optgrp_label = __( 'Modifier le rôle pour :', 'clusterpress' );

		$role_actions = array();
		foreach ( $roles as $vr => $tr ) {
			$role_actions[] = sprintf( '<option value="%1$s">%2$s</option>', esc_attr( $vr ), esc_html( $tr ) );
		}

		rsort( $role_actions );

		$other_actions = array(
			'choose' => '<option value="0">' . esc_html__( 'Sélectionner l\'action', 'clusterpress' ) . '</option>',
			'remove' => '<option value="remove">' . esc_html__( 'Supprimer du site', 'clusterpress' ) . '</option>',
			'demote' => '<option value="demote">' . esc_html__( 'Rétrograder comme adepte', 'clusterpress' ) . '</option>',
		);

		if ( cp_is_site_manage_followers() ) {
			unset( $other_actions['demote'] );
			$optgrp_label = __( 'Promouvoir comme :', 'clusterpress' );
		}

		$actions = sprintf( '%3$s<optgroup label="%1$s">%2$s</optgroup>',
			esc_attr( $optgrp_label ),
			join( "\n", $role_actions ),
			join( "\n", $other_actions )
		);

		/**
		 * Filter here to edit the user action options.
		 *
		 * @since 1.0.0
		 *
		 * @param string  $actions       The user action options.
		 * @param array   $role_actions  The actions to promote the user on a site's role.
		 * @param array   $other_actions The actions to demote or remove the user.
		 */
		return apply_filters( 'cp_user_get_user_actions', $actions, $role_actions, $other_actions );
	}

/**
 * Output the total count for the current Users loop.
 *
 * @since 1.0.0
 *
 * @return  string HTML Output
 */
function cp_user_total_count() {
	echo esc_html( cp_user_get_total_count() );
}
	/**
	 * Return the total count for the current Users loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string HTML for the total count.
	 */
	function cp_user_get_total_count() {
		$loop  = clusterpress()->user->users;

		$displayed = number_format_i18n( $loop->user_count );
		$total     = number_format_i18n( $loop->total_user_count );

		$total_count  = sprintf(
			_n( 'Total %1$s membre, %2$s affiché.', 'Total %1$s membres, %2$s affiché(s).', $total, 'clusterpress' ),
			$total,
			$displayed
		);

		/**
		 * Filter here to edit the total users count
		 *
		 * @since  1.0.0
		 *
		 * @param  string $total_count the total users count to output
		 */
		return apply_filters( 'cp_user_get_total_count', $total_count );
	}

/**
 * Check if there are pagination links to output.
 *
 * @since  1.0.0
 *
 * @return bool True if there are pagination links to output. False otherwise.
 */
function cp_user_has_pagination_links() {
	return ! empty( clusterpress()->user->users->pag_links );
}

/**
 * Output the pagination links for the current Users loop.
 *
 * @since 1.0.0
 *
 * @return string HTML output.
 */
function cp_user_pagination_links() {
	echo cp_user_get_pagination_links();
}

	/**
	 * Return the pagination links for the current Users loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string output for the pagination links.
	 */
	function cp_user_get_pagination_links() {
		/**
		 * Filter here to edit the pagination links.
		 *
		 * @since  1.0.0
		 *
		 * @param  string $value The pagination links to output.
		 */
		return apply_filters( 'cp_user_get_pagination_links', clusterpress()->user->users->pag_links );
	}

/**
 * Output a user feedback to inform no users were found.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_user_no_users_found() {
	echo cp_user_get_no_users_found();
}

	/**
	 * Get a user feedback to inform no users were found.
	 *
	 * @since 1.0.0
	 *
	 * @return string The user feedback to inform no users were found.
	 */
	function cp_user_get_no_users_found() {
		$message = __( 'Aucun utilisateur n\'a été trouvé.', 'clusterpress' );

		clusterpress()->feedbacks->add_feedback(
			'user[users-empty]',
			/**
			 * Use this filter if you wish to edit the no users message
			 *
			 * @since 1.0.0
			 *
			 * @param string $message The user feedback to inform no users were found.
			 */
			apply_filters( 'cp_user_get_no_users_found', $message ),
			'info'
		);

		cp_get_template_part( 'assets/feedbacks' );
	}
