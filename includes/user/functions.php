<?php
/**
 * ClusterPress User Functions.
 *
 * @package ClusterPress\user
 * @subpackage functions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Get a user's profile URL.
 *
 * NB: If the $user parameter is passed, there is no need to
 * include the $user_id/$user_nicename arguments. If the $user
 * parameter is not passed you need to set the $user_id or the
 * $user_nicename arguments.
 *
 * @since  1.0.0
 *
 * @param  array  $args {
 *    An array of arguments.
 *    @type string $rewrite_id    The rewrite ID of the nav item to display.
 *    @type string $slug          The slug of the nav item to display.
 *    @type int    $user_id       The user ID.
 *    @type string $user_nicename The user nicename.
 * }
 * @param  WP_User $user The user object.
 * @return string        The user's profile URL.
 */
function cp_user_get_url( $args = array(), $user = null ) {
	global $wp_rewrite;

	$r = wp_parse_args( $args, array(
		'rewrite_id'    => '',
		'slug'          => '',
		'user_id'       => 0,
		'user_nicename' => '',
	) );

	// Bail if we can't get the corresponding user.
	if ( empty( $r['user_nicename'] ) && empty( $r['user_id'] ) && is_null( $user ) ) {
		return false;
	}

	// Validate the user
	if ( ! is_a( $user, 'WP_User' ) ) {
		if ( $r['user_id'] ) {
			$user = get_user_by( 'id', $r['user_id'] );
		} else {
			$user = get_user_by( 'slug', $r['user_nicename'] );
		}
	}

	// Bail if we haven't found the user.
	if ( empty( $user->ID ) ) {
		return false;
	}

	// Check we're on the main site
	$is_main_site = cp_is_main_site();

	if ( ! $is_main_site ) {
		switch_to_blog( get_current_network_id() );
	}

	// Pretty permalinks
	if ( $wp_rewrite->using_permalinks() ) {
		$url = $wp_rewrite->root . trailingslashit( cp_get_root_slug() ) . cp_user_get_slug() . '/%' . cp_user_get_rewrite_id() . '%';

		$url = str_replace( '%' . cp_user_get_rewrite_id() . '%', $user->user_nicename, $url );
		$url = home_url( trailingslashit( $url ) );

		if ( $r['slug'] ) {
			$url = trailingslashit( $url . $r['slug'] );
		}

	// Unpretty permalinks
	} else {
		$args = array( cp_user_get_rewrite_id() => $user->user_nicename );

		if ( $r['rewrite_id'] ) {
			$args[ $r['rewrite_id'] ] = '1';
		}

		if ( ! empty( $r['slug'] ) ) {
			$parts = array_filter( explode( '/', $r['slug'] ) );

			if ( 1 !== count( $parts ) ) {
				$args[ $r['rewrite_id'] ] = $parts[1];
			}
		}

		$url = esc_url_raw( add_query_arg( $args, home_url( '/' ) ) );
	}

	if ( ! $is_main_site ) {
		restore_current_blog();
	}

	return $url;
}

/**
 * Get the registered profile fields for users.
 *
 * @since  1.0.0
 *
 * @param  WP_User $user The user object.
 * @return array         The list of registered profile fields.
 */
function cp_user_get_profile_fields( $user = null ) {
	if ( ! is_a( $user, 'WP_User' ) ) {
		$user = wp_get_current_user();
	}

	$contact_methods = wp_get_user_contact_methods( $user );
	$wp_fields       = array(
		'user_description' => __( 'A mon propos', 'clusterpress' ),
		'first_name'       => _x( 'Prénom', 'The first name of the user', 'clusterpress' ),
		'last_name'        => _x( 'Nom', 'The last name of the user', 'clusterpress' ),
		'user_url'         => __( 'site Web', 'clusterpress' ),
	);

	/**
	 * Filter here to add or remove profile fields.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list of registered profile fields to use in ClusterPress.
	 */
	return apply_filters( 'cp_user_get_profile_fields', array_merge( $wp_fields, $contact_methods ) );
}

/**
 * Get the users to list in the User Archive loop.
 *
 * @since  1.0.0
 *
 * @param  array  $args {
 *   An array of arguments.
 *   @see  CP_User_Query::prepare_query() for details about the arguments description.
 * }}
 * @return array The list of users matching the arguments.
 */
function cp_get_users( $args = array() ) {
	$r = wp_parse_args( $args, array(
		'count_total' => true,
	) );

	$users = new CP_User_Query( $r );

	if ( false === $r['count_total'] ) {
		return $users->get_results();
	}

	$results = array(
		'users' => $users->get_results(),
	);

	if ( ! empty( $r['count_total'] ) ) {
		$results['total'] = $users->get_total();
	}

	return $results;
}

/**
 * Get the Cluster default caps.
 *
 * @since  1.0.0
 *
 * @return array The list of caps the user needs to access any parts of ClusterPress.
 */
function cp_user_get_default_caps() {
	$caps = array( 'manage_options' );

	if ( is_multisite() ) {
		$caps = array( 'manage_network_users' );
	}

	/**
	 * Filter here to edit the default caps.
	 *
	 * @since  1.0.0
	 *
	 * @param  array The list of caps the user needs to access any parts of ClusterPress.
	 */
	return apply_filters( 'cp_user_get_default_caps', $caps );
}

/**
 * User capability callback validating the Archive access.
 *
 * @since  1.0.0
 *
 * @param  int   $user_id The user ID.
 * @param  array $args    Adds the context to the cap. Typically the object ID.
 * @return array $caps    The user's actual capabilities.
 */
function cp_user_archive_required_caps( $user_id = 0, $args = array() ) {
	$caps         = cp_user_get_default_caps();
	$network_type = cp_get_network_type();

	if ( ( 'private' === $network_type && ! empty( $user_id ) )
	  || ( 'public' === $network_type ) ) {
		$caps = array( 'exist' );
	}

	/**
	 * Filter here to edit user caps.
	 *
	 * @since  1.0.0
	 *
	 * @param array $caps    The user's actual capabilities.
	 * @param int   $user_id The user id.
	 * @param array $args    Complementary arguments.
	 */
	return apply_filters( 'cp_user_archive_required_caps', $caps, $user_id, $args );
}

/**
 * User capability callback validating the single access (view).
 *
 * @since  1.0.0
 *
 * @param  int   $user_id The user ID.
 * @param  array $args    Adds the context to the cap. Typically the object ID.
 * @return array $caps    The user's actual capabilities.
 */
function cp_user_read_single_required_caps( $user_id = 0, $args = array() ) {
	$caps         = cp_user_get_default_caps();
	$network_type = cp_get_network_type();
	$id           = 0;
	$toolbar      = '';

	if ( ! empty( $args[0]['user_id'] ) ) {
		$id = $args[0]['user_id'];
	}

	if ( ! empty( $args[0]['toolbar'] ) ) {
		$toolbar = $args[0]['toolbar'];
	}

	if ( ( 'private' === $network_type && ! empty( $user_id ) )
	  || ( 'public' === $network_type )
	  || ( 'closed' === $network_type && ( cp_user_is_self_profile( $id )
	    || ( (int) $user_id === (int) get_current_user_id() && 'admin_bar' === $toolbar ) )
	  ) ) {
		$caps = array( 'exist' );
	}

	/**
	 * Filter here to edit user caps.
	 *
	 * @since  1.0.0
	 *
	 * @param array $caps    The user's actual capabilities.
	 * @param int   $user_id The user id.
	 * @param array $args    Complementary arguments.
	 */
	return apply_filters( 'cp_user_read_single_required_caps', $caps, $user_id, $args );
}

/**
 * User capability callback validating the single access (edit).
 *
 * @since  1.0.0
 *
 * @param  int   $user_id The user ID.
 * @param  array $args    Adds the context to the cap. Typically the object ID.
 * @return array $caps    The user's actual capabilities.
 */
function cp_user_edit_single_required_caps( $user_id = 0, $args = array() ) {
	$caps    = cp_user_get_default_caps();
	$id      = 0;
	$toolbar = '';

	if ( ! empty( $args[0]['user_id'] ) ) {
		$id = $args[0]['user_id'];
	}

	if ( ! empty( $args[0]['toolbar'] ) ) {
		$toolbar = $args[0]['toolbar'];
	}

	if ( cp_user_is_self_profile( $id ) || ( (int) $user_id === (int) get_current_user_id() && 'admin_bar' === $toolbar ) ) {
		$caps = array( 'exist' );
	}

	/**
	 * Filter here to edit user caps.
	 *
	 * @since  1.0.0
	 *
	 * @param array $caps    The user's actual capabilities.
	 * @param int   $user_id The user id.
	 * @param array $args    Complementary arguments.
	 */
	return apply_filters( 'cp_user_edit_single_required_caps', $caps, $user_id, $args );
}

/**
 * Edit the url to validate the email change for Multisite configs.
 *
 * @since  1.0.0
 *
 * @param  string $email_text The text of the email sent by WordPress to validate
 *                            an email change.
 * @return string             The same text, except for the URL.
 */
function cp_user_reset_validate_new_email_url( $email_text = '' ) {
	$user = cp_displayed_user();
	if ( empty( $user->ID ) ) {
		return $email_text;
	}

	$new_mail_meta = get_user_meta( $user->ID, '_new_email', true );

	if ( empty( $new_mail_meta['hash'] ) ) {
		return $email_text;
	}

	$user_url = cp_user_get_url( array(
		'slug'       => cp_user_get_manage_slug() . '/' . cp_user_get_manage_account_slug(),
		'rewrite_id' => cp_user_get_manage_rewrite_id(),
	), $user );

	if ( false === $user_url ) {
		return $email_text;
	}

	return str_replace( '###ADMIN_URL###', esc_url( add_query_arg(
		'newuseremail',
		$new_mail_meta['hash'],
		$user_url
	) ), $email_text );
}

/**
 * Activate the new email on Multisite configs.
 *
 * @since  1.0.0
 *
 * @param  string $hash The secret key to activate the email.
 * @return bool         True if the new email was activated. False otherwise.
 */
function cp_user_activate_new_email( $hash = '' ) {
	// Get the current user
	$user = wp_get_current_user();

	if ( empty( $user->ID ) || empty( $hash ) ) {
		return false;
	}

	$new_email = get_user_meta( $user->ID, '_new_email', true );
	if ( empty( $new_email['hash'] ) || empty( $new_email['newemail'] ) || ! hash_equals( $new_email['hash'], $hash ) ) {
		return false;
	}

	$userdata = (object) array(
		'ID'         => $user->ID,
		'user_email' => esc_html( trim( $new_email['newemail'] ) ),
	);

	global $wpdb;
	$signups = $wpdb->signups;

	$has_signedup = $wpdb->get_var( $wpdb->prepare( "SELECT user_login FROM {$signups} WHERE user_login = %s", $user->user_login ) );

	if ( ! empty( $has_signedup ) ) {
		$wpdb->update(
			$signups,
			array( 'user_email' => $userdata->user_email ),
			array( 'user_login' => $user->user_login ),
			array( '%s' ),
			array( '%s' )
		);
	}

	wp_update_user( $userdata );
	delete_user_meta( $user->ID, '_new_email' );

	return true;
}

/**
 * Handle user profile/account edit actions.
 *
 * @since  1.0.0
 */
function cp_user_edit_user_action() {
	// Specific Multisite email account actions
	if ( is_multisite() ) {
		// Cancelling an email change
		if ( cp_is_user_manage_account() && ! empty( $_GET['dismiss'] ) && cp_get_displayed_user_id() . '_new_email' === $_GET['dismiss'] ) {

			check_admin_referer( 'dismiss-' . cp_get_displayed_user_id() . '_new_email' );

			delete_user_meta( cp_get_displayed_user_id(), '_new_email' );
			cp_feedback_redirect( 'account-01', 'updated', remove_query_arg( array( 'dismiss' ), wp_get_referer() ) );

		// Validating an email change
		} elseif ( cp_is_user_manage_account() && isset( $_GET['newuseremail'] ) && cp_user_is_self_profile() ) {
			if ( cp_user_activate_new_email( $_GET['newuseremail'] ) ) {
				cp_feedback_redirect( 'account-01', 'updated', remove_query_arg( array( 'newuseremail' ), wp_get_referer() ) );
			} else {
				cp_feedback_redirect( 'account-02', 'error', remove_query_arg( array( 'newuseremail' ), wp_get_referer() ) );
			}
		}
	}

	// Edit Profile
	if ( ! cp_is_user_manage_profile() && ! cp_is_user_manage_account() ) {
		return;
	}

	if ( cp_is_user_manage_profile() && ! empty( $_POST['cp_profile_edit'] ) ) {
		$post_data = $_POST['cp_profile_edit'];

		check_admin_referer( 'cp_profile_edit' );
	} elseif ( cp_is_user_manage_account() && ! empty( $_POST['cp_account_edit'] ) ) {
		$post_data = $_POST['cp_account_edit'];

		foreach ( $_POST as $kp => $vp ) {
			if ( isset( $post_data[ $kp ] ) || false === array_search( $kp, array(
				'pass1', 'pass1-text', 'pass2', 'pw_weak', 'email', 'user_id'
			) ) ) {
				continue;
			}

			$post_data[ $kp ] = $vp;
		}

		$post_data = array_filter( $post_data );

		check_admin_referer( 'cp_account_edit' );
	}

	if ( empty( $post_data ) ) {
		return;
	}

	$user = cp_displayed_user();
	if ( ! current_user_can( 'cp_edit_single_user' ) || empty( $user->ID ) ) {
		cp_feedback_redirect( 'not-allowed', 'error' );
	}

	$message = array(
		'error'   => 'profile-02',
		'updated' => 'profile-01',
	);
	$redirect_url = '';

	// Updating the profile
	if ( cp_is_user_manage_profile() ) {
		$fields = cp_user_get_profile_fields( $user );

		foreach ( array_keys( $fields ) as $key ) {
			if ( isset( $post_data[ $key ] ) ) {
				if ( 'user_description' === $key ) {
					$user->description = trim( $post_data[ $key ] );
				} else {
					$user->{$key} = sanitize_text_field( $post_data[ $key ] );
				}

				unset( $post_data[ $key ] );
			}
		}

	// Updating the account
	} elseif ( cp_is_user_manage_account() ) {
		// Password change ?
		$pass1 = $pass2 = '';
		if ( isset( $post_data['pass1'] ) ) {
			$pass1 = $post_data['pass1'];
		}

		if ( isset( $post_data['pass2'] ) ) {
			$pass2 = $post_data['pass2'];
		}

		if ( ( ! empty( $pass1 ) && $pass1 !== $pass2 ) || ( ! empty( $pass1 ) && false !== strpos( wp_unslash( $pass1 ), "\\" ) ) ) {
			cp_feedback_redirect( 'account-02', 'error' );
		}

		// Set the new password.
		if ( ! empty( $pass1 ) ) {
			$user->user_pass = $pass1;
		}

		// Username change ?
		if ( ( isset( $post_data['username'] ) && ! $post_data['username'] ) || ( isset( $post_data['username'] ) && ! validate_username( $post_data['username'] ) ) ) {
			cp_feedback_redirect( 'account-02', 'error' );
		}

		if ( isset( $post_data['username'] ) ) {
			$user_nicename = sanitize_text_field( wp_unslash( $post_data['username'] ) );
			$username_exists = get_user_by( 'slug', $user_nicename );

			if ( $username_exists && (int) $username_exists->ID !== (int) $user->ID ) {
				cp_feedback_redirect( 'account-03', 'error' );
			}

			$old_user_nicename   = $user->user_nicename;
			$user->user_nicename = $post_data['username'];
		}

		// Edit Display name.
		if ( ! empty( $post_data['display_name'] ) ) {
			$user->display_name = sanitize_text_field( $post_data['display_name'] );
		}

		if ( isset( $post_data['email'] ) ) {
			$sanitized_email = sanitize_text_field( wp_unslash( $post_data['email'] ) );

			// Get potential errors.
			global $errors;

			// Email change ?
			if ( $user->user_email !== $sanitized_email ) {

				if ( is_multisite() && (int) get_current_user_id() === (int) $user->ID ) {
					// Make sure the function is available
					require_once( ABSPATH . 'wp-admin/includes/ms.php' );

					if ( empty( $old_user_nicename ) || $old_user_nicename === $user->user_nicename ) {
						add_filter( 'new_user_email_content', 'cp_user_reset_validate_new_email_url', 10, 1 );
					}

					send_confirmation_on_profile_email();

					if ( empty( $old_user_nicename ) || $old_user_nicename === $user->user_nicename ) {
						remove_filter( 'new_user_email_content', 'cp_user_reset_validate_new_email_url', 10, 1 );
					}

					if ( is_wp_error( $errors ) && $errors->get_error_messages() ) {
						cp_feedback_redirect( 'account-02', 'error' );
					}
				} else {
					if ( ! is_email( $sanitized_email ) ) {
						cp_feedback_redirect( 'account-04', 'error' );
					}

					$owner_id = email_exists( $sanitized_email );

					if ( $owner_id && (int) $owner_id !== (int) $user->ID ) {
						cp_feedback_redirect( 'account-05', 'error' );
					} else {
						$user->user_email = $sanitized_email;
					}
				}
			}
		}

		// Reset message.
		$message = array(
			'error'   => 'account-02',
			'updated' => 'account-01',
		);

		// Reset the redirect url, as the usename changed
		if ( ! empty( $old_user_nicename ) && $old_user_nicename !== $user->user_nicename ) {
			$redirect_url = cp_user_get_url( array(
				'slug'       => cp_user_get_manage_slug() . '/' . cp_user_get_manage_account_slug(),
				'rewrite_id' => cp_user_get_manage_rewrite_id(),
			), $user );
		}
	}

	/**
	 * Hook here to add custom actions before the user is edited.
	 *
	 * @since  1.0.0
	 *
	 * @param  WP_User $user      The user object passed by reference.
	 * @param  array   $post_data The user submitted data.
	 */
	do_action_ref_array( 'cp_user_before_edit_user', array( &$user, $post_data ) );

	$result = wp_update_user( $user );

	/**
	 * Hook here to add custom actions after the user has been edited.
	 *
	 * @since  1.0.0
	 *
	 * @param  int|WP_Error $result    The updated user's ID or a WP_Error object if the user could not be updated.
	 * @param  array        $post_data The user submitted data.
	 */
	do_action( 'cp_user_after_edit_user', $result, $post_data );

	if ( ! is_wp_error( $result ) ) {
		cp_feedback_redirect( $message['updated'], 'updated', $redirect_url );
	} else {
		cp_feedback_redirect( $message['error'], 'error', $redirect_url );
	}
}
add_action( 'cp_page_actions', 'cp_user_edit_user_action' );

/** User Relationship functions ***********************************************/

/**
 * Check if the user relationship API has been inited.
 *
 * @since  1.0.0
 *
 * @return bool True if the user relationship API has been inited. False otherwise.
 */
function cp_user_relationships_isset() {
	$cp = clusterpress();

	if ( ! isset( $cp->user->relationships ) || ! is_a( $cp->user->relationships, 'CP_User_Relationships' ) ) {
		return false;
	}

	return true;
}

/**
 * Unset a user relationship type.
 *
 * @since  1.0.0
 *
 * @param  string $name The relationship name.
 * @return bool         True if the user relationship was unset.
 *                      False otherwise.
 */
function cp_user_relationship_unset( $name = '' ) {
	if ( ! cp_user_relationships_isset() ) {
		return false;
	}

	return clusterpress()->user->relationships->unregister_type( $name );
}

/**
 * Get a relationship object.
 *
 * @since  1.0.0
 *
 * @param  string       $name The relationship name.
 * @return false|object       The object relationship or false if it was not found.
 */
function cp_user_relationships_get_object( $name = '' ) {
	if ( ! cp_user_relationships_isset() ) {
		return false;
	}

	return clusterpress()->user->relationships->get_relationship_object( $name );
}

/**
 * Register a new user relationship.
 *
 * NB: This is the function you need to use to register new user relationships.
 *
 * @since  1.0.0
 *
 * @param  string $cluster The cluster ID to attach the relationship to. Defaults to user.
 * @param  array  $type {
 *    An array of arguments.
 *    @see  CP_User_Relationships::register_type() for a detailed description.
 * }
 * @return bool True if the relationship was successfully registered. False otherwise.
 */
function cp_user_register_relationship( $cluster = 'user', $type = array() ) {
	if ( ! cp_user_relationships_isset() ) {
		return false;
	}

	return clusterpress()->user->relationships->register_type( $cluster, $type );
}

/**
 * Register new user relationships.
 *
 * NB: if you have more than one relationship to register, you can use this function by
 * passing an array of relationship types.
 *
 * @since  1.0.0
 *
 * @param  string $cluster The cluster ID to attach the relationship to. Defaults to user.
 * @param  array  $types {
 *    An array of relationship types.
 *    @see  CP_User_Relationships::register_type() for a detailed description of the needed
 *    arguments to register a unique type.
 * }
 * @return bool True if the relationships were successfully registered. False otherwise.
 */
function cp_user_register_relationships( $cluster = 'user', $types = array() ) {
	if ( ! cp_user_relationships_isset() ) {
		return false;
	}

	return clusterpress()->user->relationships->register_types( $cluster, $types );
}

/**
 * Get the relationships matching the requested arguments.
 *
 * @since  1.0.0
 *
 * @param  string  $columns A column name or an array of column names to select.
 * @param  array    $args   {
 *     An array of arguments to fill the "WHERE" SQL clause.
 *     @see  CP_User_Relationships::get() for a detailed description.
 * }
 * @param  bool|int $offset Set the offset if you need partial/paginated results.
 * @param  bool|int $number Set the number if you need partial/paginated results.
 * @param  bool     $desc   Whether to get results ordered by id DESC or ASC.
 * @return array            The list of matching relationships.
 */
function cp_user_get_relationship( $columns = '', $args = array(), $offset = false, $number = false, $desc = false ) {
	if ( ! cp_user_relationships_isset() ) {
		return false;
	}

	return clusterpress()->user->relationships->get( $columns, $args, $offset, $number, $desc );
}

/**
 * Insert a new row of values for a registered relationship.
 *
 * @since  1.0.0
 *
 * @param  array $args {
 *     An array of arguments to fill the "WHERE" SQL clause.
 *     @see  CP_User_Relationships::add() for a detailed description.
 * }}
 * @return bool True if a new row for the relationship was inserted. False otherwise.
 */
function cp_user_add_relationship( $args = array() ) {
	if ( ! cp_user_relationships_isset() ) {
		return false;
	}

	return clusterpress()->user->relationships->add( $args );
}

/**
 * Update the values of the registered relationship matching specific clauses.
 *
 * @since  1.0.0
 *
 * @param  array $args {
 *     An array of arguments to fill the fields to update.
 *     @see  CP_User_Relationships::update() for a detailed description.
 * }}
 * @param  array $where {
 *     An array of arguments to fill the "WHERE" SQL clause.
 *     @see  CP_User_Relationships::update() for a detailed description.
 * }}
 * @return bool True if the values where updated. False otherwise.
 */
function cp_user_update_relationship( $args = array(), $where = array() ) {
	if ( ! cp_user_relationships_isset() ) {
		return false;
	}

	return clusterpress()->user->relationships->update( $args, $where );
}

/**
 * Remove one or more relationship updates.
 *
 * @since  1.0.0
 *
 * @param  array  $args {
 *     An array of arguments to fill the "WHERE" SQL clause.
 *     @see  CP_User_Relationships::remove() for a detailed description.
 * }}
 * @return bool True if the relationship(s) has(have) been removed. False otherwise.
 */
function cp_user_remove_relationship( $args = array() ) {
	if ( ! cp_user_relationships_isset() ) {
		return false;
	}

	return clusterpress()->user->relationships->remove( $args );
}

/**
 * Udpate the last active user relationship type.
 *
 * NB: Only do it every 5 minutes by using a cookie.
 *
 * @since  1.0.0
 */
function cp_user_update_last_active() {
	if ( ! is_user_logged_in() ) {
		return;
	}

	if ( isset( $_COOKIE['clusterpress_wait'] ) ) {
		return;
	}

	$user_id = get_current_user_id();
	$date    = current_time( 'mysql', 1 );

	if ( is_multisite() ) {
		$primary_id = get_current_network_id();
	} else {
		$primary_id = get_current_blog_id();
	}

	$last_id = cp_user_get_relationship( 'id', array(
		'user_id'    => $user_id,
		'primary_id' => $primary_id,
		'name'       => 'last_active',
	) );

	if ( ! empty( $last_id ) ) {
		// Check there's only one.
		if ( 1 !== count( $last_id ) ) {
			rsort( $last_id );

			$id = array_shift( $last_id );

			// Delete unneeded relationships.
			cp_user_remove_relationship( array( 'id' => $last_id ) );

		// Set the id
		} else {
			$id = reset( $last_id );
		}

		// Update the last active date
		cp_user_update_relationship( array( 'date' => $date ), array(
			'id'    => $id,
			'name'  => 'last_active',
		) );

	// Insert the last active date.
	} else {
		cp_user_add_relationship( array(
			'user_id'    => $user_id,
			'primary_id' => $primary_id,
			'name'       => 'last_active',
			'date'       => $date,
		) );
	}

	$secure = is_ssl();
	setcookie( 'clusterpress_wait', $date, time() + 5 * MINUTE_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN, $secure );

	if ( SITECOOKIEPATH != COOKIEPATH ) {
		setcookie( 'clusterpress_wait', $date, time() + 5 * MINUTE_IN_SECONDS, SITECOOKIEPATH, COOKIE_DOMAIN, $secure );
	}
}
add_action( 'cp_setup_page_title', 'cp_user_update_last_active' );

/**
 * Clear the ClusterPress user cookies on user logout.
 *
 * @since 1.0.0
 */
function cp_user_clear_cookies() {
	$secure = is_ssl();
	setcookie( 'clusterpress_wait', ' ', time() - YEAR_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN, $secure );

	if ( SITECOOKIEPATH != COOKIEPATH ) {
		setcookie( 'clusterpress_wait', ' ', time() - YEAR_IN_SECONDS, SITECOOKIEPATH, COOKIE_DOMAIN, $secure );
	}
}
add_action( 'cp_logout', 'cp_user_clear_cookies' );

/**
 * Get a single user last active date.
 *
 * @since  1.0.0
 *
 * @param  int          $user_id The user ID.
 * @return string|false          The last active date of the user. False otherwise.
 */
function cp_user_get_last_active( $user_id = 0 ) {
	if ( empty( $user_id ) ) {
		return false;
	}

	if ( ! cp_user_relationships_isset() ) {
		return false;
	}

	return clusterpress()->user->relationships->get_last_active( $user_id );
}

/**
 * Get a list of users last active date.
 *
 * @since  1.0.0
 *
 * @param  int         $user_ids An array of user IDs.
 * @return array|false           The list of last active dates by user ID. False otherwise.
 */
function cp_users_get_last_active( $user_ids = array() ) {
	if ( ! cp_user_relationships_isset() ) {
		return false;
	}

	return clusterpress()->user->relationships->get_last_actives( $user_ids );
}

/**
 * Eventually use the ClusterPress user url for comment authors.
 *
 * @since  1.0.0
 *
 * @param  string     $url        The WordPress comment author URL.
 * @param  int        $comment_id The comment ID.
 * @param  WP_Comment $comment    The comment object.
 * @return string                 The ClusterPress comment author URL.
 */
function cp_users_get_comment_author_url( $url = '', $comment_id = 0, $comment = null ) {
	// No user id or the comment author has specified a specific URL, do nothing.
	if ( empty( $comment->user_id ) || ! empty( $comment->comment_author_url ) ) {
		return $url;
	}

	$user_id = (int) $comment->user_id;

	if ( (int) get_current_user_id() === $user_id ) {
		$user = wp_get_current_user();
	} else {
		$user = get_user_by( 'id', $user_id );
	}

	if ( empty( $user->ID ) ) {
		return $url;
	}

	return esc_url( cp_user_get_url( array(), $user ) );
}
add_filter( 'get_comment_author_url', 'cp_users_get_comment_author_url', 10, 3 );

/**
 * Customize paginate args for the Sites loop according to the context.
 *
 * @since 1.0.0
 *
 * @param  array  $paginate_args The pagination args to use for the paginate_links() function
 * @return array                 The pagination args
 */
function cp_user_sites_loop_paginate_args( $paginate_args = array() ) {
	// We only need to edit the pretty links.
	if ( ! clusterpress()->permalink_structure ) {
		return $paginate_args;
	}

	// User's sites.
	if ( cp_is_user_sites() ) {
		$paginate_args['base']   = trailingslashit( cp_user_get_url( array(
			'rewrite_id'    => cp_sites_get_user_rewrite_id(),
			'slug'          => cp_sites_get_user_slug(),
		), cp_displayed_user() ) ) . '%_%';

	// Managed followed sites
	} elseif ( cp_is_user_manage_followed_sites() ) {
		$paginate_args['base']   = trailingslashit( cp_user_get_url( array(
			'rewrite_id'    => cp_user_get_manage_rewrite_id(),
			'slug'          => cp_user_get_manage_slug() . '/' . cp_site_get_user_manage_followed_slug(),
		), cp_displayed_user() ) ) . '%_%';
	}

	return $paginate_args;
}
add_filter( 'cp_sites_loop_paginate_args', 'cp_user_sites_loop_paginate_args', 10, 1 );

/**
 * Customize paginate args for the Posts loop according to the context.
 *
 * @since 1.0.0
 *
 * @param  array  $paginate_args The pagination args to use for the paginate_links() function
 * @return array                 The pagination args
 */
function cp_user_posts_loop_paginate_args( $paginate_args = array() ) {
	// We only need to edit the pretty links.
	if ( ! clusterpress()->permalink_structure ) {
		return $paginate_args;
	}

	// User's posts.
	if ( cp_is_user_posts() ) {
		$paginate_args['base']   = trailingslashit( cp_user_get_url( array(
			'rewrite_id'    => cp_user_get_posts_rewrite_id(),
			'slug'          => cp_user_get_posts_slug(),
		), cp_displayed_user() ) ) . '%_%';

	// Managed followed sites
	} elseif ( cp_is_user_likes() && cp_is_current_sub_action( cp_user_get_posts_slug() ) ) {
		$paginate_args['base']   = trailingslashit( cp_user_get_url( array(
			'rewrite_id'    => cp_user_get_posts_rewrite_id(),
			'slug'          => cp_user_get_likes_slug() . '/' . cp_user_get_posts_slug(),
		), cp_displayed_user() ) ) . '%_%';
	}

	return $paginate_args;
}
add_filter( 'cp_sites_posts_loop_paginate_args', 'cp_user_posts_loop_paginate_args', 10, 1 );

/**
 * Customize paginate args for the Comments loop according to the context.
 *
 * @since 1.0.0
 *
 * @param  array  $paginate_args The pagination args to use for the paginate_links() function
 * @return array                 The pagination args
 */
function cp_user_comments_loop_paginate_args( $paginate_args = array() ) {
	// We only need to edit the pretty links.
	if ( ! clusterpress()->permalink_structure ) {
		return $paginate_args;
	}

	// User's posts.
	if ( cp_is_user_comments() ) {
		$paginate_args['base']   = trailingslashit( cp_user_get_url( array(
			'rewrite_id'    => cp_user_get_comments_rewrite_id(),
			'slug'          => cp_user_get_comments_slug(),
		), cp_displayed_user() ) ) . '%_%';

	// Managed followed sites
	} elseif ( cp_is_user_likes() && cp_is_current_sub_action( cp_user_get_comments_slug() ) ) {
		$paginate_args['base']   = trailingslashit( cp_user_get_url( array(
			'rewrite_id'    => cp_user_get_comments_rewrite_id(),
			'slug'          => cp_user_get_likes_slug() . '/' . cp_user_get_comments_slug(),
		), cp_displayed_user() ) ) . '%_%';
	}

	return $paginate_args;
}
add_filter( 'cp_sites_comments_loop_paginate_args', 'cp_user_comments_loop_paginate_args', 10, 1 );

/**
 * Customize paginate args for the Site Followed latest Posts loop according to the context.
 *
 * @since 1.0.0
 *
 * @param  array  $paginate_args The pagination args to use for the paginate_links() function
 * @return array                 The pagination args
 */
function cp_user_followed_posts_loop_paginate_args( $paginate_args = array() ) {
	// We only need to edit the pretty links.
	if ( ! clusterpress()->permalink_structure ) {
		return $paginate_args;
	}

	$paginate_args['base'] = trailingslashit( cp_user_get_url( array(
		'rewrite_id'    => cp_site_get_user_following_rewrite_id(),
		'slug'          => cp_site_get_user_following_slug(),
	), cp_displayed_user() ) ) . '%_%';

	return $paginate_args;
}
add_filter( 'cp_sites_followed_posts_loop_paginate_args', 'cp_user_followed_posts_loop_paginate_args', 10, 1 );
