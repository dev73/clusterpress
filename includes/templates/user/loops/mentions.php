<?php
/**
 * ClusterPress Users Mentions loop template.
 *
 * @package ClusterPress\templates\user\loop
 * @subpackage mentions
 *
 * @since 1.0.0
 */

if ( cp_interactions_has_mentions() ) : ?>

	<div class="cp-pagination top">

		<div class="cp-total-count">

			<?php cp_interactions_total_mention_count(); ?>

		</div>

		<?php if ( cp_interactions_mention_has_pagination_links() ) : ?>

			<div class="cp-pagination-links">

				<?php cp_interactions_mention_pagination_links(); ?>

			</div>

		<?php endif ; ?>

	</div>

	<ul class="mention-list">

		<?php while ( cp_interactions_the_mentions() ) : cp_interactions_the_mention() ; ?>

			<li class="<?php cp_interactions_the_mention_class(); ?>">

				<div class="cp-mentions-content"><?php cp_interactions_the_mention_content() ;?></div>

				<?php cp_interactions_the_mention_actions() ; ?>

			</li>

		<?php endwhile ; ?>

	</ul><!-- // .mention-list -->

	<?php if ( cp_interactions_mention_has_pagination_links() ) : ?>

		<div class="cp-pagination bottom">

			<div class="cp-pagination-links">

				<?php cp_interactions_mention_pagination_links(); ?>

			</div>

		</div>

	<?php endif ; ?>

<?php else :

	cp_interactions_no_mentions_found();

endif;
