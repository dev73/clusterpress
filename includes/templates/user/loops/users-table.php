<?php
/**
 * ClusterPress Users Table Loop template.
 *
 * @package ClusterPress\templates\user\loop
 * @subpackage users-table
 *
 * @since 1.0.0
 */

if ( cp_user_has_users() ) : ?>

	<div class="cp-pagination top">

		<div class="cp-total-count">

			<?php cp_user_total_count(); ?>

		</div>

		<?php if ( cp_user_has_pagination_links() ) : ?>

			<div class="cp-pagination-links">

				<?php cp_user_pagination_links(); ?>

			</div>

		<?php endif ; ?>

	</div>

	<form class="cp-form" method="post" action="<?php cp_user_form_action(); ?>" data-cp-role="user-actions">

		<div class="cp-table-actions">

			<div class="cp-search-area cp-form-section">
				<label for="cp-form-search" class="screen-reader-text"><?php cp_search_placehoder(); ?></label>
				<input type="search" placeholder="<?php cp_search_placehoder(); ?>" name="cp_filters[cp_search]" id="cp-form-search">

				<button type="submit">
					<span class="dashicons dashicons-search"></span>
					<span class="screen-reader-text"><?php cp_search_placehoder(); ?></span>
				</button>
			</div>

			<div class="cp-actions-area cp-form-section">
				<select name="cp-user[action]" id="cp-user-actions">

					<?php cp_user_the_user_actions() ;?>

				</select>
				<button type="submit">
					<span class="dashicons dashicons-yes"></span>
					<span class="screen-reader-text"><?php esc_html_e( 'Appliquer', 'clusterpress' );?></span>
				</button>
			</div>

		</div>

		<table class="user-table">

			<?php cp_user_table_headers() ;?>

			<tbody>

				<?php while ( cp_user_the_users() ) : cp_user_the_user() ; ?>

					<tr class="user">

						<td class="cp-checkbox">
							<input type="checkbox" name="cp-user[users][]" class="cp-user-checkbox" value="<?php cp_user_the_user_ID(); ?>">
						</td>

						<td class="cp-member">

							<?php if ( cp_user_show_avatar() ) : ?>

								<a href="<?php cp_user_the_user_link(); ?>"><?php cp_user_the_user_avatar( array( 'width' => 40, 'height' => 40) ); ?></a>

							<?php endif ; ?>

							<h3 class="user-name<?php echo ! cp_user_show_avatar() ? ' no-avatar' : ''; ?>">
								<a href="<?php cp_user_the_user_link(); ?>"><?php cp_user_the_user_name(); ?></a>
							</h3>

						</td>

						<?php if ( cp_is_site_manage_members() ) : ?>

							<td class="cp-role">
								<?php cp_user_the_user_role(); ?>
							</td>

						<?php endif; ?>

						<td class="cp-email">
							<?php cp_user_the_user_email(); ?>
						</td>

					</tr><!-- // .user -->

				<?php endwhile ; ?>

			</tbody>

		</table><!-- // .user-list -->

		<?php wp_nonce_field( 'cp_user_actions' ); ?>

	</form><!-- // .cp-form -->

	<?php if ( cp_user_has_pagination_links() ) : ?>

		<div class="cp-pagination bottom">

			<div class="cp-pagination-links">

				<?php cp_user_pagination_links(); ?>

			</div>

		</div>

	<?php endif ; ?>

<?php else :

	cp_user_no_users_found();

endif;
