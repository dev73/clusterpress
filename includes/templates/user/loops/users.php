<?php
/**
 * ClusterPress Users loop template.
 *
 * @package ClusterPress\templates\user\loop
 * @subpackage users
 *
 * @since 1.0.0
 */

if ( cp_user_has_users() ) : ?>

	<div class="cp-pagination top">

		<div class="cp-total-count">

			<?php cp_user_total_count(); ?>

		</div>

		<?php if ( cp_user_has_pagination_links() ) : ?>

			<div class="cp-pagination-links">

				<?php cp_user_pagination_links(); ?>

			</div>

		<?php endif ; ?>

	</div>

	<ul class="user-list">

		<?php while ( cp_user_the_users() ) : cp_user_the_user() ; ?>

			<li class="user">

				<div class="wrap">

					<?php if ( cp_user_show_avatar() ) : ?>

						<div class="user-avatar">
							<a href="<?php cp_user_the_user_link(); ?>"><?php cp_user_the_user_avatar(); ?></a>
						</div><!-- // .user-avatar -->

					<?php endif ; ?>

					<div class="user-details">

						<h3 class="user-name">
							<a href="<?php cp_user_the_user_link(); ?>"><?php cp_user_the_user_name(); ?></a>
						</h3>

						<p class="last-active"><span class="dashicons dashicons-clock"></span><?php cp_user_the_user_last_active() ;?></p>

					</div><!-- // .user-details -->

				</div><!-- // .wrap -->

			</li><!-- // .user -->

		<?php endwhile ; ?>

	</ul><!-- // .user-list -->

	<?php if ( cp_user_has_pagination_links() ) : ?>

		<div class="cp-pagination bottom">

			<div class="cp-pagination-links">

				<?php cp_user_pagination_links(); ?>

			</div>

		</div>

	<?php endif ; ?>

<?php else :

	cp_user_no_users_found();

endif;
