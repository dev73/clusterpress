<?php
/**
 * ClusterPress Users Archive template.
 *
 * @package ClusterPress\templates\user\archive
 * @subpackage index
 *
 * @since 1.0.0
 */

cp_feedbacks(); ?>

<?php cp_get_template_part( 'assets/filters' ) ; ?>

<div id="cp-users-archive" class="users archive">

	<?php cp_get_template_part( 'user/loops/users' ) ; ?>

</div>
