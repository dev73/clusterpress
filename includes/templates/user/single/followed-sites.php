<?php
/**
 * ClusterPress Single User Followed Sites template.
 *
 * @package ClusterPress\templates\user\single
 * @subpackage followed-sites
 *
 * @since 1.0.0
 */
?>

<h2>
	<?php cp_user_followed_sites_loop_title(); ?>

	<?php if ( cp_sites_followed_has_unread_post() ) : ?>

		<?php cp_sites_followed_read_all_button() ;?>

	<?php endif; ?>

</h2>

<div class="view-site-followed-posts">

	<?php cp_get_template_part( 'site/loops/followed-sites' ) ; ?>

</div><!-- // .view-site-followed-posts -->
