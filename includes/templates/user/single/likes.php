<?php
/**
 * ClusterPress Single User likes template.
 *
 * @package ClusterPress\templates\user\single
 * @subpackage likes
 *
 * @since 1.0.0
 */

cp_get_template_part( 'assets/filters' ) ; ?>

<div class="view-user-likes">

	<?php switch( cp_current_sub_action() ) :

		case cp_user_get_posts_slug()    :
			cp_get_template_part( 'site/loops/posts' );
			break;

		case cp_user_get_comments_slug() :
			cp_get_template_part( 'site/loops/comments' );
			break;

		default        :
			cp_get_template_part( 'user/single/plugins' );
			break;

	endswitch ; ?>

</div>
