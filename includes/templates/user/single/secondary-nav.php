<?php
/**
 * ClusterPress Single User Secondary Nav template.
 *
 * @package ClusterPress\templates\user\single
 * @subpackage secondary-nav
 *
 * @since 1.0.0
 */

if ( cp_user_has_secondary_nav() ) : ?>

	<div class="cp-secondary-nav" data-cp-nav="secondary">

		<ul>

			<?php while ( cp_user_nav_items() ) : cp_user_nav_item(); ?>

				<li id="<?php cp_user_nav_item_id(); ?>" class="<?php cp_user_nav_classes(); ?>">
					<a href="<?php cp_user_nav_link(); ?>">
						<?php cp_user_nav_title(); ?>
					</a>
				</li>

			<?php endwhile; ?>

		</ul>

	</div>

<?php endif ;
