<?php
/**
 * ClusterPress Single User Manage template.
 *
 * @package ClusterPress\templates\user\single\manage
 * @subpackage index
 *
 * @since 1.0.0
 */

switch ( cp_current_sub_action() ) :

	case cp_user_get_manage_profile_slug()       :
		cp_get_template_part( 'user/single/manage/profile' );
		break;

	case cp_user_get_manage_account_slug()       :
		cp_get_template_part( 'user/single/manage/account' );
		break;

	case cp_site_get_user_manage_followed_slug() :
		cp_get_template_part( 'user/single/manage/followed-sites' );
		break;

	default                                      :
		cp_get_template_part( 'user/single/plugins' );
		break;

endswitch ;
