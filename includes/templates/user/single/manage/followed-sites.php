<?php
/**
 * ClusterPress Single User Manage Followed Sites template.
 *
 * @package ClusterPress\templates\user\single\manage
 * @subpackage followed-sites
 *
 * @since 1.0.0
 */

cp_site_start_followed_sites_loop() ; ?>

<div id="cp-site-followed" class="sites archive">

	<?php cp_get_template_part( 'site/loops/sites' ) ; ?>

</div>

<?php cp_site_end_followed_sites_loop() ;
