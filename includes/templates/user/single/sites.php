<?php
/**
 * ClusterPress Single User Sites template.
 *
 * @package ClusterPress\templates\user\single
 * @subpackage sites
 *
 * @since 1.0.0
 */
?>

<h2><?php cp_user_sites_loop_title(); ?></h2>

<?php cp_site_start_user_sites_loop() ; ?>

<div id="cp-user-sites" class="sites archive">

	<?php cp_get_template_part( 'site/loops/sites' ) ; ?>

</div>

<?php cp_site_end_user_sites_loop() ;
