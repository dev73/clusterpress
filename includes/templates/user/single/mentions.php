<?php
/**
 * ClusterPress Single User mentions template.
 *
 * @package ClusterPress\templates\user\single
 * @subpackage mentions
 *
 * @since 1.0.0
 */
?>

<h2>
	<?php cp_user_mentions_the_loop_title(); ?>

	<?php if ( cp_user_mentions_has_unread() ) : ?>

		<?php cp_user_mentions_read_all_button() ;?>

	<?php endif; ?>
</h2>

<div class="view-user-mentions">

	<?php cp_get_template_part( 'user/loops/mentions' ) ; ?>

</div>
