<?php
/**
 * ClusterPress Single User root template.
 *
 * @package ClusterPress\templates\user\single
 * @subpackage index
 *
 * @since 1.0.0
 */

cp_feedbacks(); ?>

<div class="cp-user-header">

	<?php if ( cp_user_show_avatar() ) : ?>

		<div class="cp-profile-photo">
			<?php cp_user_avatar() ; ?>
		</div>

	<?php endif; ?>

	<div class="cp-user-header-content">
		<p class="last-active"><span class="dashicons dashicons-clock"></span><?php cp_user_last_active() ;?></p>

		<?php cp_user_at_username(); ?>
	</div>

</div>

<div class="cp-primary-nav">

	<?php cp_get_template_part( 'user/single/primary-nav' ); ?>

</div>

<div class="cp-content">

	<?php cp_get_template_part( 'user/single/secondary-nav' ); ?>

	<?php switch ( cp_current_action() ) :

		case 'home'                            :
			cp_get_template_part( 'user/single/home' );
			break;

		case cp_user_get_manage_slug()         :
			cp_get_template_part( 'user/single/manage/index' );
			break;

		case cp_user_get_comments_slug()       :
			cp_get_template_part( 'user/single/comments' );
			break;

		case cp_user_get_posts_slug()          :
			cp_get_template_part( 'user/single/posts' );
			break;

		case cp_user_get_likes_slug()          :
			cp_get_template_part( 'user/single/likes' );
			break;

		case cp_user_get_mentions_slug()       :
			cp_get_template_part( 'user/single/mentions' );
			break;

		case cp_sites_get_user_slug()          :
			cp_get_template_part( 'user/single/sites' );
			break;

		case cp_site_get_user_following_slug() :
			cp_get_template_part( 'user/single/followed-sites' );
			break;

		default                                :
			cp_get_template_part( 'user/single/plugins' );
			break;

	endswitch ; ?>

</div>
