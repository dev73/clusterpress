<?php
/**
 * ClusterPress Single User Posts template.
 *
 * @package ClusterPress\templates\user\single
 * @subpackage posts
 *
 * @since 1.0.0
 */
?>

<h2><?php cp_user_posts_the_loop_title(); ?></h2>

<?php cp_get_template_part( 'assets/filters' ) ; ?>

<div class="view-user-posts">

	<?php cp_get_template_part( 'site/loops/posts' ) ; ?>

</div>
