<?php
/**
 * ClusterPress Site Followed loop template.
 *
 * @package ClusterPress\templates\site\loops
 * @subpackage followed-sites
 *
 * @since 1.0.0
 */

if ( cp_sites_followed_has_posts() ) : ?>

	<div class="cp-pagination top">

		<div class="cp-total-count">

			<?php cp_sites_followed_posts_total_count(); ?>

		</div>

		<?php if ( cp_sites_followed_posts_has_pagination_links() ) : ?>

			<div class="cp-pagination-links">

				<?php cp_sites_followed_posts_pagination_links(); ?>

			</div>

		<?php endif ; ?>

	</div>

	<ul>

		<?php while ( cp_sites_followed_the_posts() ) : cp_sites_followed_the_post() ; ?>

			<li class="post">

				<p class="post-meta"><?php cp_sites_followed_the_post_meta() ;?></p>

				<?php cp_sites_followed_the_post_content(); ?>

				<?php cp_sites_followed_the_post_actions() ;?>

			</li><!-- // .post -->

		<?php endwhile ; ?>

	</ul>

	<?php if ( cp_sites_followed_posts_has_pagination_links() ) : ?>

		<div class="cp-pagination bottom">

			<div class="cp-pagination-links">

				<?php cp_sites_followed_posts_pagination_links(); ?>

			</div>

		</div>

	<?php endif ; ?>

<?php else :

	cp_sites_followed_no_posts_found();

endif;
