<?php
/**
 * ClusterPress Site Archive loop template.
 *
 * @package ClusterPress\templates\site\loops
 * @subpackage sites
 *
 * @since 1.0.0
 */

if ( cp_site_has_sites() ) : ?>

	<div class="cp-pagination top">

		<div class="cp-total-count">

			<?php cp_site_total_count(); ?>

		</div>

		<?php if ( cp_site_has_pagination_links() ) : ?>

			<div class="cp-pagination-links">

				<?php cp_site_pagination_links(); ?>

			</div>

		<?php endif ; ?>

	</div>

	<ul class="site-list">

		<?php while ( cp_site_the_sites() ) : cp_site_the_site() ; ?>

			<li class="site">

				<div class="wrap">

					<div class="site-icon">
						<a href="<?php cp_site_the_site_discover_link(); ?>"><?php cp_site_the_site_icon(); ?></a>
					</div><!-- // .site-icon -->

					<div class="site-details">

						<h3 class="site-name">
							<a href="<?php cp_site_the_site_discover_link(); ?>"><?php cp_site_the_site_name(); ?></a>
						</h3>

						<p class="last-updated"><span class="dashicons dashicons-clock"></span> <?php cp_site_the_site_last_updated() ;?></p>

						<?php if ( cp_site_has_categories() ) : ?>

							<div class="site-categories">
								<span class="dashicons dashicons-category"></span>
								<?php cp_site_the_category_list(); ?>
							</div>

						<?php endif ; ?>

					</div><!-- // .site-details -->

					<?php cp_site_the_site_actions(); ?>

				</div><!-- // .wrap -->

			</li><!-- // .site -->

		<?php endwhile ; ?>

	</ul><!-- // .site-list -->

	<?php if ( cp_site_has_pagination_links() ) : ?>

		<div class="cp-pagination bottom">

			<div class="cp-pagination-links">

				<?php cp_site_pagination_links(); ?>

			</div>

		</div>

	<?php endif ; ?>

<?php else :

	cp_site_no_sites_found();

endif;
