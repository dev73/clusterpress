<?php
/**
 * ClusterPress Site Settings loop template.
 *
 * @package ClusterPress\templates\site\loops
 * @subpackage settings
 *
 * @since 1.0.0
 */

if ( cp_site_has_settings() ) : ?>

	<ul class="site-settings">

		<?php while ( cp_site_the_settings() ) : cp_site_the_setting() ; ?>

			<li class="site-setting">

				<?php cp_site_setting_the_field(); ?>

			</li><!-- // .site-setting -->

		<?php endwhile ; ?>

	</ul><!-- // .site-settings -->

<?php else :

	cp_site_no_settings_found();

endif;
