<?php
/**
 * ClusterPress Site Posts loop template.
 *
 * @package ClusterPress\templates\site\loops
 * @subpackage posts
 *
 * @since 1.0.0
 */

if ( cp_site_has_posts() ) : ?>

	<?php if ( cp_site_posts_has_pagination() ) : ?>

		<div class="cp-pagination top">

			<div class="cp-total-count">

				<?php cp_site_posts_total_count(); ?>

			</div>

			<?php if ( cp_site_posts_has_pagination_links() ) : ?>

				<div class="cp-pagination-links">

					<?php cp_site_posts_pagination_links(); ?>

				</div>

			<?php endif ; ?>

		</div>

	<?php endif ; ?>

	<ul class="post-list">

		<?php while ( cp_site_the_posts() ) : cp_site_the_post() ; ?>

			<li class="<?php cp_site_the_post_class(); ?>">

				<h3 class="cp-post-title"><a href="<?php cp_site_the_post_permalink() ;?>"><?php cp_site_the_title() ;?></a></h3>

				<?php if ( cp_site_has_post_thumbnail() ) :

					cp_site_the_post_thumbnail();

				endif ;?>

				<div class="cp-post-excerpt"><?php cp_site_the_post_excerpt() ;?></div>

				<?php cp_site_the_post_actions() ; ?>

			</li>

		<?php endwhile ; ?>

	</ul><!-- // .post-list -->

	<?php if ( cp_site_posts_has_pagination_links() ) : ?>

		<div class="cp-pagination bottom">

			<div class="cp-pagination-links">

				<?php cp_site_posts_pagination_links(); ?>

			</div>

		</div>

	<?php endif ; ?>

<?php else :

	cp_site_no_posts_found();

endif;
