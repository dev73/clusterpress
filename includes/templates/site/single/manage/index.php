<?php
/**
 * ClusterPress Single Site Manage template.
 *
 * @package ClusterPress\templates\site\single\manage
 * @subpackage index
 *
 * @since 1.0.0
 */

switch ( cp_current_sub_action() ) :

	case cp_site_get_settings_slug() :
		cp_get_template_part( 'site/single/manage/settings' );
		break;

	case cp_site_get_members_slug() :
		cp_get_template_part( 'site/single/manage/members' );
		break;

	case cp_site_get_followers_slug() :
		cp_get_template_part( 'site/single/manage/followers' );
		break;

	default                          :
		cp_get_template_part( 'site/single/plugins' );
		break;

endswitch ;
