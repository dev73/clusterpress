<?php
/**
 * ClusterPress Single Site Manage Members template.
 *
 * @package ClusterPress\templates\site\single\manage
 * @subpackage members
 *
 * @since 1.0.0
 */

cp_site_start_members_loop() ; ?>

<div id="cp-site-members" class="users archive">

	<?php cp_get_template_part( 'user/loops/users-table' ) ; ?>

</div>

<?php cp_site_end_members_loop() ;
