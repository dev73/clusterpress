<?php
/**
 * ClusterPress Single Site Primary Nav template.
 *
 * @package ClusterPress\templates\site\single
 * @subpackage primary-nav
 *
 * @since 1.0.0
 */

if ( cp_site_has_primary_nav() ) : ?>

	<ul data-cp-nav="primary">

		<?php while ( cp_site_nav_items() ) : cp_site_nav_item(); ?>

			<li id="<?php cp_site_nav_item_id(); ?>" class="<?php cp_site_nav_classes(); ?>">
				<a href="<?php cp_site_nav_link(); ?>" title="<?php cp_site_nav_link_title(); ?>">
					<?php cp_site_nav_title(); ?>
				</a>
			</li>

		<?php endwhile; ?>

	</ul>

<?php endif ;
