<?php
/**
 * ClusterPress Single Site root template.
 *
 * @package ClusterPress\templates\site\single
 * @subpackage index
 *
 * @since 1.0.0
 */

cp_feedbacks(); ?>

<div class="cp-site-header">

	<div class="cp-site-icon">
		<?php cp_site_icon() ; ?>
	</div>

	<div class="cp-site-header-content">
		<p class="cp-site-description"><?php cp_site_description() ;?></p>
		<p class="last-updated"><span class="dashicons dashicons-clock"></span> <?php cp_site_last_updated() ;?></p>

		<?php cp_site_categories(); ?>

		<?php cp_site_the_actions(); ?>
	</div>

</div>

<div class="cp-primary-nav">

	<?php cp_get_template_part( 'site/single/primary-nav' ); ?>

</div>

<div class="cp-content">

	<?php cp_get_template_part( 'site/single/secondary-nav' ); ?>

	<?php switch ( cp_current_action() ) :

		case 'home'                      :
			cp_get_template_part( 'site/single/home' );
			break;

		case cp_site_get_members_slug()   :
			cp_get_template_part( 'site/single/members' );
			break;

		case cp_site_get_followers_slug() :
			cp_get_template_part( 'site/single/followers' );
			break;

		case cp_site_get_manage_slug()   :
			cp_get_template_part( 'site/single/manage/index' );
			break;

		default                          :
			cp_get_template_part( 'site/single/plugins' );
			break;

	endswitch ; ?>

</div>
