<?php
/**
 * ClusterPress Single Site Followers template.
 *
 * @package ClusterPress\templates\site\single
 * @subpackage followers
 *
 * @since 1.0.0
 */
?>

<h2><?php cp_site_followers_loop_title(); ?></h2>

<?php cp_site_start_followers_loop() ; ?>

<div id="cp-site-followers" class="users archive">

	<?php cp_get_template_part( 'user/loops/users' ) ; ?>

</div>

<?php cp_site_end_followers_loop() ;
