<?php
/**
 * ClusterPress Single Site Plugins template.
 *
 * @package ClusterPress\templates\site\single
 * @subpackage plugins
 *
 * @since 1.0.0
 */

cp_cluster_plugins_template(); ?>
