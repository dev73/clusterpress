<?php
/**
 * ClusterPress Single Site Home template.
 *
 * @package ClusterPress\templates\site\single
 * @subpackage home
 *
 * @since 1.0.0
 */

cp_site_start_posts_loop() ;?>

<h2><?php cp_site_posts_loop_title(); ?></h2>

<div class="view-site-posts">

	<?php cp_get_template_part( 'site/loops/posts' ) ; ?>

<div>

<?php cp_site_end_posts_loop();
