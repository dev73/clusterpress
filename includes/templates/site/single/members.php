<?php
/**
 * ClusterPress Single Site Members template.
 *
 * @package ClusterPress\templates\site\single
 * @subpackage members
 *
 * @since 1.0.0
 */
?>

<h2><?php cp_site_members_loop_title(); ?></h2>

<?php cp_site_start_members_loop() ; ?>

<div id="cp-site-members" class="users archive">

	<?php cp_get_template_part( 'user/loops/users' ) ; ?>

</div>

<?php cp_site_end_members_loop() ;
