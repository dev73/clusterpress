<?php
/**
 * ClusterPress Single Site Secondary Nav template.
 *
 * @package ClusterPress\templates\site\single
 * @subpackage secondary-nav
 *
 * @since 1.0.0
 */

if ( cp_site_has_secondary_nav() ) : ?>

	<div class="cp-secondary-nav" data-cp-nav="secondary">

		<ul>

			<?php while ( cp_site_nav_items() ) : cp_site_nav_item(); ?>

				<li id="<?php cp_site_nav_item_id(); ?>" class="<?php cp_site_nav_classes(); ?>">
					<a href="<?php cp_site_nav_link(); ?>">
						<?php cp_site_nav_title(); ?>
					</a>
				</li>

			<?php endwhile; ?>

		</ul>

	</div>

<?php endif ;
