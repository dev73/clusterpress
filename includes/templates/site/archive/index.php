<?php
/**
 * ClusterPress Site Archive template.
 *
 * @package ClusterPress\templates\site\archive
 * @subpackage index
 *
 * @since 1.0.0
 */

if ( cp_current_taxonomy() ) : ?>

	<div class="sites-category page-header">

		<h2 class="page-title">
			<?php cp_sites_category_the_title(); ?>
		</h2>

		<?php if ( cp_sites_category_has_description() ) : ?>

			<div class="taxonomy-description">

				<?php cp_sites_category_the_description() ; ?>

			</div>

		<?php endif ; ?>

	</div>

<?php endif ;?>

<?php cp_feedbacks(); ?>

<?php cp_get_template_part( 'assets/filters' ) ; ?>

<div id="cp-sites-archive" class="sites archive">

	<?php cp_get_template_part( 'site/loops/sites' ) ; ?>

</div>
