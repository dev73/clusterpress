<?php
/**
 * ClusterPress Filters template.
 *
 * @package ClusterPress\templates\assets
 * @subpackage filters
 *
 * @since 1.0.0
 */

if ( ! cp_filter_use_search_form() && ! cp_filter_has_options() && ! cp_order_has_options() ) {
	return;
} ?>

<form action="" method="post" class="cp-form" data-cp-role="filters">

	<?php if ( cp_filter_use_search_form() ) : ?>

		<div class="cp-search-area cp-form-section">
			<label for="cp-form-search" class="screen-reader-text"><?php cp_search_placehoder(); ?></label>
			<input type="search" placeholder="<?php cp_search_placehoder(); ?>" name="cp_filters[cp_search]" id="cp-form-search">

			<button type="submit">
				<span class="dashicons dashicons-search"></span>
				<span class="screen-reader-text"><?php cp_search_placehoder(); ?></span>
			</button>
		</div>

	<?php endif ; ?>

	<?php if ( cp_filter_has_options() ) : ?>

		<div class="cp-filter-area cp-form-section">
			<label for="cp-form-filter" class="screen-reader-text"><?php cp_filter_placehoder(); ?></label>
			<select name="cp_filters[filter]" id="cp-form-filter">

				<?php cp_filter_options(); ?>

			</select>

			<button type="submit">
				<span class="dashicons dashicons-filter"></span>
				<span class="screen-reader-text"><?php cp_filter_placehoder(); ?></span>
			</button>
		</div>

	<?php endif ; ?>

	<?php if ( cp_order_has_options() ) : ?>

		<div class="cp-order-area cp-form-section">
			<label for="cp-form-order" class="screen-reader-text"><?php cp_order_placehoder(); ?></label>
			<select name="cp_filters[order]" id="cp-form-order">

				<?php cp_order_options(); ?>

			</select>

			<button type="submit">
				<span class="dashicons dashicons-editor-ol"></span>
				<span class="screen-reader-text"><?php cp_order_placehoder(); ?></span>
			</button>
		</div>

	<?php endif ; ?>

</form>
