<?php
/**
 * ClusterPress Comments Widget template.
 *
 * @package ClusterPress\templates\assets\widgets
 * @subpackage comments
 *
 * @since 1.0.0
 */
?>

<div class="clusterpress-liked-comments-widget">

<?php if ( cp_site_has_comments( array( 'is_widget' => true ) ) ) : ?>

	<ul>

		<?php while ( cp_site_the_comments() ) : cp_site_the_comment() ; ?>

			<li>

				<span class="cp-icon liked"></span><?php cp_interactions_comments_liked_widget_count(); ?>
				<span class="comment-author-link"><?php cp_interactions_comments_liked_the_author_link(); ?></span>

				<a href="<?php cp_interactions_comments_liked_the_permalink() ;?>">
					<?php cp_interactions_comments_liked_the_title() ;?>
				</a>

			</li>

		<?php endwhile ; ?>

	</ul>

<?php else :

	cp_site_no_comments_found();

endif; ?>

</div>
