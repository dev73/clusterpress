<?php
/**
 * ClusterPress Posts Widget template.
 *
 * @package ClusterPress\templates\assets\widgets
 * @subpackage posts
 *
 * @since 1.0.0
 */
?>

<div class="clusterpress-liked-posts-widget">

<?php if ( cp_site_has_posts( array( 'is_widget' => true ) ) ) : ?>

	<ul>

		<?php while ( cp_site_the_posts() ) : cp_site_the_post() ; ?>

			<li>

				<span class="cp-icon liked"></span><?php cp_interactions_posts_liked_widget_count(); ?>
				<a href="<?php cp_site_the_post_permalink() ;?>"><?php cp_site_the_title() ;?></a>

			</li>

		<?php endwhile ; ?>

	</ul>

<?php else :

	cp_site_no_posts_found();

endif; ?>

</div>
