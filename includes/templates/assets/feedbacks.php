<?php
/**
 * ClusterPress Feedbacks template.
 *
 * @package ClusterPress\templates\assets
 * @subpackage feedbacks
 *
 * @since 1.0.0
 */

if ( ! cp_has_feedback_message() ) return; ?>

<div class="cp-feedback" id="<?php cp_feedback_css_id(); ?>">

	<div class="dashicons dashicons-<?php cp_feedback_type(); ?>"></div>

	<div class="cp-feedback-message">
		<?php cp_feedback_message(); ?>
	</div>

</div>
