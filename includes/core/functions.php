<?php
/**
 * ClusterPress Functions.
 *
 * @package ClusterPress\core
 * @subpackage functions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Get the ClusterPress plugin basename.
 *
 * @since  1.0.0
 *
 * @return string The ClusterPress plugin basename.
 */
function cp_get_plugin_basename() {
	return clusterpress()->basename;
}

/**
 * Get the ClusterPress plugin version.
 *
 * @since  1.0.0
 *
 * @return string The ClusterPress plugin version.
 */
function cp_get_plugin_version() {
	return clusterpress()->version;
}

/**
 * Get the ClusterPress plugin db version.
 *
 * @since  1.0.0
 *
 * @return string The ClusterPress plugin db version.
 */
function cp_get_db_version() {
	return clusterpress()->db_version;
}

/**
 * Get the ClusterPress plugin dir.
 *
 * @since  1.0.0
 *
 * @return string The ClusterPress plugin dir.
 */
function cp_get_plugin_dir() {
	return clusterpress()->plugin_dir;
}

/**
 * Get the ClusterPress plugin url.
 *
 * @since  1.0.0
 *
 * @return string The ClusterPress plugin url.
 */
function cp_get_plugin_url() {
	return clusterpress()->plugin_url;
}

/**
 * Get the ClusterPress Templates dir.
 *
 * @since  1.0.0
 *
 * @return string The ClusterPress Templates dir.
 */
function cp_get_templates_dir() {
	return clusterpress()->templates;
}

/**
 * Get the ClusterPress includes dir.
 *
 * @since  1.0.0
 *
 * @return string The ClusterPress includes dir.
 */
function cp_get_includes_dir() {
	return clusterpress()->includes_dir;
}

/**
 * Get the ClusterPress includes url.
 *
 * @since  1.0.0
 *
 * @return string The ClusterPress includes url.
 */
function cp_get_includes_url() {
	return clusterpress()->includes_url;
}

/**
 * Get the ClusterPress JS url.
 *
 * @since  1.0.0
 *
 * @return string The ClusterPress JS url.
 */
function cp_get_includes_js_url() {
	return clusterpress()->js_url;
}

/**
 * Get the ClusterPress minified suffix for JS and CSS files.
 *
 * @since  1.0.0
 *
 * @return string The minified suffix.
 */
function cp_get_minified_suffix() {
	$min     = '.min';

	if ( defined( 'SCRIPT_DEBUG' ) && true === SCRIPT_DEBUG )  {
		$min = '';
	}

	/**
	 * Filter here to edit the minified suffix.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $min The minified suffix.
	 */
	return apply_filters( 'cp_get_minified_suffix', $min );
}

/**
 * Check if a Cluster is enabled.
 *
 * @since  1.0.0
 *
 * @param  string $id The Cluster ID
 * @return bool       True if the Cluster is enabled. False otherwise.
 */
function cp_cluster_is_enabled( $id = '' ) {
	if ( empty( $id ) ) {
		return false;
	}

	return (bool) array_search( $id, clusterpress()->active_clusters );
}

/**
 * Checks if the current or given site ID is the main site of the network.
 *
 * @since  1.0.0
 *
 * @param  int  $site_id The site ID to check. Optional. Defaults to the current site ID.
 * @return bool True if the site is the main site of the network. False otherwise.
 */
function cp_is_main_site( $site_id = 0 ) {
	$return = ! is_multisite();

	if ( $return ) {
		return $return;
	}

	if ( empty( $site_id ) || ! is_numeric( $site_id ) ) {
		$site_id = get_current_blog_id();
	}

	if ( (int) get_current_network_id() === (int) $site_id ) {
		$return = true;
	}

	return $return;
}

/**
 * Delete the rewrite_rules option so that WordPress regenerates it at
 * next page load.
 *
 * @since  1.0.0
 */
function cp_delete_rewrite_rules() {
	delete_option( 'rewrite_rules' );
}

/**
 * Builds an array of specific Posted vars (filters) to use
 * as JS Localized data.
 *
 * @since  1.0.0
 *
 * @return array Loop filter vars (eg: search, order..).
 */
function cp_get_filter_var() {
	$return = array();

	if ( empty( $_POST['cp_filters'] ) ) {
		return $return;
	}

	foreach ( $_POST['cp_filters'] as $key => $filter ) {
		if ( 'search' === $key ) {
			$return[ $key ] = stripslashes( $filter );
		} elseif ( 'order' === $key ) {
			$return['order'] = 'ASC';

			if ( false !== strpos( $filter, '_DESC' ) ) {
				$return['order'] = 'DESC';
				$filter = str_replace( '_DESC', '', $filter );
			}

			$return[ 'orderby' ] = sanitize_key( $filter );
		} else {
			$return[ $key ] = sanitize_text_field( $filter );
		}
	}

	return $return;
}

/**
 * Build Localized JS Data for our Rest requests.
 *
 * @since  1.0.0
 *
 * @return array The localized data to be used in Javascripts.
 */
function cp_scripts_data() {
	$cp = clusterpress();

	$scripts_data = array(
		'root_url' => esc_url_raw( rest_url( trailingslashit( $cp->rest->namespace . '/' . $cp->rest->version ) ) ),
		'nonce'    => wp_create_nonce( 'wp_rest' ),
		'u'        => get_current_user_id(),
	);

	if ( is_clusterpress() ) {
		$url       = set_url_scheme( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
		$canonical = remove_query_arg( array( 'updated', 'error' ), $url );

		$scripts_data = array_merge( $scripts_data, array(
			'filters'  => cp_get_filter_var(),
			'strings'  => array(
				'collapseNav' => __( 'Plier', 'clusterpress' ),
				'increaseNav' => __( 'Déplier', 'clusterpress' ),
			),
			'canonical' => esc_url_raw( $canonical ),
		) );
	}

	/**
	 * Filter here to add your custom scripts data.
	 *
	 * @since  1.0.0
	 *
	 * @param  array $scripts_data The localized data to be used in Javascripts.
	 */
	return apply_filters( 'cp_scripts_data', $scripts_data );
}

/**
 * Get a ClusterPress template part
 *
 * @since  1.0.0
 *
 * @param  string  $slug         The template part slug.
 * @param  string  $name         The template part name.
 * @param  bool    $load         Whether to load or return the found template part.
 * @param  bool    $require_once Whether to allow the found template to be loaded more than once.
 * @return string                Path to the template part or HTML Output.
 */
function cp_get_template_part( $slug, $name = null, $load = true, $require_once = false ) {
	$parts = CP_Template_Loader::instance();

	return $parts->get_template_part( $slug, $name, $load, $require_once );
}

/**
 * Buffer a ClusterPress template part.
 *
 * @since  1.0.0
 *
 * @param  string  $slug  The template part slug.
 * @param  string  $name  The template part name.
 * @param  bool    $echo  Whether to echo or return the buffered template part.
 * @return string         The template part buffer.
 */
function cp_buffer_template_part( $slug, $name = null, $echo = true ) {
	ob_start();

	cp_get_template_part( $slug, $name );

	// Get the output buffer contents
	$output = ob_get_clean();

	// Echo or return the output buffer contents
	if ( true === $echo ) {
		echo $output;
	} else {
		return $output;
	}
}

/**
 * Instead of returning a path, this function returns the URL to an asset (eg: css or JS).
 *
 * @since  1.0.0
 *
 * @param  string $name        The file name of the asset.
 * @param  string $type        The file extension of the asset.
 * @param  array  $plugin_data Specific dir and urls of the plugin using this function.
 * @return string              The located template asset.
 */
function cp_get_template_asset( $name = 'style', $type = 'css', $plugin_data = array() ) {
	$asset = CP_Template_Loader::instance();

	return $asset->get_template_asset( $name, $type, $plugin_data );
}

/**
 * Look for a specific clusterpress.css file that is loaded after the ClusterPress plugin's CSS.
 * This can be interesting if your theme is just needing some few adjustments.
 *
 * @since  1.0.0
 *
 * @return string the located name_of_the_active_theme/css/clusterpress.css
 */
function cp_get_theme_asset() {
	$asset = CP_Template_Loader::instance();
	return $asset->get_theme_asset();
}

/**
 * Check if the WP Query can be altered for ClusterPress needs.
 *
 * @since  1.0.0
 *
 * @param  WP_Query|null $wq The WordPress WP Query object.
 * @return bool              True if the WP Query can be altered for ClusterPress needs.
 *                           False otherwise.
 */
function cp_can_alter_posts_query( WP_Query $wq = null ) {
	$return = true;

	if ( ! $wq->is_main_query() || true === $wq->get( 'suppress_filters' ) ) {
		$return = false;
	}

	if ( $return && is_admin() ) {
		$current_screen = get_current_screen();
		$return         = empty( $current_screen->id );
	}

	return $return;
}

/**
 * Reset the WP_Query->is_page property as soon as the WordPress main loop starts.
 * This completely prevents the comment form or part of it to be output.
 *
 * @since  1.0.0
 *
 * @param  WP_Query $wq The WordPress WP Query object.
 */
function cp_loop_start( WP_Query $wq ) {
	if ( ! cp_can_alter_posts_query( $wq ) || ! is_clusterpress() ) {
		return;
	}

	$wq->is_page = false;
}
add_action( 'loop_start', 'cp_loop_start', 10, 1 );

/**
 * Conditional tag checking current page is belonging to a ClusterPress area.
 *
 * @since  1.0.0
 *
 * @return bool True if it's a ClusterPress page. False otherwise.
 */
function is_clusterpress() {
	return clusterpress()->is_cluster;
}

/**
 * Return the slug of the displayed archive.
 *
 * @since  1.0.0
 *
 * @return null|string Null if no archives page are displayed, the archive slug otherwise.
 */
function cp_current_archive() {
	$cp     = clusterpress();
	$return = null;

	if ( isset( $cp->cluster->archive ) ) {
		$return = strtolower( $cp->cluster->archive );
	}

	return $return;
}

/**
 * Return the displayed user object.
 *
 * @since  1.0.0
 *
 * @return null|WP_User Null if no users are displayed, the user object otherwise.
 */
function cp_displayed_user() {
	$user = null;
	$cp   = clusterpress();

	if ( ! empty( $cp->cluster->displayed_object->ID ) && is_a( $cp->cluster->displayed_object, 'WP_User' ) ) {
		$user = $cp->cluster->displayed_object;
	}

	return $user;
}

/**
 * Return the displayed user id.
 *
 * @since  1.0.0
 *
 * @return false|int The User ID of the displayed user. False if no users are displayed.
 */
function cp_get_displayed_user_id() {
	$user = cp_displayed_user();

	if ( ! empty( $user->ID ) ) {
		return (int) $user->ID;
	}

	return false;
}

/**
 * Return the displayed site object.
 *
 * @since  1.0.0
 *
 * @return null|WP_Site Null if no sites are displayed, the site object otherwise.
 */
function cp_displayed_site() {
	$site = null;
	$cp   = clusterpress();

	if ( ! empty( $cp->cluster->displayed_object->blog_id ) && is_a( $cp->cluster->displayed_object, 'WP_Site' ) ) {
		$site = $cp->cluster->displayed_object;
	}

	return $site;
}

/**
 * Return the displayed site id.
 *
 * @since  1.0.0
 *
 * @return false|int The Site ID of the displayed site. False if no sites are displayed.
 */
function cp_get_displayed_site_id() {
	$site = cp_displayed_site();

	if ( ! empty( $site->blog_id ) ) {
		return (int) $site->blog_id;
	}

	return false;
}

/**
 * Return the current requested action.
 *
 * @since  1.0.0
 *
 * @return null|string Null if no ClusterPress actions were requested.
 *                     The slug of the action otherwise.
 */
function cp_current_action() {
	$cp     = clusterpress();
	$return = null;

	if ( isset( $cp->cluster->action ) ) {
		$return = $cp->cluster->action;
	}

	return $return;
}

/**
 * Return the current requested sub action.
 *
 * @since  1.0.0
 *
 * @return null|string Null if no ClusterPress sub actions were requested.
 *                     The slug of the sub action otherwise.
 */
function cp_current_sub_action() {
	$cp     = clusterpress();
	$return = null;

	if ( isset( $cp->cluster->sub_action ) ) {
		$return = $cp->cluster->sub_action;
	}

	return $return;
}

/**
 * Return the displayed taxonomy object.
 *
 * NB: Only used for site categories.
 *
 * @since  1.0.0
 *
 * @return null|object Null if no taxonomIes are displayed, the taxonomy object otherwise.
 */
function cp_current_taxonomy() {
	$taxonomy = null;
	$cp       = clusterpress();

	if ( ! empty( $cp->cluster->current_taxonomy ) && is_object( $cp->cluster->current_taxonomy ) ) {
		$taxonomy = $cp->cluster->current_taxonomy;
	}

	return $taxonomy;
}

/**
 * Return the displayed term id.
 *
 * NB: Only used for site categories.
 *
 * @since  1.0.0
 *
 * @return false|int The Term ID of the displayed taxonomy. False if no taxonomies are displayed.
 */
function cp_get_current_term_id() {
	$term_id  = 0;
	$taxonomy = cp_current_taxonomy();

	if ( ! empty( $taxonomy->term_id ) ) {
		$term_id = (int) $taxonomy->term_id;
	}

	return $term_id;
}

/**
 * Return the ID of the Cluster the current displayed page belongs to.
 *
 * @since  1.0.0
 *
 * @return string The ID of the Cluster (eg: user, site).
 */
function cp_current_cluster() {
	$cp              = clusterpress();
	$active_clusters = $cp->active_clusters;

	if ( ! is_clusterpress() ) {
		return;
	}

	if ( ! is_null( $cp->cluster->current ) ) {
		return $cp->cluster->current;
	}

	foreach ( $active_clusters as $cluster ) {

		// Archive
		if ( isset( $cp->{$cluster}->archive_slug ) && $cp->{$cluster}->archive_slug === get_query_var( cp_get_root_rewrite_id() ) )  {
			$cp->cluster->current = $cluster;
			break;
		}

		// Taxonomy
		if ( isset( $cp->{$cluster}->archive_slug ) && $cluster === 'site' && cp_current_taxonomy() && cp_current_taxonomy()->taxonomy === cp_sites_get_taxonomy() )  {
			$cp->cluster->current = $cluster;
			break;
		}

		// Single
		if ( ! empty( $cp->{$cluster}->has_single ) && isset( $cp->{$cluster}->rewrite_id ) && get_query_var( $cp->{$cluster}->rewrite_id ) )  {
			$cp->cluster->current = $cluster;
			break;
		}
	}

	return $cp->cluster->current;
}

/**
 * Conditional tag checking current page matches the given archive slug.
 *
 * @since  1.0.0
 *
 * @param  string $archive Slug of the archive page to check.
 * @return bool            True if current page matches the given archive slug.
 *                         False otherwise.
 */
function cp_is_current_archive( $archive = '' ) {
	return $archive === cp_current_archive();
}

/**
 * Conditional tag checking current page matches the given action slug.
 *
 * @since  1.0.0
 *
 * @param  string $action  Slug of the action to check.
 * @return bool            True if current page matches the given action slug.
 *                         False otherwise.
 */
function cp_is_current_action( $action = '' ) {
	return $action === cp_current_action();
}

/**
 * Conditional tag checking current page matches the given sub action slug.
 *
 * @since  1.0.0
 *
 * @param  string $sub_action  Slug of the sub action to check.
 * @return bool                True if current page matches the given sub action slug.
 *                             False otherwise.
 */
function cp_is_current_sub_action( $sub_action = '' ) {
	return $sub_action === cp_current_sub_action();
}

/**
 * Conditional tag checking the users archive page is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if it's the users archive page. False otherwise.
 */
function cp_is_users() {
	return is_clusterpress() && cp_is_current_archive( cp_users_get_slug() );
}

/**
 * Conditional tag checking a user is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if a user is displayed. False otherwise.
 */
function cp_is_user() {
	return is_clusterpress() && cp_displayed_user();
}

/**
 * Check if the displayed user is also the loggedin user.
 * In other words if the user is viewing his own profile.
 *
 * @since  1.0.0
 *
 * @param  int $user_id The User ID. Optional. Defaults to the displayed user.
 * @return bool         True if the user is viewing his own profile. False otherwise.
 */
function cp_user_is_self_profile( $user_id = 0 ) {
	if ( empty( $user_id ) ) {
		$user_id = cp_get_displayed_user_id();
	}

	return is_user_logged_in() && (int) get_current_user_id() === (int) $user_id;
}

/**
 * Check the current displayed page belongs to the user Cluster.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page belongs to the user Cluster.
 *              False otherwise.
 */
function cp_is_user_cluster() {
	return cp_is_user() || cp_is_users();
}

/**
 * Check the current displayed page is the user's posts page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the user's posts page.
 *              False otherwise.
 */
function cp_is_user_posts() {
	return cp_is_user() && cp_is_current_action( cp_user_get_posts_slug() );
}

/**
 * Check the current displayed page is the user's comments page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the user's comments page.
 *              False otherwise.
 */
function cp_is_user_comments() {
	return cp_is_user() && cp_is_current_action( cp_user_get_comments_slug() );
}

/**
 * Check the current displayed page is the user's likes page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the user's likes page.
 *              False otherwise.
 */
function cp_is_user_likes() {
	return cp_is_user() && cp_is_current_action( cp_user_get_likes_slug() );
}

/**
 * Check the current displayed page is the user's sites page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the user's sites page.
 *              False otherwise.
 */
function cp_is_user_sites() {
	return cp_is_user() && cp_is_current_action( cp_sites_get_user_slug() );
}

/**
 * Check the current displayed page is the user's followed sites page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the user's followed sites page.
 *              False otherwise.
 */
function cp_is_user_followed_sites() {
	return cp_is_user() && cp_is_current_action( cp_site_get_user_following_slug() );
}

/**
 * Check the current displayed page is belonging to the user's manage pages.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is belonging to the user's manage pages.
 *              False otherwise.
 */
function cp_is_user_manage() {
	return cp_is_current_action( cp_user_get_manage_slug() ) && cp_is_user();
}

/**
 * Check the current displayed page is the user's manage profile page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the user's manage profile page.
 *              False otherwise.
 */
function cp_is_user_manage_profile() {
	return cp_is_user_manage() && cp_is_current_sub_action( cp_user_get_manage_profile_slug() );
}

/**
 * Check the current displayed page is the user's manage account page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the user's manage account page.
 *              False otherwise.
 */
function cp_is_user_manage_account() {
	return cp_is_user_manage() && cp_is_current_sub_action( cp_user_get_manage_account_slug() );
}

/**
 * Check the current displayed page is the user's manage followed sites page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the user's manage followed sites page.
 *              False otherwise.
 */
function cp_is_user_manage_followed_sites() {
	return cp_is_user_manage() && cp_is_current_sub_action( cp_site_get_user_manage_followed_slug() );
}

/**
 * Conditional tag checking the sites archive page is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if it's the sites archive page. False otherwise.
 */
function cp_is_sites() {
	return is_clusterpress() && cp_is_current_archive( cp_sites_get_slug() );
}

/**
 * Conditional tag checking a site is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if a site is displayed. False otherwise.
 */
function cp_is_site() {
	return is_clusterpress() && cp_displayed_site();
}

/**
 * Check the current displayed page is the site's discovery home page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the site's discovery home page.
 *              False otherwise.
 */
function cp_is_site_home() {
	return cp_is_site() && 'home' === cp_current_action() && ! cp_current_sub_action();
}

/**
 * Check the current displayed page is the site's discovery members page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the site's discovery members page.
 *              False otherwise.
 */
function cp_is_site_members() {
	return cp_is_site() && cp_is_current_action( cp_site_get_members_slug() ) && ! cp_current_sub_action();
}

/**
 * Check the current displayed page is the site's discovery followers page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the site's discovery followers page.
 *              False otherwise.
 */
function cp_is_site_followers() {
	return cp_is_site() && cp_is_current_action( cp_site_get_followers_slug() ) && ! cp_current_sub_action();
}

/**
 * Check the current displayed page belongs to a site's discovery manage pages.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page belongs to a site's discovery manage pages.
 *              False otherwise.
 */
function cp_is_site_manage() {
	return cp_is_current_action( cp_site_get_manage_slug() ) && cp_is_site();
}

/**
 * Check the current displayed page is the site's manage settings page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the site's manage settings page.
 *              False otherwise.
 */
function cp_is_site_manage_settings() {
	return cp_is_site_manage() && cp_is_current_sub_action( cp_site_get_settings_slug() );
}

/**
 * Check the current displayed page is the site's manage members page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the site's manage members page.
 *              False otherwise.
 */
function cp_is_site_manage_members() {
	return cp_is_site_manage() && cp_is_current_sub_action( cp_site_get_members_slug() );
}

/**
 * Check the current displayed page is the site's manage followers page.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is the site's manage followers page.
 *              False otherwise.
 */
function cp_is_site_manage_followers() {
	return cp_is_site_manage() && cp_is_current_sub_action( cp_site_get_followers_slug() );
}

/**
 * Check the current displayed page is listing site's having a specific category.
 *
 * @since  1.0.0
 *
 * @return bool True if the current displayed page is listing site's having a specific category.
 *              False otherwise.
 */
function cp_is_sites_category() {
	return cp_is_sites() && ! is_null( cp_current_taxonomy() );
}

/**
 * Get the archive link for a given Cluster ID.
 *
 * @since  1.0.0
 *
 * @param  string $cluster The Cluster ID.
 * @return string          The archive link of the Cluster.
 *                         (Eg: Users directory or Sites directory).
 */
function cp_get_archive_link( $cluster = 'user' ) {
	global $wp_rewrite;
	$cp = clusterpress();

	if ( empty( $cp->{$cluster}->archive_slug ) ) {
		return false;
	}

	// Set rewrite if not yet available
	if ( is_null( $wp_rewrite ) ) {
		$rewrite = new WP_Rewrite();
	} else {
		$rewrite = $wp_rewrite;
	}

	// Pretty permalinks
	if ( $rewrite->using_permalinks() ) {
		$url = $rewrite->root . trailingslashit( cp_get_root_slug() ) . $cp->{$cluster}->archive_slug;
		$url = home_url( trailingslashit( $url ) );

	// Unpretty permalinks
	} else {
		$args = array( cp_get_root_rewrite_id() => $cp->{$cluster}->archive_slug );
		$url = esc_url_raw( add_query_arg( $args, home_url( '/' ) ) );
	}

	return $url;
}

/**
 * Reset the document title meta tag to include ClusterPress specific data.
 *
 * @since  1.0.0
 *
 * @param  array       $title_parts The WordPress generated Title parts.
 * @param  array       $page_title  The Cluster API generated Title parts.
 * @return false|array              The title parts to use into the document title meta tag.
 */
function cp_reset_document_title( $title_parts = array(), $page_title = array() ) {
	if ( empty( $page_title ) ) {
		return false;
	}

	$title_index = array_search( 'title', array_keys( $title_parts ) );
	if ( false === $title_index ) {
		return false;
	}

	array_splice( $title_parts, $title_index, 1, $page_title );
	$new_title_parts = array();

	foreach ( $title_parts as $k => $v ) {
		if ( $k === $title_index ) {
			$new_title_parts['title'] = $v;
		} else {
			$new_title_parts[ $k ] = $v;
		}
	}

	return $new_title_parts;
}

/**
 * Redirect the user with a feedback code after a submitted action.
 *
 * @since  1.0.0
 *
 * @param  string $code The feedback code.
 * @param  string $type The feedback type (error or updated).
 * @param  string $url  The url to redirect the user to. Optional.
 *                      Defaults to the referer.
 */
function cp_feedback_redirect( $code, $type = 'error', $url = '' ) {
	if ( empty( $url ) ) {
		$url = wp_get_referer();
	}

	$url = remove_query_arg( array( 'updated', 'error' ), $url );

	$url = add_query_arg( $type, cp_current_cluster() . '[' . $code . ']', $url );
	wp_safe_redirect( $url );
	exit;
}

/**
 * Register a new section to the given Cluster ID.
 *
 * NB: This function makes it easy to use the CP_Cluster_Section API,
 * a bit like register_post_type() does it for Post Types.
 *
 * @since  1.0.0
 *
 * @param  string $id   The Cluster ID (Eg: user or site).
 * @param  array  $args The Cluster Section args {
 *         @see CP_Cluster_Section::__construct() for a detailled description
 *         of available arguments.
 * }}
 */
function cp_register_cluster_section( $id = '', $args = array() ) {
	if ( ! doing_action( 'cp_register_cluster_sections' ) ) {
		return false;
	}

	$cp = clusterpress();

	if ( empty( $id ) || ! isset( $cp->{$id}->sections ) || empty( $args['id'] ) ) {
		return false;
	}

	if ( empty( $args['cluster_id'] ) ) {
		$args['cluster_id'] = $id;
	}

	$section = new CP_Cluster_Section( $args );

	if ( is_wp_error( $section->error ) ) {
		// weirdly it does nothing ??
		_doing_it_wrong( __FUNCTION__, $section->error->get_error_message(), '1.0.0' );
	} else {
		$cp->{$id}->sections[ $args['id'] ] = $section;
	}
}

/**
 * Get the WP Nav Items for ClusterPress available archive pages.
 *
 * @since  1.0.0
 *
 * @return array A list of WP Nav Items object.
 */
function cp_get_archive_wp_nav_items() {
	$cp              = clusterpress();
	$active_clusters = $cp->active_clusters;

	if ( empty( $active_clusters ) ) {
		return array();
	}

	$archive_nav_items = array();

	foreach ( $active_clusters as $cluster ) {
		if ( empty( $cp->{$cluster}->archive_slug ) || ( 'site' === $cluster && ! is_multisite() ) ) {
			continue;
		}

		$archive_nav_items[] = (object) array(
			'object' => $cluster,
			'title'  => $cp->{$cluster}->archive_title,
			'url'    => cp_get_archive_link( $cluster ),
		);
	}

	return $archive_nav_items;
}

/**
 * Ajax Callback used by the Batch Task UI.
 *
 * NB: This is mainly used to perform Post Install/Upgrade complex tasks inside the
 * ClusterPress Administration. But it can also be used to perform othe front-end actions
 * such as sending batch emails.
 *
 * @since  1.0.0
 *
 * @return string a JSON Object.
 */
function cp_ajax_process_batch() {
	$error = array(
		'message'   => __( 'La tâche courante n\'a pu s\'accomplir en raison d\'une erreur.', 'clusterpress' ),
		'type'      => 'error'
	);

	if ( empty( $_POST['id'] ) || ! isset( $_POST['count'] ) || ! isset( $_POST['done'] ) ) {
		wp_send_json_error( $error );
	}

	// Add the action to the error
	$callback          = sanitize_key( $_POST['id'] );
	$error['callback'] = $callback;

	// Check nonce
	if ( empty( $_POST['_clusterpress_batch_nonce'] ) || ! wp_verify_nonce( $_POST['_clusterpress_batch_nonce'], 'clusterpress-batch' ) ) {
		wp_send_json_error( $error );
	}

	// Check capability
	if ( ! current_user_can( 'manage_options' ) || ! is_callable( $callback ) ) {
		wp_send_json_error( $error );
	}

	$number = 20;
	if ( ! empty( $_POST['number'] ) ) {
		$number = (int) $_POST['number'];
	}

	$did = call_user_func_array( $callback, array( $number ) );

	wp_send_json_success( array( 'done' => $did, 'callback' => $callback ) );
}
add_action( 'wp_ajax_cp_process_batch', 'cp_ajax_process_batch' );

/**
 * Get the list of built-in Clusters (Core/User/Site & interactions).
 *
 * @since  1.0.0
 *
 * @return array The list of built-in Clusters.
 */
function cp_get_built_in_clusters() {
	$img_dir_url = cp_get_includes_url()  . 'admin/img/';

	$cluster_template = array(
		'name'           => '',
		'slug'           => dirname( cp_get_plugin_basename() ),
		'version'        => cp_get_plugin_version(),
		'author'         => '<a href="https://imathi.eu">imath</a>',
		'author_profile' => 'https://profiles.wordpress.org/imath',
		'requires'       => '4.7',
		'tested'         => '4.7',
		'short_description' => '',
		'icons' => array(
			'svg' => $img_dir_url . 'core.svg',
		),
		'_built_in' => true,
	);

	return array(
		'core' => array_merge( $cluster_template, array(
			'name'              => __( 'Coeur', 'clusterpress' ),
			'short_description' => __( 'C\'est le moteur de tous les autres Clusters, il contient les API et fonctions nécessaires à la bonne culture de vos vignes et une fermention optimisée de vos raisins. Ce qui vous garantira la production de grands crus!', 'clusterpress' ),
		) ),
		'user' => array_merge( $cluster_template, array(
			'name'              => __( 'Utilisateurs', 'clusterpress' ),
			'short_description' => __( 'Vos vignerons, oenologues ou amateurs de bonnes bouteilles sont dorlotés par ce Cluster. Une étiquette les présente dans l\'annuaire et leur vestiaire personnalisé leur permettra d\'effectuer des pressurages et des dégustations en toute convivialité.', 'clusterpress' ),
			'icons'             => array(
				'svg' => $img_dir_url . 'users.svg',
			)
		) ),
		'interactions' => array_merge( $cluster_template, array(
			'name'              => __( 'Intéractions', 'clusterpress' ),
			'short_description' => __( 'Ce Cluster pimente et épice vos cépages. En complément des mentions qui peuvent être utilisées pour annoncer la période des vendanges, les &quot;likes&quot; permettent aux amoureux d\'apprécier les millésimes d\'exception.', 'clusterpress' ),
			'icons'             => array(
				'svg' => $img_dir_url . 'interactions.svg',
			),
			'_built_in'         => 'interactions',
		) ),
		'site' => array_merge( $cluster_template, array(
			'name'              => __( 'Site', 'clusterpress' ),
			'short_description' => __( 'Qu\'ils soient de pays, de table, d\'AOC ou de toute autre catégorie, vos vins sont toujours soigneusement classés au sein d\'un annuaire. Les portes de leur cave personnalisée seront toujours ouvertes à leurs adeptes.', 'clusterpress' ),
			'icons'             => array(
				'svg' => $img_dir_url . 'site.svg',
			),
			'_built_in'         => 'site',
		) ),
	);
}

if ( ! function_exists( 'wp_doing_ajax' ) ) :
/**
 * As WordPress 4.7 is still in beta...
 *
 * @todo Remove when 1.0.0 will be released (Just after WordPress 4.7).
 */
function wp_doing_ajax() {
	return apply_filters( 'wp_doing_ajax', defined( 'DOING_AJAX' ) && DOING_AJAX );
}
endif;

/**
 * Load Custom Language Packs, making sure they will be merged with GlotPress ones.
 *
 * We are not using The WordPress function here, as it does not pay attention to
 * corporate needs (custom language files).
 * @see https://core.trac.wordpress.org/ticket/37819
 *
 * @since  1.0.0
 *
 * @param  string $domain Your plugin's domain name.
 * @param  string $mofile Absolute path to your plugin's mofile.
 * @return bool   True if the translation is loaded, false otherwise.
 */
function cp_load_textdomain( $domain = '', $mofile = '' ) {
	global $l10n, $l10n_unloaded;
	$cp = clusterpress();

	if ( ! $domain ) {
		$domain = $cp->domain;
	}

	/**
	 * Traditional WordPress plugin locale filter
	 *
	 * @since  1.0.0
	 *
	 * @param string $value  The locale activated on the site (eg: fr_FR)
	 * @param string $domain The plugin domain
	 */
	$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

	if ( empty( $locale ) && empty( $mofile ) ) {
		$mofile = $domain . '.mo';
	} elseif ( empty( $mofile ) ) {
		$mofile = sprintf( '%1$s-%2$s.mo', $domain, $locale );
	} else {
		$location = $mofile;
	}

	/**
	 * Filter here to edit/add/remove possible locations for the mo file.
	 *
	 * @since  1.0.0
	 *
	 * @param array $value The list of possible locations for the mo file.
	 */
	$locations = apply_filters( 'cp_load_textdomain_mofile', array(
		'plugin' => $cp->lang_dir . $mofile,
		'custom' => WP_LANG_DIR . '/' . $domain . '/' . $mofile,
	) );

	if ( ! empty( $location ) ) {
		if ( isset( $locations['plugin'] ) ) {
			$locations['plugin'] = $location;
		} else {
			array_unshift( $locations, $location );
		}
	}

	// Loop in locations to merge existing
	foreach ( $locations as $type => $file ) {
		/**
		 * Filter here to edit the file of the iterated type.
		 *
		 * @since  1.0.0
		 *
		 * @param string $file Path to the mo file.
		 * @param string $type Type of location.
		 */
		$mofile = apply_filters( 'cp_load_textdomain_mofile', $file, $type );

		// The mofile is not available, carry on with next one.
		if ( ! is_readable( $mofile ) ) {
			continue;
		}

		$mo = new MO();
		if ( ! $mo->import_from_file( $mofile ) )  {
			continue;
		}

		if ( isset( $l10n[ $domain ] ) ) {
			$mo->merge_originals_with( $l10n[ $domain ] );
		}

		unset( $l10n_unloaded[ $domain ] );

		// Not using references seems to fix the issue.
		$l10n[ $domain ] = $mo;
	}

	if ( empty( $l10n[ $domain ] ) || ! is_a( $l10n[ $domain ], 'MO' ) ) {
		return load_plugin_textdomain( $domain );
	}
}
