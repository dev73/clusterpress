<?php
/**
 * ClusterPress Actions.
 *
 * @package ClusterPress\core
 * @subpackage actions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * ClusterPress alias of WordPress 'plugins_loaded'.
 *
 * @since 1.0.0
 */
function cp_loaded() {
	/**
	 * Used to set and load Clusters.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_loaded' );
}
add_action( 'plugins_loaded', 'cp_loaded', 11 );

/**
 * ClusterPress alias of WordPress 'init'.
 *
 * @since 1.0.0
 */
function cp_init() {
	/**
	 * Used to register many objects.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_init' );
}
add_action( 'init', 'cp_init', 9 );

/**
 * ClusterPress alias of WordPress 'parse_query'.
 *
 * @since 1.0.0
 */
function cp_parse_query( $query ) {
	/**
	 * Used to set ClusterPress template globals.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_parse_query', $query );
}
add_action( 'parse_query', 'cp_parse_query', 2 );

/**
 * ClusterPress alias of WordPress 'widgets_init'.
 *
 * @since 1.0.0
 */
function cp_widgets_init() {
	/**
	 * Used to register ClusterPress widgets.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_widgets_init' );
}
add_action( 'widgets_init', 'cp_widgets_init', 10 );

/**
 * ClusterPress alias of WordPress 'wp_enqueue_scripts'.
 *
 * @since 1.0.0
 */
function cp_enqueue_scripts() {
	/**
	 * Used to register & enqueue ClusterPress scripts and styles.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_enqueue_scripts' );
}
add_action( 'wp_enqueue_scripts', 'cp_enqueue_scripts', 10 );

/**
 * ClusterPress alias of WordPress 'wp_head'.
 *
 * @since 1.0.0
 */
function cp_head() {
	/**
	 * Used to add some metas into the header.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_head' );
}
add_action( 'wp_head', 'cp_head', 10 );

/**
 * ClusterPress alias of WordPress 'wp_footer'.
 *
 * @since 1.0.0
 */
function cp_footer() {
	/**
	 * Not internally used.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_footer' );
}
add_action( 'wp_footer', 'cp_footer', 10 );

/**
 * ClusterPress alias of WordPress 'wp_login'.
 *
 * @since 1.0.0
 *
 * @param  string  $user_login The user login.
 * @param  WP_User $user       The user object.
 */
function cp_login( $user_login = '', $user = null ) {
	/**
	 * Not internally used.
	 *
	 * @since  1.0.0
	 *
	 * @param  string  $user_login The user login.
	 * @param  WP_User $user       The user object.
	 */
	do_action( 'cp_login', $user_login, $user );
}
add_action( 'wp_login', 'cp_login',  10, 2 );

/**
 * ClusterPress alias of WordPress 'wp_logout'.
 *
 * @since 1.0.0
 */
function cp_logout() {
	/**
	 * Used to clear the 'clusterpress_wait' cookie.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_logout' );
}
add_action( 'wp_logout', 'cp_logout', 10 );

/**
 * ClusterPress alias of WordPress 'set_current_user'.
 *
 * @since 1.0.0
 */
function cp_setup_current_user() {
	/**
	 * Not internally used.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_setup_current_user' );
}
add_action( 'set_current_user', 'cp_setup_current_user', 10 );

/**
 * ClusterPress alias of WordPress 'set_current_user'.
 *
 * @since 1.0.0
 */
function cp_after_setup_theme() {
	/**
	 * Not internally used.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_after_setup_theme' );
}
add_action( 'after_setup_theme', 'cp_after_setup_theme', 15 );

/**
 * ClusterPress alias of WordPress 'template_redirect'.
 *
 * @since 1.0.0
 */
function cp_template_redirect() {
	/**
	 * Hooked by cp_page_actions & cp_setup_page_title to respectively handle
	 * user actions and set the document title for ClusterPress areas.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_template_redirect' );
}
add_action( 'template_redirect', 'cp_template_redirect', 8 );

/**
 * ClusterPress alias of WordPress 'admin_bar_menu'.
 *
 * @since 1.0.0
 *
 * @param WP_Admin_Bar $wp_admin_bar The WordPress Admin Bar.
 */
function cp_admin_bar_menu( WP_Admin_Bar $wp_admin_bar ) {
	/**
	 * Used to build the My Account menu for the logged in user
	 *
	 * @since  1.0.0
	 *
	 * @param WP_Admin_Bar $wp_admin_bar The WordPress Admin Bar.
	 */
	do_action( 'cp_admin_bar_menu', $wp_admin_bar );
}
add_action( 'admin_bar_menu', 'cp_admin_bar_menu', 15 );

/**
 * Early hooks cp_init to trigger the ClusterPress Section registration hook.
 *
 * @since 1.0.0
 */
function cp_register_cluster_sections() {
	/**
	 * Hook here to register your custom Cluster sections
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_register_cluster_sections' );
}
add_action( 'cp_init', 'cp_register_cluster_sections', 1 );

/**
 * Hooks cp_init to trigger the ClusterPress capability hook.
 *
 * @since  1.0.0
 */
function cp_set_capabilities_map() {
	/**
	 * The CP_Cluster API uses it to store cap mappings for each Clusters.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_set_capabilities_map' );
}
add_action( 'cp_init', 'cp_set_capabilities_map', 2 );

/**
 * Hooks cp_init to trigger the ClusterPress post type registration hook.
 *
 * @since  1.0.0
 */
function cp_register_post_types() {
	/**
	 * The CP_Cluster API uses it to register post types for each Clusters.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_register_post_types' );
}
add_action( 'cp_init', 'cp_register_post_types', 3 );

/**
 * Hooks cp_init to trigger the ClusterPress taxonomy registration hook.
 *
 * @since  1.0.0
 */
function cp_register_taxonomies() {
	/**
	 * The CP_Cluster API uses it to register taxonomies for each Clusters.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_register_taxonomies' );
}
add_action( 'cp_init', 'cp_register_taxonomies', 4 );

/**
 * Hooks cp_init to trigger the ClusterPress Post Type support hook.
 *
 * @since  1.0.0
 */
function cp_add_post_types_support() {
	/**
	 * The CP_Cluster API uses it to add features to registered Post types.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_add_post_types_support' );
}
add_action( 'cp_init', 'cp_add_post_types_support', 5 );

/**
 * Hooks cp_init to trigger the ClusterPress Toolbars hook.
 *
 * @since  1.0.0
 */
function cp_set_toolbars() {
	/**
	 * The CP_Cluster API uses it to add navs to Clusters having single views.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_set_toolbars' );
}
add_action( 'cp_init', 'cp_set_toolbars', 5 );

/**
 * Hooks cp_init to trigger the ClusterPress rewrite tags hook.
 *
 * @since  1.0.0
 */
function cp_add_rewrite_tags() {
	/**
	 * The CP_Cluster API uses it to add rewrite tags for each Clusters.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_add_rewrite_tags' );
}
add_action( 'cp_init', 'cp_add_rewrite_tags', 6 );

/**
 * Hooks cp_init to trigger the ClusterPress rewrite rules hook.
 *
 * @since  1.0.0
 */
function cp_add_rewrite_rules() {
	/**
	 * The CP_Cluster API uses it to add rewrite rules for each Clusters.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_add_rewrite_rules' );
}
add_action( 'cp_init', 'cp_add_rewrite_rules', 8 );

/**
 * Hooks cp_init to trigger the ClusterPress permastructs hook.
 *
 * @since  1.0.0
 */
function cp_add_permastructs() {
	/**
	 * The CP_Cluster API uses it to add permastructs to Clusters having single views.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_add_permastructs' );
}
add_action( 'cp_init', 'cp_add_permastructs', 9 );

/**
 * Hooks cp_init to trigger the ClusterPress feedbacks hook.
 *
 * @since  1.0.0
 */
function cp_register_feedbacks() {
	/**
	 * The CP_Cluster API uses it to register each Clusters feedbacks.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_register_feedbacks' );
}
add_action( 'cp_init', 'cp_register_feedbacks', 10 );

/**
 * Hooks cp_init to trigger the ClusterPress register relationships hook.
 *
 * @since  1.0.0
 */
function cp_register_relationships() {
	/**
	 * Hook here if your Cluster needs to add User relationships.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_register_relationships' );
}
add_action( 'cp_init', 'cp_register_relationships', 11 );

/**
 * ClusterPress alias of WordPress 'rest_api_init'.
 *
 * @since 1.0.0
 */
function cp_rest_api_init() {
	/**
	 * The CP_Cluster API uses it to register Rest routes for Clusters.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_rest_api_init' );
}
add_action( 'rest_api_init', 'cp_rest_api_init', 10 );

/**
 * Early hooks cp_loaded to trigger the ClusterPress set clusters hook.
 *
 * @since 1.0.0
 */
function cp_set_clusters() {
	/**
	 * ClusterPress uses it to bootstrap Clusters.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_set_clusters' );
}
add_action( 'cp_loaded', 'cp_set_clusters', 1 );

/**
 * Hooks cp_loaded to trigger the ClusterPress load clusters hook.
 *
 * @since 1.0.0
 */
function cp_load_cluster() {
	/**
	 * The CP_Cluster API uses it to include the Clusters files and classes.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_load_cluster' );
}
add_action( 'cp_loaded', 'cp_load_cluster', 3 );

/**
 * Hooks cp_loaded to trigger the ClusterPress include clusers hook.
 *
 * @since 1.0.0
 */
function cp_include_clusters() {
	/**
	 * ClusterPress uses it to inform Clusters are fully loaded.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_include_clusters' );
}
add_action( 'cp_loaded', 'cp_include_clusters', 4 );

/**
 * Hooks cp_loaded to trigger the ClusterPress Cluster cache groups hook.
 *
 * @since 1.0.0
 */
function cp_cluster_cache_groups() {
	/**
	 * The CP_Cluster API uses it to set cache groups.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_cluster_cache_groups' );
}
add_action( 'cp_loaded', 'cp_cluster_cache_groups', 5 );

/**
 * Hooks cp_template_redirect to trigger the ClusterPress page actions hook.
 *
 * @since 1.0.0
 */
function cp_page_actions() {
	/**
	 * ClusterPress uses it to handle user actions.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_page_actions' );
}
add_action( 'cp_template_redirect', 'cp_page_actions', 4 );

/**
 * Hooks cp_template_redirect to trigger the ClusterPress page title hook.
 *
 * @since 1.0.0
 */
function cp_setup_page_title() {
	/**
	 * The CP_Cluster API uses it to set up the document title for ClusterPress areas.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_setup_page_title' );
}
add_action( 'cp_template_redirect', 'cp_setup_page_title', 10 );

// Load the admin.
if ( is_admin() ) {
	/**
	 * Launch the ClusterPress Administration.
	 *
	 * @since  1.0.0
	 */
	add_action( 'cp_loaded', 'cp_admin' );
}
