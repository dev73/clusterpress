<?php
/**
 * ClusterPress Tags.
 *
 * @package ClusterPress\core
 * @subpackage tags
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Register ClusterPress front-end scripts.
 *
 * @since 1.0.0
 */
function cp_register_scripts() {
	$version = cp_get_plugin_version();
	$url     = cp_get_includes_js_url();
	$min     = cp_get_minified_suffix();

	wp_register_script(
		'clusterpress-ajax',
		sprintf( '%1$sajax%2$s.js', $url, $min ),
		array( 'jquery' ),
		$version,
		true
	);

	wp_localize_script( 'clusterpress-ajax', 'clusterPress', cp_scripts_data() );

	// All clusters shared script
	wp_register_script(
		'clusterpress-common',
		sprintf( '%1$scommon%2$s.js', $url, $min ),
		array( 'clusterpress-ajax' ),
		$version,
		true
	);

	/**
	 * Hook here to register your custom Cluster scripts
	 *
	 * @since 1.0.0
	 *
	 * @param string $url     The url to the javascript.
	 * @param string $min     Whether to load the minified version of the script.
	 * @param string $version The CLusterPress plugin version.
	 */
	do_action( 'cp_register_scripts', $url, $min, $version );
}
add_action( 'cp_enqueue_scripts', 'cp_register_scripts', 1 );

/**
 * Enqueue ClusterPress front-end styles and scripts
 *
 * @since 1.0.0
 */
function cp_enqueue_cssjs() {
	if ( is_clusterpress() ) {
		$min = cp_get_minified_suffix();

		wp_enqueue_style( 'clusterpress', cp_get_template_asset( 'css/style' . $min, 'css' ), array( 'dashicons' ), cp_get_plugin_version() );

		$theme_asset = cp_get_theme_asset();
		if ( $theme_asset ) {
			wp_enqueue_style( 'clusterpress-theme', $theme_asset, array( 'clusterpress' ), cp_get_plugin_version() );
		}

		// Enqueue the common script
		wp_enqueue_script( 'clusterpress-common' );

		/**
		 * Hook here to enqueue your scripts and styles only in the ClusterPress areas.
		 *
		 * @since 1.0.0
		 */
		do_action( 'cp_enqueue_cssjs_clusterpress', $min );
	}

	/**
	 * Hook here to enqueue your scripts and styles everywhere in the network.
	 *
	 * @since 1.0.0
	 */
	do_action( 'cp_enqueue_cssjs' );
}
add_action( 'cp_enqueue_scripts', 'cp_enqueue_cssjs' );

/**
 * Output the Cluster's page title into the title meta tag.
 *
 * @since 1.0.0
 *
 * @param  array $title_parts The WordPress generated title parts.
 * @return array              The ClusterPress generated title parts.
 */
function cp_title_parts( $title_parts = array() ) {
	$cp      = clusterpress();
	$cluster = cp_current_cluster();

	if ( is_null( $cluster ) ) {
		return $title_parts;
	}

	if ( empty( $cp->{$cluster}->page_title ) ) {
		return $title_parts;
	}

	$reset_title_parts = cp_reset_document_title( $title_parts, $cp->{$cluster}->page_title );

	if ( ! $reset_title_parts ) {
		return $title_parts;
	}

	/**
	 * Filter here to edit the ClusterPress generated title parts.
	 *
	 * @since 1.0.0
	 *
	 * @param array $reset_title_parts The ClusterPress generated title parts.
	 * @param array $title_parts       The WordPress generated title parts.
	 */
	return apply_filters( 'cp_title_parts', $reset_title_parts, $title_parts );
}
add_filter( 'cp_document_title_parts', 'cp_title_parts', 10, 1 );

/**
 * Output the Cluster's body classes.
 *
 * @since 1.0.0
 *
 * @param  array $wp_classes     The WordPress generated body classes.
 * @param  array $custom_classes The custom body classes.
 * @return array                 The ClusterPress generated body classes.
 */
function cp_set_body_class( $wp_classes = array(), $custom_classes = array() ) {
	$classes = array( 'clusterpress' );

	$cluster = cp_current_cluster();
	if ( $cluster ) {
		$classes[] = sprintf( 'cp-%s', $cluster );
	}

	if ( cp_user_is_self_profile() ) {
		$classes[] = 'cp-self-profile';
	}

	// Force Twentyseventeen to display the one column style
	$is_two_col = array_search( 'page-two-column', $wp_classes );

	if ( false !== $is_two_col ) {
		$wp_classes[ $is_two_col ] = 'page-one-column';
	}

	$classes = array_merge( $wp_classes, $classes );

	/**
	 * Filter here to edit the ClusterPress generated body classes.
	 *
	 * @since 1.0.0
	 *
	 * @param array $classes        The ClusterPress generated body classes.
	 * @param array $wp_classes     The WordPress generated body classes.
	 * @param array $custom_classes The custom body classes.
	 */
	return apply_filters( 'cp_set_body_class', $classes, $wp_classes, $custom_classes );
}
add_filter( 'cp_body_class', 'cp_set_body_class', 10, 2 );

/**
 * Output the Cluster's post classes.
 *
 * @since 1.0.0
 *
 * @param  array $wp_classes     The WordPress generated post classes.
 * @return array                 The ClusterPress generated post classes.
 */
function cp_set_post_class( $post_classes = array() ) {
	$cp_classes = array_filter( array(
		'type-page',
		cp_current_action(),
		cp_current_sub_action(),
	) );

	$classes = array_merge( $post_classes, $cp_classes );

	/**
	 * Filter here to edit the ClusterPress generated body classes.
	 *
	 * @since 1.0.0
	 *
	 * @param array $classes      The ClusterPress generated post classes.
	 * @param array $post_classes The WordPress generated post classes.
	 */
	return apply_filters( 'cp_set_body_class', $classes, $post_classes );
}
add_filter( 'cp_post_class', 'cp_set_post_class', 10, 1 );

/**
 * Output a user feedback if needed.
 *
 * @since 1.0.0
 *
 * @return null|string Nothing or the feedback HTML output.
 */
function cp_feedbacks() {
	$current_url = parse_url( $_SERVER['REQUEST_URI'] );

	if ( empty( $current_url['query'] ) ) {
		return;
	}

	$vars = wp_parse_args( $current_url['query'] );

	if ( ! isset( $vars['error'] ) && ! isset( $vars['updated'] ) ) {
		return;
	}

	/**
	 * When the site is not using pretty links, we need to only keep
	 * the error or updated vars before resetting the vars.
	 */
	$vars = array_intersect_key( $vars, array(
		'error'   => true,
		'updated' => true,
	) );

	clusterpress()->feedbacks->set_feedback( reset( $vars ), key( $vars ) );
	cp_get_template_part( 'assets/feedbacks' );
}

/**
 * Get the feedback data from the list each Clusters set.
 *
 * @since 1.0.0
 *
 * @param  string $key The Feedback key.
 * @return array       The feedback data.
 */
function cp_get_feedback_data( $key = '' ) {
	if ( empty( $key ) ) {
		return false;
	}

	$active_feedback = clusterpress()->feedbacks->get_feedback();

	if ( empty( $active_feedback[ $key ] ) ) {
		return false;
	}

	/**
	 * Another chance to edit the feedback data.
	 *
	 * @since 1.0.0
	 *
	 * @param array  $active_feedback The feedback data.
	 * @param string $key             The Feedback key.
	 */
	return apply_filters( 'cp_get_feedback_data', $active_feedback[ $key ], $key );
}

/**
 * Output the feedback type (error, info or updated) using a dashicon.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_feedback_type() {
	echo esc_attr( cp_get_feedback_type() );
}

	/**
	 * Return the dashicon that stands for the feedback type (error, info or updated).
	 *
	 * @since 1.0.0
	 *
	 * @return string The dashicon ID.
	 */
	function cp_get_feedback_type() {
		$type     = cp_get_feedback_data( 'type' );
		$dashicon = '';

		if ( 'error' === $type ) {
			$dashicon = 'thumbs-down';
		} elseif ( 'info' === $type ) {
			$dashicon = 'info';
		} else {
			$dashicon = 'thumbs-up';
		}

		/**
		 * Filter here to use another dashicon for the feedback.
		 *
		 * @since 1.0.0
		 *
		 * @param string $dashicon The dashicon ID.
		 */
		return apply_filters( 'cp_get_feedback_type', $dashicon );
	}

/**
 * Check wether there is a feedback message to output or not.
 *
 * @since 1.0.0
 *
 * @return bool True if there's a feedback message to display. False otherwises.
 */
function cp_has_feedback_message() {
	$has_feedback_message = cp_get_feedback_data( 'message' );
	return ! empty( $has_feedback_message );
}

/**
 * Output the feedback message
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_feedback_message() {
	echo cp_get_feedback_message();
}

	/**
	 * Return the feedback message
	 *
	 * @since 1.0.0
	 *
	 * @return string the feedback message
	 */
	function cp_get_feedback_message() {
		$message = cp_get_feedback_data( 'message' );

		/**
		 * Used to sanitize and allow edits.
		 *
		 * @since 1.0.0
		 *
		 * @param string $message The feedback message to output.
		 */
		return apply_filters( 'cp_get_feedback_message', $message );
	}

/**
 * Output the feedback css ID
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_feedback_css_id() {
	echo esc_attr( cp_get_feedback_css_id() );
}

	/**
	 * Return the feedback css ID
	 *
	 * @since 1.0.0
	 *
	 * @return string The css ID.
	 */
	function cp_get_feedback_css_id() {
		$css_id = cp_get_feedback_data( 'css_id' );

		/**
		 * Used to sanitize and allow edits.
		 *
		 * @since 1.0.0
		 *
		 * @param string $css_id The feedback css ID to output.
		 */
		return apply_filters( 'cp_get_feedback_css_id', $css_id );
	}

/**
 * Output a submit button and a nonce field.
 *
 * @since 1.0.0
 *
 * @param  string $nonce The nonce string to check.
 * @return string HTML Output.
 */
function cp_submit_button( $nonce = '' ) {
	if ( empty( $nonce ) ) {
		return;
	}

	/**
	 * Add custom action/output before the submit button.
	 *
	 * @since 1.0.0
	 *
	 * @param string $nonce The nonce string to check.
	 */
	do_action( 'cp_before_submit_button', $nonce );

	printf( '<div class="submit">
			<input class="button" type="submit" name="%1$s" value="%2$s" id="submit">
		</div>',
		 esc_attr( $nonce ) . '_submit',
		esc_attr__( 'Enregistrer', 'clusterpress' )
	);

	/**
	 * Add custom action/output after the submit button.
	 *
	 * @since 1.0.0
	 *
	 * @param string $nonce The nonce string to check.
	 */
	do_action( 'cp_after_submit_button', $nonce );

	wp_nonce_field( $nonce );
}

/**
 * Output the custom Cluster content.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_cluster_plugins_template() {
	/**
	 * Used to add the custom Cluster's content.
	 *
	 * @since 1.0.0
	 */
	do_action( 'cp_cluster_plugins_template' );
}

/**
 * Check if the search form should be used in the form filters.
 * By default it's only on when viewing a ClusterPress archive page.
 *
 * @since 1.0.0
 *
 * @return bool True to use the Search form. False otherwise.
 */
function cp_filter_use_search_form() {
	$current_archive = cp_current_archive();

	/**
	 * Filter here allow/disallow the search form in form filters
	 *
	 * @since 1.0.0
	 *
	 * @param bool $value.
	 */
	return apply_filters( 'cp_filter_use_search_form', ! empty( $current_archive ) );
}

/**
 * Output the search placeholder.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_search_placehoder() {
	echo esc_attr( cp_get_search_placehoder() );
}

	/**
	 * Return the search placeholder.
	 *
	 * @since 1.0.0
	 *
	 * @return string The search placeholder.
	 */
	function cp_get_search_placehoder() {
		/**
		 * Filter here to edit the search placeholder
		 *
		 * @since 1.0.0
		 *
		 * @param string $value The search placeholder.
		 */
		return apply_filters( 'cp_get_search_placehoder', __( 'Rechercher', 'clusterpress' ) );
	}

/**
 * Populate options for select tags.
 *
 * @since 1.0.0
 *
 * @return array An array of option tags.
 */
function cp_select_get_options( $type = 'filter', $return_type = '' ) {
	$return = array();

	$options = (array) apply_filters( "cp_{$type}_get_options", array() );

	if ( 'has_options' === $return_type ) {
		return ! empty( $options );
	}

	foreach ( $options as $option ) {
		if ( ! isset( $option['value'] ) || ! isset( $option['text'] ) ) {
			continue;
		}

		$return[] = sprintf( '<option value="%1$s">%2$s</option>',
			esc_attr( $option['value'] ),
			esc_html( $option['text'] )
		);
	}

	return $return;
}

/**
 * Output the filter placeholder.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_filter_placehoder() {
	echo esc_attr( cp_get_filter_placehoder() );
}

	/**
	 * Return the filter placeholder.
	 *
	 * @since 1.0.0
	 *
	 * @return string The filter placeholder.
	 */
	function cp_get_filter_placehoder() {
		/**
		 * Filter here to edit the filter placeholder
		 *
		 * @since 1.0.0
		 *
		 * @param string $value The filter placeholder.
		 */
		return apply_filters( 'cp_get_filter_placehoder', __( 'Filtrer selon :', 'clusterpress' ) );
	}

/**
 * Check whether a filter has options
 *
 * @since 1.0.0
 *
 * @return bool True if the filter has options. False otherwise.
 */
function cp_filter_has_options() {
	return cp_select_get_options( 'filter', 'has_options' );
}

/**
 * Output the filter options.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_filter_options() {
	echo join( "\n", cp_select_get_options( 'filter' ) );
}

/**
 * Check whether a select order has options
 *
 * @since 1.0.0
 *
 * @return bool True if the select order has options. False otherwise.
 */
function cp_order_has_options() {
	return cp_select_get_options( 'order', 'has_options' );
}

/**
 * Output the select order placeholder.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_order_placehoder() {
	echo esc_attr( cp_get_order_placehoder() );
}

	/**
	 * Return the select order placeholder.
	 *
	 * @since 1.0.0
	 *
	 * @return string The select order placeholder.
	 */
	function cp_get_order_placehoder() {
		/**
		 * Filter here to edit the select order placeholder
		 *
		 * @since 1.0.0
		 *
		 * @param string $value The select order placeholder.
		 */
		return apply_filters( 'cp_get_order_placehoder', __( 'Classer selon :', 'clusterpress' ) );
	}

/**
 * Output the select order options.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_order_options() {
	echo join( "\n", cp_select_get_options( 'order' ) );
}

/**
 * Build and return a group of action links.
 *
 * @since 1.0.0
 *
 * @param array $args {
 *     An array of arguments.
 *     @type string $class        The container class
 *     @type string $action_type  The Object type for the actions.
 *     @type array  $actions      The list of actions.
 * }
 * @return string HTML group of action links.
 */
function cp_cluster_actions_group( $args = array() ) {
	$r = wp_parse_args( $args, array(
		'class'       => '',
		'action_type' => '',
		'actions'     => array(),
	) );

	if ( empty( $r['actions'] ) ) {
		return;
	}

	$actions = array();

	foreach ( $r['actions'] as $action ) {
		$before = $after = '';

		if ( ! empty( $action['wrapper_class'] ) ) {
			$before = sprintf( '<span class="%s">', esc_attr( $action['wrapper_class'] ) );
			$after  = '</span>';
		}

		if ( ! empty( $action['dashicon'] ) ) {
			$before .= sprintf( '<span class="dashicons %s"></span>', $action['dashicon'] );
		}

		$actions[] = $before . $action['content'] . $after;
	}

	return apply_filters( 'cp_cluster_actions_group',
		sprintf( '<div class="%1$s" data-cp-action-type="%2$s">%3$s</div>',
			$r['class'],
			$r['action_type'],
			join( "\n", $actions )
	), $args );
}
