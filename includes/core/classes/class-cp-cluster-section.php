<?php
/**
 * ClusterPress Section Class.
 *
 * Sections are the areas you can add to a single User or Site.
 *
 * @package ClusterPress\core\classes
 * @subpackage cluster-section
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Cluster's Section Class
 *
 * @since 1.0.0
 */
class CP_Cluster_Section {

	/**
	 * User feedback.
	 *
	 * @var WP_Error
	 */
	public $error = false;

	/**
	 * Default args of the section.
	 *
	 * @var array
	 */
	private $default_args = array(
		'id'               => '',
		'name'             => '',
		'cluster_id'       => '',
		'slug'             => '',
		'rewrite_id'       => '',
		'template'         => '',
		'template_dir'     => '',
		'display_callback' => '',
		'toolbar_items'    => array(),
	);

	/**
	 * The Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args {
	 *     An array of arguments.
	 *     @type string $id               The ID for your section.
	 *     @type string $name             The name of your section.
	 *     @type string $cluster_id       The ID of the Cluster the section is added to (eg: user or site).
	 *     @type array  $slug             The slug for your section.
	 *     @type string $rewrite_id       The rewrite var for your section, which is also the ID of the generated nav.
	 *     @type string $template         The template to use when displaying the page(s) of your section (Optional).
	 *     @type string $template_dir     The custom template dir (Optional).
	 *     @type string $display_callback The callback function to use to display the content of your section's page(s).
	 *     @type array  $toolbar_items    The nav item(s) for your section.
	 * }
	 */
	public function __construct( $args = array() ) {
		if ( empty( $args['id'] ) || empty( $args['cluster_id'] ) || empty( $args['rewrite_id'] ) ) {
			$this->error = new WP_Error( 'missing_parameter' );
		}

		$p  = wp_parse_args( $args, $this->default_args );
		$cp = clusterpress();

		if ( ! cp_cluster_is_enabled( $p['cluster_id'] ) ) {
			$this->error = new WP_Error( 'missing_cluster', __( 'Le cluster que vous essayez d\'étendre n\'est pas actif.', 'clusterpress' ) );
		}

		if ( empty( $cp->{$p['cluster_id']}->has_single ) ) {
			$this->error = new WP_Error( 'incompatible_cluster', __( 'Le cluster que vous essayez d\'étendre ne supporte pas les sections.', 'clusterpress' ) );
		}

		if ( ! $p['slug'] ) {
			$p['slug'] = $p['id'];
		}

		if ( ! $p['name'] ) {
			$p['name'] = $p['id'];
		}

		// If the template dir is not reachable, reset the template arg.
		if ( $p['template_dir'] && ! is_dir( $p['template_dir'] ) ) {
			$p['template'] = '';
		}

		foreach ( $p as $k => $v ) {
			$this->{$k} = $v;
		}

		$this->set_hooks();
	}

	/**
	 * Hook on some ClusterPress actions to build the section.
	 *
	 * @since 1.0.0
	 */
	public function set_hooks() {
		add_action( 'cp_' . $this->cluster_id . '_rewrite_tags',  array( $this, 'rewrite_tags'  ), 10, 1 );
		add_action( 'cp_' . $this->cluster_id . '_rewrite_rules', array( $this, 'rewrite_rules' ), 10, 1 );
		add_action( 'cp_' . $this->cluster_id . '_set_toolbars',  array( $this, 'set_toolbars'  ), 10, 2 );

		// Output something if we can!
		if ( $this->template || $this->display_callback ) {
			add_action( 'cp_cluster_plugins_template', array( $this, 'section_template' ), 10, 1 );
		}
	}

	/**
	 * Takes care of adding a new rewrite tag for your section.
	 *
	 * @since 1.0.0
	 */
	public function rewrite_tags() {
		// Add the rewrite tag
		if ( empty( $this->toolbar_items['children'] ) ) {
			$regex = '([1]{1,})';
		} else {
			$regex = '([^/]+)';
		}

		add_rewrite_tag( '%' . $this->rewrite_id . '%', $regex );
	}

	/**
	 * Takes care of adding new rewrite rules for your section.
	 *
	 * @since 1.0.0
	 */
	public function rewrite_rules() {
		// It's a User section
		if ( 'user' === $this->cluster_id ) {
			$rewrite_data = array(
				'regex'              => trailingslashit( cp_get_root_slug() ) . cp_user_get_slug(),
				'cluster_rewrite_id' => cp_user_get_rewrite_id(),
			);

		// It's a Site section
		} elseif( 'site' === $this->cluster_id ) {
			$rewrite_data = array(
				'regex'              => trailingslashit( cp_get_root_slug() ) . cp_site_get_slug(),
				'cluster_rewrite_id' => cp_site_get_rewrite_id(),
			);
		} else {
			/**
			 * It's a custom cluster, use the following filter to add custom rewrite data.
			 *
			 * @since 1.0.0
			 *
			 * @param array $value {
			 *     An array of arguments.
			 *     @type string $regex              The regex for the custom Cluster.
			 *     @type string $cluster_rewrite_id The rewrite ID of the custom Cluster.
			 * }
			 * @param string $id         Your section's ID.
			 * @param string $cluster_id The Cluster ID you are adding a section to.
			 */
			$rewrite_data = apply_filters( 'cp_cluster_section_slug_regex', array(), $this->id, $this->cluster_id );
		}

		if ( empty( $rewrite_data['regex'] ) || empty( $rewrite_data['cluster_rewrite_id'] ) ) {
			return;
		}

		$page_slug  = cp_get_paged_slug();
		$page_id    = 'paged';

		// Manage subnav, no need to register new rules.
		if ( ! empty( $this->toolbar_items['parent'] ) && false !== array_search( $this->toolbar_items['parent'], array(
			cp_user_get_manage_rewrite_id(),
			cp_site_get_manage_rewrite_id(),
		) ) ) {
			return;

		// Nav items having subnavs
		} elseif ( ! empty( $this->toolbar_items['children'] ) && wp_list_filter( $this->toolbar_items['children'], array( 'parent' => $this->rewrite_id ) ) ) {
			add_rewrite_rule(
				$rewrite_data['regex'] . '/([^/]+)/' . $this->slug . '/([^/]+)/' . $page_slug . '/?([0-9]{1,})/?$',
				'index.php?' . $rewrite_data['cluster_rewrite_id'] . '=$matches[1]&' . $this->rewrite_id . '=$matches[2]&' . $page_id . '=$matches[3]',
				'top'
			);

			add_rewrite_rule(
				$rewrite_data['regex'] . '/([^/]+)/' . $this->slug . '/([^/]+)/?$',
				'index.php?' . $rewrite_data['cluster_rewrite_id'] . '=$matches[1]&' . $this->rewrite_id . '=$matches[2]',
				'top'
			);

		// Nav items having no subnavs
		} else {
			add_rewrite_rule(
				$rewrite_data['regex'] . '/([^/]+)/' . $this->slug . '/' . $page_slug . '/?([0-9]{1,})/?$',
				'index.php?' . $rewrite_data['cluster_rewrite_id'] . '=$matches[1]&' . $this->rewrite_id . '=1&' . $page_id . '=$matches[2]',
				'top'
			);

			add_rewrite_rule(
				$rewrite_data['regex'] . '/([^/]+)/' . $this->slug . '/?$',
				'index.php?' . $rewrite_data['cluster_rewrite_id'] . '=$matches[1]&' . $this->rewrite_id . '=1',
				'top'
			);
		}
	}

	/**
	 * Takes care of adding your custom nav item(s) to the Cluster's toolbar.
	 *
	 * @since 1.0.0
	 */
	public function set_toolbars( $nodes = array(), $toolbar ) {
		if ( empty( $this->toolbar_items ) ) {
			return;
		}

		$capability = sprintf( 'cp_read_single_%s', $this->cluster_id );

		if ( 0 !== key( $this->toolbar_items ) ) {
			$subitems  = array();
			$main_item = $this->toolbar_items;

			// Check for sub items.
			if ( ! empty( $this->toolbar_items['children'] ) ) {
				$subitems  = $this->toolbar_items['children'];
				$main_item = array_diff_key( $main_item, array( 'children' => true ) );
			}

			$items = array_merge( array( $main_item ), $subitems );
		} else {
			$items = $this->toolbar_items;
		}

		foreach ( $items as $item ) {
			$t = wp_parse_args( $item, array(
				'id'           => $this->rewrite_id,
				'slug'         => $this->slug,
				'parent_group' => 'single-bar',
				'title'        => $this->name,
				'href'         => '#',
				'position'     => 99,
				'capability'   => $capability,
			) );

			$toolbar->add_menu( $t );
		}
	}

	/**
	 * Temporarly add the Custom Template Dir to the ClusterPress Template Stack.
	 *
	 * @since 1.0.0
	 */
	public function section_template_stack( $stack = array() ) {
		// Make sure to put it at the last position
		$index = max( array_keys( $stack ) ) + 1;

		$stack[ $index ] = $this->template_dir;

		return $stack;
	}

	/**
	 * Display the Section's content.
	 *
	 * @since 1.0.0
	 */
	public function section_template() {
		$show = false;

		if ( ( 'user' === $this->cluster_id && cp_is_current_action( $this->slug ) && cp_is_user() ) || ( cp_is_user_manage() && cp_is_current_sub_action( $this->slug ) ) ) {
			$show = true;
		} elseif ( ( 'site' === $this->cluster_id && cp_is_current_action( $this->slug ) && cp_is_site() ) || ( cp_is_site_manage() && cp_is_current_sub_action( $this->slug ) ) ) {
			$show = true;
		}

		if ( ! $show ) {
			return;
		}

		// Check for a display callback and use it if available
		if ( $this->display_callback && is_callable( $this->display_callback ) ) {
			$args = array();

			if ( 'user' === $this->cluster_id ) {
				$args['user_id'] = cp_get_displayed_user_id();
			} elseif ( 'site' === $this->cluster_id ) {
				$args['site_id'] = cp_get_displayed_site_id();
			}

			return call_user_func_array( $this->display_callback, $args );

		// Use a template.
		} else {

			/**
			 * Temporarly filter the ClusterPress Templates
			 * stack to include the provided template dir.
			 */
			if ( $this->template_dir ) {
				add_filter( 'cp_template_stack', array( $this, 'section_template_stack' ), 10, 1 );
			}

			// Load the Template
			cp_get_template_part( $this->template );

			/**
			 * Stop filtering the ClusterPress Templates
			 * stack now the template was loaded.
			 */
			if ( $this->template_dir ) {
				remove_filter( 'cp_template_stack', array( $this, 'section_template_stack' ), 10, 1 );
			}
		}
	}
}
