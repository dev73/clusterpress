<?php
/**
 * Core Cluster.
 *
 * @package ClusterPress\core\classes
 * @subpackage core-cluster
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * The Core CLuster Class.
 *
 * @since 1.0.0
 */
class CP_Core_Cluster extends CP_Cluster {

	/**
	 * The constructor
	 *
	 * @since 1.0.0
	 *
	 * @param array args The Core Cluster arguments.
	 * {@see CP_Cluster::__construct() for a detailled list of available arguments}
	 */
	public function __construct( $args = array() )  {
		parent::__construct( $args );
	}

	/**
	 * Add the Core Cluster to ClusterPress main instance.
	 *
	 * @since 1.0.0
	 */
	public static function start() {
		$cp = clusterpress();

		if ( empty( $cp->core ) ) {
			$cp->core = new self( array(
				'id'            => 'core',
				'name'          => __( 'Coeur', 'clusterpress' ),
				'dir'           => 'core',
				'slug'          => cp_get_root_slug(),
				'rewrite_id'    => cp_get_root_rewrite_id(),
				'archive_title' => __( 'Réseau', 'clusterpress' ),
			) );
		}

		return $cp->core;
	}

	/**
	 * Set specific hooks for the core Cluster.
	 *
	 * @since 1.0.0
	 */
	public function set_cluster_hooks() {
		// Add a specific menu to ease the access to Clusterpress Admin screens
		if ( is_multisite() ) {
			// Once wp_admin_bar_my_sites_menu is rendered
			add_action( 'admin_bar_menu', array( $this, 'network_admin_bar' ), 30 );
		}
	}

	/**
	 * Analyse the WordPress Query to eventually set ClusterPress globals.
	 * Check the user can access to the required archive page if requested.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Query $q The WordPress main query.
	 */
	public function parse_query( $q = null ) {
		if ( ! cp_can_alter_posts_query( $q ) ) {
			return;
		}

		$archive = $q->get( cp_get_root_rewrite_id() );

		clusterpress()->is_cluster = (bool) $archive;

		if ( $archive ) {
			clusterpress()->cluster->archive = $archive;
			$archive_cap = sprintf( 'cp_read_%ss_archive', cp_current_cluster() );

			if ( ! current_user_can( $archive_cap ) ) {
				$q->set_404();
				return;
			}
		}

		parent::parse_query( $q );
	}

	/**
	 * Add the network rewrite tag.
	 *
	 * @since 1.0.0
	 *
	 * @param array $tags.
	 * {@see CP_Cluster::__rewrite_tags() for a detailled list of available arguments}
	 */
	public function rewrite_tags( $tags = array() ) {
		parent::rewrite_tags( array( array(
			'tag'   => '%' . $this->rewrite_id . '%',
			'regex' => '([^/]+)',
		) ) );
	}

	/**
	 * Add the network rewrite rules.
	 *
	 * @since 1.0.0
	 *
	 * @param array $rules.
	 * {@see CP_Cluster::__rewrite_rules() for a detailled list of available arguments}
	 */
	public function rewrite_rules( $rules = array() ) {
		parent::rewrite_rules( array( array(
			'regex' => $this->slug . '/([^/]+)/?$',
			'query' => 'index.php?' . $this->rewrite_id . '=$matches[1]',
		) ) );
	}

	/**
	 * Add the network permastruct.
	 *
	 * @since 1.0.0
	 *
	 * @param string $name
	 * @param string $struct
	 * @param array  $args
	 * {@see CP_Cluster::permastructs() for a detailled list of available arguments}
	 */
	public function permastructs( $name = '', $struct = '', $args = array() ) {
		parent::permastructs(
			$this->rewrite_id,
			$this->slug . '/%' . $this->rewrite_id . '%'
		);
	}

	/**
	 * Return the Core settings
	 *
	 * @since 1.0.0.
	 *
	 * @return  array The list of settings for the cluser.
	 */
	public function get_settings() {
		$settings = array(
			'sections' => array(
				'core_cluster_main_settings' => array(
					'title'    => __( 'Réglages principaux', 'clusterpress' ),
					'callback' => 'cp_core_main_settings_callback',
				),
				'core_cluster_slug_settings' => array(
					'title'    => __( 'Personnalisation des URLs', 'clusterpress' ),
					'callback' => 'cp_core_slug_settings_callback',
				),
			),
			'fields' => array(
				'core_cluster_main_settings' => array(
					'clusterpress_network_type' => array(
						'title'             => __( 'Type de réseau', 'clusterpress' ),
						'callback'          => 'cp_core_settings_network_type',
						'sanitize_callback' => 'sanitize_text_field',
					),
				),
				'core_cluster_slug_settings' => array(
					'clusterpress_root_slug' => array(
						'title'             => __( 'Racine du réseau', 'clusterpress' ),
						'callback'          => 'cp_core_settings_root_slug',
						'sanitize_callback' => 'cp_sanitize_slug',
					),
				),
			),
		);

		// Remove the Slugs settings if not using pretty URLs
		if ( ! clusterpress()->permalink_structure ) {
			unset( $settings['sections']['core_cluster_slug_settings'] );
		}

		return $settings;
	}

	/**
	 * Adds a shortcut inside the Network Admin Toolbar to access to ClusterPress admin.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Admin_Bar $wp_admin_bar The WordPress Admin Bar.
	 */
	public static function network_admin_bar( WP_Admin_Bar $wp_admin_bar ) {
		if ( ! current_user_can( 'manage_network_plugins' ) ) {
			return;
		}

		$wp_admin_bar->add_menu( array(
			'parent' => 'network-admin',
			'id'     => 'network-admin-clusterpress',
			'title'  => __( 'ClusterPress', 'clusterpress' ),
			'href'   => esc_url( add_query_arg( 'page', 'clusterpress-main', network_admin_url( 'admin.php' ) ) ),
		) );
	}
}
