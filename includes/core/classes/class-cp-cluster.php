<?php
/**
 * Abstract Class to use in order to organize Clusters.
 *
 * This class was insprired by the BP_Component one.
 * Credits https://buddypress.org
 *
 * @package ClusterPress\core\classes
 * @subpackage cluster
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Extend this class within your code to organize your custom Cluster.
 *
 * @since 1.0.0
 */
abstract class CP_Cluster {

	/**
	 * Default args of the Cluster.
	 *
	 * @var array
	 */
	private $default_args = array(
		'id'            => '',
		'name'          => '',
		'dir'           => '',
		'absdir'        => '',
		'slug'          => '',
		'rewrite_id'    => '',
		'archive_title' => '',
		'has_single'    => false,
		'page_title'    => '',
	);

	/**
	 * Construct the Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args {
	 *     An array of arguments.
	 *     @type string $id            The ID of the Cluster's section.
	 *     @type string $name          The name of the Cluster's the section.
	 *     @type string $dir           The slug for your section.
	 *     @type string $absdir        Full path to the Cluster's root dir files.
	 *     @type string $slug          The slug of the Cluster's section.
	 *     @type string $rewrite_id    The rewrite var for your section, which is also the ID of the generated nav.
	 *     @type string $archive title The slug for your section.
	 *     @type bool   $has_single    True if the Cluster is adding an object's profile section.
	 *     @type string $page title    The page title to use in body casses.
	 * }
	 */
	public function __construct( $args = array() ) {
		if ( empty( $args['id'] ) ) {
			return new WP_Error( 'missing_parameter' );
		}

		$p = wp_parse_args( $args, $this->default_args );

		if ( ! $p['slug'] ) {
			$p['slug'] = $p['id'];
		}

		if ( ! $p['archive_title'] ) {
			$p['archive_title'] = $p['slug'];
		}

		foreach ( $p as $k => $v ) {
			if ( 'absdir' === $k && ! empty( $v ) && ! is_dir( $v ) ) {
				continue;
			}

			$this->{$k} = $v;
		}

		if ( $this->has_single ) {
			$this->toolbar = new CP_Cluster_Toolbar;

			// Init sections to an empty array.
			$this->sections = array();
		}

		// Set Up additional globals
		$this->set_up_additional_globals();

		// Set common hooks
		$this->set_hooks();

		// Allow Clusters to set specific ones
		$this->set_cluster_hooks();
	}

	/**
	 * Allow Plugins to add additional globals
	 *
	 * @since 1.0.0
	 */
	public function set_up_additional_globals() {
		/**
		 * Dynamic action to let plugins add additional globals to Clusters.
		 *
		 * @since  1.0.0
		 *
		 * @param  object $value The Cluster Class passed by reference.
		 */
		do_action_ref_array( 'cp_' . $this->id . '_cluster_globals', array( &$this ) );
	}

	/**
	 * Hooks at various events to fully load the Cluster.
	 *
	 * @since 1.0.0
	 */
	private function set_hooks() {
		// Load the Cluster files.
		add_action( 'cp_load_cluster', array( $this, 'load_cluster' ), 1 );

		// These hooks are only fired on the main site.
		if ( cp_is_main_site() ) {
			// Cache groups
			add_action( 'cp_cluster_cache_groups', array( $this, 'set_cluster_cache_groups' ), 10    );

			// Query
			add_action( 'cp_parse_query',          array( $this, 'parse_query'              ), 10, 1 );

			// Custom Post Types
			add_action( 'cp_register_post_types',  array( $this, 'register_post_types'      )        );

			// Custom Taxonomies
			add_action( 'cp_register_taxonomies',  array( $this, 'register_taxonomies'      )        );

			// Rewrite rules
			add_action( 'cp_add_rewrite_tags',     array( $this, 'rewrite_tags'             )        );
			add_action( 'cp_add_rewrite_rules',    array( $this, 'rewrite_rules'            )        );
			add_action( 'cp_add_permastructs',     array( $this, 'permastructs'             )        );

			// Navigation
			add_action( 'cp_set_toolbars',         array( $this, 'toolbars'                 )        );
			add_action( 'cp_admin_bar_menu',       array( $this, 'admin_bar'                )        );

			// Page title
			add_action( 'cp_setup_page_title',     array( $this, 'setup_page_title'         )        );

			// Settings
			add_action( 'cp_register_settings',    array( $this, 'register_settings'        )        );
		}

		// Add post types supports
		add_action( 'cp_add_post_types_support',               array( $this, 'add_post_types_support' )        );

		// Rest routes need to be set on each sites.
		add_action( 'cp_rest_api_init',                        array( $this, 'rest_routes'            ), 10    );

		// User feedbacks
		add_action( 'cp_register_feedbacks',                   array( $this, 'register_feedbacks'     )        );

		// Caps
		add_action( 'cp_set_capabilities_map',                 array( $this, 'map_caps'               )        );

		// Upgrades
		add_action( 'cp_upgrade_clusters',                     array( $this, 'upgrade'                ), 10, 1 );

		// Installation (if needed)
		add_action( 'cp_install_clusters',                     array( $this, 'install'                )        );

		// Post Install Tasks (if needed)
		add_filter( 'cp_admin_get_cluster_post_install_tasks', array( $this, 'set_cluster_tasks'      ), 10, 1 );
	}

	/**
	 * Override this method to add your cluster's specific hooks.
	 *
	 * @since 1.0.0
	 */
	public function set_cluster_hooks() {}

	/**
	 * Sets the Includes dir for the Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @param  string $file The file to load.
	 * @return string       Path to the Cluster's includes dir.
	 */
	protected function set_dir( $file = '' ) {
		$dir = array( '', $file );

		if ( ! empty( $this->absdir ) ) {
			$dir[0] = trailingslashit( $this->absdir );

		// Check if the cluster is trying to include a file from another directory
		} elseif ( strstr( $file, '../' ) ) {
			preg_match( '/\.\.\/(.*?)\//', $file, $outdir );

			if ( empty( $outdir[0] ) || empty( $outdir[1] ) ) {
				return $dir;
			}

			$possible_dir = trailingslashit( clusterpress()->includes_dir ) . $outdir[1];

			if ( ! is_dir( $possible_dir ) ) {
				return $dir;
			}

			$dir = array( trailingslashit( $possible_dir ), str_replace( $outdir[0], '', $file ) );
		} else {
			$dir[0] = trailingslashit( clusterpress()->includes_dir . $this->id );
		}

		return $dir;
	}

	/**
	 * Prepare the files to be loaded.
	 *
	 * @since 1.0.0
	 *
	 * @param  string $file The file to load.
	 * @return string       Absolute path to the file.
	 */
	public function prepare_files( $file = '' ) {
		$validate_dir_file  = $this->set_dir( $file );
		list( $dir, $file ) = $validate_dir_file;

		if ( empty( $dir ) || empty( $file ) ) {
			return;
		}

		return $dir . $file . '.php';
	}

	/**
	 * Prepare the classes to be loaded.
	 *
	 * @since 1.0.0
	 *
	 * @param  string $class_file The class to load.
	 * @return string             Absolute path to the class.
	 */
	public function prepare_class_files( $class_file = '' ) {
		$validate_dir_class_file  = $this->set_dir( $class_file );
		list( $dir, $class_file ) = $validate_dir_class_file;

		if ( empty( $dir ) || empty( $class_file ) ) {
			return;
		}

		return $dir . 'classes/class-cp-' . $class_file . '.php';
	}

	/**
	 * Loops in each Cluster's files list to include them.
	 *
	 * @since 1.0.0
	 *
	 * @param array $files The list of files to include.
	 */
	public function load_cluster( $files = array() ) {
		$includes = array();

		if ( ! empty( $files['files'] ) ) {
			$includes = array_merge( $includes, array_map( array( $this, 'prepare_files' ), $files['files'] ) );
		}

		if ( ! empty( $files['class_files'] ) ) {
			$includes = array_merge( $includes, array_map( array( $this, 'prepare_class_files' ), $files['class_files'] ) );
		}

		foreach ( $includes as $include ) {
			if ( ! is_file( $include ) ) {
				continue;
			}

			require( $include );
		}

		/**
		 * Dynamic action to inform files for the Cluster are fully loaded.
		 *
		 * @since 1.0.0
		 */
		do_action( 'cp_' . $this->id . '_cluster_loaded' );
	}

	/**
	 * Set the cache groups.
	 *
	 * @since 1.0.0
	 */
	public function set_cluster_cache_groups() {
		/**
		 * Dynamic action to inform cache groups are set for the Cluster.
		 *
		 * @since 1.0.0
		 */
		do_action( 'cp_' . $this->id . '_cache_groups_set' );
	}

	/**
	 * Parse & eventually Edit the WP_Query.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Query $q The WordPress Query.
	 */
	public function parse_query( $q = null ) {
		/**
		 * Dynamic action to inform cache groups are set for the Cluster.
		 *
		 * @since 1.0.0
		 *
		 * @param WP_Quert $q The WordPress Query passed by reference.
		 */
		do_action_ref_array( 'cp_' . $this->id . '_parse_query', array( &$q ) );
	}

	/**
	 * Register the Post types needed by your Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @param array $post_types an array containing associative arrays {
	 *     An array of arguments.
	 *     @type string $post_type The post type name.
	 *     @type array  $args      The post type arguments {@see register_post_type()
	 *                             for the complete list of possible arguments}}.
	 * }
	 */
	public function register_post_types( $post_types = array() ) {
		$types = array();

		if ( ! empty( $post_types ) && is_array( $post_types ) ) {
			foreach ( $post_types as $post_type ) {
				if ( ! isset( $post_type['post_type'] ) || ! isset( $post_type['args'] ) ) {
					continue;
				}

				register_post_type( $post_type['post_type'], $post_type['args'] );
				$types[] = $post_type['post_type'];
			}
		}

		/**
		 * Dynamic action to inform post types are registered for the Cluster.
		 *
		 * @since 1.0.0
		 *
		 * @param array $types The registered post type names list.
		 */
		do_action( 'cp_' . $this->id . '_register_post_types', $types );
	}

	/**
	 * Register the Taxonomies needed by your Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @param array $taxonomies an array containing associative arrays {
	 *     An array of arguments.
	 *     @type string       $taxonomy    The Taxonomy name.
	 *     @type array|string $object_type The object type or list of object types the taxonomy is associated to.
	 *     @type array        $args        The taxonomy arguments {@see register_taxonomy()
	 *                                     for the complete list of possible arguments}}.
	 * }
	 */
	public function register_taxonomies( $taxonomies = array() ) {
		$taxos = array();

		if ( ! empty( $taxonomies ) && is_array( $taxonomies ) ) {
			foreach ( $taxonomies as $taxonomy ) {
				if ( ! isset( $taxonomy['taxonomy'] ) || ( ! isset( $taxonomy['object_type'] ) && ! is_null( $taxonomy['object_type'] ) ) || ! isset( $taxonomy['args'] ) ) {
					continue;
				}

				register_taxonomy( $taxonomy['taxonomy'], $taxonomy['object_type'], $taxonomy['args'] );
				$taxos[] = $taxonomy['taxonomy'];
			}
		}

		/**
		 * Dynamic action to inform taxonomies are registered for the Cluster.
		 *
		 * @since 1.0.0
		 *
		 * @param array $taxos The registered taxonomy names list.
		 */
		do_action( 'cp_' . $this->id . '_register_taxonomies', $taxos );
	}

	/**
	 * Add rewrite tags for your Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @param array $tags an array containing associative arrays {
	 *     An array of arguments.
	 *     @type string $tag   Name of the new rewrite tag.
	 *     @type string $regex Regular expression to substitute the tag for in rewrite rules.
	 * }
	 */
	public function rewrite_tags( $tags = array() ) {
		if ( is_array( $tags ) ) {
			foreach ( $tags as $tag ) {
				if ( empty( $tag['tag'] ) || empty( $tag['regex'] ) ) {
					continue;
				}

				add_rewrite_tag( $tag['tag'], $tag['regex'] );
			}
		}

		/**
		 * Dynamic action to inform rewrite tags are set for the Cluster.
		 *
		 * @since 1.0.0
		 *
		 * @param array $tags The rewrite tags list.
		 */
		do_action( 'cp_' . $this->id . '_rewrite_tags', $tags );
	}

	/**
	 * Add rewrite rules for your Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @param array $rules an array containing associative arrays {
	 *     An array of arguments.
	 *     @type string       $regex Regular expression to match request against.
	 *     @type string|array $query The corresponding query vars for this rewrite rule.
	 * }
	 */
	public function rewrite_rules( $rules = array() ) {
		$priority = 'top';

		if ( is_array( $rules ) ) {
			foreach ( $rules as $rule ) {
				if ( empty( $rule['regex'] ) || empty( $rule['query'] ) ) {
					continue;
				}

				add_rewrite_rule( $rule['regex'], $rule['query'], $priority );
			}
		}

		/**
		 * Dynamic action to inform rewrite rules are set for the Cluster.
		 *
		 * @since 1.0.0
		 *
		 * @param array $rules The rewrite rules list.
		 */
		do_action( 'cp_' . $this->id . '_rewrite_rules', $rules );
	}

	/**
	 * Add permastruct for your Cluster.
	 *
	 * @since 1.0.0
	 *
	 * {@see add_permastruct() for a detailed description of the arguments}}
	 * @param string $name
	 * @param string $struct
	 * @param array  $args
	 */
	public function permastructs( $name = '', $struct = '', $args = array() ) {
		if ( empty( $name ) || empty( $struct ) ) {
			return;
		}

		$r = wp_parse_args( $args, array(
			'with_front'  => false,
			'ep_mask'     => EP_NONE,
			'paged'       => true,
			'feed'        => false,
			'forcomments' => false,
			'walk_dirs'   => true,
			'endpoints'   => false,
		) );

		// Add the permastruct.
		add_permastruct( $name, $struct, $r );

		/**
		 * Dynamic action to inform the permastruct is set for the Cluster.
		 *
		 * @since 1.0.0
		 *
		 * {@see add_permastruct() for a detailed description of the paremeters}}
		 * @param string $name
		 * @param string $struct
		 * @param array  $args
		 */
		do_action( 'cp_' . $this->id . '_permastruct', $name, $struct, $args );
	}

	/**
	 * Set the Cluster toolbar(s)
	 *
	 * @since 1.0.0
	 *
	 * @param array $nodes an array containing associative arrays {
	 *     An array of arguments.
	 *     @type string $type  Whether it's a group or a menu.
	 *     @type string $args  The nav item arguments.
	 * }
	 */
	public function toolbars( $nodes = array() ) {
		if ( empty( $nodes ) || ! is_array( $nodes ) ) {
			return;
		}

		foreach ( $nodes as $node ) {
			if ( ! isset( $node['type'] ) || ! isset( $node['args'] ) ) {
				continue;
			}

			if ( 'group' === $node['type'] ) {
				$this->toolbar->add_group( $node['args'] );
			} else {
				$this->toolbar->add_menu( $node['args'] );
			}
		}

		/**
		 * Dynamic action to inform the nodes has been added to the Cluster toolbar.
		 *
		 * @since 1.0.0
		 *
		 * @param array              $nodes   The list of nav items.
		 * @param CP_Cluster_Toolbar $toolbar The Cluster's toolbar object.
		 */
		do_action( 'cp_' . $this->id . '_set_toolbars', $nodes, $this->toolbar );
	}

	/**
	 * Get the needed Admin bar items for the Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @return array The list of Admin bar items to create.
	 */
	public function get_account_bar_menus() {
		return array();
	}

	/**
	 * Create the Admin bar items for the Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @return WP_Admin_Bar $wp_admin_bar The WordPress Admin Bar.
	 */
	final public function admin_bar( WP_Admin_Bar $wp_admin_bar ) {
		if ( ! is_user_logged_in() ) {
			return;
		}

		$wp_admin_bar->add_group( array(
			'parent' => 'my-account',
			'id'     => 'clusterpress-user-bar',
		) );

		$menus = (array) $this->get_account_bar_menus();

		if ( empty( $menus ) ) {
			return;
		}

		// Sort items.
		if ( isset( $this->toolbar ) ) {
			$menus = $this->toolbar->sort_items( $menus );
		}

		foreach ( $menus as $menu ) {
			if ( ! is_array( $menu ) ) {
				$menu = (array) $menu;
			}

			if ( ! $menu['id'] || ! current_user_can( $menu['capability'], array( 'toolbar' => 'admin_bar' ) ) ) {
				continue;
			}

			$parent = 'clusterpress-user-bar';
			if ( ! empty( $menu['parent'] ) ) {
				$parent = $menu['parent'];
			}

			if ( ! empty( $menu['count'] ) ) {
				$menu['title'] = sprintf( '<span class="pending-count">%1$d</span> %2$s',
					number_format_i18n( $menu['count'] ),
					$menu['title']
				);
			}

			$wp_admin_bar->add_menu( array_merge( $menu, array( 'parent' => $parent ) ) );
		}

		/**
		 * Dynamic action to inform the nav items has been added to the WordPress Admin Bar.
		 *
		 * @since 1.0.0
		 *
		 * @param array        $menus        The list of nav items.
		 * @param WP_Admin_Bar $wp_admin_bar The WordPress Admin Bar object.
		 */
		do_action( 'cp_' . $this->id . '_set_admin_bar', $menus, $wp_admin_bar );
	}

	/**
	 * Set the title to use in the document <meta> tag.
	 *
	 * @since 1.0.0
	 *
	 * @param array $title_parts The list of terms to add to the title tag.
	 */
	public function setup_page_title( $title_parts = array() ) {
		if ( empty( $title_parts ) ) {
			return;
		}

		$this->page_title = $title_parts;

		/**
		 * Dynamic action to inform the page title is set for the Cluster.
		 *
		 * @since 1.0.0
		 *
		 * @param array $title_parts The list of terms to add to the title tag.
		 */
		do_action( 'cp_' . $this->id . '_setup_page_title', $title_parts );
	}

	/**
	 * Get the list of settings sections/fields for the Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @return array The list of settings sections/fields to register.
	 */
	public function get_settings() {
		return array();
	}

	/**
	 * Register the settings sections and fields for the Cluster.
	 *
	 * @since 1.0.0
	 */
	final public function register_settings() {
		$cp = clusterpress();

		if ( ! is_admin() || ! current_user_can( 'manage_options' ) || ! isset( $cp->admin ) ) {
			return;
		}

		$settings = $this->get_settings();

		if ( empty( $settings['sections'] ) || empty( $settings['fields'] ) ) {
			return;
		}

		$settings_sections = array();

		foreach ( (array) $settings['sections'] as $ks => $section ) {
			if ( is_numeric( $ks ) || empty( $section['title'] ) || empty( $section['callback'] ) ) {
				continue;
			}

			$settings_sections[ $ks ] = 'clusterpress_' . $this->id;

			if ( ! isset( $cp->admin->settings_tabs ) ) {
				$cp->admin->settings_tabs = array();
			}

			if ( ! isset( $cp->admin->settings_tabs[ $this->id ] ) ) {
				$cp->admin->settings_tabs[ $this->id ] = $this->name;
			}

			// Add the section.
			add_settings_section( $ks, $section['title'], $section['callback'], $settings_sections[ $ks ] );
		}

		foreach ( (array) $settings['fields'] as $section => $fields ) {
			// Check the section exists.
			if ( ! isset( $settings_sections[ $section ] ) ) {
				continue;
			}

			foreach ( $fields as $option => $field ) {
				if ( is_numeric( $option ) || empty( $field['title'] ) || empty( $field['callback'] ) || empty( $field['sanitize_callback'] ) ) {
					continue;
				}

				if ( empty( $field['args'] ) ) {
					$field['args'] = array();
				}

				// Add the field
				add_settings_field( $option, $field['title'], $field['callback'], $settings_sections[ $section ], $section, $field['args'] );

				// Register the setting
				register_setting( $settings_sections[ $section ], $option, $field['sanitize_callback'] );
			}
		}

		/**
		 * Dynamic action to inform the settings are registered for the Cluster.
		 *
		 * @since 1.0.0
		 */
		do_action( 'cp_' . $this->id . '_register_settings' );
	}

	/**
	 * Add features to registered post types.
	 *
	 * @since 1.0.0
	 *
	 * @param array $post_types List of post type names.
	 * @param array $features   List of post type features.
	 */
	public function add_post_types_support( $post_types = array(), $features = array() ) {
		if ( ! empty( $post_types ) && ! empty( $features ) ) {
			$post_types = (array) $post_types;

			foreach ( $post_types as $post_type ) {
				foreach ( $features as $feature ) {
					add_post_type_support( $post_type, $feature );
				}
			}
		}

		/**
		 * Dynamic action to inform the features were added to the post types.
		 *
		 * @since 1.0.0
		 *
		 * @param array $post_types List of post type names.
		 * @param array $features   List of post type features.
		 */
		do_action( 'cp_' . $this->id . '_add_post_types_support', $post_types, $features );
	}

	/**
	 * Get Rest API routes for the Cluster
	 *
	 * @since 1.0.0
	 *
	 * @return array The list of Rest API Routes to set.
	 */
	public function get_routes() {
		return array();
	}

	/**
	 * Set Rest API Routes for the Cluster.
	 *
	 * @since 1.0.0
	 */
	final public function rest_routes() {
		$routes = $this->get_routes();

		if ( empty( $routes ) ) {
			return;
		}

		$cp   = clusterpress();
		$root = sprintf( '%1$s/%2$s', $cp->rest->namespace, $cp->rest->version );

		foreach ( $routes as $route ) {
			if ( empty( $route['endpoint'] ) || empty( $route['callback'] ) ) {
				continue;
			}

			if ( ! is_callable( $route['callback'] ) ) {
				continue;
			}

			$has_permission_callback = ! empty( $route['permission_callback'] );

			if ( $has_permission_callback && ! is_callable( $route['permission_callback'] ) ) {
				continue;
			}

			$p = array(
				'methods'  => 'GET',
				'callback' => $route['callback'],
			);

			if ( $has_permission_callback ) {
				$p['permission_callback'] = $route['permission_callback'];
			}

			if ( ! empty( $route['methods'] ) ) {
				$p['methods'] = $route['methods'];
			}

			if ( ! empty( $route['args'] ) && is_array( $route['args'] ) ) {
				$p = array_merge( $p, $route['args'] );
			}

			register_rest_route( $root, $route['endpoint'], $p );
		}

		/**
		 * Dynamic action to inform the Rest API routes are set for the Cluster.
		 *
		 * @since 1.0.0
		 */
		do_action( 'cp_' . $this->id . '_register_routes' );
	}

	/**
	 * Get Feedbacks used by the Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @return array The list of Feedbacks to set.
	 */
	public function get_feedbacks() {
		return array();
	}

	/**
	 * Set Feedbacks for the Cluster.
	 *
	 * @since 1.0.0
	 */
	final public function register_feedbacks() {
		$feedbacks = $this->get_feedbacks();
		$cp        = clusterpress();

		if ( empty( $feedbacks ) ) {
			return;
		}

		if ( is_null( $cp->feedbacks ) ) {
			$cp->feedbacks = new CP_Feedback;
		}

		foreach ( $feedbacks as $code => $message ) {
			if ( is_array( $message ) && isset( $message['cluster'] ) && isset( $message['message'] ) ) {
				$cp->feedbacks->add( $message['cluster'] . '[' . sanitize_key( $code ) . ']', $message['message'] );
			} else {
				$cp->feedbacks->add( $this->id . '[' . sanitize_key( $code ) . ']', $message );
			}
		}
	}

	/**
	 * Get the capabilities map used by the Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @return array The list of capabilities map to set.
	 */
	public function get_caps_map() {
		return array();
	}

	/**
	 * Set capabilities map for the Cluster.
	 *
	 * @since 1.0.0
	 */
	final public function map_caps() {
		$cp   = clusterpress();
		$caps = $this->get_caps_map();

		if ( is_null( $cp->cluster->caps ) ) {
			$cp->cluster->caps = array();
		}

		foreach( $caps as $c => $cap ) {
			if ( is_numeric( $c ) || ! is_callable( $cap ) ) {
				continue;
			}

			$cp->cluster->caps[ $c ] = $cap;
		}
	}

	/**
	 * Override this function to run your simple Cluster's upgrade routines.
	 *
	 * @since 1.0.0
	 */
	public function upgrade( $version = 0 ) {}

	/**
	 * Override this function to run your Cluster's install tasks.
	 *
	 * @since 1.0.0
	 */
	public function install() {}

	/**
	 * Get the Post install/upgrade batch tasks required by the Cluster.
	 *
	 * @since 1.0.0
	 *
	 * @return array The list of Post install/upgrade batch tasks to run.
	 */
	public function get_cluster_tasks() {
		return array();
	}

	/**
	 * Merge and return the Post install/upgrade batch tasks for all Clusters.
	 *
	 * @since 1.0.0
	 *
	 * @return array The full list of Post install/upgrade batch tasks to run.
	 */
	final public function set_cluster_tasks( $tasks = array() ) {
		$cluster_tasks = $this->get_cluster_tasks();

		if ( empty( $cluster_tasks ) ) {
			return $tasks;
		}

		return array_merge( $cluster_tasks, $tasks );
	}
}
