<?php
/**
 * Main Template loader class.
 *
 * Credits http://github.com/GaryJones/Gamajo-Template-Loader.
 *
 * @package ClusterPress\core\classes
 * @subpackage template-loader
 *
 * @since 1.0.0
 */

if ( ! class_exists( 'CP_Template_Loader' ) ) :
/**
 * Template loader class.
 *
 * @since 1.0.0
 */
class CP_Template_Loader {
	/**
	 * Plugin Prefix.
	 *
	 * @var string
	 */
	protected $filter_prefix;

	/**
	 * Directory name where custom templates for this plugin
	 * should be found in the theme.
	 *
	 * @var string
	 */
	protected $theme_template_directory;

	/**
	 * The Template Loader instanc.
	 *
	 * @var object
	 */
	private static $instance = null;

	/**
	 * Set the filter prefix and the theme directory.
	 *
	 * @since  1.0.0
	 */
	public function __construct() {
		$this->filter_prefix            = 'cp';
		$this->theme_template_directory = 'clusterpress';
	}

	/**
	 * Get Main Instance.
	 *
	 * @since  1.0.0
	 *
	 * @return object The main instance.
	 */
	public static function instance() {
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Get a template part.
	 *
	 * @since  1.0.0
	 *
	 * @param string  $slug
	 * @param string  $name Optional. Default null.
	 * @param bool    $load Optional. Default true.
	 * @return string
	 */
	public function get_template_part( $slug, $name = null, $load = true, $require_once = true ) {
		/**
		 * Hook here to run custom code
		 *
		 * @since  1.0.0
		 *
		 * @param string  $slug
		 * @param string  $name Optional. Default null.
		 */
		do_action( 'get_template_part_' . $slug, $slug, $name );

		// Get files names of templates, for given slug and name.
		$templates = $this->get_template_file_names( $slug, $name );

		// Return the part that is found
		return $this->locate_template( $templates, $load, $require_once );
	}

	/**
	 * Given a slug and optional name, create the file names of templates.
	 *
	 * @since 1.0.0
	 *
	 * @param string  $slug
	 * @param string  $name
	 * @return array
	 */
	protected function get_template_file_names( $slug, $name, $ext = 'php' ) {
		$templates = array();
		if ( isset( $name ) ) {
			$templates[] = $slug . '-' . $name . '.' . $ext;
		}
		$templates[] = $slug . '.' . $ext;

		/**
		 * Allow template choices to be filtered.
		 */
		return apply_filters( $this->filter_prefix . '_get_template_part', $templates, $slug, $name );
	}

	/**
	 * Retrieve the name of the highest priority template file that exists.
	 *
	 * @since 1.0.0
	 *
	 * @param string|array $template_names Template file(s) to search for, in order.
	 * @param bool         $load           If true the template file will be loaded if it is found.
	 * @param bool         $require_once   Whether to require_once or require. Default true.
	 *                                     Has no effect if $load is false.
	 * @return string The template filename if one is located.
	 */
	public function locate_template( $template_names, $load = false, $require_once = true ) {
		// No file found yet
		$located = false;

		// Remove empty entries
		$template_names = array_filter( (array) $template_names );
		$template_paths = $this->get_template_stack();

		// Try to find a template file
		foreach ( $template_names as $template_name ) {
			// Trim off any slashes from the template name
			$template_name = ltrim( $template_name, '/' );

			// Try locating this template file by looping through the template paths
			foreach ( $template_paths as $template_path ) {
				if ( file_exists( $template_path . $template_name ) ) {
					$located = $template_path . $template_name;
					break 2;
				}
			}
		}

		if ( $load && $located ) {
			load_template( $located, $require_once );
		}

		return $located;
	}

	/**
	 * Return a list of paths to check for template locations.
	 *
	 * @since 1.0.0
	 *
	 * @return array The template stack.
	 */
	protected function get_template_stack() {
		$theme_directory = trailingslashit( $this->theme_template_directory );

		$stack = array(
			10  => trailingslashit( get_template_directory() ) . $theme_directory,
			100 => untrailingslashit( cp_get_templates_dir() ),
		);

		// Only add this conditionally, so non-child themes don't redundantly check active theme twice.
		if ( is_child_theme() ) {
			$stack[1] = trailingslashit( get_stylesheet_directory() ) . $theme_directory;
		}

		if ( ! empty( $this->theme_only ) ) {
			$stack = array_intersect_key( $stack, array( 1 => true, 10 => true ) );
		}

		/**
		 * Allow ordered list of template paths to be amended.
		 *
		 * @since  1.0.0
		 *
		 * @param  array $stack The template stack.
		 */
		$stack = apply_filters( $this->filter_prefix . '_template_stack', $stack );

		// sort the file paths based on priority
		ksort( $stack, SORT_NUMERIC );

		return array_map( 'trailingslashit', $stack );
	}

	/**
	 * Return the url to the plugin's stylesheet.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $name        The file name of the asset.
	 * @param  string $type        The file extension of the asset.
	 * @param  array  $plugin_data Specific dir and urls of the plugin using this function.
	 * @return string              The located template asset.
	 */
	public function get_template_asset( $name = 'style', $type = 'css', $plugin_data = '' ) {
		$asset = $this->get_template_file_names( $name, null, $type );

		$located = $this->locate_template( $asset );

		if ( empty( $plugin_data ) ) {
			$plugin_data = array(
				'dir' => cp_get_plugin_dir(),
				'url' => cp_get_plugin_url(),
			);
		}

		// Microsoft is annoying...
		$slashed_located     = str_replace( '\\', '/', $located );
		$slashed_content_dir = str_replace( '\\', '/', WP_CONTENT_DIR );
		$slashed_plugin_dir  = str_replace( '\\', '/', $plugin_data['dir'] );

		// Should allways be the case for regular configs.
		if ( false !== strpos( $slashed_located, $slashed_content_dir ) ) {
			$located = str_replace( $slashed_content_dir, content_url(), $slashed_located );

		// If not, try this.
		} else {
			$located = str_replace( $slashed_plugin_dir, $plugin_data['url'], $slashed_located );
		}

		return $located;
	}

	/**
	 * Look for a specific ClusterPress stylesheet into the theme.
	 *
	 * @since 1.0.0
	 *
	 * @return string path to the found template.
	 */
	public function get_theme_asset() {
		// Only look inside the css folder of the theme or of the child theme.
		$this->theme_only               = true;
		$this->theme_template_directory = 'css';

		$theme_asset = $this->get_template_asset( 'clusterpress', 'css' );

		// Restore defaults.
		unset( $this->theme_only );
		$this->theme_template_directory = 'clusterpress';

		return $theme_asset;
	}
}

endif;
