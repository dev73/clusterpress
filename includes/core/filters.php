<?php
/**
 * ClusterPress Filters.
 *
 * @package ClusterPress\core
 * @subpackage filters
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * ClusterPress alias of WordPress 'template_include'.
 *
 * @since 1.0.0
 *
 * @param  string $template Path to the template to load.
 * @return string           Path to the template to load.
 */
function cp_template_include( $template = '' ) {
	/**
	 * Used to select the base template file for ClusterPress template parts.
	 *
	 * @since  1.0.0
	 *
	 * @param string $template path to the template to load.
	 */
	return apply_filters( 'cp_template_include', $template );
}
add_filter( 'template_include', 'cp_template_include', 10, 1 );

/**
 * Use a specific base template (most of the time the page.php one)
 * for ClusterPress content.
 *
 * @since  1.0.0
 *
 * @param  string $template Path to the base template to load.
 * @return string           Path to the base template to load.
 */
function cp_set_base_template( $template = '' ) {
	if ( ! is_clusterpress() || is_404() ) {
		return $template;
	}

	// Get the template WordPress found.
	$current_template = wp_basename( $template, '.php' );

	// Try to see if the theme has a specific template for ClusterPress.
	$specific = get_query_template( 'clusterpress' );

	if ( ! empty( $specific ) ) {
		$template = $specific;

	} else if ( 'page' !== $current_template ) {
		// else Try the page template
		$template = get_query_template( 'page', array( 'page.php' ) );
	}

	if ( empty( $template ) ) {
		// finally fall back to the index template
		$template = get_index_template();
	}

	/**
	 * This action is used to trigger template parts injection.
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_do_templates' );

	return $template;
}
add_filter( 'cp_template_include', 'cp_set_base_template' );

/**
 * Shortcircuit the WP Query if in a ClusterPress area.
 *
 * This filter is very important as it sets a fake post for
 * the ClusterPress displayed page and avoids notices. It also
 * makes sure no extra requests will be performed to get a regular
 * post by WordPress.
 *
 * @since  1.0.0
 *
 * @param  null   $return   A null value to use the regular WP Query.
 * @param  WP_Query $wq     The WP Query object.
 * @return null|array       Null if not in a ClusterPress area.
 *                          An array containing a ClusterPress Post otherwise.
 */
function cp_posts_pre_query( $return = null, WP_Query $wq ) {
	if ( ! cp_can_alter_posts_query( $wq ) || ! is_clusterpress() ) {
		return $return;
	}

	// Set the post
	$post = (object) array(
		'ID'             => 0,
		'post_type'      => 'clusterpress',
		'post_title'     => $wq->get( 'cp_post_title' ),
		'comment_status' => 'closed',
		'comment_count'  => 0,
	);

	// Set the queried object to avoid notices
	$wq->queried_object    = get_post( $post );
	$wq->queried_object_id = $wq->queried_object->ID;

	// Set the Posts list to be limited to our custom post.
	$posts = array( $post );

	// Reset some WP Query properties
	$wq->found_posts   = 1;
	$wq->max_num_pages = 1;
	$wq->posts         = $posts;
	$wq->post          = $post;
	$wq->post_count    = 1;
	$wq->is_home       = false;
	$wq->is_front_page = false;
	$wq->is_page       = true;
	$wq->is_single     = false;
	$wq->is_archive    = false;
	$wq->is_tax        = false;

	return $posts;
}
add_filter( 'posts_pre_query', 'cp_posts_pre_query', 10, 2 );

/**
 * ClusterPress alias of WordPress 'document_title_parts'.
 *
 * @since 1.0.0
 *
 * @param  array $title_parts List of terms to output into the document meta tag.
 * @return array              List of terms to output into the document meta tag.
 */
function cp_document_title_parts( $title_parts = array() ) {
	if ( ! is_clusterpress() ) {
		return $title_parts;
	}

	/**
	 * Set the document title parts for Clusterpress Areas.
	 *
	 * @since 1.0.0
	 *
	 * @param array $title_parts List of terms to output into the document meta tag.
	 */
	return apply_filters( 'cp_document_title_parts', $title_parts );
}
add_filter( 'document_title_parts', 'cp_document_title_parts', 10, 1 );

/**
 * ClusterPress alias of WordPress 'body_class'.
 *
 * @since 1.0.0
 *
 * @param  array $wp_classes     List of body classes.
 * @param  array $custom_classes List of custom classes.
 * @return array                 List of body classes.
 */
function cp_body_class( $wp_classes = array(), $custom_classes = array() ) {
	if ( ! is_clusterpress() ) {
		return $wp_classes;
	}

	/**
	 * Set the body classes for Clusterpress Areas.
	 *
	 * @since 1.0.0
	 *
	 * @param array $wp_classes     List of body classes.
	 * @param array $custom_classes List of custom classes.
	 */
	return apply_filters( 'cp_body_class', $wp_classes, $custom_classes );
}
add_filter( 'body_class', 'cp_body_class', 20, 2 );

/**
 * ClusterPress alias of WordPress 'post_class'.
 *
 * @since 1.0.0
 *
 * @param  array $post_classes List of post classes.
 * @return array               List of post classes.
 */
function cp_post_class( $post_classes = array() ) {
	if ( ! is_clusterpress() ) {
		return $post_classes;
	}

	/**
	 * Set the post classes for Clusterpress Areas.
	 *
	 * @since 1.0.0
	 *
	 * @param  array $post_classes List of post classes.
	 */
	return apply_filters( 'cp_post_class', $post_classes );
}
add_filter( 'post_class', 'cp_post_class', 10, 1 );

/**
 * Custom filter to format the Custom Post Types mentions.
 *
 * @since  1.0.0
 *
 * @param  string $text The Post Type content.
 * @return string       The Post Type content with the mention links if needed.
 */
function cp_mentions_custom_post_type( $text = '' ) {
	return apply_filters( 'cp_mentions_custom_post_type', $text );
}

/**
 * Map Capabilities for each Clusters
 *
 * @since  1.0.0
 *
 * @param  array   $caps    The user's actual capabilities.
 * @param  string  $cap     Capability name being checked.
 * @param  int     $user_id The user ID.
 * @param  array   $args    Adds the context to the cap. Typically the object ID.
 * @return array            The user's actual capabilities.
 */
function cp_map_meta_caps( $caps = array(), $cap = '', $user_id = 0, $args = array() ) {
	$map = clusterpress()->cluster->caps;

	if ( isset( $map[ $cap ] ) && is_callable( $map[ $cap ] ) ) {
		$caps = call_user_func_array( $map[ $cap ], array( $user_id, $args ) );

		if ( ! is_array( $caps ) ) {
			$caps = array( 'manage_options' );

			if ( is_multisite() ) {
				$caps = array( 'manage_network_users' );
			}
		}
	}

	/**
	 * Filter here to edit ClusterPress caps.
	 *
	 * @since  1.0.0
	 *
	 * @param  array   $caps    The user's actual capabilities.
	 * @param  string  $cap     Capability name being checked.
	 * @param  integer $user_id The user ID
	 * @param  array   $args    Adds the context to the cap. Typically the object ID.
	 */
	return apply_filters( 'cp_map_meta_caps', $caps, $cap, $user_id, $args );
}
add_filter( 'map_meta_cap', 'cp_map_meta_caps', 10, 4 );

/**
 * Transform urls into links.
 *
 * @since  1.0.0
 *
 * @param  string $value    The value of the field
 * @param  string $field_id The field ID
 * @return string           The value of the field
 */
function cp_make_url_fields_clickable( $value = '', $field_id = '' ) {
	/**
	 * Filter here to add your custom field ids if you need to make them clickable.
	 *
	 * @since  1.0.0
	 *
	 * @param array $value The list of profile field ids containing an url.
	 */
	$url_fields = apply_filters( 'cp_get_clickable_url_fields', array( 'user_url' ) );
	if ( false === array_search( $field_id, $url_fields ) ) {
		return $value;
	}

	return make_clickable( $value );
}

/**
 * Sanitizes the ClusterPress feedback message.
 *
 * @since 1.0.0
 *
 * @param  string $message The message before sanitization.
 * @return string          The sanitized message.
 */
function cp_sanitize_feedback( $message = '' ) {
	return wp_kses( $message, array(
		'a'   => array( 'href' => true ),
		'p'   => array(),
		'img' => array(
			'src'    => true,
			'height' => true,
			'width'  => true,
			'class'  => true,
			'alt'    => true
		),
	) );
}

/**
 * Make sure the user can access to the ClusterPress WP nav item URL before setting it.
 *
 * @since  1.0.0
 *
 * @param  array  $menu_items WP Nav Items list.
 * @return array              WP Nav Items list.
 */
function cp_validate_nav_menu_items( $menu_items = array() ) {
	$cp_menu_items = wp_list_filter( $menu_items, array ( 'type' => 'clusterpress' ) );

	if ( empty( $cp_menu_items ) ) {
		return $menu_items;
	}

	foreach ( $cp_menu_items as $km => $om ) {
		$capability = sprintf( 'cp_read_%ss_archive', $om->object );

		/**
		 * In case it's a private or closed network, remove the menu item
		 * if the user can't access to it.
		 */
		if ( ! cp_cluster_is_enabled( $om->object ) || ! current_user_can( $capability ) ) {
			unset( $menu_items[ $km ] );
			continue;
		}

		$menu_items[ $km ]->url = cp_get_archive_link( $om->object );

		if ( $om->object === cp_current_cluster() ) {
			$menu_items[$km]->classes = array_merge( $om->classes, array( 'current-menu-item', 'current_page_item' ) );
		}
	}

	return $menu_items;
}
add_filter( 'wp_get_nav_menu_items', 'cp_validate_nav_menu_items', 10, 1 );

/**
 * Add ClusterPress WP Nav Items to the Customizer.
 *
 * @since  1.0.0
 *
 * @param  array  $items  The array of menu items.
 * @param  string $type   The object type.
 * @param  string $object The object name.
 * @param  int    $page   The current page number.
 * @return array          The array of menu items.
 */
function cp_customizer_get_nav_menus_items( $items = array(), $type = '', $object = '', $page = 0 ) {
	if ( 'clusterpress' === $type ) {
		$cp_items = cp_get_archive_wp_nav_items();
	}

	foreach ( $cp_items as $cp_item ) {
		$items[] = array(
			'id'         => "cp-{$cp_item->object}",
			'title'      => html_entity_decode( $cp_item->title, ENT_QUOTES, get_bloginfo( 'charset' ) ),
			'type'       => $type,
			'url'        => esc_url_raw( $cp_item->url ),
			'type_label' => _x( 'Lien personnalisé', 'customizer menu type label', 'clusterpress' ),
			'object'     => $cp_item->object,
			'object_id'  => -1,
		);
	}

	return array_slice( $items, 10 * $page, 10 );
}
add_filter( 'customize_nav_menu_available_items', 'cp_customizer_get_nav_menus_items', 10, 4 );

/**
 * Add ClusterPress Archive items to the available Customizer Post types.
 *
 * @since  1.0.0
 *
 * @param  array $item_types Custom menu item types.
 * @return array             Custom menu item types + ClusterPress item types.
 */
function cp_customizer_set_nav_menus_item_types( $item_types = array() ) {
	$item_types = array_merge( $item_types, array(
		'clusterpress' => array(
			'title'  => _x( 'ClusterPress', 'customizer menu section title', 'clusterpress' ),
			'type'   => 'clusterpress',
			'object' => 'clusterpress',
		),
	) );

	return $item_types;
}
add_filter( 'customize_nav_menu_available_item_types', 'cp_customizer_set_nav_menus_item_types', 10, 1 );

/**
 * Reset the ClusterPress content and remove all content filters if needed.
 *
 * @since 1.0.0
 *
 * @param string $content The regular WordPress content.
 * @return                The regular WordPress content or ClusterPress template parts.
 */
function cp_the_content( $content = '' ) {
	// Get the current Post.
	$post = get_post();

	if ( ! empty( $post->post_type ) && 'clusterpress' === $post->post_type ) {
		/**
		 * Now we are sure it's a ClusterPress content, reset the
		 * filters applied to the content. Each Cluster will have to
		 * apply it at the places they will need to.
		 */
		remove_all_filters( 'the_content' );

		/**
		 * Filter here to inject your cluster's content.
		 *
		 * @see CP_User_Screens::__construct() for an example of use.
		 *
		 * @since 1.0.0
		 *
		 * @param string $value   An empty string.
		 * @param string $content The content of the ClusterPress Post type.
		 *                        It should be empty too at this stage.
		 */
		return apply_filters( 'cp_the_content', '', $content );

	// Otherwise, leave the content alone!
	}

	return $content;
}
add_filter( 'the_content', 'cp_the_content', 0, 1 );

/** Template Tags Output Filters **********************************************/

add_filter( 'cp_user_get_field_value',                         'cp_make_url_fields_clickable',       9, 2 );
add_filter( 'cp_user_get_field_value',                         'cp_sanitize_feedback',              10    );
add_filter( 'cp_user_get_field_value',                         'wp_unslash',                        10    );
add_filter( 'cp_user_get_field_value',                         'wptexturize',                       10    );
add_filter( 'cp_user_get_field_value',                         'convert_smilies',                   10    );
add_filter( 'cp_user_edit_field_value',                        'cp_sanitize_feedback',              10    );
add_filter( 'cp_user_edit_field_value',                        'wp_unslash',                        10    );
add_filter( 'cp_site_get_post_excerpt',                        'wptexturize',                       10    );
add_filter( 'cp_site_get_post_excerpt',                        'convert_smilies',                   20    );
add_filter( 'cp_site_get_post_excerpt',                        'wpautop',                           10    );
add_filter( 'cp_site_get_post_excerpt',                        'wp_make_content_images_responsive', 10    );
add_filter( 'cp_site_get_comment_excerpt',                     'wptexturize',                       10    );
add_filter( 'cp_site_get_comment_excerpt',                     'convert_smilies',                   20    );
add_filter( 'cp_site_get_comment_excerpt',                     'wpautop',                           10    );
add_filter( 'cp_site_get_comment_excerpt',                     'convert_chars',                     10    );
add_filter( 'cp_site_get_comment_excerpt',                     'wp_unslash',                        10    );
add_filter( 'cp_site_get_comment_excerpt',                     'make_clickable',                     9    );
add_filter( 'cp_get_feedback_message',                         'cp_sanitize_feedback',              10    );
add_filter( 'cp_interactions_get_posts_liked_widget_count',    'intval'                                   );
add_filter( 'cp_interactions_get_comments_liked_widget_count', 'intval'                                   );
add_filter( 'cp_interactions_get_mention_content',             'wptexturize'                              );
add_filter( 'cp_interactions_get_mention_content',             'convert_smilies',                   20    );
add_filter( 'cp_interactions_get_mention_content',             'wp_kses_post'                             );
add_filter( 'cp_interactions_get_mention_content',             'wpautop'                                  );
add_filter( 'cp_sites_followed_get_post_meta',                 'cp_sanitize_feedback',              10    );
