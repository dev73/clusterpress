<?php
/**
 * ClusterPress Options.
 *
 * @package ClusterPress\core
 * @subpackage options
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/** Generic & Feature Options *************************************************/

/**
 * Get the ClusterPress Version saved in DB.
 *
 * @since 1.0.0
 *
 * @return string The ClusterPress Raw DB Version.
 */
function cp_get_raw_db_version() {
	return get_network_option( 0, '_clusterpress_version', 0 );
}

/**
 * Get the network type preference.
 *
 * @since 1.0.0
 *
 * @param  string $default The admin preference about the network type.
 *                         It can be 'public', 'private' or 'closed'.
 *                         Defaults to 'public'.
 * @return string          The network type.
 */
function cp_get_network_type( $default = 'public' ) {
	return get_network_option( 0, 'clusterpress_network_type', $default );
}

/**
 * Get the enabled optional Clusters.
 *
 * @since 1.0.0
 *
 * @param  array $default The list of enabled Cluster IDs.
 *                        Defaults to an empty list.
 * @return array          The list of enabled Cluster IDs.
 */
function cp_get_enabled_clusters( $default = array() ) {
	return get_network_option( 0, 'cp_active_clusters', $default );
}

/**
 * Get the title for the Users archive page.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Users archive page.
 *                         Defaults to 'Membres'.
 * @return string          The title for the Users archive page.
 */
function cp_users_get_title( $default = '' ) {
	if ( empty( $default ) ) {
		$default = esc_html__( 'Membres', 'clusterpress' );
	}

	return get_network_option( 0, 'clusterpress_users_archive_title', $default );
}

/**
 * Get the enabled profile fields.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Users archive page.
 *                         Defaults to false to list all available fields.
 * @return false|array     The list of enabled fields.
 */
function cp_user_get_enabled_fields( $default = false ) {
	return get_network_option( 0, 'clusterpress_user_profile_fields', $default );
}

/**
 * Are mentions enabled ?
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the option.
 *                         Defaults to true.
 * @return bool            True if mentions are enabled. False otherwise.
 */
function cp_interactions_are_mentions_enabled( $default = true ) {
	return (bool) get_network_option( 0, 'clusterpress_interactions_mentions_enabled', $default ) && cp_cluster_is_enabled( 'interactions' );
}

/**
 * Are likes enabled ?
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the option.
 *                         Defaults to true.
 * @return bool            True if likes are enabled. False otherwise.
 */
function cp_interactions_is_like_enabled( $default = true ) {
	return (bool) get_network_option( 0, 'clusterpress_interactions_like_enabled', $default ) && cp_cluster_is_enabled( 'interactions' );
}

/**
 * Get the title for the Sites archive page.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Sites archive page.
 *                         Defaults to 'Sites'.
 * @return string          The title for the Sites archive page.
 */
function cp_sites_get_title( $default = '' ) {
	if ( empty( $default ) ) {
		$default = esc_html__( 'Sites', 'clusterpress' );
	}

	return get_network_option( 0, 'clusterpress_sites_archive_title', $default );
}

/**
 * Can users follow sites ?
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the option.
 *                         Defaults to true.
 * @return bool            True if users can follow sites. False otherwise.
 */
function cp_site_is_following_enabled( $default = true ) {
	return (bool) get_network_option( 0, 'clusterpress_site_following_enabled', $default );
}

/**
 * Get the Sites category taxonomy name.
 *
 * @since 1.0.0
 */
function cp_sites_get_taxonomy() {
	/**
	 * Filter here to use another taxonomy name.
	 *
	 * @since 1.0.0
	 *
	 * @param string $value The taxonomy name.
	 */
	return apply_filters( 'cp_sites_get_taxonomy', 'cp_site_category' );
}

/** Url Slugs and Rewrite IDs *************************************************/

/**
 * Get the WordPress pagination slug
 *
 * @since 1.0.0
 *
 * @return string the WordPress pagination slug.
 */
function cp_get_paged_slug() {
	global $wp_rewrite;

	if ( empty( $wp_rewrite ) ) {
		return false;
	}

	return $wp_rewrite->pagination_base;
}

/**
 * Get the ClusterPress "network" root slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the root slug.
 *                         Defaults to "reseau".
 * @return string          The ClusterPress "network" root slug.
 */
function cp_get_root_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'reseau', 'network root slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_get_root_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_root_slug', $default );
}

/**
 * Get the ClusterPress "network" root rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the root rewrite ID.
 *                         Defaults to "cp_network".
 * @return string          The ClusterPress "network" root rewrite ID.
 */
function cp_get_root_rewrite_id( $default = 'cp_network' ) {
	return $default;
}

/**
 * Get the Users archive slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Users archive slug.
 *                         Defaults to "utilisateurs".
 * @return string          The Users archive slug.
 */
function cp_users_get_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'utilisateurs', 'users slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_users_get_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_users_slug', $default );
}

/**
 * Get the User single slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User single slug.
 *                         Defaults to "utilisateur".
 * @return string          The User single slug.
 */
function cp_user_get_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'utilisateur', 'user slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_user_get_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_user_slug', $default );
}

/**
 * Get the User single rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User single rewrite ID.
 *                         Defaults to "cp_username".
 * @return string          The User single rewrite ID.
 */
function cp_user_get_rewrite_id( $default = 'cp_username' ) {
	return $default;
}

/**
 * Get the User manage root slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User manage root slug.
 *                         Defaults to "gerer".
 * @return string          The User manage root slug.
 */
function cp_user_get_manage_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'gerer', 'user manage slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_user_get_manage_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_user_manage_slug', $default );
}

/**
 * Get the User manage rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User manage rewrite ID.
 *                         Defaults to "cp_manage_user".
 * @return string          The User manage rewrite ID.
 */
function cp_user_get_manage_rewrite_id( $default = 'cp_manage_user' ) {
	return $default;
}

/**
 * Get the User manage account slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User manage account slug.
 *                         Defaults to "compte".
 * @return string          The User manage account slug.
 */
function cp_user_get_manage_account_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'compte', 'user account slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_user_get_manage_account_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_user_account_slug', $default );
}

/**
 * Get the User manage profile slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User manage profile slug.
 *                         Defaults to "profil".
 * @return string          The User manage profile slug.
 */
function cp_user_get_manage_profile_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'profil', 'user profile slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_user_get_manage_profile_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_user_profile_slug', $default );
}

/**
 * Get the User likes slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User likes slug.
 *                         Defaults to "likes".
 * @return string          The User likes slug.
 */
function cp_user_get_likes_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'likes', 'user likes slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_user_get_likes_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_user_likes_slug', $default );
}

/**
 * Get the User likes rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User likes rewrite ID.
 *                         Defaults to "cp_user_likes".
 * @return string          The User likes rewrite ID.
 */
function cp_user_get_likes_rewrite_id( $default = 'cp_user_likes' ) {
	return $default;
}

/**
 * Get the User mentions slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User mentions slug.
 *                         Defaults to "mentions".
 * @return string          The User mentions slug.
 */
function cp_user_get_mentions_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'mentions', 'user mentions slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_user_get_mentions_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_user_mentions_slug', $default );
}

/**
 * Get the User mentions rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User mentions rewrite ID.
 *                         Defaults to "cp_mentions".
 * @return string          The User mentions rewrite ID.
 */
function cp_user_get_mentions_rewrite_id( $default = 'cp_mentions' ) {
	return $default;
}

/**
 * Get the User posts slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User posts slug.
 *                         Defaults to "articles".
 * @return string          The User posts slug.
 */
function cp_user_get_posts_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'articles', 'user posts slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_user_get_posts_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_user_posts_slug', $default );
}

/**
 * Get the User posts rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User posts rewrite ID.
 *                         Defaults to "cp_posts".
 * @return string          The User posts rewrite ID.
 */
function cp_user_get_posts_rewrite_id( $default = 'cp_posts' ) {
	return $default;
}

/**
 * Get the User comments slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User comments slug.
 *                         Defaults to "ciommentaires".
 * @return string          The User comments slug.
 */
function cp_user_get_comments_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'commentaires', 'user comments slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_user_get_comments_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_user_comments_slug', $default );
}

/**
 * Get the User comments rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User comments rewrite ID.
 *                         Defaults to "cp_comments".
 * @return string          The User comments rewrite ID.
 */
function cp_user_get_comments_rewrite_id( $default = 'cp_comments' ) {
	return $default;
}

/**
 * Get the Sites archive slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Sites archive slug.
 *                         Defaults to "sites".
 * @return string          The Sites archive slug.
 */
function cp_sites_get_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'sites', 'sites slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_sites_get_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_sites_slug', $default );
}

/**
 * Get the Site single slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Site single slug.
 *                         Defaults to "site".
 * @return string          The Site single slug.
 */
function cp_site_get_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'site', 'site slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_site_get_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_site_slug', $default );
}

/**
 * Get the Site single rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Site single rewrite ID.
 *                         Defaults to "cp_sitename".
 * @return string          The Site single rewrite ID.
 */
function cp_site_get_rewrite_id( $default = 'cp_sitename' ) {
	return $default;
}

/**
 * Get the Site manage root slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Site manage root slug.
 *                         Defaults to "gerer".
 * @return string          The Site manage root slug.
 */
function cp_site_get_manage_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'gerer', 'site manage slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_site_get_manage_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_site_manage_slug', $default );
}

/**
 * Get the Site manage root rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Site manage root rewrite ID.
 *                         Defaults to "cp_manage_site".
 * @return string          The Site manage root rewrite ID.
 */
function cp_site_get_manage_rewrite_id( $default = 'cp_manage_site' ) {
	return $default;
}

/**
 * Get the Site members slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Site members slug.
 *                         Defaults to "membres".
 * @return string          The Site members slug.
 */
function cp_site_get_members_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'membres', 'site members slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_site_get_members_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_site_members_slug', $default );
}

/**
 * Get the Site members rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Site members rewrite ID.
 *                         Defaults to "cp_site_members".
 * @return string          The Site members rewrite ID.
 */
function cp_site_get_members_rewrite_id( $default = 'cp_site_members' ) {
	return $default;
}

/**
 * Get the Follow Site action slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Follow Site action slug.
 *                         Defaults to "suivre".
 * @return string          The Follow Site action slug.
 */
function cp_site_get_follow_action_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'suivre', 'site follow action slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_site_get_follow_action_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_site_follow_action_slug', $default );
}

/**
 * Get the Site followers slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Site followers slug.
 *                         Defaults to "adeptes".
 * @return string          The Site followers slug.
 */
function cp_site_get_followers_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'adeptes', 'site followers slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_site_get_followers_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_site_followers_slug', $default );
}

/**
 * Get the Site members rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Site members rewrite ID.
 *                         Defaults to "cp_site_members".
 * @return string          The Site members rewrite ID.
 */
function cp_site_get_followers_rewrite_id( $default = 'cp_site_followers' ) {
	return $default;
}

/**
 * Get the User's Sites slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User's Sites slug.
 *                         Defaults to "sites".
 * @return string          The User's Sites slug.
 */
function cp_sites_get_user_slug() {
	return apply_filters( 'cp_sites_get_user_slug', cp_sites_get_slug() );
}

/**
 * Get the User's Sites rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User's Sites rewrite ID.
 *                         Defaults to "cp_user_site".
 * @return string          The User's Sites rewrite ID.
 */
function cp_sites_get_user_rewrite_id( $default = 'cp_user_sites' ) {
	return $default;
}

/**
 * Get the User's Sites Followed slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User's Sites Followed slug.
 *                         Defaults to "suivis".
 * @return string          The User's Sites Followed slug.
 */
function cp_site_get_user_following_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'suivis', 'following site slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_site_get_user_following_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_user_following_slug', $default );
}

/**
 * Get the User's Sites Followed rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User's Sites Followed rewrite ID.
 *                         Defaults to "cp_user_following".
 * @return string          The User's Sites Followed rewrite ID.
 */
function cp_site_get_user_following_rewrite_id( $default = 'cp_user_following' ) {
	return $default;
}

/**
 * Get the User's manage followed sites slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User's manage followed sites slug.
 *                         Defaults to "sites-suivis".
 * @return string          The User's manage followed sites slug.
 */
function cp_site_get_user_manage_followed_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'sites-suivis', 'site followed slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_site_get_user_manage_followed_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_user_manage_followed_slug', $default );
}

/**
 * Get the User's manage followed sites rewrite ID.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the User's manage followed sites rewrite ID.
 *                         Defaults to "cp_user_manage_followed".
 * @return string          The User's manage followed sites rewrite ID.
 */
function cp_site_get_user_manage_followed_rewrite_id( $default = 'cp_user_manage_followed' ) {
	return $default;
}

/**
 * Get the Site's category root slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Site's category root slug.
 *                         Defaults to "categorie".
 * @return string          The Site's category root slug.
 */
function cp_site_get_category_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'categorie', 'site category slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_site_get_category_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_site_category_slug', $default );
}

/**
 * Get the Site's settings slug.
 *
 * @since 1.0.0
 *
 * @param  string $default The default value for the Site's settings slug.
 *                         Defaults to "categorie".
 * @return string          The Site's settings slug.
 */
function cp_site_get_settings_slug( $default = '' ) {
	if ( empty( $default ) ) {
		/* translators: the string will be used in the url, please do not use caps or weird characters  */
		$default = _x( 'reglages', 'site settings slug for ClusterPress', 'clusterpress' );
	}

	/**
	 * Don't want to query the DB, use this filter.
	 *
	 * @since 1.0.0
	 *
	 * @param false|string A string to shortcircuit the DB Query
	 * @param string       The default slug.
	 */
	$slug = apply_filters( 'cp_site_get_settings_slug', false, $default );
	if ( false !== $slug ) {
		return $slug;
	}

	return get_network_option( 0, 'clusterpress_site_settings_slug', $default );
}
