<?php
/**
 * Plugin Name: ClusterPress
 * Plugin URI: https://cluster.press
 * Description: Survitaminez votre réseau de publication WordPress et partez au "front" ! Profitez des quatre "Clusters" de ClusterPress pour mettre en valeur vos contenus et leurs auteurs. Vos utilisateurs ont enfin leurs quartiers au sein de votre site pour gérer leur profil, leur compte et toutes leurs contributions (Articles et commentaires). Vous restez aux commandes ! Choisissez d'ouvrir à tous l'annuaire des utilisateurs, de le restreindre à vos membres ou de le cacher complètement. Vous utilisez une configuration Multisite ? Très bon choix, les sites de votre réseau sont automatiquement mis en lumière au sein d'un annuaire catégorisable et bénéficient d'une zone individualisée propice à la découverte de leurs contenus au sein du site principal. Ajoutez un zeste de piment et activez le "Cluster" des intéractions pour enrichir vos contenus de mentions et améliorer leur attrait grâce aux "likes".
 * Version: 1.0.0-beta1
 * Requires at least: 4.7
 * Tested up to: 4.7
 * License: GPLv2 or later (license.txt)
 * Author: imath
 * Author URI: https://imathi.eu/
 * Text Domain: clusterpress
 * Domain Path: /includes/languages/
 * Network: True
 * Default Language: fr_FR
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Main ClusterPres Class
 *
 * @since  1.0.0
 */
final class ClusterPress {

	/**
	 * Array to store all ClusterPress globals.
	 *
	 * @var array
	 */
	private $data;

	/**
	 * Main ClusterPress Instance.
	 *
	 * @var object
	 */
	protected static $instance;

	/**
	 * Check whether a ClusterPress global is set.
	 *
	 * @since  1.0.0
	 *
	 * @param  string  $key The name of the global.
	 * @return bool         True if the global is set. False otherwise.
	 */
	public function __isset( $key ) {
		return isset( $this->data[ $key ] );
	}

	/**
	 * Get a specific global.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $key The name of the global.
	 * @return mixed       The value for the requested global.
	 */
	public function __get( $key ) {
		$value = null;

		if ( isset( $this->data[ $key ] ) ) {
			$value =  $this->data[ $key ];
		}

		return $value;
	}

	/**
	 * Set the value of a specific global.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $key   The name of the global.
	 * @param  mixed  $value The value of the global.
	 */
	public function __set( $key, $value ) {
		$this->data[ $key ] = $value;
	}

	/**
	 * Unset a specific ClusterPress global.
	 *
	 * @since  1.0.0
	 *
	 * @param  string  $key The name of the global.
	 */
	public function __unset( $key ) {
		if ( isset( $this->data[ $key ] ) ) {
			unset( $this->data[ $key ] );
		}
	}

	/**
	 * Class Construtor
	 *
	 * @since  1.0.0
	 */
	public function __construct() {
		$this->set_globals();
		$this->includes();
		$this->load_translation();
	}

	/**
	 * Set the instance and construct main class.
	 *
	 * @since  1.0.0
	 */
	public static function start() {
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Set some useful globals.
	 *
	 * @since  1.0.0
	 */
	private function set_globals() {
		// ClusterPress version.
		$this->version = '1.0.0-beta1';

		// Database version.
		$this->db_version = 100;

		// Base name.
		$this->file       = __FILE__;
		$this->basename   = plugin_basename( $this->file );

		// Path and URL.
		$this->plugin_dir = plugin_dir_path( $this->file );
		$this->plugin_url = plugin_dir_url ( $this->file );

		// Includes dir & url.
		$this->includes_dir = trailingslashit( $this->plugin_dir . 'includes' );
		$this->includes_url = trailingslashit( $this->plugin_url . 'includes' );

		// JS/Templates.
		$this->js_url    = trailingslashit( $this->includes_url . 'js'        );
		$this->templates = trailingslashit( $this->includes_dir . 'templates' );

		// i18n: Domain, language of the plugin and default directory to add others.
		$this->domain   = 'clusterpress';
		$this->language = 'fr_FR';
		$this->lang_dir = trailingslashit( $this->includes_dir . 'languages' );

		// Permalink structure
		$this->permalink_structure = get_option( 'permalink_structure' );

		// Required clusters are always loaded.
		$this->required_clusters = array( 'core', 'user' );

		// Rest namespace and version.
		$this->rest = (object) array(
			'namespace' => 'clusterpress',
			'version'   => 'v1',
		);

		/**
		 * Using 0 as the network id makes sure it's ok for non multisite configs.
		 * By default it will use the current network id for multisite configs.
		 */
		$this->optional_clusters = get_network_option( 0, 'cp_active_clusters', array() );

		// Active clusters are required ones & the activated optional ones.
		$this->active_clusters   = array_merge( $this->required_clusters, $this->optional_clusters );

		// Template globals, used for to check if a ClusterPress area is displayed.
		$this->is_cluster = false;
		$this->cluster    = (object) array_fill_keys( array(
			'current', 'archive', 'displayed_object', 'action', 'sub_action', 'current_taxonomy', 'caps'
		), null );

		$this->feedbacks  = null;
		$this->show_avatars = (bool) get_option( 'show_avatars', false );
	}

	/**
	 * Load all core classes & files. Once done, load the Clusters bootstrap classes.
	 *
	 * @since 1.0.0
	 */
	private function includes() {
		// Make sure to load the core files.
		$core_files = array(
			'core/functions',
			'core/actions',
			'core/filters',
			'core/options',
			'core/tags',
			'core/upgrade',
			'core/classes/class-cp-cluster',
			'core/classes/class-cp-cluster-toolbar',
			'core/classes/class-cp-template-loader',
			'core/classes/class-cp-cluster-loop',
			'core/classes/class-cp-feedback',
			'core/classes/class-cp-cluster-section',
		);

		// Only load the Admin if needed.
		if ( is_admin() ) {
			$core_files = array_merge( $core_files, array(
				'admin/admin',
				'admin/functions',
				'admin/settings',
				'admin/updates',
			) );
		}

		// Loding core.
		foreach ( $core_files as $file ) {
			require( $this->includes_dir . $file . '.php' );
		}

		// Now load every active cluster boostrap classes.
		foreach ( $this->active_clusters as $key => $cluster ) {
			$priority = $key + 1;

			$this->set_cluster_class( $cluster, $priority );
		}
	}

	/**
	 * Load the files and classes of built in & custom Clusters.
	 *
	 * @since  1.0.0
	 *
	 * @param string  $cluster  The Cluster ID.
	 * @param int     $priority The order/priority to which the Cluster should be loaded.
	 */
	public function set_cluster_class( $cluster = '', $priority = 10 ) {
		$built_in_clusters = array(
			'core'         => true,
			'user'         => true,
			'interactions' => true,
			'site'         => true,
		);

		if ( isset( $built_in_clusters[ $cluster ] ) ) {
			$includes_dir = $this->includes_dir;
		} else {
			/**
			 * Custom Clusters can filter here to provide their own includes directory.
			 *
			 * @since  1.0.0
			 *
			 * @param string $value   The path to the custom includes directory. Defaults to an empty string.
			 * @param string $cluster The Cluster's ID
			 */
			$includes_dir = apply_filters( 'cp_custom_cluster_includes_dir', '', $cluster );
		}

		if ( empty( $includes_dir ) ) {
			return;
		}

		// Set the class file.
		$class_file = trailingslashit( $includes_dir ) . $cluster . '/classes/class-cp-' . $cluster . '-cluster.php';

		// Make sure file is callable.
		if ( ! file_exists( $class_file ) ) {
			return;
		}

		// Load the class file
		require( $class_file );

		// Set the class name.
		$class_name = sprintf( 'CP_%s_Cluster', ucfirst( $cluster ) );

		// Make sure it exists.
		if ( ! class_exists( $class_name ) ) {
			return;
		}

		// Launch the cluster.
		add_action( 'cp_set_clusters', array( $class_name, 'start' ), $priority );
	}

	/**
	 * Loads the ClusterPress translations
	 *
	 * @since  1.0.0
	 */
	private function load_translation() {
		add_action( 'cp_loaded', 'cp_load_textdomain', 0 );
	}
}

/**
 * Main Instance of ClusterPress.
 *
 * @since  1.0.0
 *
 * @return ClusterPress The plugin's main instance.
 */
function clusterpress() {
	return ClusterPress::start();
}
add_action( 'plugins_loaded', 'clusterpress' );

/**
 * Activation function for ClusterPress
 *
 * @since 1.0.0
 */
function clusterpress_activation() {
	// Add a transient to redirect after activation.
	set_site_transient( '_clusterpress_activation_redirect', true, 30 );
}
add_action( 'activate_' . plugin_basename( __FILE__ ) , 'clusterpress_activation' );

/**
 * Deactivation function for ClusterPress
 *
 * @since 1.0.0
 */
function clusterpress_deactivation() {
	delete_site_transient( 'clusterpress_updates' );
}
add_action( 'activate_' . plugin_basename( __FILE__ ) , 'clusterpress_deactivation' );
