<?php
/**
 * @group mentions
 */
class CP_UnitTests_Interactions_Tags extends CP_UnitTestCase {
	protected $current_user;
	protected $displayed_user;

	public function setUp() {
		parent::setUp();

		$this->current_user = wp_get_current_user();
		$this->displayed_user = clusterpress()->cluster->displayed_object;
	}

	public function tearDown() {
		wp_set_current_user( $this->current_user->ID );
		clusterpress()->cluster->displayed_object = $this->displayed_user;

		parent::tearDown();
	}

	public function test_cp_interactions_get_mention_content_for_post() {
		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'imath',
		) );

		$p1 = $this->factory->post->create( array(
			'post_content' => 'this is a mention @imath',
		) );

		$user = get_user_by( 'id', $u1 );
		clusterpress()->cluster->displayed_object = $user;
		wp_set_current_user( 0 );

		$mention_content = '';

		// Set the loop
		cp_interactions_has_mentions();
		while ( cp_interactions_the_mentions() ) {
			cp_interactions_the_mention();

			$mention_content = cp_interactions_get_mention_content();
		}

		$expected = apply_filters( 'cp_interactions_get_mention_content', sprintf( __( '%s a été mentionné dans un article mais n\'en a pas pris connaissance jusqu\'à présent.', 'clusterpress' ), $user->display_name ) );

		$this->assertSame( $mention_content, $expected );

		// Self profile
		wp_set_current_user( $u1 );

		// Set the loop
		cp_interactions_has_mentions();
		while ( cp_interactions_the_mentions() ) {
			cp_interactions_the_mention();

			$mention_content = cp_interactions_get_mention_content();
		}

		$expected = apply_filters( 'cp_interactions_get_mention_content', __( 'Vous avez été nouvellement mentionné dans un article.', 'clusterpress' ) );

		$this->assertSame( $mention_content, $expected );

		// Mark all read!
		cp_interactions_mark_all_mentions_read( $u1 );

		wp_set_current_user( 0 );

		// Set the loop
		cp_interactions_has_mentions();
		while ( cp_interactions_the_mentions() ) {
			cp_interactions_the_mention();

			$mention_content = cp_interactions_get_mention_content();
		}

		$ago = sprintf( __( 'il y a %s', 'clusterpress'), human_time_diff( strtotime( cp_interactions_get_current_mention()->date ) ) );

		$expected = apply_filters( 'cp_interactions_get_mention_content', sprintf( __( '%1$s a été mentionné dans un article et en a pris connaissance %2$s.', 'clusterpress' ), $user->display_name, $ago ) );

		$this->assertSame( $mention_content, $expected );

		// Self profile
		wp_set_current_user( $u1 );

		// Set the loop
		cp_interactions_has_mentions();
		while ( cp_interactions_the_mentions() ) {
			cp_interactions_the_mention();

			$mention_content = cp_interactions_get_mention_content();
		}

		$expected = apply_filters( 'cp_interactions_get_mention_content', sprintf( __( 'Vous avez été mentionné dans un article et en avez pris connaissance %s.', 'clusterpress' ), $ago ) );

		$this->assertSame( $mention_content, $expected );
	}

	public function test_cp_interactions_get_mention_content_for_comment() {
		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'imath',
		) );

		$u2 = $this->factory->user->create( array(
			'user_nicename' => 'thima',
		) );

		$p2 = $this->factory->post->create( array(
			'post_content' => 'this post has no mentions',
		) );

		$c2 = $this->factory->comment->create( array(
			'comment_post_ID' => $p2,
			'user_id'         => $u2,
			'comment_content' => 'this is a mention in a comment @thima',
		) );

		$user = get_user_by( 'id', $u2 );
		clusterpress()->cluster->displayed_object = $user;
		wp_set_current_user( 0 );

		$mention_content = '';

		// Set the loop
		cp_interactions_has_mentions();
		while ( cp_interactions_the_mentions() ) {
			cp_interactions_the_mention();

			$mention_content = cp_interactions_get_mention_content();
		}

		$expected = apply_filters( 'cp_interactions_get_mention_content', sprintf( __( '%s a été mentionné dans un commentaire mais n\'en a pas pris connaissance jusqu\'à présent.', 'clusterpress' ), $user->display_name ) );

		$this->assertSame( $mention_content, $expected );

		// Self profile
		wp_set_current_user( $u2 );

		// Set the loop
		cp_interactions_has_mentions();
		while ( cp_interactions_the_mentions() ) {
			cp_interactions_the_mention();

			$mention_content = cp_interactions_get_mention_content();
		}

		$expected = apply_filters( 'cp_interactions_get_mention_content', __( 'Vous avez été nouvellement mentionné dans un commentaire.', 'clusterpress' ) );

		$this->assertSame( $mention_content, $expected );

		// Mark all read!
		cp_interactions_mark_all_mentions_read( $u2 );

		wp_set_current_user( 0 );

		// Set the loop
		cp_interactions_has_mentions();
		while ( cp_interactions_the_mentions() ) {
			cp_interactions_the_mention();

			$mention_content = cp_interactions_get_mention_content();
		}

		$ago = sprintf( __( 'il y a %s', 'clusterpress'), human_time_diff( strtotime( cp_interactions_get_current_mention()->date ) ) );

		$expected = apply_filters( 'cp_interactions_get_mention_content', sprintf( __( '%1$s a été mentionné dans un commentaire et en a pris connaissance  %2$s.', 'clusterpress' ), $user->display_name, $ago ) );

		$this->assertSame( $mention_content, $expected );

		// Self profile
		wp_set_current_user( $u2 );

		// Set the loop
		cp_interactions_has_mentions();
		while ( cp_interactions_the_mentions() ) {
			cp_interactions_the_mention();

			$mention_content = cp_interactions_get_mention_content();
		}

		$expected = apply_filters( 'cp_interactions_get_mention_content', sprintf( __( 'Vous avez été mentionné dans un commentaire et en avez pris connaissance %s.', 'clusterpress' ), $ago ) );

		$this->assertSame( $mention_content, $expected );
	}

	public function test_cp_interactions_get_mention_content_for_page() {
		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'imath',
		) );

		add_post_type_support( 'page', 'clusterpress_post_mentions' );
		$this->register_relationship();

		$p1 = $this->factory->post->create( array(
			'post_content' => 'this is a mention @imath',
			'post_type'    => 'page',
		) );

		$user = get_user_by( 'id', $u1 );
		clusterpress()->cluster->displayed_object = $user;
		wp_set_current_user( 0 );

		$mention_content = '';

		// Set the loop
		cp_interactions_has_mentions();
		while ( cp_interactions_the_mentions() ) {
			cp_interactions_the_mention();

			$mention_content = cp_interactions_get_mention_content();
		}

		$expected = apply_filters( 'cp_interactions_get_mention_content', sprintf( __( '%s a été mentionné dans une page mais n\'en a pas pris connaissance jusqu\'à présent.', 'clusterpress' ), $user->display_name ) );

		$this->assertSame( $mention_content, $expected );

		// Self profile
		wp_set_current_user( $u1 );

		// Set the loop
		cp_interactions_has_mentions();
		while ( cp_interactions_the_mentions() ) {
			cp_interactions_the_mention();

			$mention_content = cp_interactions_get_mention_content();
		}

		$expected = apply_filters( 'cp_interactions_get_mention_content', __( 'Vous avez été nouvellement mentionné dans une page.', 'clusterpress' ) );

		$this->assertSame( $mention_content, $expected );

		// Mark all read!
		cp_interactions_mark_all_mentions_read( $u1 );

		wp_set_current_user( 0 );

		// Set the loop
		cp_interactions_has_mentions();
		while ( cp_interactions_the_mentions() ) {
			cp_interactions_the_mention();

			$mention_content = cp_interactions_get_mention_content();
		}

		$ago = sprintf( __( 'il y a %s', 'clusterpress'), human_time_diff( strtotime( cp_interactions_get_current_mention()->date ) ) );

		$expected = apply_filters( 'cp_interactions_get_mention_content', sprintf( __( '%1$s a été mentionné dans une page et en a pris connaissance %2$s.', 'clusterpress' ), $user->display_name, $ago ) );

		$this->assertSame( $mention_content, $expected );

		// Self profile
		wp_set_current_user( $u1 );

		// Set the loop
		cp_interactions_has_mentions();
		while ( cp_interactions_the_mentions() ) {
			cp_interactions_the_mention();

			$mention_content = cp_interactions_get_mention_content();
		}

		$expected = apply_filters( 'cp_interactions_get_mention_content', sprintf( __( 'Vous avez été mentionné dans une page et en avez pris connaissance %s.', 'clusterpress' ), $ago ) );

		$this->assertSame( $mention_content, $expected );

		remove_post_type_support( 'page', 'clusterpress_post_mentions' );
		cp_user_relationship_unset( 'page_mentions' );
	}

	public function register_relationship() {
		cp_user_register_relationship( 'user', array(
			'name'             => 'page_mentions',
			'cluster'          => 'user',
			'format_callback'  => array(
				'selfnew' => __( 'Vous avez été nouvellement mentionné dans une page.', 'clusterpress' ),
				'self'    => __( 'Vous avez été mentionné dans une page et en avez pris connaissance %s.', 'clusterpress' ),
				'read'    => __( '%1$s a été mentionné dans une page et en a pris connaissance %2$s.', 'clusterpress' ),
				'unread'  => __( '%s a été mentionné dans une page mais n\'en a pas pris connaissance jusqu\'à présent.', 'clusterpress' ),
			),
		) );
	}
}
