<?php
/**
 * @group mentions
 */
class CP_UnitTests_Interactions_Functions extends WP_UnitTestCase {

	public function test_cp_interactions_get_user_mentions() {
		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'imath',
		) );

		$u2 = $this->factory->user->create( array(
			'user_nicename' => 'thima',
		) );

		$p1 = $this->factory->post->create( array(
			'post_content' => 'this is a mention @imath',
		) );

		$p2 = $this->factory->post->create( array(
			'post_content' => 'this is another mention @imath',
		) );

		$c1 = $this->factory->comment->create( array(
			'comment_post_ID' => $p1,
			'user_id'         => $u1,
			'comment_content' => 'this is a mention in a comment @thima',
		) );

		$m1 = cp_interactions_get_user_mentions( $u1 );
		$this->assertEquals( wp_list_pluck( $m1, 'primary_id' ), array( $p2, $p1 ) );
		$this->assertEquals( wp_cache_get( $u1, 'cp_user_mentions' ), $m1 );

		$m2 = cp_interactions_get_user_mentions( $u2 );
		$this->assertEquals( wp_list_pluck( $m2, 'primary_id' ), array( $c1 ) );
		$this->assertEquals( wp_cache_get( $u2, 'cp_user_mentions' ), $m2 );

		// Reset Cache for next tests.
		wp_cache_delete( $u1, 'cp_user_mentions' );
		wp_cache_delete( $u2, 'cp_user_mentions' );
	}

	public function test_cp_interactions_delete_mentions() {
		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'foobar',
		) );

		$u2 = $this->factory->user->create( array(
			'user_nicename' => 'barfoo',
		) );

		$p1 = $this->factory->post->create( array(
			'post_content' => 'there is no mentsions here',
		) );

		$c1 = $this->factory->comment->create( array(
			'comment_post_ID' => $p1,
			'user_id'         => $u1,
			'comment_content' => 'this is a mention in a comment @barfoo',
		) );

		$c2 = $this->factory->comment->create( array(
			'comment_post_ID' => $p1,
			'user_id'         => $u1,
			'comment_content' => 'this is another mention in a comment @barfoo',
		) );

		$m2 = cp_interactions_get_user_mentions( $u2 );
		$this->assertTrue( 2 === count( $m2 ) );

		cp_interactions_delete_mentions( array(
			'primary_id'   => 0,
			'name'         => 'post_mentions',
		) );

		$m2b = cp_interactions_get_user_mentions( $u2 );
		$this->assertTrue( 2 === count( wp_list_filter( $m2b, array( 'name' => 'comment_mentions' ) ) ) );

		cp_interactions_delete_mentions( array(
			'primary_id'   => $c2,
			'name'         => 'comment_mentions',
		) );

		$m2t = cp_interactions_get_user_mentions( $u2 );
		$this->assertTrue( 1 === count( wp_list_filter( $m2t, array( 'name' => 'comment_mentions' ) ) ) );

		$p1 = $this->factory->post->create( array(
			'post_content' => 'there is 2 mentsions here @barfoo @foobar',
		) );

		cp_interactions_delete_mentions( array(
			'name'         => array( 'comment_mentions', 'post_mentions' ),
		) );

		$m2q = cp_interactions_get_user_mentions( $u2 );
		$this->assertTrue( 0 === count( $m2q ) );

		$m1 = cp_interactions_get_user_mentions( $u1 );
		$this->assertTrue( 0 === count( $m1 ) );
	}

	public function test_cp_interactions_mark_all_mentions_read() {
		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'django',
		) );

		$u2 = $this->factory->user->create( array(
			'user_nicename' => 'godjan',
		) );

		$p1 = $this->factory->post->create( array(
			'post_content' => 'there is 2 mentsions here @django @godjan',
		) );

		$c1 = $this->factory->comment->create( array(
			'comment_post_ID' => $p1,
			'user_id'         => $u2,
			'comment_content' => 'this is a mention in a comment @django',
		) );

		cp_interactions_mark_all_mentions_read( $u1 );

		$m1 = cp_interactions_get_user_mentions( $u1 );
		$this->assertTrue( 0 === count( $m1 ) );

		$m2 = cp_interactions_get_user_mentions( $u2 );
		$this->assertTrue( 1 === count( $m2 ) );
	}

	public function test_cp_interactions_get_cpt_user_mentions() {
		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'imath',
		) );

		add_post_type_support( 'page', 'clusterpress_post_mentions' );
		$this->register_relationship();

		$p1 = $this->factory->post->create( array(
			'post_content' => 'this is a mention @imath',
			'post_type'    => 'page',
		) );

		$p2 = $this->factory->post->create( array(
			'post_content' => 'this is another mention @imath',
			'post_type'    => 'page',
		) );

		$m1 = cp_interactions_get_user_mentions( $u1 );
		$this->assertEquals( wp_list_pluck( $m1, 'primary_id' ), array( $p2, $p1 ) );
		$this->assertEquals( wp_cache_get( $u1, 'cp_user_mentions' ), $m1 );

		remove_post_type_support( 'page', 'clusterpress_post_mentions' );
		cp_user_relationship_unset( 'page_mentions' );

		// Reset Cache for next tests.
		wp_cache_delete( $u1, 'cp_user_mentions' );
	}

	public function test_cp_interactions_delete_cpt_mentions() {
		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'foobar',
		) );

		add_post_type_support( 'page', 'clusterpress_post_mentions' );
		$this->register_relationship();

		$p1 = $this->factory->post->create( array(
			'post_content' => 'this is a mention in a paget @foobar',
			'post_type'    => 'page',
		) );

		$m1 = cp_interactions_get_user_mentions( $u1 );
		$this->assertTrue( 1 === count( $m1 ) );

		cp_interactions_delete_mentions( array(
			'name'         => 'page_mentions',
		) );

		$m1 = cp_interactions_get_user_mentions( $u1 );
		$this->assertTrue( 0 === count( $m1 ) );

		remove_post_type_support( 'page', 'clusterpress_post_mentions' );
		cp_user_relationship_unset( 'page_mentions' );
	}

	public function test_cp_interactions_mark_all_cpt_mentions_read() {
		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'django',
		) );

		$u2 = $this->factory->user->create( array(
			'user_nicename' => 'godjan',
		) );

		add_post_type_support( 'page', 'clusterpress_post_mentions' );
		$this->register_relationship();

		$p1 = $this->factory->post->create( array(
			'post_content' => 'there is 2 mentsions here @django @godjan',
			'post_type'    => 'page',
		) );

		cp_interactions_mark_all_mentions_read( $u1 );

		$m1 = cp_interactions_get_user_mentions( $u1 );
		$this->assertTrue( 0 === count( $m1 ) );

		$m2 = cp_interactions_get_user_mentions( $u2 );
		$this->assertTrue( 1 === count( $m2 ) );

		remove_post_type_support( 'page', 'clusterpress_post_mentions' );
		cp_user_relationship_unset( 'page_mentions' );
	}

	public function register_relationship() {
		cp_user_register_relationship( 'user', array(
			'name'             => 'page_mentions',
			'cluster'          => 'user',
		) );
	}
}
