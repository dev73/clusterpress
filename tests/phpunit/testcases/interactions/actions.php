<?php
/**
 * @group mentions
 */
class CP_UnitTests_Interactions_Actions extends CP_UnitTestCase {

	public function test_cp_interactions_mention_user() {
		global $post;
		$reset_post = $post;

		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'imath',
		) );

		$u2 = $this->factory->user->create( array(
			'user_nicename' => 'thima',
		) );

		$p1 = $this->factory->post->create( array(
			'post_content' => 'this is a mention @imath',
		) );

		$post = get_post( $p1 );
		setup_postdata( $post );

		add_filter( 'cp_site_get_post_actions', '__return_empty_array' );
		$content = apply_filters( 'the_content', $post->post_content );

		preg_match( '/<a href=\"(.*)\">(.*)<\/a>/', $content, $matches );

		$this->assertEquals( $matches[1], cp_user_get_url( array( 'user_id' => $u1 ) ) );

		$c1 = $this->factory->comment->create( array(
			'comment_post_ID' => $p1,
			'user_id'         => $u1,
			'comment_content' => 'this is a mention in a comment @thima',
		) );

		$comment = get_comment( $c1 );

		$comment_content = apply_filters( 'comment_text', $comment->comment_content, $comment );
		remove_filter( 'cp_site_get_post_actions', '__return_empty_array' );

		preg_match( '/<a href=\"(.*)\">(.*)<\/a>/', $comment_content, $matches );

		$this->assertEquals( $matches[1], cp_user_get_url( array( 'user_id' => $u2 ) ) );

		$post = $reset_post;
	}

	public function test_cp_interactions_mention_users() {
		global $post;
		$reset_post = $post;

		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'imath',
		) );

		$u2 = $this->factory->user->create( array(
			'user_nicename' => 'thima',
		) );

		$p1 = $this->factory->post->create( array(
			'post_content' => 'this is a mention @imath & @thima',
		) );

		$post = get_post( $p1 );
		setup_postdata( $post );

		add_filter( 'cp_site_get_post_actions', '__return_empty_array' );
		$content = apply_filters( 'the_content', $post->post_content );
		remove_filter( 'cp_site_get_post_actions', '__return_empty_array' );

		preg_match_all( '/<a href="(.+?)">/', $content, $matches );

		$this->assertEquals( $matches[1], array( cp_user_get_url( array( 'user_id' => $u1 ) ), cp_user_get_url( array( 'user_id' => $u2 ) ) ) );

		$post = $reset_post;
	}

	/**
	 * @group multisite
	 */
	public function test_cp_interactions_remove_mentions_for_site() {
		$b1 = $this->factory->blog->create();

		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'imath',
		) );

		$u2 = $this->factory->user->create( array(
			'user_nicename' => 'thima',
		) );

		switch_to_blog( $b1 );

		$p1 = $this->factory->post->create( array(
			'post_content' => 'this is a mention @imath & @thima',
		) );

		$p2 = $this->factory->post->create( array(
			'post_content' => 'this is a mention @imath',
		) );

		restore_current_blog();

		$m1 = cp_interactions_get_user_mentions( $u1 );
		$this->assertTrue( 2 === count( $m1 ) );

		$m2 = cp_interactions_get_user_mentions( $u2 );
		$this->assertTrue( 1 === count( $m2 ) );

		wpmu_delete_blog( $b1 );

		$m1 = cp_interactions_get_user_mentions( $u1 );
		$this->assertTrue( 0 === count( $m1 ) );

		$m2 = cp_interactions_get_user_mentions( $u2 );
		$this->assertTrue( 0 === count( $m2 ) );
	}

	public function test_cp_interactions_cpt_mention_user() {
		global $post;
		$reset_post = $post;

		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'imath',
		) );

		$u2 = $this->factory->user->create( array(
			'user_nicename' => 'thima',
		) );

		add_post_type_support( 'page', 'clusterpress_post_mentions' );
		$this->register_relationship();

		$p1 = $this->factory->post->create( array(
			'post_content' => 'this is a mention @imath',
			'post_type'    => 'page',
		) );

		$post = get_post( $p1 );
		setup_postdata( $post );

		add_filter( 'cp_site_get_post_actions', '__return_empty_array' );
		$content = apply_filters( 'the_content', $post->post_content );

		preg_match( '/<a href=\"(.*)\">(.*)<\/a>/', $content, $matches );

		$this->assertEquals( $matches[1], cp_user_get_url( array( 'user_id' => $u1 ) ) );

		remove_post_type_support( 'page', 'clusterpress_post_mentions' );
		cp_user_relationship_unset( 'page_mentions' );

		$post = $reset_post;
	}

	/**
	 * @group multisite
	 */
	public function test_cp_interactions_remove_cpt_mentions_for_site() {
		$b1 = $this->factory->blog->create();

		$u1 = $this->factory->user->create( array(
			'user_nicename' => 'imath',
		) );

		$u2 = $this->factory->user->create( array(
			'user_nicename' => 'thima',
		) );

		switch_to_blog( $b1 );

		add_post_type_support( 'page', 'clusterpress_post_mentions' );
		$this->register_relationship();

		$p1 = $this->factory->post->create( array(
			'post_content' => 'this is a mention @imath & @thima',
			'post_type'    => 'page',
		) );

		$p2 = $this->factory->post->create( array(
			'post_content' => 'this is a mention @imath',
			'post_type'    => 'page',
		) );

		restore_current_blog();

		$m1 = cp_interactions_get_user_mentions( $u1 );
		$this->assertTrue( 2 === count( $m1 ) );

		$m2 = cp_interactions_get_user_mentions( $u2 );
		$this->assertTrue( 1 === count( $m2 ) );

		wpmu_delete_blog( $b1 );

		$m1 = cp_interactions_get_user_mentions( $u1 );
		$this->assertTrue( 0 === count( $m1 ) );

		$m2 = cp_interactions_get_user_mentions( $u2 );
		$this->assertTrue( 0 === count( $m2 ) );

		remove_post_type_support( 'page', 'clusterpress_post_mentions' );
		cp_user_relationship_unset( 'page_mentions' );
	}

	public function register_relationship() {
		cp_user_register_relationship( 'user', array(
			'name'             => 'page_mentions',
			'cluster'          => 'user',
		) );
	}
}
