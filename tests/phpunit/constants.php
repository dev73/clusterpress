<?php

if ( false !== getenv( 'WP_DEVELOP_DIR' ) ) {
	define( 'WP_ROOT_DIR', getenv( 'WP_DEVELOP_DIR' ) );
	define( 'WP_TESTS_DIR', WP_ROOT_DIR . '/tests/phpunit' );
} else {
	die( 'WordPress Root dir was not found' );
}

if ( file_exists( WP_ROOT_DIR . '/wp-tests-config.php' ) ) {
	// Standard develop.svn.wordpress.org setup
	define( 'WP_TESTS_CONFIG_PATH', WP_ROOT_DIR . '/wp-tests-config.php' );
} else {
	die( "wp-tests-config.php could not be found.\n" );
}
