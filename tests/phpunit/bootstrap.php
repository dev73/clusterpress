<?php

// Load Constants
require_once( dirname( __FILE__ ) . '/constants.php' );

// Load the test config.
require_once WP_TESTS_CONFIG_PATH;
require_once WP_ROOT_DIR . '/tests/phpunit/includes/functions.php';

$multisite = (int) ( defined( 'WP_TESTS_MULTISITE') && WP_TESTS_MULTISITE );

// Remove all previously generated ClusterPress tables.
system( WP_PHP_BINARY . ' ' . escapeshellarg( dirname( __FILE__ ) . '/reset.php' ) . ' ' . escapeshellarg( WP_TESTS_CONFIG_PATH ) . ' ' . escapeshellarg( WP_TESTS_DIR ) . ' ' . $multisite );

// Bootstrap the plugin
function _bootstrap_clusterpress() {
	delete_network_option( 0, '_clusterpress_version' );
	update_network_option( 0, 'cp_active_clusters', array( 'interactions', 'site' ) );

	// load ClusterPress
	require dirname( __FILE__ ) . '/../../clusterpress.php';

	// Run the upgrade method of each clusters.
	add_action( 'cp_set_clusters', '_clusterpress_set', 6 );
}
tests_add_filter( 'muplugins_loaded', '_bootstrap_clusterpress' );

// Create needed tables
function _clusterpress_set() {
	cp_upgrade();
}

require WP_ROOT_DIR . '/tests/phpunit/includes/bootstrap.php';

// include our testcase
require( dirname( __FILE__ ) . '/testcase.php' );
